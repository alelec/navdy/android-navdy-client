.class public Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;
.super Ljava/lang/Object;
.source "EdgeIntersectionList.java"


# instance fields
.field edge:Lcom/vividsolutions/jts/geomgraph/Edge;

.field private nodeMap:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/Edge;)V
    .locals 1
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->nodeMap:Ljava/util/Map;

    .line 54
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 55
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Coordinate;ID)Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .locals 3
    .param p1, "intPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "segmentIndex"    # I
    .param p3, "dist"    # D

    .prologue
    .line 64
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;ID)V

    .line 65
    .local v1, "eiNew":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->nodeMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 66
    .local v0, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    if-eqz v0, :cond_0

    .line 70
    .end local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :goto_0
    return-object v0

    .line 69
    .restart local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->nodeMap:Ljava/util/Map;

    invoke-interface {v2, v1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 70
    goto :goto_0
.end method

.method public addEndpoints()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 101
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v1, v1, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .line 102
    .local v0, "maxSegIndex":I
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v1, v1, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v4

    invoke-virtual {p0, v1, v4, v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->add(Lcom/vividsolutions/jts/geom/Coordinate;ID)Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 103
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v1, v1, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->add(Lcom/vividsolutions/jts/geom/Coordinate;ID)Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 104
    return-void
.end method

.method public addSplitEdges(Ljava/util/List;)V
    .locals 5
    .param p1, "edgeList"    # Ljava/util/List;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->addEndpoints()V

    .line 119
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 121
    .local v2, "it":Ljava/util/Iterator;
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 122
    .local v1, "eiPrev":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 124
    .local v0, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    invoke-virtual {p0, v1, v0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->createSplitEdge(Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v3

    .line 125
    .local v3, "newEdge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    move-object v1, v0

    .line 128
    goto :goto_0

    .line 129
    .end local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v3    # "newEdge":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method createSplitEdge(Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)Lcom/vividsolutions/jts/geomgraph/Edge;
    .locals 12
    .param p1, "ei0"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .param p2, "ei1"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .prologue
    .line 138
    iget v7, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    iget v8, p1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    sub-int/2addr v7, v8

    add-int/lit8 v4, v7, 0x2

    .line 140
    .local v4, "npts":I
    iget-object v7, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v7, v7, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v8, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    aget-object v3, v7, v8

    .line 145
    .local v3, "lastSegStartPt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v8, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->dist:D

    const-wide/16 v10, 0x0

    cmpl-double v7, v8, v10

    if-gtz v7, :cond_0

    iget-object v7, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v7, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 146
    .local v6, "useIntPt1":Z
    :goto_0
    if-nez v6, :cond_1

    .line 147
    add-int/lit8 v4, v4, -0x1

    .line 150
    :cond_1
    new-array v5, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 151
    .local v5, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .line 152
    .local v1, "ipt":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "ipt":I
    .local v2, "ipt":I
    new-instance v7, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v8, p1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v7, v8}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v7, v5, v1

    .line 153
    iget v7, p1, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    add-int/lit8 v0, v7, 0x1

    .local v0, "i":I
    move v1, v2

    .end local v2    # "ipt":I
    .restart local v1    # "ipt":I
    :goto_1
    iget v7, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    if-gt v0, v7, :cond_3

    .line 154
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "ipt":I
    .restart local v2    # "ipt":I
    iget-object v7, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v7, v7, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, v7, v0

    aput-object v7, v5, v1

    .line 153
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "ipt":I
    .restart local v1    # "ipt":I
    goto :goto_1

    .line 145
    .end local v0    # "i":I
    .end local v1    # "ipt":I
    .end local v5    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v6    # "useIntPt1":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 156
    .restart local v0    # "i":I
    .restart local v1    # "ipt":I
    .restart local v5    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v6    # "useIntPt1":Z
    :cond_3
    if-eqz v6, :cond_4

    iget-object v7, p2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v7, v5, v1

    .line 157
    :cond_4
    new-instance v7, Lcom/vividsolutions/jts/geomgraph/Edge;

    new-instance v8, Lcom/vividsolutions/jts/geomgraph/Label;

    iget-object v9, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v9, v9, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-direct {v8, v9}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(Lcom/vividsolutions/jts/geomgraph/Label;)V

    invoke-direct {v7, v5, v8}, Lcom/vividsolutions/jts/geomgraph/Edge;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    return-object v7
.end method

.method public isIntersection(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 90
    .local v0, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v2, v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    const/4 v2, 0x1

    .line 93
    .end local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->nodeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 162
    const-string v2, "Intersections:"

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 165
    .local v0, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->print(Ljava/io/PrintStream;)V

    goto :goto_0

    .line 167
    .end local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_0
    return-void
.end method
