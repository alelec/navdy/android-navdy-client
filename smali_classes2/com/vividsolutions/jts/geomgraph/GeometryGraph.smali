.class public Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
.super Lcom/vividsolutions/jts/geomgraph/PlanarGraph;
.source "GeometryGraph.java"


# instance fields
.field private areaPtLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

.field private argIndex:I

.field private boundaryNodeRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field private boundaryNodes:Ljava/util/Collection;

.field private hasTooFewPoints:Z

.field private invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private lineEdgeMap:Ljava/util/Map;

.field private parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private final ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

.field private useBoundaryDeterminationRule:Z


# direct methods
.method public constructor <init>(ILcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "argIndex"    # I
    .param p2, "parentGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 123
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 126
    return-void
.end method

.method public constructor <init>(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 2
    .param p1, "argIndex"    # I
    .param p2, "parentGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    const/4 v1, 0x0

    .line 128
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;-><init>()V

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->lineEdgeMap:Ljava/util/Map;

    .line 91
    iput-object v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodeRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->useBoundaryDeterminationRule:Z

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->hasTooFewPoints:Z

    .line 101
    iput-object v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 103
    iput-object v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->areaPtLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 105
    new-instance v0, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 129
    iput p1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    .line 130
    iput-object p2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 131
    iput-object p3, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodeRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 132
    if-eqz p2, :cond_0

    .line 135
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 137
    :cond_0
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 196
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-void

    .line 200
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v0, :cond_1

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->useBoundaryDeterminationRule:Z

    .line 203
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addPolygon(Lcom/vividsolutions/jts/geom/Polygon;)V

    goto :goto_0

    .line 205
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addLineString(Lcom/vividsolutions/jts/geom/LineString;)V

    goto :goto_0

    .line 206
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addPoint(Lcom/vividsolutions/jts/geom/Point;)V

    goto :goto_0

    .line 207
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;)V

    goto :goto_0

    .line 208
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;)V

    goto :goto_0

    .line 209
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_6
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;)V

    goto :goto_0

    .line 210
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_7
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_8

    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;)V

    goto :goto_0

    .line 211
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private addCollection(Lcom/vividsolutions/jts/geom/GeometryCollection;)V
    .locals 3
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;

    .prologue
    .line 216
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 217
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 218
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 220
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method private addLineString(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 7
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 288
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-static {v4}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->removeRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 290
    .local v0, "coord":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v4, v0

    if-ge v4, v6, :cond_0

    .line 291
    iput-boolean v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->hasTooFewPoints:Z

    .line 292
    aget-object v2, v0, v3

    iput-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 310
    :goto_0
    return-void

    .line 298
    :cond_0
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    new-instance v4, Lcom/vividsolutions/jts/geomgraph/Label;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    invoke-direct {v4, v5, v3}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(II)V

    invoke-direct {v1, v0, v4}, Lcom/vividsolutions/jts/geomgraph/Edge;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 299
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->lineEdgeMap:Ljava/util/Map;

    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    .line 306
    array-length v4, v0

    if-lt v4, v6, :cond_1

    :goto_1
    const-string v4, "found LineString with single point"

    invoke-static {v2, v4}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 307
    iget v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    aget-object v3, v0, v3

    invoke-direct {p0, v2, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertBoundaryPoint(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 308
    iget v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    invoke-direct {p0, v2, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertBoundaryPoint(ILcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    :cond_1
    move v2, v3

    .line 306
    goto :goto_1
.end method

.method private addPoint(Lcom/vividsolutions/jts/geom/Point;)V
    .locals 3
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Point;

    .prologue
    .line 226
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 227
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    .line 228
    return-void
.end method

.method private addPolygon(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 5
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 268
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-direct {p0, v2, v4, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addPolygonRing(Lcom/vividsolutions/jts/geom/LinearRing;II)V

    .line 273
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 274
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 279
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0, v0, v3, v4}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addPolygonRing(Lcom/vividsolutions/jts/geom/LinearRing;II)V

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 284
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_0
    return-void
.end method

.method private addPolygonRing(Lcom/vividsolutions/jts/geom/LinearRing;II)V
    .locals 8
    .param p1, "lr"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "cwLeft"    # I
    .param p3, "cwRight"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 241
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 264
    :goto_0
    return-void

    .line 243
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-static {v4}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->removeRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 245
    .local v0, "coord":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_1

    .line 246
    iput-boolean v6, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->hasTooFewPoints:Z

    .line 247
    aget-object v4, v0, v7

    iput-object v4, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 251
    :cond_1
    move v2, p2

    .line 252
    .local v2, "left":I
    move v3, p3

    .line 253
    .local v3, "right":I
    invoke-static {v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 254
    move v2, p3

    .line 255
    move v3, p2

    .line 257
    :cond_2
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    new-instance v4, Lcom/vividsolutions/jts/geomgraph/Label;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    invoke-direct {v4, v5, v6, v2, v3}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(IIII)V

    invoke-direct {v1, v0, v4}, Lcom/vividsolutions/jts/geomgraph/Edge;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 259
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->lineEdgeMap:Ljava/util/Map;

    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    .line 263
    iget v4, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    aget-object v5, v0, v7

    invoke-direct {p0, v4, v5, v6}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    goto :goto_0
.end method

.method private addSelfIntersectionNode(ILcom/vividsolutions/jts/geom/Coordinate;I)V
    .locals 1
    .param p1, "argIndex"    # I
    .param p2, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "loc"    # I

    .prologue
    .line 433
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->isBoundaryNode(ILcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 434
    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->useBoundaryDeterminationRule:Z

    if-eqz v0, :cond_1

    .line 435
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertBoundaryPoint(ILcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    .line 437
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    goto :goto_0
.end method

.method private addSelfIntersectionNodes(I)V
    .locals 6
    .param p1, "argIndex"    # I

    .prologue
    .line 415
    iget-object v5, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 416
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 417
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    .line 418
    .local v1, "eLoc":I
    iget-object v5, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "eiIt":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 419
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 420
    .local v2, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v5, v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, p1, v5, v1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addSelfIntersectionNode(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    goto :goto_0

    .line 423
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eLoc":I
    .end local v2    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v3    # "eiIt":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private createEdgeSetIntersector()Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleMCSweepLineIntersector;-><init>()V

    return-object v0
.end method

.method public static determineBoundary(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;I)I
    .locals 1
    .param p0, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;
    .param p1, "boundaryCount"    # I

    .prologue
    .line 78
    invoke-interface {p0, p1}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->isInBoundary(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private insertBoundaryPoint(ILcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 6
    .param p1, "argIndex"    # I
    .param p2, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 398
    iget-object v5, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v5, p2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v3

    .line 400
    .local v3, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    .line 402
    .local v1, "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    const/4 v0, 0x1

    .line 404
    .local v0, "boundaryCount":I
    const/4 v2, -0x1

    .line 405
    .local v2, "loc":I
    const/4 v5, 0x0

    invoke-virtual {v1, p1, v5}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v2

    .line 406
    const/4 v5, 0x1

    if-ne v2, v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 409
    :cond_0
    iget-object v5, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodeRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-static {v5, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->determineBoundary(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;I)I

    move-result v4

    .line 410
    .local v4, "newLoc":I
    invoke-virtual {v1, p1, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(II)V

    .line 411
    return-void
.end method

.method private insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V
    .locals 3
    .param p1, "argIndex"    # I
    .param p2, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "onLocation"    # I

    .prologue
    .line 382
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2, p2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v1

    .line 383
    .local v1, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 384
    .local v0, "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    if-nez v0, :cond_0

    .line 385
    new-instance v2, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-direct {v2, p1, p3}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(II)V

    iput-object v2, v1, Lcom/vividsolutions/jts/geomgraph/Node;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    .line 389
    :goto_0
    return-void

    .line 388
    :cond_0
    invoke-virtual {v0, p1, p3}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(II)V

    goto :goto_0
.end method


# virtual methods
.method public addEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V
    .locals 4
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    const/4 v3, 0x1

    .line 318
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    .line 319
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 321
    .local v0, "coord":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-direct {p0, v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    .line 322
    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    invoke-direct {p0, v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    .line 323
    return-void
.end method

.method public addPoint(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 331
    iget v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->insertPoint(ILcom/vividsolutions/jts/geom/Coordinate;I)V

    .line 332
    return-void
.end method

.method public computeEdgeIntersections(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    .param p2, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p3, "includeProper"    # Z

    .prologue
    .line 366
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    const/4 v2, 0x1

    invoke-direct {v1, p2, p3, v2}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;ZZ)V

    .line 367
    .local v1, "si":Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getBoundaryNodes()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getBoundaryNodes()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->setBoundaryNodes(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 369
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->createEdgeSetIntersector()Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;

    move-result-object v0

    .line 370
    .local v0, "esi":Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    iget-object v3, p1, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    invoke-virtual {v0, v2, v3, v1}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;->computeIntersections(Ljava/util/List;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 377
    return-object v1
.end method

.method public computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .locals 5
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "computeRingSelfNodes"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 344
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    invoke-direct {v1, p1, v4, v3}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;ZZ)V

    .line 345
    .local v1, "si":Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->createEdgeSetIntersector()Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;

    move-result-object v0

    .line 347
    .local v0, "esi":Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
    if-nez p2, :cond_1

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v2, v2, Lcom/vividsolutions/jts/geom/LinearRing;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v2, v2, Lcom/vividsolutions/jts/geom/Polygon;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v2, v2, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v2, :cond_1

    .line 351
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    invoke-virtual {v0, v2, v1, v3}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;->computeIntersections(Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Z)V

    .line 357
    :goto_0
    iget v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->addSelfIntersectionNodes(I)V

    .line 358
    return-object v1

    .line 354
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    invoke-virtual {v0, v2, v1, v4}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;->computeIntersections(Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Z)V

    goto :goto_0
.end method

.method public computeSplitEdges(Ljava/util/List;)V
    .locals 3
    .param p1, "edgelist"    # Ljava/util/List;

    .prologue
    .line 189
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->edges:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 191
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    iget-object v2, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->addSplitEdges(Ljava/util/List;)V

    goto :goto_0

    .line 193
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method public findEdge(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geomgraph/Edge;
    .locals 1
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->lineEdgeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    return-object v0
.end method

.method public getBoundaryNodeRule()Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodeRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    return-object v0
.end method

.method public getBoundaryNodes()Ljava/util/Collection;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodes:Ljava/util/Collection;

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->argIndex:I

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->getBoundaryNodes(I)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodes:Ljava/util/Collection;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->boundaryNodes:Ljava/util/Collection;

    return-object v0
.end method

.method public getBoundaryPoints()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 7

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getBoundaryNodes()Ljava/util/Collection;

    move-result-object v0

    .line 173
    .local v0, "coll":Ljava/util/Collection;
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v6

    new-array v5, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 174
    .local v5, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .line 175
    .local v1, "i":I
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 176
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 177
    .local v4, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/Coordinate;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v6, v5, v1

    move v1, v2

    .line 178
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 179
    .end local v4    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-object v5
.end method

.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getInvalidPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public hasTooFewPoints()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->hasTooFewPoints:Z

    return v0
.end method

.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_1

    .line 452
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->areaPtLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    if-nez v0, :cond_0

    .line 453
    new-instance v0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;

    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->areaPtLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->areaPtLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v0, p1}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    .line 457
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->parentGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, p1, v1}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    goto :goto_0
.end method
