.class public Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
.super Ljava/lang/Object;
.source "QuadEdgeSubdivision.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$1;,
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCoordinatesVisitor;,
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleVertexListVisitor;,
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleEdgesListVisitor;,
        Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCircumcentreVisitor;
    }
.end annotation


# static fields
.field private static final EDGE_COINCIDENCE_TOL_FACTOR:D = 1000.0


# instance fields
.field private edgeCoincidenceTolerance:D

.field private frameEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

.field private locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

.field private quadEdges:Ljava/util/List;

.field private seg:Lcom/vividsolutions/jts/geom/LineSegment;

.field private startingEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

.field private tolerance:D

.field private triEdges:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

.field private visitedKey:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Envelope;D)V
    .locals 2
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "tolerance"    # D

    .prologue
    const/4 v1, 0x3

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitedKey:I

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    .line 96
    new-array v0, v1, [Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    .line 461
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 674
    new-array v0, v1, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->triEdges:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 112
    iput-wide p2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    .line 113
    const-wide v0, 0x408f400000000000L    # 1000.0

    div-double v0, p2, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->edgeCoincidenceTolerance:D

    .line 115
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->createFrame(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 117
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->initSubdiv()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->startingEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 118
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/triangulate/quadedge/LastFoundQuadEdgeLocator;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    .line 119
    return-void
.end method

.method private createFrame(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 14
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v0

    .line 124
    .local v0, "deltaX":D
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v2

    .line 125
    .local v2, "deltaY":D
    const-wide/16 v4, 0x0

    .line 126
    .local v4, "offset":D
    cmpl-double v6, v0, v2

    if-lez v6, :cond_0

    .line 127
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    mul-double v4, v0, v6

    .line 132
    :goto_0
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v7, 0x0

    new-instance v8, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v10

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v12

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double/2addr v10, v12

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v12

    add-double/2addr v12, v4

    invoke-direct {v8, v10, v11, v12, v13}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    aput-object v8, v6, v7

    .line 135
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v7, 0x1

    new-instance v8, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v10

    sub-double/2addr v10, v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v12

    sub-double/2addr v12, v4

    invoke-direct {v8, v10, v11, v12, v13}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    aput-object v8, v6, v7

    .line 136
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v7, 0x2

    new-instance v8, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v10

    add-double/2addr v10, v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v12

    sub-double/2addr v12, v4

    invoke-direct {v8, v10, v11, v12, v13}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    aput-object v8, v6, v7

    .line 138
    new-instance v6, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v7, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    iget-object v8, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    invoke-virtual {v8}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 140
    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameEnv:Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v7, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 141
    return-void

    .line 129
    :cond_0
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    mul-double v4, v2, v6

    goto :goto_0
.end method

.method private fetchTriangleToVisit(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Ljava/util/Stack;ZLjava/util/Set;)[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 5
    .param p1, "edge"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p2, "edgeStack"    # Ljava/util/Stack;
    .param p3, "includeFrame"    # Z
    .param p4, "visitedEdges"    # Ljava/util/Set;

    .prologue
    .line 689
    move-object v0, p1

    .line 690
    .local v0, "curr":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    const/4 v1, 0x0

    .line 691
    .local v1, "edgeCount":I
    const/4 v2, 0x0

    .line 693
    .local v2, "isFrame":Z
    :cond_0
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->triEdges:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    aput-object v0, v4, v1

    .line 695
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 696
    const/4 v2, 0x1

    .line 699
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    .line 700
    .local v3, "sym":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-interface {p4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 701
    invoke-virtual {p2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    :cond_2
    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 706
    add-int/lit8 v1, v1, 0x1

    .line 707
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 708
    if-ne v0, p1, :cond_0

    .line 710
    if-eqz v2, :cond_3

    if-nez p3, :cond_3

    .line 711
    const/4 v4, 0x0

    .line 712
    :goto_0
    return-object v4

    :cond_3
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->triEdges:[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    goto :goto_0
.end method

.method public static getTriangleEdges(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 4
    .param p0, "startQE"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p1, "triEdge"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 77
    aput-object p0, p1, v1

    .line 78
    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    aput-object v0, p1, v2

    .line 79
    aget-object v0, p1, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    aput-object v0, p1, v3

    .line 80
    aget-object v0, p1, v3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    aget-object v1, p1, v1

    if-eq v0, v1, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Edges do not form a triangle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    return-void
.end method

.method private initSubdiv()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 146
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v3, v3, v5

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v4, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 147
    .local v0, "ea":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v3, v3, v6

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v4, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 148
    .local v1, "eb":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 149
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v4, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 150
    .local v2, "ec":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 151
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 152
    return-object v0
.end method


# virtual methods
.method public connect(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 2
    .param p1, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p2, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 218
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->connect(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 219
    .local v0, "q":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    return-object v0
.end method

.method public delete(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 5
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 231
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 232
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 234
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 235
    .local v2, "eSym":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 236
    .local v0, "eRot":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 239
    .local v1, "eRotSym":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 240
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 241
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 242
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 244
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->delete()V

    .line 245
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->delete()V

    .line 246
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->delete()V

    .line 247
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->delete()V

    .line 248
    return-void
.end method

.method public getEdges(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v9, 0x0

    .line 834
    invoke-virtual {p0, v9}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getPrimaryEdges(Z)Ljava/util/List;

    move-result-object v5

    .line 835
    .local v5, "quadEdges":Ljava/util/List;
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v0, v6, [Lcom/vividsolutions/jts/geom/LineString;

    .line 836
    .local v0, "edges":[Lcom/vividsolutions/jts/geom/LineString;
    const/4 v1, 0x0

    .line 837
    .local v1, "i":I
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 838
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 839
    .local v4, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    const/4 v6, 0x2

    new-array v6, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x1

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p1, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v6

    aput-object v6, v0, v1

    move v1, v2

    .line 841
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 842
    .end local v4    # "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v6

    return-object v6
.end method

.method public getEdges()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    return-object v0
.end method

.method public getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    return-object v0
.end method

.method public getPrimaryEdges(Z)Ljava/util/List;
    .locals 6
    .param p1, "includeFrame"    # Z

    .prologue
    .line 588
    iget v5, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitedKey:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitedKey:I

    .line 590
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 591
    .local v2, "edges":Ljava/util/List;
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 592
    .local v1, "edgeStack":Ljava/util/Stack;
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->startingEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 596
    .local v4, "visitedEdges":Ljava/util/Set;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 597
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 598
    .local v0, "edge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 599
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->getPrimary()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    .line 601
    .local v3, "priQE":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    if-nez p1, :cond_1

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 602
    :cond_1
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 604
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 605
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 608
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 611
    .end local v0    # "edge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .end local v3    # "priQE":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_3
    return-object v2
.end method

.method public getTolerance()D
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    return-wide v0
.end method

.method public getTriangleCoordinates(Z)Ljava/util/List;
    .locals 2
    .param p1, "includeFrame"    # Z

    .prologue
    .line 777
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCoordinatesVisitor;

    invoke-direct {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCoordinatesVisitor;-><init>()V

    .line 778
    .local v0, "visitor":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCoordinatesVisitor;
    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V

    .line 779
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCoordinatesVisitor;->getTriangles()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getTriangleEdges(Z)Ljava/util/List;
    .locals 2
    .param p1, "includeFrame"    # Z

    .prologue
    .line 725
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleEdgesListVisitor;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleEdgesListVisitor;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$1;)V

    .line 726
    .local v0, "visitor":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleEdgesListVisitor;
    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V

    .line 727
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleEdgesListVisitor;->getTriangleEdges()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getTriangleVertices(Z)Ljava/util/List;
    .locals 2
    .param p1, "includeFrame"    # Z

    .prologue
    .line 751
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleVertexListVisitor;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleVertexListVisitor;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$1;)V

    .line 752
    .local v0, "visitor":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleVertexListVisitor;
    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V

    .line 753
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleVertexListVisitor;->getTriangleVertices()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getTriangles(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 8
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 853
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTriangleCoordinates(Z)Ljava/util/List;

    move-result-object v4

    .line 854
    .local v4, "triPtsList":Ljava/util/List;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v5, v6, [Lcom/vividsolutions/jts/geom/Polygon;

    .line 855
    .local v5, "tris":[Lcom/vividsolutions/jts/geom/Polygon;
    const/4 v0, 0x0

    .line 856
    .local v0, "i":I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 857
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object v3, v6

    check-cast v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 858
    .local v3, "triPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v6

    aput-object v6, v5, v0

    move v0, v1

    .line 860
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 861
    .end local v3    # "triPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    invoke-virtual {p1, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v6

    return-object v6
.end method

.method public getVertexUniqueEdges(Z)Ljava/util/List;
    .locals 8
    .param p1, "includeFrame"    # Z

    .prologue
    .line 547
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 548
    .local v0, "edges":Ljava/util/List;
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 549
    .local v6, "visitedVertices":Ljava/util/Set;
    iget-object v7, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 550
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 551
    .local v3, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    .line 553
    .local v4, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-interface {v6, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 554
    invoke-interface {v6, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 555
    if-nez p1, :cond_1

    invoke-virtual {p0, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 556
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 565
    :cond_2
    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 566
    .local v2, "qd":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v5

    .line 568
    .local v5, "vd":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 569
    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 570
    if-nez p1, :cond_3

    invoke-virtual {p0, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 571
    :cond_3
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 575
    .end local v2    # "qd":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .end local v3    # "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .end local v4    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .end local v5    # "vd":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_4
    return-object v0
.end method

.method public getVertices(Z)Ljava/util/Collection;
    .locals 6
    .param p1, "includeFrame"    # Z

    .prologue
    .line 507
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 508
    .local v4, "vertices":Ljava/util/Set;
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 509
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 510
    .local v1, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    .line 512
    .local v2, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    if-nez p1, :cond_1

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 513
    :cond_1
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 520
    :cond_2
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    .line 522
    .local v3, "vd":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    if-nez p1, :cond_3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 523
    :cond_3
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 525
    .end local v1    # "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .end local v2    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .end local v3    # "vd":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_4
    return-object v4
.end method

.method public getVoronoiCellPolygon(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Polygon;
    .locals 9
    .param p1, "qe"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p2, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 925
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 926
    .local v2, "cellPts":Ljava/util/List;
    move-object v5, p1

    .line 930
    .local v5, "startQE":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 931
    .local v0, "cc":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 934
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object p1

    .line 935
    if-ne p1, v5, :cond_0

    .line 937
    new-instance v3, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 938
    .local v3, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    const/4 v7, 0x0

    invoke-virtual {v3, v2, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->addAll(Ljava/util/Collection;Z)Z

    .line 939
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->closeRing()V

    .line 941
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v7

    const/4 v8, 0x4

    if-ge v7, v8, :cond_1

    .line 942
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v7, v3}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 943
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v3, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->get(I)Ljava/lang/Object;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;Z)Z

    .line 946
    :cond_1
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 947
    .local v4, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v8}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    .line 949
    .local v1, "cellPoly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    .line 950
    .local v6, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/vividsolutions/jts/geom/Polygon;->setUserData(Ljava/lang/Object;)V

    .line 951
    return-object v1
.end method

.method public getVoronoiCellPolygons(Lcom/vividsolutions/jts/geom/GeometryFactory;)Ljava/util/List;
    .locals 6
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 900
    new-instance v4, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCircumcentreVisitor;

    invoke-direct {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCircumcentreVisitor;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V

    .line 902
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 903
    .local v0, "cells":Ljava/util/List;
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getVertexUniqueEdges(Z)Ljava/util/List;

    move-result-object v1

    .line 904
    .local v1, "edges":Ljava/util/Collection;
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 905
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 906
    .local v3, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p0, v3, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getVoronoiCellPolygon(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 908
    .end local v3    # "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    return-object v0
.end method

.method public getVoronoiDiagram(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 877
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getVoronoiCellPolygons(Lcom/vividsolutions/jts/geom/GeometryFactory;)Ljava/util/List;

    move-result-object v0

    .line 878
    .local v0, "vorCells":Ljava/util/List;
    invoke-static {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometryArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v1

    return-object v1
.end method

.method public insertSite(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 383
    .local v1, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    invoke-virtual {p1, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;D)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    iget-wide v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    invoke-virtual {p1, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;D)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v2, v1

    .line 398
    :goto_0
    return-object v2

    .line 390
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    invoke-virtual {p0, v3, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 391
    .local v0, "base":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-static {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 392
    move-object v2, v0

    .line 394
    .local v2, "startEdge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->connect(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 395
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 396
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    if-ne v3, v2, :cond_2

    goto :goto_0
.end method

.method public isFrameBorderEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 6
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 425
    new-array v0, v5, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 426
    .local v0, "leftTri":[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTriangleEdges(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 428
    new-array v1, v5, [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 429
    .local v1, "rightTri":[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-static {v5, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTriangleEdges(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 433
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    .line 434
    .local v2, "vLeftTriOther":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v4

    .line 437
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    .line 438
    .local v3, "vRightTriOther":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 441
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isFrameEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 409
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    :cond_0
    const/4 v0, 0x1

    .line 411
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFrameVertex(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z
    .locals 4
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 452
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 458
    :cond_0
    :goto_0
    return v0

    .line 454
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 456
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->frameVertex:[Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 458
    goto :goto_0
.end method

.method public isOnEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 5
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p2, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 474
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vividsolutions/jts/geom/LineSegment;->setCoordinates(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 475
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v2, p2}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 477
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->edgeCoincidenceTolerance:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isVertexOfEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z
    .locals 4
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p2, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 489
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    invoke-virtual {p2, v0, v2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->tolerance:D

    invoke-virtual {p2, v0, v2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 490
    :cond_0
    const/4 v0, 0x1

    .line 492
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    new-instance v1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-direct {v1, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;->locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 6
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v3, 0x0

    .line 346
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    new-instance v5, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-direct {v5, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-interface {v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;->locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 347
    .local v1, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    if-nez v1, :cond_1

    move-object v2, v3

    .line 361
    :cond_0
    :goto_0
    return-object v2

    .line 351
    :cond_1
    move-object v0, v1

    .line 352
    .local v0, "base":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 353
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 355
    :cond_2
    move-object v2, v0

    .line 357
    .local v2, "locEdge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_3
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 359
    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 360
    if-ne v2, v0, :cond_3

    move-object v2, v3

    .line 361
    goto :goto_0
.end method

.method public locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 320
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    invoke-interface {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;->locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public locateFromEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 5
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "startEdge"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 268
    const/4 v1, 0x0

    .line 269
    .local v1, "iter":I
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 271
    .local v2, "maxIter":I
    move-object v0, p2

    .line 274
    .local v0, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :goto_0
    add-int/lit8 v1, v1, 0x1

    .line 285
    if-le v1, v2, :cond_0

    .line 286
    new-instance v3, Lcom/vividsolutions/jts/triangulate/quadedge/LocateFailureException;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->toLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/LocateFailureException;-><init>(Lcom/vividsolutions/jts/geom/LineSegment;)V

    throw v3

    .line 294
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 308
    :cond_1
    return-object v0

    .line 296
    :cond_2
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->rightOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 297
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    goto :goto_0

    .line 298
    :cond_3
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->rightOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 299
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    goto :goto_0

    .line 300
    :cond_4
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->rightOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 301
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    goto :goto_0
.end method

.method public makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 2
    .param p1, "o"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "d"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 203
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 204
    .local v0, "q":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->quadEdges:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    return-object v0
.end method

.method public setLocator(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;)V
    .locals 0
    .param p1, "locator"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locator:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeLocator;

    .line 193
    return-void
.end method

.method public visitTriangles(Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;Z)V
    .locals 5
    .param p1, "triVisitor"    # Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;
    .param p2, "includeFrame"    # Z

    .prologue
    .line 649
    iget v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitedKey:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->visitedKey:I

    .line 653
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 654
    .local v1, "edgeStack":Ljava/util/Stack;
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->startingEdge:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v1, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 656
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 658
    .local v3, "visitedEdges":Ljava/util/Set;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 659
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 660
    .local v0, "edge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 661
    invoke-direct {p0, v0, v1, p2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->fetchTriangleToVisit(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Ljava/util/Stack;ZLjava/util/Set;)[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 663
    .local v2, "triEdges":[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    if-eqz v2, :cond_0

    .line 664
    invoke-interface {p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;->visit([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    goto :goto_0

    .line 667
    .end local v0    # "edge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .end local v2    # "triEdges":[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_1
    return-void
.end method
