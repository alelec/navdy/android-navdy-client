.class public Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;
.super Ljava/lang/Object;
.source "TrianglePredicate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkRobustInCircle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 12
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 306
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->isInCircleNonRobust(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    .line 307
    .local v3, "nonRobustInCircle":Z
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->isInCircleDDSlow(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    .line 308
    .local v2, "isInCircleDD":Z
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->isInCircleCC(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    .line 310
    .local v1, "isInCircleCC":Z
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/geom/Triangle;->circumcentre(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 311
    .local v0, "circumCentre":Lcom/vividsolutions/jts/geom/Coordinate;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p radius diff a = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 315
    if-ne v3, v2, :cond_0

    if-eq v3, v1, :cond_1

    .line 316
    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inCircle robustness failure (double result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", DD result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", CC result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 320
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;

    const/4 v6, 0x4

    new-array v6, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object p2, v6, v7

    const/4 v7, 0x3

    aput-object p3, v6, v7

    invoke-direct {v5, v6}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-static {v5}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 322
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Circumcentre = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Lcom/vividsolutions/jts/io/WKTWriter;->toPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " radius = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 324
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p radius diff a = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    div-double/2addr v6, v8

    sub-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 326
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p radius diff b = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    div-double/2addr v6, v8

    sub-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 328
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p radius diff c = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    div-double/2addr v6, v8

    sub-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 330
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->println()V

    .line 332
    :cond_1
    return-void
.end method

.method public static isInCircleCC(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 8
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 288
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/geom/Triangle;->circumcentre(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 289
    .local v0, "cc":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 290
    .local v2, "ccRadius":D
    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    sub-double v4, v6, v2

    .line 291
    .local v4, "pRadiusDiff":D
    const-wide/16 v6, 0x0

    cmpg-double v1, v4, v6

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isInCircleDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 10
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 211
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-static {p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    .line 213
    .local v0, "aTerm":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-static {p0, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    .line 215
    .local v1, "bTerm":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-static {p0, p1, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    .line 217
    .local v2, "cTerm":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    iget-wide v8, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9}, Lcom/vividsolutions/jts/math/DD;->sqr(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v4

    .line 220
    .local v4, "pTerm":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    .line 221
    .local v5, "sum":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_0

    const/4 v3, 0x1

    .line 223
    .local v3, "isInCircle":Z
    :goto_0
    return v3

    .line 221
    .end local v3    # "isInCircle":Z
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isInCircleDDNormalized(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 22
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 243
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    .line 244
    .local v5, "adx":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    .line 245
    .local v6, "ady":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v9

    .line 246
    .local v9, "bdx":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    .line 247
    .local v10, "bdy":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v13

    .line 248
    .local v13, "cdx":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v14

    .line 250
    .local v14, "cdy":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v5, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v9, v6}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v4

    .line 251
    .local v4, "abdet":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v9, v14}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v13, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v8

    .line 252
    .local v8, "bcdet":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v13, v6}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v5, v14}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v12

    .line 253
    .local v12, "cadet":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v5, v5}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v6, v6}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    .line 254
    .local v7, "alift":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v9, v9}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v10, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    .line 255
    .local v11, "blift":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v13, v13}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v14, v14}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v15

    .line 257
    .local v15, "clift":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v7, v8}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v11, v12}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    invoke-virtual {v15, v4}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    .line 261
    .local v17, "sum":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual/range {v17 .. v17}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmpl-double v18, v18, v20

    if-lez v18, :cond_0

    const/16 v16, 0x1

    .line 263
    .local v16, "isInCircle":Z
    :goto_0
    return v16

    .line 261
    .end local v16    # "isInCircle":Z
    :cond_0
    const/16 v16, 0x0

    goto :goto_0
.end method

.method public static isInCircleDDSlow(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 21
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 166
    move-object/from16 v0, p3

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    .line 167
    .local v6, "px":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p3

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    .line 168
    .local v7, "py":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v8

    .line 169
    .local v8, "ax":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v9

    .line 170
    .local v9, "ay":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    .line 171
    .local v2, "bx":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    .line 172
    .local v3, "by":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v4

    .line 173
    .local v4, "cx":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v10, v11}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    .line 175
    .local v5, "cy":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v8, v8}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    invoke-virtual {v9, v9}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vividsolutions/jts/math/DD;->add(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    invoke-static/range {v2 .. v7}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDSlow(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v14

    .line 177
    .local v14, "aTerm":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v2, v2}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    invoke-virtual {v3, v3}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vividsolutions/jts/math/DD;->add(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v20

    move-object v10, v4

    move-object v11, v5

    move-object v12, v6

    move-object v13, v7

    invoke-static/range {v8 .. v13}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDSlow(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v15

    .line 179
    .local v15, "bTerm":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v4, v4}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    invoke-virtual {v5, v5}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vividsolutions/jts/math/DD;->add(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v20

    move-object v10, v2

    move-object v11, v3

    move-object v12, v6

    move-object v13, v7

    invoke-static/range {v8 .. v13}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDSlow(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v16

    .line 181
    .local v16, "cTerm":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v6, v6}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    invoke-virtual {v7, v7}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/vividsolutions/jts/math/DD;->add(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v20

    move-object v10, v2

    move-object v11, v3

    move-object v12, v4

    move-object v13, v5

    invoke-static/range {v8 .. v13}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triAreaDDSlow(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v18

    .line 184
    .local v18, "pTerm":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v14, v15}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/vividsolutions/jts/math/DD;->add(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v10

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v19

    .line 185
    .local v19, "sum":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual/range {v19 .. v19}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmpl-double v10, v10, v12

    if-lez v10, :cond_0

    const/16 v17, 0x1

    .line 187
    .local v17, "isInCircle":Z
    :goto_0
    return v17

    .line 185
    .end local v17    # "isInCircle":Z
    :cond_0
    const/16 v17, 0x0

    goto :goto_0
.end method

.method public static isInCircleNonRobust(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 10
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 72
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triArea(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    mul-double/2addr v2, v4

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {p0, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triArea(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {p0, p1, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triArea(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->triArea(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 78
    .local v0, "isInCircle":Z
    :goto_0
    return v0

    .line 72
    .end local v0    # "isInCircle":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInCircleNormalized(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 32
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 101
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    sub-double v4, v28, v30

    .line 102
    .local v4, "adx":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v30, v0

    sub-double v6, v28, v30

    .line 103
    .local v6, "ady":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    sub-double v12, v28, v30

    .line 104
    .local v12, "bdx":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v30, v0

    sub-double v14, v28, v30

    .line 105
    .local v14, "bdy":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v30, v0

    sub-double v20, v28, v30

    .line 106
    .local v20, "cdx":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v30, v0

    sub-double v22, v28, v30

    .line 108
    .local v22, "cdy":D
    mul-double v28, v4, v14

    mul-double v30, v12, v6

    sub-double v2, v28, v30

    .line 109
    .local v2, "abdet":D
    mul-double v28, v12, v22

    mul-double v30, v20, v14

    sub-double v10, v28, v30

    .line 110
    .local v10, "bcdet":D
    mul-double v28, v20, v6

    mul-double v30, v4, v22

    sub-double v18, v28, v30

    .line 111
    .local v18, "cadet":D
    mul-double v28, v4, v4

    mul-double v30, v6, v6

    add-double v8, v28, v30

    .line 112
    .local v8, "alift":D
    mul-double v28, v12, v12

    mul-double v30, v14, v14

    add-double v16, v28, v30

    .line 113
    .local v16, "blift":D
    mul-double v28, v20, v20

    mul-double v30, v22, v22

    add-double v24, v28, v30

    .line 115
    .local v24, "clift":D
    mul-double v28, v8, v10

    mul-double v30, v16, v18

    add-double v28, v28, v30

    mul-double v30, v24, v2

    add-double v26, v28, v30

    .line 116
    .local v26, "disc":D
    const-wide/16 v28, 0x0

    cmpl-double v28, v26, v28

    if-lez v28, :cond_0

    const/16 v28, 0x1

    :goto_0
    return v28

    :cond_0
    const/16 v28, 0x0

    goto :goto_0
.end method

.method public static isInCircleRobust(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 1
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 149
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->isInCircleNormalized(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method

.method private static triArea(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 8
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 128
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v2, v4

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public static triAreaDDFast(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/DD;
    .locals 6
    .param p0, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 229
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v2, v4, v5}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v4, v5}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v3, v4, v5}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    .line 233
    .local v0, "t1":Lcom/vividsolutions/jts/math/DD;
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v2, v4, v5}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v4, v5}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v3, v4, v5}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    .line 237
    .local v1, "t2":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    return-object v2
.end method

.method public static triAreaDDSlow(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;
    .locals 3
    .param p0, "ax"    # Lcom/vividsolutions/jts/math/DD;
    .param p1, "ay"    # Lcom/vividsolutions/jts/math/DD;
    .param p2, "bx"    # Lcom/vividsolutions/jts/math/DD;
    .param p3, "by"    # Lcom/vividsolutions/jts/math/DD;
    .param p4, "cx"    # Lcom/vividsolutions/jts/math/DD;
    .param p5, "cy"    # Lcom/vividsolutions/jts/math/DD;

    .prologue
    .line 204
    invoke-virtual {p2, p0}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    invoke-virtual {p5, p1}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    invoke-virtual {p3, p1}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    invoke-virtual {p4, p0}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    return-object v0
.end method
