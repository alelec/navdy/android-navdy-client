.class public Lcom/vividsolutions/jts/index/kdtree/KdNode;
.super Ljava/lang/Object;
.source "KdNode.java"


# instance fields
.field private count:I

.field private data:Ljava/lang/Object;

.field private left:Lcom/vividsolutions/jts/index/kdtree/KdNode;

.field private p:Lcom/vividsolutions/jts/geom/Coordinate;

.field private right:Lcom/vividsolutions/jts/index/kdtree/KdNode;


# direct methods
.method public constructor <init>(DDLjava/lang/Object;)V
    .locals 3
    .param p1, "_x"    # D
    .param p3, "_y"    # D
    .param p5, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 59
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 60
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->left:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 61
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->right:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    .line 63
    iput-object p5, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->data:Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 73
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 74
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->left:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 75
    iput-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->right:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 76
    const/4 v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    .line 77
    iput-object p2, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->data:Ljava/lang/Object;

    .line 78
    return-void
.end method


# virtual methods
.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    return v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getLeft()Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->left:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    return-object v0
.end method

.method public getRight()Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->right:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    return-object v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    return-wide v0
.end method

.method increment()V
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    .line 136
    return-void
.end method

.method public isRepeated()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 153
    iget v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->count:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setLeft(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V
    .locals 0
    .param p1, "_left"    # Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->left:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 159
    return-void
.end method

.method setRight(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V
    .locals 0
    .param p1, "_right"    # Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/vividsolutions/jts/index/kdtree/KdNode;->right:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 164
    return-void
.end method
