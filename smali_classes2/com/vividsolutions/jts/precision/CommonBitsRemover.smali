.class public Lcom/vividsolutions/jts/precision/CommonBitsRemover;
.super Ljava/lang/Object;
.source "CommonBitsRemover.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;,
        Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;
    }
.end annotation


# instance fields
.field private ccFilter:Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;

.field private commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;-><init>(Lcom/vividsolutions/jts/precision/CommonBitsRemover;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->ccFilter:Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;

    .line 74
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->ccFilter:Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 87
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->ccFilter:Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/precision/CommonBitsRemover$CommonCoordinateFilter;->getCommonCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 88
    return-void
.end method

.method public addCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 123
    new-instance v0, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;

    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;-><init>(Lcom/vividsolutions/jts/precision/CommonBitsRemover;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 124
    .local v0, "trans":Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 125
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->geometryChanged()V

    .line 126
    return-void
.end method

.method public getCommonCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const-wide/16 v4, 0x0

    .line 104
    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 112
    :goto_0
    return-object p1

    .line 106
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->commonCoord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 107
    .local v0, "invCoord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    neg-double v2, v2

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 108
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    neg-double v2, v2

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 109
    new-instance v1, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;

    invoke-direct {v1, p0, v0}, Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;-><init>(Lcom/vividsolutions/jts/precision/CommonBitsRemover;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 110
    .local v1, "trans":Lcom/vividsolutions/jts/precision/CommonBitsRemover$Translater;
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 111
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->geometryChanged()V

    goto :goto_0
.end method
