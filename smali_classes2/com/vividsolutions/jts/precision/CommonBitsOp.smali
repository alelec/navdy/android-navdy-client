.class public Lcom/vividsolutions/jts/precision/CommonBitsOp;
.super Ljava/lang/Object;
.source "CommonBitsOp.java"


# instance fields
.field private cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

.field private returnToOriginalPrecision:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/precision/CommonBitsOp;-><init>(Z)V

    .line 60
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "returnToOriginalPrecision"    # Z

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->returnToOriginalPrecision:Z

    .line 70
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->returnToOriginalPrecision:Z

    .line 71
    return-void
.end method

.method private computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "result"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->returnToOriginalPrecision:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->addCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 149
    :cond_0
    return-object p1
.end method

.method private removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 160
    new-instance v1, Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-direct {v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;-><init>()V

    iput-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    .line 161
    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 162
    iget-object v2, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 163
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v0
.end method

.method private removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 176
    new-instance v1, Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-direct {v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;-><init>()V

    iput-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    .line 177
    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 178
    iget-object v1, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {v1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 179
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 180
    .local v0, "geom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    aput-object v1, v0, v2

    .line 181
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vividsolutions/jts/precision/CommonBitsOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    aput-object v1, v0, v2

    .line 182
    return-object v0
.end method


# virtual methods
.method public buffer(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "distance"    # D

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 132
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0, p2, p3}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public difference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 106
    .local v0, "geom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->difference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public intersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 82
    .local v0, "geom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public symDifference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 119
    .local v0, "geom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->symDifference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public union(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 94
    .local v0, "geom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/precision/CommonBitsOp;->computeResultPrecision(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method
