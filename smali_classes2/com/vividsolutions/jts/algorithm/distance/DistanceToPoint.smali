.class public Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;
.super Ljava/lang/Object;
.source "DistanceToPoint.java"


# static fields
.field private static tempSegment:Lcom/vividsolutions/jts/geom/LineSegment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->tempSegment:Lcom/vividsolutions/jts/geom/LineSegment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method public static computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 4
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 51
    instance-of v3, p0, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v3, :cond_1

    .line 52
    check-cast p0, Lcom/vividsolutions/jts/geom/LineString;

    .end local p0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 54
    .restart local p0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v3, p0, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_2

    .line 55
    check-cast p0, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    goto :goto_0

    .line 57
    .restart local p0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_3

    move-object v1, p0

    .line 58
    check-cast v1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 59
    .local v1, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 60
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 61
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v0, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 59
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 65
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {p2, v3, p1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0
.end method

.method public static computeDistance(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 1
    .param p0, "segment"    # Lcom/vividsolutions/jts/geom/LineSegment;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 82
    .local v0, "closestPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2, v0, p1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 83
    return-void
.end method

.method public static computeDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 6
    .param p0, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 71
    .local v1, "coords":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 72
    sget-object v3, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->tempSegment:Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v4, v1, v2

    add-int/lit8 v5, v2, 0x1

    aget-object v5, v1, v5

    invoke-virtual {v3, v4, v5}, Lcom/vividsolutions/jts/geom/LineSegment;->setCoordinates(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 74
    sget-object v3, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->tempSegment:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 75
    .local v0, "closestPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2, v0, p1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMinimum(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 71
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "closestPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-void
.end method

.method public static computeDistance(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V
    .locals 2
    .param p0, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ptDist"    # Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 88
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 89
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method
