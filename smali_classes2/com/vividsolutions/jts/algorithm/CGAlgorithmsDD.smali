.class public Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;
.super Ljava/lang/Object;
.source "CGAlgorithmsDD.java"


# static fields
.field private static final DP_SAFE_EPSILON:D = 1.0E-15


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static intersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 26
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "q2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 173
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    .line 175
    .local v5, "denom1":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    .line 177
    .local v6, "denom2":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v4

    .line 186
    .local v4, "denom":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v12

    .line 188
    .local v12, "numx1":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v13

    .line 190
    .local v13, "numx2":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v12, v13}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v7

    .line 191
    .local v7, "numx":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v7, v4}, Lcom/vividsolutions/jts/math/DD;->selfDivide(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v8

    .line 193
    .local v8, "fracP":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v18

    .line 195
    .local v18, "x":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v15

    .line 197
    .local v15, "numy1":Lcom/vividsolutions/jts/math/DD;
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v16

    .line 199
    .local v16, "numy2":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual/range {v15 .. v16}, Lcom/vividsolutions/jts/math/DD;->subtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v14

    .line 200
    .local v14, "numy":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v14, v4}, Lcom/vividsolutions/jts/math/DD;->selfDivide(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v10

    .line 202
    .local v10, "fracQ":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v11}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/math/DD;->selfAdd(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vividsolutions/jts/math/DD;->doubleValue()D

    move-result-wide v20

    .line 204
    .local v20, "y":D
    new-instance v17, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct/range {v17 .. v21}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v17
.end method

.method public static orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 8
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "q"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 62
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->orientationIndexFilter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v4

    .line 63
    .local v4, "index":I
    const/4 v5, 0x1

    if-gt v4, v5, :cond_0

    .line 72
    .end local v4    # "index":I
    :goto_0
    return v4

    .line 66
    .restart local v4    # "index":I
    :cond_0
    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    neg-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    .line 67
    .local v0, "dx1":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    neg-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    .line 68
    .local v2, "dy1":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    neg-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    .line 69
    .local v1, "dx2":Lcom/vividsolutions/jts/math/DD;
    iget-wide v6, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/math/DD;->valueOf(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    neg-double v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/vividsolutions/jts/math/DD;->selfAdd(D)Lcom/vividsolutions/jts/math/DD;

    move-result-object v3

    .line 72
    .local v3, "dy2":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/math/DD;->selfMultiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/math/DD;->signum()I

    move-result v4

    goto :goto_0
.end method

.method private static orientationIndexFilter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 18
    .param p0, "pa"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pb"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "pc"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 119
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double v4, v12, v14

    .line 120
    .local v4, "detleft":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v12, v14

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    mul-double v6, v12, v14

    .line 121
    .local v6, "detright":D
    sub-double v2, v4, v6

    .line 123
    .local v2, "det":D
    const-wide/16 v12, 0x0

    cmpl-double v12, v4, v12

    if-lez v12, :cond_2

    .line 124
    const-wide/16 v12, 0x0

    cmpg-double v12, v6, v12

    if-gtz v12, :cond_0

    .line 125
    invoke-static {v2, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->signum(D)I

    move-result v12

    .line 148
    :goto_0
    return v12

    .line 128
    :cond_0
    add-double v8, v4, v6

    .line 143
    .local v8, "detsum":D
    :goto_1
    const-wide v12, 0x3cd203af9ee75616L    # 1.0E-15

    mul-double v10, v12, v8

    .line 144
    .local v10, "errbound":D
    cmpl-double v12, v2, v10

    if-gez v12, :cond_1

    neg-double v12, v2

    cmpl-double v12, v12, v10

    if-ltz v12, :cond_5

    .line 145
    :cond_1
    invoke-static {v2, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->signum(D)I

    move-result v12

    goto :goto_0

    .line 131
    .end local v8    # "detsum":D
    .end local v10    # "errbound":D
    :cond_2
    const-wide/16 v12, 0x0

    cmpg-double v12, v4, v12

    if-gez v12, :cond_4

    .line 132
    const-wide/16 v12, 0x0

    cmpl-double v12, v6, v12

    if-ltz v12, :cond_3

    .line 133
    invoke-static {v2, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->signum(D)I

    move-result v12

    goto :goto_0

    .line 136
    :cond_3
    neg-double v12, v4

    sub-double v8, v12, v6

    .restart local v8    # "detsum":D
    goto :goto_1

    .line 140
    .end local v8    # "detsum":D
    :cond_4
    invoke-static {v2, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithmsDD;->signum(D)I

    move-result v12

    goto :goto_0

    .line 148
    .restart local v8    # "detsum":D
    .restart local v10    # "errbound":D
    :cond_5
    const/4 v12, 0x2

    goto :goto_0
.end method

.method public static signOfDet2x2(Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;Lcom/vividsolutions/jts/math/DD;)I
    .locals 3
    .param p0, "x1"    # Lcom/vividsolutions/jts/math/DD;
    .param p1, "y1"    # Lcom/vividsolutions/jts/math/DD;
    .param p2, "x2"    # Lcom/vividsolutions/jts/math/DD;
    .param p3, "y2"    # Lcom/vividsolutions/jts/math/DD;

    .prologue
    .line 85
    invoke-virtual {p0, p3}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v1

    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/math/DD;->multiply(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/math/DD;->selfSubtract(Lcom/vividsolutions/jts/math/DD;)Lcom/vividsolutions/jts/math/DD;

    move-result-object v0

    .line 86
    .local v0, "det":Lcom/vividsolutions/jts/math/DD;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/math/DD;->signum()I

    move-result v1

    return v1
.end method

.method private static signum(D)I
    .locals 4
    .param p0, "x"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 153
    cmpl-double v0, p0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    .line 154
    :cond_0
    cmpg-double v0, p0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
