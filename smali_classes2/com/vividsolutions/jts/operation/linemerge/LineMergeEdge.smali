.class public Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;
.super Lcom/vividsolutions/jts/planargraph/Edge;
.source "LineMergeEdge.java"


# instance fields
.field private line:Lcom/vividsolutions/jts/geom/LineString;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 0
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/Edge;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;->line:Lcom/vividsolutions/jts/geom/LineString;

    .line 52
    return-void
.end method


# virtual methods
.method public getLine()Lcom/vividsolutions/jts/geom/LineString;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;->line:Lcom/vividsolutions/jts/geom/LineString;

    return-object v0
.end method
