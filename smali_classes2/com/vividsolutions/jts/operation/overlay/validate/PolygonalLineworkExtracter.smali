.class Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;
.super Ljava/lang/Object;
.source "FuzzyPointLocator.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryFilter;


# instance fields
.field private linework:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;->linework:Ljava/util/List;

    .line 129
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 136
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 137
    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    .line 138
    .local v1, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;->linework:Ljava/util/List;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 140
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;->linework:Ljava/util/List;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "i":I
    .end local v1    # "poly":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_0
    return-void
.end method

.method public getLinework()Ljava/util/List;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/validate/PolygonalLineworkExtracter;->linework:Ljava/util/List;

    return-object v0
.end method
