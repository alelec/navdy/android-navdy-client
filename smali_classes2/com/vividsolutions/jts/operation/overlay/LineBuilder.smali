.class public Lcom/vividsolutions/jts/operation/overlay/LineBuilder;
.super Ljava/lang/Object;
.source "LineBuilder.java"


# instance fields
.field private geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private lineEdgesList:Ljava/util/List;

.field private op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

.field private ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

.field private resultLineList:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/operation/overlay/OverlayOp;Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/algorithm/PointLocator;)V
    .locals 1
    .param p1, "op"    # Lcom/vividsolutions/jts/operation/overlay/OverlayOp;
    .param p2, "geometryFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;
    .param p3, "ptLocator"    # Lcom/vividsolutions/jts/algorithm/PointLocator;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->lineEdgesList:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->resultLineList:Ljava/util/List;

    .line 56
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    .line 57
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 58
    iput-object p3, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 59
    return-void
.end method

.method private buildLines(I)V
    .locals 6
    .param p1, "opCode"    # I

    .prologue
    .line 168
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->lineEdgesList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 170
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    .line 171
    .local v2, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    .line 172
    .local v3, "line":Lcom/vividsolutions/jts/geom/LineString;
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->resultLineList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geomgraph/Edge;->setInResult(Z)V

    goto :goto_0

    .line 175
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v2    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v3    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    return-void
.end method

.method private collectBoundaryTouchEdge(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;ILjava/util/List;)V
    .locals 3
    .param p1, "de"    # Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .param p2, "opCode"    # I
    .param p3, "edges"    # Ljava/util/List;

    .prologue
    const/4 v2, 0x1

    .line 148
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 149
    .local v0, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isLineEdge()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isVisited()Z

    move-result v1

    if-nez v1, :cond_0

    .line 151
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInteriorAreaEdge()Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->isInResult()Z

    move-result v1

    if-nez v1, :cond_0

    .line 155
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInResult()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInResult()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->isInResult()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    move v1, v2

    :goto_1
    invoke-static {v1}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 158
    invoke-static {v0, p2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isResultOfOp(Lcom/vividsolutions/jts/geomgraph/Label;I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-ne p2, v2, :cond_0

    .line 161
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setVisitedEdge(Z)V

    goto :goto_0

    .line 155
    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private collectLineEdge(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;ILjava/util/List;)V
    .locals 3
    .param p1, "de"    # Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .param p2, "opCode"    # I
    .param p3, "edges"    # Ljava/util/List;

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    .line 123
    .local v1, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v0

    .line 125
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isLineEdge()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isVisited()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, p2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isResultOfOp(Lcom/vividsolutions/jts/geomgraph/Label;I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->isCovered()Z

    move-result v2

    if-nez v2, :cond_0

    .line 130
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setVisitedEdge(Z)V

    .line 134
    :cond_0
    return-void
.end method

.method private collectLines(I)V
    .locals 3
    .param p1, "opCode"    # I

    .prologue
    .line 103
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getGraph()Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 105
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->lineEdgesList:Ljava/util/List;

    invoke-direct {p0, v0, p1, v2}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->collectLineEdge(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;ILjava/util/List;)V

    .line 106
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->lineEdgesList:Ljava/util/List;

    invoke-direct {p0, v0, p1, v2}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->collectBoundaryTouchEdge(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;ILjava/util/List;)V

    goto :goto_0

    .line 108
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    :cond_0
    return-void
.end method

.method private findCoveredLineEdges()V
    .locals 8

    .prologue
    .line 81
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getGraph()Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 82
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 84
    .local v4, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->findCoveredLineEdges()V

    goto :goto_0

    .line 91
    .end local v4    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getGraph()Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 92
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 93
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    .line 94
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isLineEdge()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->isCoveredSet()Z

    move-result v6

    if-nez v6, :cond_1

    .line 95
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isCoveredByA(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    .line 96
    .local v2, "isCovered":Z
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geomgraph/Edge;->setCovered(Z)V

    goto :goto_1

    .line 99
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v1    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v2    # "isCovered":Z
    :cond_2
    return-void
.end method

.method private labelIsolatedLine(Lcom/vividsolutions/jts/geomgraph/Edge;I)V
    .locals 4
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "targetIndex"    # I

    .prologue
    .line 196
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v3, p2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getArgGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    .line 197
    .local v0, "loc":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(II)V

    .line 198
    return-void
.end method

.method private labelIsolatedLines(Ljava/util/List;)V
    .locals 5
    .param p1, "edgesList"    # Ljava/util/List;

    .prologue
    const/4 v4, 0x0

    .line 179
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 180
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 181
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    .line 183
    .local v2, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->isIsolated()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 184
    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 185
    invoke-direct {p0, v0, v4}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->labelIsolatedLine(Lcom/vividsolutions/jts/geomgraph/Edge;I)V

    goto :goto_0

    .line 187
    :cond_1
    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->labelIsolatedLine(Lcom/vividsolutions/jts/geomgraph/Edge;I)V

    goto :goto_0

    .line 190
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v2    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    :cond_2
    return-void
.end method


# virtual methods
.method public build(I)Ljava/util/List;
    .locals 1
    .param p1, "opCode"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->findCoveredLineEdges()V

    .line 66
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->collectLines(I)V

    .line 68
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->buildLines(I)V

    .line 69
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->resultLineList:Ljava/util/List;

    return-object v0
.end method
