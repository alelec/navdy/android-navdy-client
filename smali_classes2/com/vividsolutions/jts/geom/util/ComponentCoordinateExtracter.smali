.class public Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;
.super Ljava/lang/Object;
.source "ComponentCoordinateExtracter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# instance fields
.field private coords:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "coords"    # Ljava/util/List;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->coords:Ljava/util/List;

    .line 73
    return-void
.end method

.method public static getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v0, "coords":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;

    invoke-direct {v1, v0}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 62
    return-object v0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 78
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->coords:Ljava/util/List;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_1
    return-void
.end method
