.class abstract Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;
.super Ljava/lang/Object;
.source "PreparedPolygonPredicate.java"


# instance fields
.field protected prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

.field private targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 1
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .line 63
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getPointLocator()Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 64
    return-void
.end method


# virtual methods
.method protected isAllTestComponentsInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 76
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 77
    .local v0, "coords":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 78
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    .local v3, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v4, v3}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 80
    .local v2, "loc":I
    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    .line 81
    const/4 v4, 0x0

    .line 83
    .end local v2    # "loc":I
    .end local v3    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method protected isAllTestComponentsInTargetInterior(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 96
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 97
    .local v0, "coords":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 98
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 99
    .local v3, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v4, v3}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 100
    .local v2, "loc":I
    if-eqz v2, :cond_0

    .line 101
    const/4 v4, 0x0

    .line 103
    .end local v2    # "loc":I
    .end local v3    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method protected isAnyTargetComponentInAreaTest(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "targetRepPts"    # Ljava/util/List;

    .prologue
    .line 157
    new-instance v3, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;

    invoke-direct {v3, p1}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 158
    .local v3, "piaLoc":Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 159
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 160
    .local v2, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {v3, v2}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v1

    .line 161
    .local v1, "loc":I
    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    .line 162
    const/4 v4, 0x1

    .line 164
    .end local v1    # "loc":I
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected isAnyTestComponentInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 116
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 117
    .local v0, "coords":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 118
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 119
    .local v3, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v4, v3}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 120
    .local v2, "loc":I
    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    .line 121
    const/4 v4, 0x1

    .line 123
    .end local v2    # "loc":I
    .end local v3    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected isAnyTestComponentInTargetInterior(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 136
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 137
    .local v0, "coords":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 138
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 139
    .local v3, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;->targetPointLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v4, v3}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    .line 140
    .local v2, "loc":I
    if-nez v2, :cond_0

    .line 141
    const/4 v4, 0x1

    .line 143
    .end local v2    # "loc":I
    .end local v3    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
