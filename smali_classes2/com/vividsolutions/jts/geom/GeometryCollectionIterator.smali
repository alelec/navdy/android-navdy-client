.class public Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;
.super Ljava/lang/Object;
.source "GeometryCollectionIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private atStart:Z

.field private index:I

.field private max:I

.field private parent:Lcom/vividsolutions/jts/geom/Geometry;

.field private subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->parent:Lcom/vividsolutions/jts/geom/Geometry;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->atStart:Z

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->index:I

    .line 89
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    iput v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->max:I

    .line 90
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 98
    iget-boolean v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->atStart:Z

    if-eqz v1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    if-eqz v1, :cond_2

    .line 102
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 105
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    .line 107
    :cond_2
    iget v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->index:I

    iget v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->max:I

    if-lt v1, v2, :cond_0

    .line 108
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 120
    iget-boolean v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->atStart:Z

    if-eqz v1, :cond_1

    .line 121
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->atStart:Z

    .line 122
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->parent:Lcom/vividsolutions/jts/geom/Geometry;

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    if-eqz v1, :cond_3

    .line 125
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 129
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    .line 132
    :cond_3
    iget v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->index:I

    iget v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->max:I

    if-lt v1, v2, :cond_4

    .line 133
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 135
    :cond_4
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->parent:Lcom/vividsolutions/jts/geom/Geometry;

    iget v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->index:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->index:I

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 136
    .local v0, "obj":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v1, v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v1, :cond_0

    .line 137
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local v0    # "obj":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    .line 139
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->subcollectionIterator:Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
