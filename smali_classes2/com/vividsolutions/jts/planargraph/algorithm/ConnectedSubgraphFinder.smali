.class public Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;
.super Ljava/lang/Object;
.source "ConnectedSubgraphFinder.java"


# instance fields
.field private graph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/planargraph/PlanarGraph;)V
    .locals 0
    .param p1, "graph"    # Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->graph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    .line 51
    return-void
.end method

.method private addEdges(Lcom/vividsolutions/jts/planargraph/Node;Ljava/util/Stack;Lcom/vividsolutions/jts/planargraph/Subgraph;)V
    .locals 4
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p2, "nodeStack"    # Ljava/util/Stack;
    .param p3, "subgraph"    # Lcom/vividsolutions/jts/planargraph/Subgraph;

    .prologue
    .line 98
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/planargraph/Node;->setVisited(Z)V

    .line 99
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 100
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 101
    .local v0, "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/vividsolutions/jts/planargraph/Subgraph;->add(Lcom/vividsolutions/jts/planargraph/Edge;)V

    .line 102
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    .line 103
    .local v2, "toNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Node;->isVisited()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p2, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 105
    .end local v0    # "de":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .end local v2    # "toNode":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    return-void
.end method

.method private addReachable(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Subgraph;)V
    .locals 3
    .param p1, "startNode"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p2, "subgraph"    # Lcom/vividsolutions/jts/planargraph/Subgraph;

    .prologue
    .line 83
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    .line 84
    .local v1, "nodeStack":Ljava/util/Stack;
    invoke-virtual {v1, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 85
    :goto_0
    invoke-virtual {v1}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 86
    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/Node;

    .line 87
    .local v0, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-direct {p0, v0, v1, p2}, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->addEdges(Lcom/vividsolutions/jts/planargraph/Node;Ljava/util/Stack;Lcom/vividsolutions/jts/planargraph/Subgraph;)V

    goto :goto_0

    .line 89
    .end local v0    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_0
    return-void
.end method

.method private findSubgraph(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Subgraph;
    .locals 2
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 70
    new-instance v0, Lcom/vividsolutions/jts/planargraph/Subgraph;

    iget-object v1, p0, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->graph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/planargraph/Subgraph;-><init>(Lcom/vividsolutions/jts/planargraph/PlanarGraph;)V

    .line 71
    .local v0, "subgraph":Lcom/vividsolutions/jts/planargraph/Subgraph;
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->addReachable(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Subgraph;)V

    .line 72
    return-object v0
.end method


# virtual methods
.method public getConnectedSubgraphs()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 55
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v3, "subgraphs":Ljava/util/List;
    iget-object v4, p0, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->graph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setVisited(Ljava/util/Iterator;Z)V

    .line 58
    iget-object v4, p0, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->graph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;->edgeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 59
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/Edge;

    .line 60
    .local v0, "e":Lcom/vividsolutions/jts/planargraph/Edge;
    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    .line 61
    .local v2, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Node;->isVisited()Z

    move-result v4

    if-nez v4, :cond_0

    .line 62
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/planargraph/algorithm/ConnectedSubgraphFinder;->findSubgraph(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Subgraph;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    .end local v0    # "e":Lcom/vividsolutions/jts/planargraph/Edge;
    .end local v2    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    return-object v3
.end method
