.class public final enum Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;
.super Ljava/lang/Enum;
.source "NavigationManager.java"


# annotations
.annotation build Lcom/nokia/maps/annotation/HybridPlus;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/here/android/mpa/guidance/NavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TtsOutputFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum NUANCE:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

.field public static final enum RAW:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

.field private static final synthetic a:[Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 911
    new-instance v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v2}, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->RAW:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    .line 915
    new-instance v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    const-string v1, "NUANCE"

    invoke-direct {v0, v1, v3}, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->NUANCE:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    .line 906
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->RAW:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->NUANCE:Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->a:[Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;
    .locals 1

    .prologue
    .line 906
    const-class v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    return-object v0
.end method

.method public static values()[Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;
    .locals 1

    .prologue
    .line 906
    sget-object v0, Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->a:[Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    invoke-virtual {v0}, [Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/here/android/mpa/guidance/NavigationManager$TtsOutputFormat;

    return-object v0
.end method
