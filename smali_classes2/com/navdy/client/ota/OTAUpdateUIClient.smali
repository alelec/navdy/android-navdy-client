.class public interface abstract Lcom/navdy/client/ota/OTAUpdateUIClient;
.super Ljava/lang/Object;
.source "OTAUpdateUIClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/ota/OTAUpdateUIClient$Error;
    }
.end annotation


# virtual methods
.method public abstract isInForeground()Z
.end method

.method public abstract onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V
.end method

.method public abstract onErrorCheckingForUpdate(Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V
.end method

.method public abstract onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V
.end method

.method public abstract onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JB)V
.end method
