.class public interface abstract Lcom/navdy/client/ota/OTAUpdateServiceInterface;
.super Ljava/lang/Object;
.source "OTAUpdateServiceInterface.java"

# interfaces
.implements Landroid/os/IBinder;


# virtual methods
.method public abstract cancelDownload()V
.end method

.method public abstract cancelUpload()V
.end method

.method public abstract checkForUpdate()Z
.end method

.method public abstract downloadOTAUpdate()V
.end method

.method public abstract getHUDBuildVersionText()Ljava/lang/String;
.end method

.method public abstract getOTAUpdateState()Lcom/navdy/client/ota/OTAUpdateService$State;
.end method

.method public abstract getUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;
.end method

.method public abstract isCheckingForUpdate()Z
.end method

.method public abstract lastKnownUploadSize()J
.end method

.method public abstract registerUIClient(Lcom/navdy/client/ota/OTAUpdateUIClient;)V
.end method

.method public abstract resetUpdate()V
.end method

.method public abstract setNetworkDownloadApproval(Z)V
.end method

.method public abstract toggleAutoDownload(Z)V
.end method

.method public abstract unregisterUIClient()V
.end method
