.class public final enum Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
.super Ljava/lang/Enum;
.source "DestinationsServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DestinationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

.field public static final enum FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

.field public static final enum SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .line 41
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    const-string v1, "FAVORITES"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->$VALUES:[Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->$VALUES:[Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    return-object v0
.end method
