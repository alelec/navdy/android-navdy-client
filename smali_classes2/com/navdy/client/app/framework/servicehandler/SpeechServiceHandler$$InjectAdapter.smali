.class public final Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;
.super Ldagger/internal/Binding;
.source "SpeechServiceHandler$$InjectAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/Binding",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;",
        ">;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private audioRouter:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Lcom/navdy/client/app/framework/util/TTSAudioRouter;",
            ">;"
        }
    .end annotation
.end field

.field private sharedPreferences:Ldagger/internal/Binding;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/internal/Binding",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 29
    const-string v0, "com.navdy.client.app.framework.servicehandler.SpeechServiceHandler"

    const-string v1, "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler"

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-direct {p0, v0, v1, v2, v3}, Ldagger/internal/Binding;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Object;)V

    .line 30
    return-void
.end method


# virtual methods
.method public attach(Ldagger/internal/Linker;)V
    .locals 3
    .param p1, "linker"    # Ldagger/internal/Linker;

    .prologue
    .line 39
    const-string v0, "com.navdy.client.app.framework.util.TTSAudioRouter"

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->audioRouter:Ldagger/internal/Binding;

    .line 40
    const-string v0, "android.content.SharedPreferences"

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Ldagger/internal/Linker;->requestBinding(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/ClassLoader;)Ldagger/internal/Binding;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    .line 41
    return-void
.end method

.method public get()Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;-><init>()V

    .line 60
    .local v0, "result":Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->injectMembers(Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;)V

    .line 61
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->get()Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    move-result-object v0

    return-object v0
.end method

.method public getDependencies(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;",
            "Ljava/util/Set",
            "<",
            "Ldagger/internal/Binding",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "getBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    .local p2, "injectMembersBindings":Ljava/util/Set;, "Ljava/util/Set<Ldagger/internal/Binding<*>;>;"
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->audioRouter:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public injectMembers(Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;)V
    .locals 1
    .param p1, "object"    # Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->audioRouter:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    iput-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .line 71
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->sharedPreferences:Ldagger/internal/Binding;

    invoke-virtual {v0}, Ldagger/internal/Binding;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 72
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler$$InjectAdapter;->injectMembers(Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;)V

    return-void
.end method
