.class Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;
.super Landroid/telephony/PhoneStateListener;
.source "TelephonyServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "serviceState"    # Landroid/telephony/ServiceState;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->access$002(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;I)I

    .line 65
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service state changed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-static {v2}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 66
    return-void
.end method
