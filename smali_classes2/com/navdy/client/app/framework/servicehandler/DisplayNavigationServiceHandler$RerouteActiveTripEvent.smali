.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RerouteActiveTripEvent"
.end annotation


# instance fields
.field public final destination:Lcom/navdy/client/app/framework/models/Destination;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final routeId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "routeId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 81
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 82
    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;->routeId:Ljava/lang/String;

    .line 83
    return-void
.end method
