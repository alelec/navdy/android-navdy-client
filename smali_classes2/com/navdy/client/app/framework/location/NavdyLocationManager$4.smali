.class Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getLastTripUpdateCoordinates()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    .line 460
    const-wide/16 v2, 0x0

    .line 461
    .local v2, "navdyLatitude":D
    const-wide/16 v4, 0x0

    .line 462
    .local v4, "navdyLongitude":D
    const-wide/16 v6, 0x0

    .line 464
    .local v6, "time":J
    const/4 v8, 0x0

    .line 467
    .local v8, "tripsCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 469
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 470
    invoke-interface {v8}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    invoke-static {v8, v9}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v1

    .line 472
    .local v1, "trip":Lcom/navdy/client/app/framework/models/Trip;
    if-eqz v1, :cond_1

    .line 473
    iget-wide v10, v1, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    cmpl-double v9, v10, v12

    if-nez v9, :cond_0

    iget-wide v10, v1, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    cmpl-double v9, v10, v12

    if-eqz v9, :cond_3

    .line 474
    :cond_0
    iget-wide v2, v1, Lcom/navdy/client/app/framework/models/Trip;->endLat:D

    .line 475
    iget-wide v4, v1, Lcom/navdy/client/app/framework/models/Trip;->endLong:D

    .line 480
    :goto_0
    iget-wide v6, v1, Lcom/navdy/client/app/framework/models/Trip;->endTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    .end local v1    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_1
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 487
    new-instance v0, Landroid/location/Location;

    const-string v9, ""

    invoke-direct {v0, v9}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 488
    .local v0, "carLocation":Landroid/location/Location;
    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 489
    invoke-virtual {v0, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 490
    invoke-virtual {v0, v6, v7}, Landroid/location/Location;->setTime(J)V

    .line 492
    iget-object v9, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v9, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1100(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 493
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$000()Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    .line 494
    :try_start_1
    iget-object v9, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v9, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$802(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Landroid/location/Location;

    .line 495
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 497
    :cond_2
    return-void

    .line 477
    .end local v0    # "carLocation":Landroid/location/Location;
    .restart local v1    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :cond_3
    :try_start_2
    iget-wide v2, v1, Lcom/navdy/client/app/framework/models/Trip;->startLat:D

    .line 478
    iget-wide v4, v1, Lcom/navdy/client/app/framework/models/Trip;->startLong:D
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 484
    .end local v1    # "trip":Lcom/navdy/client/app/framework/models/Trip;
    :catchall_0
    move-exception v9

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v9

    .line 495
    .restart local v0    # "carLocation":Landroid/location/Location;
    :catchall_1
    move-exception v9

    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v9
.end method
