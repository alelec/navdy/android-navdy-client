.class public Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;
.super Ljava/lang/Object;
.source "SocialNotificationHandler.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static handleFacebookNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 10
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 48
    .local v5, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v4

    .line 49
    .local v4, "notification":Landroid/app/Notification;
    invoke-static {v4}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v2

    .line 51
    .local v2, "extras":Landroid/os/Bundle;
    const-string v7, "android.text"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "event":Ljava/lang/String;
    sget-object v7, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[navdyinfo-facebook] event["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v7, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v8, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    new-instance v7, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v8, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 59
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 60
    invoke-virtual {v7, v5}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 61
    invoke-virtual {v7, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    iget-wide v8, v4, Landroid/app/Notification;->when:J

    .line 62
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    .line 63
    invoke-virtual {v7, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v6

    .line 65
    .local v6, "socialEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v6}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 66
    return-void
.end method

.method public static handleSocialNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 4
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 44
    :goto_0
    return-void

    .line 32
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 42
    sget-object v1, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calendar notification not handled ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :sswitch_0
    const-string v2, "com.facebook.katana"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "com.twitter.android"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    .line 34
    :pswitch_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->handleFacebookNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 38
    :pswitch_1
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->handleTwitterNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 32
    nop

    :sswitch_data_0
    .sparse-switch
        0xa20b87 -> :sswitch_1
        0x2a9664f1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static handleTwitterNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 14
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 69
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 70
    .local v6, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v5

    .line 71
    .local v5, "notification":Landroid/app/Notification;
    invoke-static {v5}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v1

    .line 73
    .local v1, "extras":Landroid/os/Bundle;
    const-string v10, "android.title"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 74
    .local v7, "sender":Ljava/lang/String;
    const-string v10, "android.text"

    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 75
    .local v4, "message":Ljava/lang/String;
    const/4 v9, 0x0

    .line 77
    .local v9, "to":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v10, "@"

    invoke-virtual {v4, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 78
    const-string v10, " "

    invoke-virtual {v4, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 79
    .local v3, "index":I
    const/4 v10, -0x1

    if-eq v3, v10, :cond_0

    .line 80
    const/4 v10, 0x0

    invoke-virtual {v4, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 81
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 84
    .end local v3    # "index":I
    :cond_0
    sget-object v10, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "[navdyinfo-twitter] sender["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] to["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] message["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_FROM:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_TO:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v9}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v10, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v11, Lcom/navdy/service/library/events/glances/SocialConstants;->SOCIAL_MESSAGE:Lcom/navdy/service/library/events/glances/SocialConstants;

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/SocialConstants;->name()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v10, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v11, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_SOCIAL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 93
    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 94
    invoke-virtual {v10, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 95
    invoke-virtual {v10, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    iget-wide v12, v5, Landroid/app/Notification;->when:J

    .line 96
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    .line 97
    invoke-virtual {v10, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 99
    .local v8, "socialEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v8}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 100
    return-void
.end method
