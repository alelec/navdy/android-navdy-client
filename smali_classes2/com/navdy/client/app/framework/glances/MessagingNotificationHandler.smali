.class public Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;
.super Ljava/lang/Object;
.source "MessagingNotificationHandler.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static handleFacebookMessengerNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 12
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 194
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v7

    .line 195
    .local v7, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v6

    .line 196
    .local v6, "notification":Landroid/app/Notification;
    invoke-static {v6}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v1

    .line 198
    .local v1, "extras":Landroid/os/Bundle;
    const-string v8, "android.title"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "from":Ljava/lang/String;
    const-string v8, "android.text"

    invoke-static {v1, v8}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 200
    .local v4, "message":Ljava/lang/String;
    sget-object v8, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[navdyinfo-fbmessenger] from["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] message["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 202
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v8, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v9, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    new-instance v8, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v9, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    new-instance v8, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v9, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 208
    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v8

    .line 209
    invoke-virtual {v8, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v8

    .line 210
    invoke-virtual {v8, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v8

    iget-wide v10, v6, Landroid/app/Notification;->when:J

    .line 211
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v8

    .line 212
    invoke-virtual {v8, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v5

    .line 214
    .local v5, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v5}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 215
    return-void
.end method

.method private static handleGoogleTalkNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 16
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 62
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 63
    .local v8, "packageName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v6

    .line 66
    .local v6, "notification":Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v11

    .line 67
    .local v11, "tag":Ljava/lang/String;
    invoke-static {v11}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 68
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    .line 69
    const-string v12, ":sms:"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 71
    sget-object v12, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "navdyinfo-hangout is sms["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 76
    sget-object v12, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "navdyinfo-hangout is sms["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_2
    iget-object v12, v6, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    if-eqz v12, :cond_0

    .line 82
    const-string v1, ""

    .line 84
    .local v1, "contactName":Ljava/lang/String;
    sget-object v12, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->splitTickerTextOnColon:Ljava/util/regex/Pattern;

    iget-object v13, v6, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 85
    .local v4, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v12

    const/4 v13, 0x2

    if-ge v12, v13, :cond_4

    .line 86
    :cond_3
    iget-object v12, v6, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "body":Ljava/lang/String;
    :goto_1
    sget-object v12, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[navdyinfo-hangout] hangout message received sender["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] message["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 93
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v10

    .line 94
    .local v10, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-nez v10, :cond_5

    .line 95
    sget-object v12, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v13, "[navdyinfo-hangout]  lost connection"

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 88
    .end local v0    # "body":Ljava/lang/String;
    .end local v10    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_4
    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    const/4 v12, 0x2

    invoke-virtual {v4, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "body":Ljava/lang/String;
    goto :goto_1

    .line 99
    .restart local v10    # "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_5
    invoke-virtual {v10}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v12

    iget-object v9, v12, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    .line 101
    .local v9, "protocol":Ljava/lang/String;
    invoke-static {v9}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->isHudVersionCompatible(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_6

    .line 103
    const/4 v12, 0x1

    invoke-static {v1, v1, v0, v12}, Lcom/navdy/client/debug/util/NotificationBuilder;->buildSmsNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v7

    .line 104
    .local v7, "notificationEvent":Lcom/navdy/service/library/events/notification/NotificationEvent;
    invoke-virtual {v10, v7}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    goto/16 :goto_0

    .line 106
    .end local v7    # "notificationEvent":Lcom/navdy/service/library/events/notification/NotificationEvent;
    :cond_6
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, "id":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v12, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v13, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v13}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13, v1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    new-instance v12, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v13, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v13}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13, v0}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    new-instance v12, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v13, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 112
    invoke-virtual {v12, v13}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v12

    .line 113
    invoke-virtual {v12, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v12

    .line 114
    invoke-virtual {v12, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v12

    iget-wide v14, v6, Landroid/app/Notification;->when:J

    .line 115
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v12

    .line 116
    invoke-virtual {v12, v2}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v12

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v5

    .line 118
    .local v5, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v5}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    goto/16 :goto_0
.end method

.method public static handleMessageNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 4
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    :goto_0
    return-void

    .line 38
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 57
    sget-object v1, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message notification not handled ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 38
    :sswitch_0
    const-string v2, "com.google.android.talk"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "com.Slack"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "com.whatsapp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "com.facebook.orca"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    .line 40
    :pswitch_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->handleGoogleTalkNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 44
    :pswitch_1
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->handleSlackNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 48
    :pswitch_2
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->handleWhatsAppNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 53
    :pswitch_3
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->handleFacebookMessengerNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        -0x5c4004a1 -> :sswitch_2
        0x36211dfc -> :sswitch_3
        0x3a63b8c3 -> :sswitch_1
        0x5a539273 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static handleSlackNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 12
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v8

    .line 125
    .local v8, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v7

    .line 126
    .local v7, "notification":Landroid/app/Notification;
    invoke-static {v7}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v2

    .line 128
    .local v2, "extras":Landroid/os/Bundle;
    const-string v9, "android.title"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 129
    .local v3, "from":Ljava/lang/String;
    const-string v9, "android.text"

    invoke-static {v2, v9}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, "message":Ljava/lang/String;
    const-string v9, "android.summaryText"

    invoke-virtual {v2, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "domain":Ljava/lang/String;
    sget-object v9, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[navdyinfo-slack] from["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] domain["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] message["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 134
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v9, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v10, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v9, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v10, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v9, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v10, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_DOMAIN:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v10}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10, v1}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    new-instance v9, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v9}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v10, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 141
    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v9

    .line 142
    invoke-virtual {v9, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v9

    .line 143
    invoke-virtual {v9, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v9

    iget-wide v10, v7, Landroid/app/Notification;->when:J

    .line 144
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v9

    .line 145
    invoke-virtual {v9, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v6

    .line 147
    .local v6, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v6}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 148
    return-void
.end method

.method private static handleWhatsAppNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 14
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 151
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 152
    .local v10, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v9

    .line 153
    .local v9, "notification":Landroid/app/Notification;
    invoke-static {v9}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v1

    .line 155
    .local v1, "extras":Landroid/os/Bundle;
    const-string v11, "android.title"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "from":Ljava/lang/String;
    const-string v11, "android.text"

    invoke-static {v1, v11}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 159
    .local v7, "message":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getFirstParticipantInCarConversation(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 160
    .local v2, "firstParticipantInCarConversation":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 161
    move-object v3, v2

    .line 165
    :cond_0
    invoke-static {v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getLastTextLine(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    .line 166
    .local v5, "lastLine":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 167
    move-object v7, v5

    .line 176
    :cond_1
    :goto_0
    sget-object v11, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[navdyinfo-whatsapp] from["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] message["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "id":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v3}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    new-instance v11, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v12, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual {v12}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12, v7}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    new-instance v11, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct {v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v12, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 184
    invoke-virtual {v11, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 185
    invoke-virtual {v11, v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 186
    invoke-virtual {v11, v4}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    iget-wide v12, v9, Landroid/app/Notification;->when:J

    .line 187
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    .line 188
    invoke-virtual {v11, v0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v8

    .line 190
    .local v8, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v8}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 191
    return-void

    .line 170
    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    .end local v4    # "id":Ljava/lang/String;
    .end local v8    # "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :cond_2
    invoke-static {v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getLastMessageInCarConversation(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    .line 171
    .local v6, "lastMessageInCarConversation":Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 172
    move-object v7, v6

    goto/16 :goto_0
.end method
