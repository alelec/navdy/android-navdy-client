.class public Lcom/navdy/client/app/framework/glances/GlancesHelper;
.super Ljava/lang/Object;
.source "GlancesHelper.java"


# static fields
.field private static final HUD_VERSION_GLANCE_CHANGE:F = 0.2f

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/glances/GlancesHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getActions(Landroid/app/Notification;)Ljava/util/HashSet;
    .locals 5
    .param p0, "notif"    # Landroid/app/Notification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Notification;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 29
    .local v3, "actions":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static {p0}, Landroid/support/v4/app/NotificationCompat;->getActionCount(Landroid/app/Notification;)I

    move-result v1

    .line 30
    .local v1, "actionCount":I
    const/4 v2, 0x0

    .local v2, "actionIndex":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 31
    invoke-static {p0, v2}, Landroid/support/v4/app/NotificationCompat;->getAction(Landroid/app/Notification;I)Landroid/support/v4/app/NotificationCompat$Action;

    move-result-object v0

    .line 32
    .local v0, "action":Landroid/support/v4/app/NotificationCompat$Action;
    iget-object v4, v0, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 33
    iget-object v4, v0, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 37
    .end local v0    # "action":Landroid/support/v4/app/NotificationCompat$Action;
    :cond_1
    return-object v3
.end method

.method public static getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isHudVersionCompatible(Ljava/lang/String;)Z
    .locals 4
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 78
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 79
    .local v0, "f":F
    const v3, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_0

    const/4 v2, 0x1

    .line 82
    .end local v0    # "f":F
    :cond_0
    :goto_0
    return v2

    .line 80
    :catch_0
    move-exception v1

    .line 81
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static printGlance(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 6
    .param p0, "event"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 54
    sget-object v2, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[glance-event] id["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " app["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " type["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " postTime["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 59
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 60
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[glance-event] no data"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 67
    :cond_1
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_5

    .line 68
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "[glance-event] no actions"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    :cond_3
    return-void

    .line 62
    :cond_4
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/events/glances/KeyValue;

    .line 63
    .local v1, "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    sget-object v3, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[glance-event] data key["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/glances/KeyValue;->key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] val["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/navdy/service/library/events/glances/KeyValue;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v1    # "keyValue":Lcom/navdy/service/library/events/glances/KeyValue;
    :cond_5
    iget-object v2, p0, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    .line 71
    .local v0, "action":Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;
    sget-object v3, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[glance-event] action["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 2
    .param p0, "message"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 41
    invoke-static {p0}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->printGlance(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    .line 43
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 44
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 47
    :cond_0
    return-void
.end method
