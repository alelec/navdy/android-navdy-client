.class public Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
.super Ljava/lang/Object;
.source "TTSAudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TTSAudioStatus"
.end annotation


# instance fields
.field public currentStreamMaxVolume:I

.field public currentStreamVolume:I

.field public outputDeviceName:Ljava/lang/String;

.field public playingTTS:Z

.field public streamType:I

.field public throughBluetooth:Z

.field public throughHFp:Z

.field public waitingForHfp:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;-><init>()V

    .line 107
    .local v0, "ttsAudioStatus":Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 108
    iget v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    iput v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 109
    iget v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    iput v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 110
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 111
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughHFp:Z

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughHFp:Z

    .line 112
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    iput-object v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 113
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    iput-boolean v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 114
    iget v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    iput v1, v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    .line 115
    return-object v0
.end method

.method public reset()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 120
    .local v0, "wasPlayingTTS":Z
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    .line 121
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    .line 122
    iput v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    .line 123
    iput v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    .line 124
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    .line 125
    iput-boolean v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughHFp:Z

    .line 126
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    .line 127
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    .line 128
    return v0
.end method
