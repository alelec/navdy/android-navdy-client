.class final Lcom/navdy/client/app/framework/util/SupportTicketService$4;
.super Ljava/lang/Object;
.source "SupportTicketService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SupportTicketService;->collectHudLog(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$4;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 677
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 678
    .local v1, "applicationContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/PathManager;->getLogsFolder()Ljava/lang/String;

    move-result-object v3

    .line 680
    .local v3, "logsFolder":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 684
    .local v7, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :try_start_0
    new-instance v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    sget-object v2, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    const-wide/16 v4, 0x3a98

    new-instance v6, Lcom/navdy/client/app/framework/util/SupportTicketService$4$1;

    invoke-direct {v6, p0, v3, v7}, Lcom/navdy/client/app/framework/util/SupportTicketService$4$1;-><init>(Lcom/navdy/client/app/framework/util/SupportTicketService$4;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 731
    .local v0, "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    invoke-virtual {v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->pullFile()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 739
    .end local v0    # "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    :goto_0
    return-void

    .line 732
    :catch_0
    move-exception v8

    .line 733
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error collecting hud logs "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 734
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 735
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$4;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v2, v7}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionSucceeded(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 737
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$4;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionFailed()V

    goto :goto_0
.end method
