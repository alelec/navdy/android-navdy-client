.class Lcom/navdy/client/app/framework/util/ContactObserver$1;
.super Ljava/lang/Object;
.source "ContactObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/ContactObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/ContactObserver;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/ContactObserver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/ContactObserver;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/ContactObserver$1;->this$0:Lcom/navdy/client/app/framework/util/ContactObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 32
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactObserver;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "sending fresh contact info to Display"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 34
    new-instance v1, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;-><init>()V

    const/16 v2, 0x1e

    .line 35
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;->maxContacts(Ljava/lang/Integer;)Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;

    move-result-object v1

    .line 36
    invoke-virtual {v1}, Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest$Builder;->build()Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;

    move-result-object v0

    .line 38
    .local v0, "favoriteContactsRequest":Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ContactObserver$1;->this$0:Lcom/navdy/client/app/framework/util/ContactObserver;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/ContactObserver;->access$100(Lcom/navdy/client/app/framework/util/ContactObserver;)Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->onFavoriteContactsRequest(Lcom/navdy/service/library/events/contacts/FavoriteContactsRequest;)V

    .line 39
    return-void
.end method
