.class public Lcom/navdy/client/app/framework/util/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# static fields
.field private static final DEFAULT_CACHE_SIZE:I = 0x1400000

.field private static final DEFAULT_IMAGE_MAX_SIZE:I = 0x400000

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final cache:Lcom/navdy/client/app/framework/util/BitmapCache;

.field private final cacheCriteriaSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ImageCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    const/high16 v0, 0x1400000

    const/high16 v1, 0x400000

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>(II)V

    .line 21
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "cacheSize"    # I
    .param p2, "cacheCriteriaSize"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Lcom/navdy/client/app/framework/util/BitmapCache;

    invoke-direct {v0, p1}, Lcom/navdy/client/app/framework/util/BitmapCache;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cache:Lcom/navdy/client/app/framework/util/BitmapCache;

    .line 25
    iput p2, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cacheCriteriaSize:I

    .line 26
    return-void
.end method

.method public static getBitmapSize(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 63
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public clearCache()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cache:Lcom/navdy/client/app/framework/util/BitmapCache;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/BitmapCache;->evictAll()V

    .line 55
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 56
    sget-object v0, Lcom/navdy/client/app/framework/util/ImageCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clearCache-explicit gc"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 29
    if-nez p1, :cond_1

    .line 30
    const/4 v0, 0x0

    .line 36
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cache:Lcom/navdy/client/app/framework/util/BitmapCache;

    invoke-virtual {v1, p1}, Lcom/navdy/client/app/framework/util/BitmapCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 33
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 34
    sget-object v1, Lcom/navdy/client/app/framework/util/ImageCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cache hit ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] size["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 40
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 45
    .local v0, "size":I
    iget v1, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cacheCriteriaSize:I

    if-lt v0, v1, :cond_2

    .line 46
    sget-object v1, Lcom/navdy/client/app/framework/util/ImageCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cache image size ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] > large ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cacheCriteriaSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/ImageCache;->cache:Lcom/navdy/client/app/framework/util/BitmapCache;

    invoke-virtual {v1, p1, p2}, Lcom/navdy/client/app/framework/util/BitmapCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v1, Lcom/navdy/client/app/framework/util/ImageCache;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cache put ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] size["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
