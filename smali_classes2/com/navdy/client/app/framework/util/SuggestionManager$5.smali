.class final Lcom/navdy/client/app/framework/util/SuggestionManager$5;
.super Ljava/lang/Object;
.source "SuggestionManager.java"

# interfaces
.implements Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager;->buildCalendarSuggestions(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$calendarSuggestions:Ljava/util/ArrayList;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$calendarSuggestions:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/util/SuggestionManager$5;Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/SuggestionManager$5;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    .prologue
    .line 750
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->processNextCalEvent(Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    return-void
.end method

.method private processNextCalEvent(Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
    .locals 17
    .param p2, "i"    # I
    .param p3, "addedCount"    # I
    .param p4, "callback"    # Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;II",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 787
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    const/4 v3, 0x2

    move/from16 v0, p3

    if-ge v0, v3, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, p2

    if-lt v0, v3, :cond_1

    .line 788
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$calendarSuggestions:Ljava/util/ArrayList;

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    .line 858
    :goto_0
    return-void

    .line 792
    :cond_1
    invoke-interface/range {p1 .. p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 793
    .local v4, "event":Lcom/navdy/client/app/framework/models/CalendarEvent;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to add this calendar entry to the list of suggestions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 795
    new-instance v10, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v10}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 796
    .local v10, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v3, v4, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    iput-object v3, v10, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 798
    iget-object v3, v4, Lcom/navdy/client/app/framework/models/CalendarEvent;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v3, :cond_2

    .line 800
    iget-object v3, v4, Lcom/navdy/client/app/framework/models/CalendarEvent;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iput-wide v6, v10, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 801
    iget-object v3, v4, Lcom/navdy/client/app/framework/models/CalendarEvent;->latLng:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iput-wide v6, v10, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 804
    :cond_2
    invoke-static {v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v9

    .line 805
    .local v9, "finalDestination":Lcom/navdy/client/app/framework/models/Destination;
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Lcom/navdy/client/app/framework/models/Destination;->setIsCalendarEvent(Z)V

    .line 808
    new-instance v2, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-object/from16 v8, p4

    invoke-direct/range {v2 .. v9}, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;-><init>(Lcom/navdy/client/app/framework/util/SuggestionManager$5;Lcom/navdy/client/app/framework/models/CalendarEvent;Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 855
    .local v2, "listener":Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->getInstance()Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    move-result-object v11

    iget-wide v12, v10, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v14, v10, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-object/from16 v16, v2

    invoke-virtual/range {v11 .. v16}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    goto :goto_0
.end method


# virtual methods
.method public onCalendarEventsRead(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    const/4 v3, 0x0

    .line 754
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 757
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 758
    .local v0, "now":J
    new-instance v2, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;-><init>(Lcom/navdy/client/app/framework/util/SuggestionManager$5;J)V

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 776
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    invoke-direct {p0, p1, v3, v3, v2}, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->processNextCalEvent(Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    .line 781
    .end local v0    # "now":J
    :goto_0
    return-void

    .line 778
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "No Calendar Event to process"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 779
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    iget-object v3, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$calendarSuggestions:Ljava/util/ArrayList;

    invoke-interface {v2, v3}, Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;->onSuggestionBuildComplete(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
