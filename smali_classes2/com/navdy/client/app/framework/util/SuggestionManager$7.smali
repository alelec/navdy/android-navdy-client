.class final Lcom/navdy/client/app/framework/util/SuggestionManager$7;
.super Ljava/lang/Object;
.source "SuggestionManager.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager;->onCalendarChanged(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$sendToHud:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .prologue
    .line 1075
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$7;->val$sendToHud:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1079
    .local p1, "calendarSuggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$900()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 1080
    .local v0, "recommendation":Lcom/navdy/client/app/framework/models/Suggestion;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Suggestion;->getType()Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    if-eq v2, v3, :cond_0

    .line 1081
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1084
    .end local v0    # "recommendation":Lcom/navdy/client/app/framework/models/Suggestion;
    :cond_1
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$902(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 1086
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$7;->val$sendToHud:Z

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$200(Z)V

    .line 1087
    return-void
.end method
