.class public interface abstract Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NavdyRouteListener"
.end annotation


# virtual methods
.method public abstract onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .param p1    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onReroute()V
.end method

.method public abstract onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .param p1    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .param p1    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onStopRoute()V
.end method

.method public abstract onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
