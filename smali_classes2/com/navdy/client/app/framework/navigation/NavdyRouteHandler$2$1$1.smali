.class Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

.field final synthetic val$newRequestId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    .prologue
    .line 313
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iput-object p2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->val$newRequestId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private prepareNavigation(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "newRequestId"    # Ljava/lang/String;
    .param p3, "initiatedOnHud"    # Z

    .prologue
    .line 328
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$000(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    .line 329
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "processDestination complete, but state is not CALCULATING_ROUTES anymore, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 339
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/navdy/client/app/framework/models/Destination;->equals(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 334
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "processDestination complete, but current destinations are not the same, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1000(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->navigateToDestination(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 321
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NONE:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    if-eq p2, v0, :cond_0

    .line 322
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "error while calling processDestination before navigating to it"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->val$newRequestId:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$initiatedOnHud:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->prepareNavigation(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Z)V

    .line 325
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 316
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->val$newRequestId:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->this$2:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$initiatedOnHud:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;->prepareNavigation(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Z)V

    .line 317
    return-void
.end method
