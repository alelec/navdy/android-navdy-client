.class Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;
.super Ljava/lang/Object;
.source "TelephonySupport21_LG_G3.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->reject(Landroid/media/session/MediaController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;

.field final synthetic val$m:Landroid/media/session/MediaController;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;Landroid/media/session/MediaController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->this$0:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;

    iput-object p2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->val$m:Landroid/media/session/MediaController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 51
    :try_start_0
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x4f

    invoke-direct {v0, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 52
    .local v0, "event":Landroid/view/KeyEvent;
    iget-object v2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v2, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 53
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HEADSETHOOK keydown sent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v4}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 55
    const/16 v2, 0x5dc

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SystemUtils;->sleep(I)V

    .line 57
    new-instance v0, Landroid/view/KeyEvent;

    .end local v0    # "event":Landroid/view/KeyEvent;
    const/4 v2, 0x1

    const/16 v3, 0x4f

    invoke-direct {v0, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 58
    .restart local v0    # "event":Landroid/view/KeyEvent;
    iget-object v2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v2, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 59
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HEADSETHOOK keyup sent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v4}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v0    # "event":Landroid/view/KeyEvent;
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
