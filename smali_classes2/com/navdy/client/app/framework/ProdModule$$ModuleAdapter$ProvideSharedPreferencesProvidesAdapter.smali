.class public final Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideSharedPreferencesProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Landroid/content/SharedPreferences;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Landroid/content/SharedPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/client/app/framework/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/framework/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/client/app/framework/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 100
    const-string v0, "android.content.SharedPreferences"

    const-string v1, "com.navdy.client.app.framework.ProdModule"

    const-string v2, "provideSharedPreferences"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 101
    iput-object p1, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    .line 102
    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;->setLibrary(Z)V

    .line 103
    return-void
.end method


# virtual methods
.method public get()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/ProdModule;->provideSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;->get()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method
