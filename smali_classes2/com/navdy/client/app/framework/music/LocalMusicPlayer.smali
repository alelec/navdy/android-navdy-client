.class public Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
.super Ljava/lang/Object;
.source "LocalMusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;
    }
.end annotation


# static fields
.field private static final PROGRESS_DELAY:I = 0x3e8

.field private static final SCRUB_DELAY:I = 0x12c

.field private static final SCRUB_STEP:I = 0x2710

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final DEFAULT_VOLUME:F

.field private final DUCKING_VOLUME:F

.field private audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private currentPosition:I

.field private currentTrackIndex:I

.field private fastForwardRunnable:Ljava/lang/Runnable;

.field private firstUnplayableTrack:J

.field private handler:Landroid/os/Handler;

.field private isDuckedDueToAudioFocusLoss:Z

.field private isPausedDueToAudioFocusLoss:Z

.field private listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private originalList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private progressUpdateRunnable:Ljava/lang/Runnable;

.field private rewindRunnable:Ljava/lang/Runnable;

.field private shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

.field private tracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const v0, 0x3e6b851f    # 0.23f

    iput v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->DUCKING_VOLUME:F

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->DEFAULT_VOLUME:F

    .line 42
    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 43
    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 44
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    .line 46
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isDuckedDueToAudioFocusLoss:Z

    .line 47
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isPausedDueToAudioFocusLoss:Z

    .line 48
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 50
    new-instance v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->progressUpdateRunnable:Ljava/lang/Runnable;

    .line 65
    new-instance v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$2;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->fastForwardRunnable:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$3;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->rewindRunnable:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$4;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    return v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->seek(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isDuckedDueToAudioFocusLoss:Z

    return v0
.end method

.method static synthetic access$602(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isDuckedDueToAudioFocusLoss:Z

    return p1
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->unduck()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isPausedDueToAudioFocusLoss:Z

    return v0
.end method

.method static synthetic access$802(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isPausedDueToAudioFocusLoss:Z

    return p1
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->duck()V

    return-void
.end method

.method private createMediaPlayer(I)V
    .locals 12
    .param p1, "index"    # I

    .prologue
    const/4 v11, 0x1

    .line 198
    iget-object v8, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v8, :cond_0

    .line 199
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->releaseCurrentPlayer()V

    .line 202
    :cond_0
    if-ltz p1, :cond_1

    iget-object v8, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lt p1, v8, :cond_2

    .line 203
    :cond_1
    sget-object v8, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Index out of range"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 233
    :goto_0
    return-void

    .line 208
    :cond_2
    iget-object v8, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v3, v8, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    .line 210
    .local v3, "trackIdStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v4

    .line 216
    .local v4, "trackId":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://media/external/audio/media/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 217
    .local v7, "uriString":Ljava/lang/String;
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 218
    .local v6, "uri":Landroid/net/Uri;
    sget-object v8, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "creating media player for index "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", URI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 220
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v10, 0x3

    invoke-virtual {v8, v9, v10, v11}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 224
    .local v1, "result":I
    if-eq v1, v11, :cond_3

    .line 225
    sget-object v8, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t get audio focus: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 229
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v6}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/MediaPlayer;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 230
    :catch_0
    move-exception v2

    .line 231
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v8, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MediaPlayer.create() threw "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 211
    .end local v1    # "result":I
    .end local v2    # "t":Ljava/lang/Throwable;
    .end local v4    # "trackId":J
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "uriString":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 212
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v8, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid track ID "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private duck()V
    .locals 2

    .prologue
    const v1, 0x3e6b851f    # 0.23f

    .line 361
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 362
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no mediaPlayer!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 366
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .locals 2

    .prologue
    .line 387
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    return-object v0
.end method

.method private releaseCurrentPlayer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 377
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 379
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 380
    iput-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 381
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isDuckedDueToAudioFocusLoss:Z

    if-nez v0, :cond_0

    .line 382
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 384
    :cond_0
    return-void
.end method

.method private seek(I)V
    .locals 2
    .param p1, "positionChange"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 357
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 358
    return-void
.end method

.method private unduck()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 369
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 370
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no mediaPlayer!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 374
    :goto_0
    return-void

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method


# virtual methods
.method public getListener()Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    return-object v0
.end method

.method public initWithQueue(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    const/4 v2, -0x1

    .line 121
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "init"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 122
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->originalList:Ljava/util/List;

    .line 123
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->originalList:Ljava/util/List;

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    .line 124
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 125
    iput v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 126
    iput v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 127
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    .line 128
    return-void
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()V
    .locals 2

    .prologue
    .line 261
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 262
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 263
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 264
    .local v0, "numTracks":I
    if-lez v0, :cond_0

    .line 265
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    rem-int/2addr v1, v0

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 267
    :cond_0
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play(I)V

    .line 268
    return-void
.end method

.method public pause(Z)V
    .locals 3
    .param p1, "releaseMediaPlayer"    # Z

    .prologue
    .line 241
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->progressUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 244
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 245
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no mediaPlayer!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 250
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    if-eqz v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;->onPlaybackStateUpdate(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)V

    .line 254
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 255
    if-eqz p1, :cond_0

    .line 256
    invoke-direct {p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->releaseCurrentPlayer()V

    goto :goto_0
.end method

.method public play()V
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play(I)V

    .line 237
    return-void
.end method

.method public play(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const-wide/16 v6, -0x1

    const v4, 0x3e6b851f    # 0.23f

    .line 143
    sget-object v1, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "play "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->progressUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 146
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    iget v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    if-ne v1, v2, :cond_1

    .line 147
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->createMediaPlayer(I)V

    .line 152
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_3

    .line 153
    sget-object v1, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "MediaPlayer init failed"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 154
    iput p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 155
    int-to-long v2, p1

    iget-wide v4, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 156
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$5;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$5;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 165
    :goto_1
    iget-wide v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 166
    int-to-long v2, p1

    iput-wide v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    goto :goto_0

    .line 163
    :cond_2
    sget-object v1, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "No playable tracks in the queue"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 170
    :cond_3
    iput-wide v6, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->firstUnplayableTrack:J

    .line 171
    iget-boolean v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->isDuckedDueToAudioFocusLoss:Z

    if-eqz v1, :cond_4

    .line 172
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v4, v4}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 174
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 175
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    if-ne p1, v1, :cond_5

    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    .line 176
    sget-object v1, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume - seeking to position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 177
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 179
    :cond_5
    iput p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 180
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$6;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$6;-><init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 188
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    iget v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 190
    .local v0, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    new-instance v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v2, v0}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    iget-object v3, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 191
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v2

    .line 192
    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v2

    .line 190
    invoke-interface {v1, v2}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;->onMetadataUpdate(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 193
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->progressUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public previous()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 271
    const/4 v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentPosition:I

    .line 272
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    const/16 v2, 0x7d0

    if-le v1, v2, :cond_0

    .line 273
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 284
    :goto_0
    return-void

    .line 275
    :cond_0
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 276
    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 277
    .local v0, "numTracks":I
    if-lez v0, :cond_1

    .line 278
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    add-int/2addr v1, v0

    rem-int/2addr v1, v0

    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 282
    :goto_1
    iget v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->play(I)V

    goto :goto_0

    .line 280
    :cond_1
    iput v3, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    goto :goto_1
.end method

.method public setListener(Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    .line 132
    return-void
.end method

.method public shuffle(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)V
    .locals 8
    .param p1, "shuffleMode"    # Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .prologue
    const/4 v7, -0x1

    .line 311
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-ne v5, p1, :cond_1

    .line 312
    sget-object v5, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Already set shuffle mode "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    const/4 v0, 0x0

    .line 317
    .local v0, "currentTrackId":Ljava/lang/String;
    iget v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    if-eq v5, v7, :cond_2

    .line 318
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    iget v6, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    iget-object v0, v5, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    .line 321
    :cond_2
    sget-object v5, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_SONGS:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-ne p1, v5, :cond_4

    .line 322
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 323
    .local v4, "shuffledList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 324
    iput-object v4, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    .line 334
    .end local v4    # "shuffledList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    :goto_1
    if-eqz v0, :cond_3

    .line 335
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 336
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 337
    .local v2, "info":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v5, v2, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->trackId:Ljava/lang/String;

    invoke-static {v5, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 338
    iput v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    .line 344
    .end local v1    # "i":I
    .end local v2    # "info":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_3
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 347
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    if-eq v5, v7, :cond_0

    .line 348
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    iget v6, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->currentTrackIndex:I

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .line 349
    .local v3, "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->listener:Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    new-instance v6, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    invoke-direct {v6, v3}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;-><init>(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 350
    invoke-virtual {v6, p1}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;

    move-result-object v6

    .line 351
    invoke-virtual {v6}, Lcom/navdy/service/library/events/audio/MusicTrackInfo$Builder;->build()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v6

    .line 349
    invoke-interface {v5, v6}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;->onMetadataUpdate(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    goto :goto_0

    .line 325
    .end local v3    # "musicTrackInfo":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_4
    sget-object v5, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_OFF:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-ne p1, v5, :cond_5

    .line 326
    iget-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->originalList:Ljava/util/List;

    iput-object v5, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->tracks:Ljava/util/List;

    goto :goto_1

    .line 328
    :cond_5
    sget-object v5, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unsupported shuffle mode "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 335
    .restart local v1    # "i":I
    .restart local v2    # "info":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public startFastForward()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 288
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no mediaPlayer!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 292
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->fastForwardRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public startRewind()V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 300
    sget-object v0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "no mediaPlayer!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 304
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->rewindRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public stopFastForward()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->fastForwardRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 296
    return-void
.end method

.method public stopRewind()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->rewindRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 308
    return-void
.end method
