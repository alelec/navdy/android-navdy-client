.class public Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
.super Ljava/lang/Object;
.source "GoogleTextSearchDestinationResult.java"


# static fields
.field public static final FORMATTED_PHONE_NUMBER:Ljava/lang/String; = "formatted_phone_number"

.field public static final INTERNATIONAL_PHONE_NUMBER:Ljava/lang/String; = "formatted_phone_number"

.field static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public address:Ljava/lang/String;

.field public city:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public countryCode:Ljava/lang/String;

.field public distance:D

.field public icon:Ljava/lang/String;

.field public jsonString:Ljava/lang/String;

.field public lat:Ljava/lang/String;

.field public lng:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public open_hours:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public open_now:Z

.field public phone:Ljava/lang/String;

.field public place_id:Ljava/lang/String;

.field public price_level:Ljava/lang/String;

.field public rating:Ljava/lang/String;

.field public state:Ljava/lang/String;

.field public streetName:Ljava/lang/String;

.field public streetNumber:Ljava/lang/String;

.field public types:[Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public zipCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private constructor <init>(Lorg/json/JSONObject;)V
    .locals 30
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->jsonString:Ljava/lang/String;

    .line 73
    const-string v27, "name"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 74
    const-string v27, "name"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    .line 90
    :cond_0
    :goto_0
    const-string v27, "geometry"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 91
    const-string v27, "geometry"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 92
    .local v11, "geometry":Lorg/json/JSONObject;
    const-string v27, "location"

    move-object/from16 v0, v27

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 93
    .local v15, "location":Lorg/json/JSONObject;
    const-string v27, "lat"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    .line 94
    const-string v27, "lng"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    .line 96
    .end local v11    # "geometry":Lorg/json/JSONObject;
    .end local v15    # "location":Lorg/json/JSONObject;
    :cond_1
    const-string v27, "routes"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 97
    const-string v27, "routes"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/json/JSONArray;

    .line 98
    .local v19, "results":Lorg/json/JSONArray;
    sget-object v28, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Routes: "

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    if-eqz v19, :cond_8

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v27

    :goto_1
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 99
    if-eqz v19, :cond_2

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONArray;->length()I

    move-result v27

    if-lez v27, :cond_2

    .line 100
    const/16 v27, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 101
    .local v9, "firstRoute":Lorg/json/JSONObject;
    if-eqz v9, :cond_2

    .line 102
    const-string v27, "legs"

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/json/JSONArray;

    .line 103
    .local v14, "legs":Lorg/json/JSONArray;
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v27

    if-lez v27, :cond_2

    .line 104
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x1

    move/from16 v0, v27

    invoke-virtual {v14, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 105
    .local v13, "lastLeg":Lorg/json/JSONObject;
    if-eqz v13, :cond_2

    .line 106
    const-string v27, "end_location"

    move-object/from16 v0, v27

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 107
    .local v6, "endLocation":Lorg/json/JSONObject;
    if-eqz v6, :cond_2

    .line 108
    const-string v27, "lng"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 109
    .local v8, "endLocationLng":Ljava/lang/String;
    const-string v27, "lat"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 110
    .local v7, "endLocationLat":Ljava/lang/String;
    sget-object v27, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Received endLocation from the last leg: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 111
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_2

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_2

    .line 112
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    .line 113
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    .line 121
    .end local v6    # "endLocation":Lorg/json/JSONObject;
    .end local v7    # "endLocationLat":Ljava/lang/String;
    .end local v8    # "endLocationLng":Ljava/lang/String;
    .end local v9    # "firstRoute":Lorg/json/JSONObject;
    .end local v13    # "lastLeg":Lorg/json/JSONObject;
    .end local v14    # "legs":Lorg/json/JSONArray;
    .end local v19    # "results":Lorg/json/JSONArray;
    :cond_2
    const-string v27, "lat"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3

    const-string v27, "lng"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 122
    const-string v27, "lat"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    .line 123
    const-string v27, "lng"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    .line 125
    :cond_3
    const-string v27, "opening_hours"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 126
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    .line 127
    const-string v27, "opening_hours"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    .line 128
    .local v18, "opening_hours":Lorg/json/JSONObject;
    const-string v27, "open_now"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_now:Z

    .line 129
    const-string v27, "weekday_text"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v26

    .line 130
    .local v26, "weekday_text":Lorg/json/JSONArray;
    invoke-virtual/range {v26 .. v26}, Lorg/json/JSONArray;->length()I

    move-result v27

    if-lez v27, :cond_9

    .line 131
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    invoke-virtual/range {v26 .. v26}, Lorg/json/JSONArray;->length()I

    move-result v27

    move/from16 v0, v27

    if-ge v12, v0, :cond_9

    .line 132
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 133
    .local v5, "dayOfWeek":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v27, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "day of week: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 75
    .end local v5    # "dayOfWeek":Ljava/lang/String;
    .end local v12    # "i":I
    .end local v18    # "opening_hours":Lorg/json/JSONObject;
    .end local v26    # "weekday_text":Lorg/json/JSONArray;
    :cond_5
    const-string v27, "terms"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 76
    const-string v27, "terms"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v21

    .line 78
    .local v21, "terms":Lorg/json/JSONArray;
    const/16 v27, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 79
    .local v10, "firstTerm":Lorg/json/JSONObject;
    const-string v27, "value"

    move-object/from16 v0, v27

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    .line 81
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v4, "addressParts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x1

    .restart local v12    # "i":I
    :goto_3
    invoke-virtual/range {v21 .. v21}, Lorg/json/JSONArray;->length()I

    move-result v27

    move/from16 v0, v27

    if-ge v12, v0, :cond_6

    .line 83
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v20

    .line 84
    .local v20, "term":Lorg/json/JSONObject;
    const-string v27, "value"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 86
    .end local v20    # "term":Lorg/json/JSONObject;
    :cond_6
    const-string v27, ", "

    move-object/from16 v0, v27

    invoke-static {v4, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    goto/16 :goto_0

    .line 87
    .end local v4    # "addressParts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "firstTerm":Lorg/json/JSONObject;
    .end local v12    # "i":I
    .end local v21    # "terms":Lorg/json/JSONArray;
    :cond_7
    const-string v27, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 88
    const-string v27, "description"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 98
    .restart local v19    # "results":Lorg/json/JSONArray;
    :cond_8
    const-string v27, "null"

    goto/16 :goto_1

    .line 140
    .end local v19    # "results":Lorg/json/JSONArray;
    :cond_9
    const-string v27, "formatted_address"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 141
    const-string v27, "formatted_address"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    .line 147
    :cond_a
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static/range {v27 .. v28}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 148
    const-string v27, ""

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    .line 151
    :cond_b
    const-string v27, "address_components"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 152
    .local v3, "addressComponents":Lorg/json/JSONArray;
    if-eqz v3, :cond_14

    .line 153
    const/4 v2, 0x0

    .local v2, "aci":I
    :goto_5
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v27

    move/from16 v0, v27

    if-ge v2, v0, :cond_14

    .line 155
    :try_start_0
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    .line 156
    .local v17, "obj":Lorg/json/JSONObject;
    const-string v27, "long_name"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 157
    .local v16, "longName":Ljava/lang/String;
    const-string v27, "types"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v24

    .line 158
    .local v24, "types":Lorg/json/JSONArray;
    const/16 v22, 0x0

    .local v22, "ti":I
    :goto_6
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v27

    move/from16 v0, v22

    move/from16 v1, v27

    if-ge v0, v1, :cond_c

    .line 159
    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 160
    .local v23, "type":Ljava/lang/String;
    const-string v27, "street_number"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 161
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetNumber:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    .end local v16    # "longName":Ljava/lang/String;
    .end local v17    # "obj":Lorg/json/JSONObject;
    .end local v22    # "ti":I
    .end local v23    # "type":Ljava/lang/String;
    .end local v24    # "types":Lorg/json/JSONArray;
    :cond_c
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 142
    .end local v2    # "aci":I
    .end local v3    # "addressComponents":Lorg/json/JSONArray;
    :cond_d
    const-string v27, "vicinity"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 143
    const-string v27, "vicinity"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    goto/16 :goto_4

    .line 164
    .restart local v2    # "aci":I
    .restart local v3    # "addressComponents":Lorg/json/JSONArray;
    .restart local v16    # "longName":Ljava/lang/String;
    .restart local v17    # "obj":Lorg/json/JSONObject;
    .restart local v22    # "ti":I
    .restart local v23    # "type":Ljava/lang/String;
    .restart local v24    # "types":Lorg/json/JSONArray;
    :cond_e
    :try_start_1
    const-string v27, "route"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 165
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetName:Ljava/lang/String;

    goto :goto_7

    .line 186
    .end local v16    # "longName":Ljava/lang/String;
    .end local v17    # "obj":Lorg/json/JSONObject;
    .end local v22    # "ti":I
    .end local v23    # "type":Ljava/lang/String;
    .end local v24    # "types":Lorg/json/JSONArray;
    :catch_0
    move-exception v27

    goto :goto_7

    .line 168
    .restart local v16    # "longName":Ljava/lang/String;
    .restart local v17    # "obj":Lorg/json/JSONObject;
    .restart local v22    # "ti":I
    .restart local v23    # "type":Ljava/lang/String;
    .restart local v24    # "types":Lorg/json/JSONArray;
    :cond_f
    const-string v27, "locality"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 169
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->city:Ljava/lang/String;

    goto :goto_7

    .line 172
    :cond_10
    const-string v27, "administrative_area_level_1"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 173
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->state:Ljava/lang/String;

    goto :goto_7

    .line 176
    :cond_11
    const-string v27, "country"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 177
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->country:Ljava/lang/String;

    .line 178
    const-string v27, "short_name"

    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->countryCode:Ljava/lang/String;

    goto :goto_7

    .line 181
    :cond_12
    const-string v27, "postal_code"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 182
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->zipCode:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_7

    .line 158
    :cond_13
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_6

    .line 192
    .end local v2    # "aci":I
    .end local v16    # "longName":Ljava/lang/String;
    .end local v17    # "obj":Lorg/json/JSONObject;
    .end local v22    # "ti":I
    .end local v23    # "type":Ljava/lang/String;
    .end local v24    # "types":Lorg/json/JSONArray;
    :cond_14
    const-string v27, "formatted_phone_number"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 193
    const-string v27, "formatted_phone_number"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->phone:Ljava/lang/String;

    .line 195
    :cond_15
    const-string v27, "website"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 196
    const-string v27, "website"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->url:Ljava/lang/String;

    .line 198
    :cond_16
    const-string v27, "icon"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_17

    .line 199
    const-string v27, "icon"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->icon:Ljava/lang/String;

    .line 201
    :cond_17
    const-string v27, "place_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_18

    .line 202
    const-string v27, "place_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    .line 204
    :cond_18
    const-string v27, "types"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_19

    .line 205
    const-string v27, "types"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v25

    .line 206
    .local v25, "typesJsonArray":Lorg/json/JSONArray;
    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v27

    const-string v28, ", "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    .line 208
    .end local v25    # "typesJsonArray":Lorg/json/JSONArray;
    :cond_19
    const-string v27, "price_level"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 209
    const-string v27, "price_level"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    .line 211
    :cond_1a
    const-string v27, "rating"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 212
    const-string v27, "rating"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->rating:Ljava/lang/String;

    .line 216
    :cond_1b
    const-string v27, "reviews"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 217
    const-string v27, "photos"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 218
    const-string v27, "reference"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 219
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->jsonString:Ljava/lang/String;

    .line 220
    return-void
.end method

.method public static createDestinationObject(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .locals 4
    .param p0, "jsonString"    # Ljava/lang/String;

    .prologue
    .line 51
    :try_start_0
    new-instance v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-object v1

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to parse the json string for a Google text search result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 55
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static createDestinationObject(Lorg/json/JSONObject;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .locals 4
    .param p0, "jsonObject"    # Lorg/json/JSONObject;

    .prologue
    .line 61
    :try_start_0
    new-instance v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    return-object v1

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Lorg/json/JSONException;
    sget-object v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to parse the json object for a Google text search result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getType(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "accounting"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 613
    :goto_0
    return-object v0

    .line 322
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "airport"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->AIRPORT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "amusement_park"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 328
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "aquarium"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 329
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 331
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "art_gallery"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 332
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 334
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "atm"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ATM:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 337
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bakery"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 338
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 340
    :cond_6
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bank"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 341
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 343
    :cond_7
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bar"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 344
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BAR:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto :goto_0

    .line 346
    :cond_8
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "beauty_salon"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 347
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 349
    :cond_9
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bicycle_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 350
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 352
    :cond_a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "book_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 353
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 355
    :cond_b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bowling_alley"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 356
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 358
    :cond_c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bus_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 359
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 361
    :cond_d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cafe"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 362
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->COFFEE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 364
    :cond_e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "campground"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 365
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 367
    :cond_f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "car_dealer"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 368
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 370
    :cond_10
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "car_rental"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 371
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 373
    :cond_11
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "car_repair"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 374
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 376
    :cond_12
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "car_wash"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 377
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 379
    :cond_13
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "casino"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 380
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 382
    :cond_14
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cemetery"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 383
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 385
    :cond_15
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "church"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 386
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 388
    :cond_16
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "city_hall"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 389
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 391
    :cond_17
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clothing_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 392
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 394
    :cond_18
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "convenience_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 395
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 397
    :cond_19
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "courthouse"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 398
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 400
    :cond_1a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dentist"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 401
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 403
    :cond_1b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "department_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 404
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 406
    :cond_1c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doctor"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 407
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 409
    :cond_1d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "electrician"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 410
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 412
    :cond_1e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "electronics_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 413
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 415
    :cond_1f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "embassy"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 416
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 418
    :cond_20
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "establishment"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 419
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 421
    :cond_21
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "finance"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 422
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->BANK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 424
    :cond_22
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fire_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 425
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 427
    :cond_23
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "florist"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 428
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 430
    :cond_24
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "food"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 431
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 433
    :cond_25
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "funeral_home"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 434
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 436
    :cond_26
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "furniture_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 437
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 439
    :cond_27
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gas_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 440
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GAS_STATION:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 442
    :cond_28
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "general_contractor"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 443
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 445
    :cond_29
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "grocery_or_supermarket"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 446
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 448
    :cond_2a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gym"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 449
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->GYM:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 451
    :cond_2b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hair_care"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 452
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 454
    :cond_2c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hardware_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 455
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 457
    :cond_2d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "health"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 458
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 460
    :cond_2e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hindu_temple"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 461
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 463
    :cond_2f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "home_goods_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 464
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 466
    :cond_30
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hospital"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 467
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 469
    :cond_31
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "insurance_agency"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 470
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 472
    :cond_32
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "jewelry_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 473
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 475
    :cond_33
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "laundry"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 476
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 478
    :cond_34
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "lawyer"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 479
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 481
    :cond_35
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "library"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 482
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 484
    :cond_36
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "liquor_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 485
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 487
    :cond_37
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "local_government_office"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 488
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 490
    :cond_38
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "locksmith"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 491
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 493
    :cond_39
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "lodging"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 494
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 496
    :cond_3a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "meal_delivery"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 497
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 499
    :cond_3b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "meal_takeaway"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 500
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 502
    :cond_3c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mosque"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 503
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 505
    :cond_3d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "movie_rental"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 506
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 508
    :cond_3e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "movie_theater"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 509
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 511
    :cond_3f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "moving_company"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 512
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 514
    :cond_40
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "museum"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 515
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 517
    :cond_41
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "night_club"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 518
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 520
    :cond_42
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "painter"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 521
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 523
    :cond_43
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "park"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 524
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARK:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 526
    :cond_44
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "parking"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 527
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PARKING:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 529
    :cond_45
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pet_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 530
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 532
    :cond_46
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pharmacy"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 533
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 535
    :cond_47
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "physiotherapist"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 536
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->HOSPITAL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 538
    :cond_48
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "place_of_worship"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 539
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 541
    :cond_49
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "plumber"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 542
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 544
    :cond_4a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "police"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 545
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 547
    :cond_4b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "post_office"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 548
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 550
    :cond_4c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "real_estate_agency"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 551
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 553
    :cond_4d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "restaurant"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 554
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->RESTAURANT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 556
    :cond_4e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "roofing_contractor"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 557
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 559
    :cond_4f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rv_park"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 560
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 562
    :cond_50
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "school"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 563
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 565
    :cond_51
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "shoe_store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 566
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 568
    :cond_52
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "shopping_mall"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 569
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 571
    :cond_53
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "spa"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 572
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 574
    :cond_54
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "stadium"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 575
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 577
    :cond_55
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "street_address"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 578
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 580
    :cond_56
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "storage"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 581
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 583
    :cond_57
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "store"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 584
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 586
    :cond_58
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "subway_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 587
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 589
    :cond_59
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "synagogue"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 590
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 592
    :cond_5a
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "taxi_stand"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 593
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 595
    :cond_5b
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "train_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 596
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 598
    :cond_5c
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "transit_station"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 599
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->TRANSIT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 601
    :cond_5d
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "travel_agency"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 602
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->STORE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 604
    :cond_5e
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "university"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 605
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->SCHOOL:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 607
    :cond_5f
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "veterinary_care"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 608
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 610
    :cond_60
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zoo"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 611
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->ENTERTAINMENT:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0

    .line 613
    :cond_61
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    goto/16 :goto_0
.end method

.method private getType([Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 5
    .param p1, "types"    # [Ljava/lang/String;

    .prologue
    .line 276
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 278
    .local v1, "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    if-eqz p1, :cond_0

    array-length v4, p1

    if-gtz v4, :cond_1

    .line 290
    .end local v1    # "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    :cond_0
    :goto_0
    return-object v1

    .line 282
    .restart local v1    # "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    :cond_1
    const/4 v4, 0x0

    aget-object v3, p1, v4

    .line 284
    .local v3, "typesString":Ljava/lang/String;
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "array":Lorg/json/JSONArray;
    invoke-static {v0}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->parseTypes(Lorg/json/JSONArray;)Lcom/navdy/client/app/framework/models/Destination$Type;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 286
    .end local v0    # "array":Lorg/json/JSONArray;
    :catch_0
    move-exception v2

    .line 287
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public static parseTypes(Lorg/json/JSONArray;)Lcom/navdy/client/app/framework/models/Destination$Type;
    .locals 5
    .param p0, "array"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 294
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 296
    .local v0, "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-gtz v4, :cond_1

    move-object v2, v0

    .line 311
    :cond_0
    :goto_0
    return-object v2

    .line 300
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 301
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 302
    .local v3, "type":Ljava/lang/String;
    invoke-static {v3}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->getType(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v2

    .line 303
    .local v2, "tempType":Lcom/navdy/client/app/framework/models/Destination$Type;
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-eq v2, v4, :cond_2

    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$Type;->PLACE:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v2, v4, :cond_0

    .line 306
    :cond_2
    sget-object v4, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    if-ne v0, v4, :cond_3

    .line 307
    move-object v0, v2

    .line 300
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v2    # "tempType":Lcom/navdy/client/app/framework/models/Destination$Type;
    .end local v3    # "type":Ljava/lang/String;
    :cond_4
    move-object v2, v0

    .line 311
    goto :goto_0
.end method


# virtual methods
.method public toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 8
    .param p1, "searchResultType"    # Lcom/navdy/client/app/framework/models/Destination$SearchType;
    .param p2, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const-wide/16 v6, 0x0

    .line 248
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 249
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 250
    .local v0, "newDisplayLat":D
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 252
    .local v2, "newDisplayLong":D
    iget-wide v4, p2, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    cmpl-double v4, v0, v4

    if-nez v4, :cond_0

    iget-wide v4, p2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    cmpl-double v4, v2, v4

    if-eqz v4, :cond_1

    .line 253
    :cond_0
    iput-wide v6, p2, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 254
    iput-wide v6, p2, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 256
    :cond_1
    iput-wide v0, p2, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 257
    iput-wide v2, p2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .line 258
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 259
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetNumber:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 260
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetName:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 261
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->city:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->city:Ljava/lang/String;

    .line 262
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->state:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->state:Ljava/lang/String;

    .line 263
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->zipCode:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->zipCode:Ljava/lang/String;

    .line 264
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->country:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->country:Ljava/lang/String;

    .line 265
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->countryCode:Ljava/lang/String;

    invoke-virtual {p2, v4}, Lcom/navdy/client/app/framework/models/Destination;->setCountryCode(Ljava/lang/String;)V

    .line 266
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 267
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v4

    iput v4, p2, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 268
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->jsonString:Ljava/lang/String;

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    .line 272
    iget-object v4, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->getType([Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v4

    iput-object v4, p2, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 273
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 224
    const-string v0, "Name: %s,\tLat: %s,\tLng: %s,\tAddress: %s,\tstreetNumber: %s,\tstreetName: %s,\tcity: %s,\tstate: %s,\tzipCode: %s,\tcountry: %s (%s),\tSuggestionType: %s,\tRating: %s,\tPrice Level: %s,\tPlace Id: %s,\tOpen Hours: %s,\tOpen Now: %s,\tPhone: %s,\tUrl: %s"

    const/16 v1, 0x13

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetNumber:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->streetName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->city:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->state:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->zipCode:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->country:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->countryCode:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    .line 237
    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->rating:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_now:Z

    .line 242
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x11

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->phone:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->url:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 224
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
