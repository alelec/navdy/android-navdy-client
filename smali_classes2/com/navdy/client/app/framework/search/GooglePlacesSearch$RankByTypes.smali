.class final enum Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;
.super Ljava/lang/Enum;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RankByTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

.field public static final enum DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

.field public static final enum PROMINENCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 134
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    const-string v1, "PROMINENCE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->PROMINENCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    .line 135
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    .line 133
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->PROMINENCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    .prologue
    .line 138
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$RankByTypes:[I

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 143
    const-string v0, "distance"

    :goto_0
    return-object v0

    .line 140
    :pswitch_0
    const-string v0, "prominence"

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    const-class v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    return-object v0
.end method
