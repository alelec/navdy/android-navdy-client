.class Lcom/navdy/client/app/ui/glances/GlancesFragment$2;
.super Ljava/lang/Object;
.source "GlancesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/glances/GlancesFragment;->getGlanceSwitchClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

.field final synthetic val$pkg:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;->val$pkg:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;->val$pkg:Ljava/lang/String;

    check-cast p1, Landroid/widget/Switch;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 198
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;->val$pkg:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isDrivingGlance(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$2;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$100(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    .line 202
    :cond_0
    return-void
.end method
