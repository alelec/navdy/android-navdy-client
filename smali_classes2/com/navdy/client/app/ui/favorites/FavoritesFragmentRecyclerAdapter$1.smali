.class Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;
.super Ljava/lang/Object;
.source "FavoritesFragmentRecyclerAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->setupDynamicShortcuts()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 165
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x19

    if-ge v8, v9, :cond_0

    .line 204
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v8, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-static {v8}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;

    move-result-object v8

    const-class v9, Landroid/content/pm/ShortcutManager;

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ShortcutManager;

    .line 171
    .local v6, "shortcutManager":Landroid/content/pm/ShortcutManager;
    invoke-static {}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "favoritesAdapter.getItemCount() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v10}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItemCount()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 173
    iget-object v8, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v8}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItemCount()I

    move-result v4

    .line 174
    .local v4, "itemCount":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 176
    .local v7, "shortcuts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ShortcutInfo;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "max shortcut count is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$200()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 177
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    invoke-static {}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$200()I

    move-result v8

    if-ge v2, v8, :cond_3

    .line 178
    iget-object v8, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v8, v2}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->getItem(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 179
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v0, :cond_1

    .line 177
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 183
    :cond_1
    new-instance v3, Landroid/content/Intent;

    iget-object v8, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-static {v8}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$300(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {v3, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "extra_destination"

    iget v9, v0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 185
    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "favoriteLabel":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 189
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v8

    iget-object v1, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v1    # "favoriteLabel":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 191
    .restart local v1    # "favoriteLabel":Ljava/lang/String;
    :cond_2
    new-instance v8, Landroid/content/pm/ShortcutInfo$Builder;

    iget-object v9, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-static {v9}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$600(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;

    move-result-object v9

    iget v10, v0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/pm/ShortcutInfo$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 192
    invoke-virtual {v8, v1}, Landroid/content/pm/ShortcutInfo$Builder;->setShortLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .line 193
    invoke-static {v9}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$500(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0802e5

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v1, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/pm/ShortcutInfo$Builder;->setLongLabel(Ljava/lang/CharSequence;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    .line 194
    invoke-static {v9}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$400(Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v10

    invoke-static {v9, v10}, Landroid/graphics/drawable/Icon;->createWithResource(Landroid/content/Context;I)Landroid/graphics/drawable/Icon;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/content/pm/ShortcutInfo$Builder;->setIcon(Landroid/graphics/drawable/Icon;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v8

    .line 195
    invoke-virtual {v8, v3}, Landroid/content/pm/ShortcutInfo$Builder;->setIntent(Landroid/content/Intent;)Landroid/content/pm/ShortcutInfo$Builder;

    move-result-object v8

    .line 196
    invoke-virtual {v8}, Landroid/content/pm/ShortcutInfo$Builder;->build()Landroid/content/pm/ShortcutInfo;

    move-result-object v5

    .line 198
    .local v5, "shortcut":Landroid/content/pm/ShortcutInfo;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 201
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v1    # "favoriteLabel":Ljava/lang/String;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "shortcut":Landroid/content/pm/ShortcutInfo;
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "shortcuts.size() = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 203
    invoke-virtual {v6, v7}, Landroid/content/pm/ShortcutManager;->setDynamicShortcuts(Ljava/util/List;)Z

    goto/16 :goto_0
.end method
