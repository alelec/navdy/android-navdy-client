.class Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;
.super Ljava/lang/Object;
.source "ProgressCircle.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/customviews/ProgressCircle;->startAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/customviews/ProgressCircle;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/customviews/ProgressCircle;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;->this$0:Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "valueAnimator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;->this$0:Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    invoke-static {v0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->access$000(Lcom/navdy/client/app/ui/customviews/ProgressCircle;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animationupdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;->this$0:Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->access$102(Lcom/navdy/client/app/ui/customviews/ProgressCircle;F)F

    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;->this$0:Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->invalidate()V

    .line 82
    return-void
.end method
