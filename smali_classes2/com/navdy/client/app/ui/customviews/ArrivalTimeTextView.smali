.class public Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;
.super Landroid/widget/TextView;
.source "ArrivalTimeTextView.java"


# static fields
.field private static final UPDATE_INTERVAL:J = 0xea60L


# instance fields
.field private currentTimeToArrival:I

.field private final updateTime:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView$1;-><init>(Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->updateTime:Ljava/lang/Runnable;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

    .prologue
    .line 14
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->currentTimeToArrival:I

    return v0
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->updateTime:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 49
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 50
    return-void
.end method

.method public setTimeToArrival(I)V
    .locals 1
    .param p1, "timeToArrival"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->updateTime:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 42
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->currentTimeToArrival:I

    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->updateTime:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->post(Ljava/lang/Runnable;)Z

    .line 44
    return-void
.end method
