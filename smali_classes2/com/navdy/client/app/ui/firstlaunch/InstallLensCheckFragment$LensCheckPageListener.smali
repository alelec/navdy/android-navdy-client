.class Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;
.super Ljava/lang/Object;
.source "InstallLensCheckFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LensCheckPageListener"
.end annotation


# instance fields
.field final dots:[Landroid/view/View;

.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;)V
    .locals 4

    .prologue
    .line 114
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1001c9

    .line 116
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1001ce

    .line 117
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1001d3

    .line 118
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->dots:[Landroid/view/View;

    .line 115
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;
    .param p2, "x1"    # Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$1;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;-><init>(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;)V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 134
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 122
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->dots:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 127
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->dots:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;->dots:[Landroid/view/View;

    aget-object v2, v1, v0

    if-ne p1, v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_1
    const/4 v1, 0x4

    goto :goto_1

    .line 131
    :cond_2
    return-void
.end method
