.class Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;
.super Ljava/lang/Object;
.source "CarMdUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ObdLocation"
.end annotation


# instance fields
.field accessNote:Ljava/lang/String;

.field public location:I

.field public note:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "location"    # I
    .param p2, "accessNote"    # Ljava/lang/String;
    .param p3, "note"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    sget v0, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;->DEFAULT_OBD_LOCATION:I

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->location:I

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->accessNote:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->note:Ljava/lang/String;

    .line 58
    iput p1, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->location:I

    .line 59
    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->accessNote:Ljava/lang/String;

    .line 60
    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/CarMdUtils$ObdLocation;->note:Ljava/lang/String;

    .line 61
    return-void
.end method
