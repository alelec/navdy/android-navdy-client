.class Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;
.super Ljava/lang/Object;
.source "AppSetupPagerAdapter.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->updateIllustration(Ljava/lang/ref/WeakReference;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

.field final synthetic val$fadeIn:Landroid/view/animation/Animation;

.field final synthetic val$hud:Landroid/widget/ImageView;

.field final synthetic val$hudRes:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;Landroid/widget/ImageView;ILandroid/view/animation/Animation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$hud:Landroid/widget/ImageView;

    iput p3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$hudRes:I

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$fadeIn:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$hud:Landroid/widget/ImageView;

    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$hudRes:I

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->access$000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;)Lcom/navdy/client/app/framework/util/ImageCache;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 288
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$hud:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;->val$fadeIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 289
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 292
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 283
    return-void
.end method
