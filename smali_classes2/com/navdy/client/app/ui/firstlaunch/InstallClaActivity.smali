.class public Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "InstallClaActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->finish()V

    .line 43
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f03006c

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->setContentView(I)V

    .line 24
    const v0, 0x7f1000b1

    const v1, 0x7f0201d8

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->loadImage(II)V

    .line 25
    return-void
.end method

.method public onNextClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_step"

    const v2, 0x7f030077

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->startActivity(Landroid/content/Intent;)V

    .line 39
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 30
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;->hideSystemUI()V

    .line 32
    const-string v0, "Installation_Plug_CLA"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 33
    return-void
.end method
