.class public Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "InstallCompleteActivity.java"


# static fields
.field public static final EXTRA_COMING_FROM_SETTINGS:Ljava/lang/String; = "comming_from_settings"


# instance fields
.field private comingFromSettings:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->comingFromSettings:Z

    return-void
.end method

.method private startInstallActivityAtThisStep(I)V
    .locals 6
    .param p1, "step"    # I

    .prologue
    const/4 v5, 0x1

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 124
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->getVideoUrlForStep(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "videoUrl":Ljava/lang/String;
    const-string v3, "Video_URL"

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v3, "Video_Tapped_On_Install_Complete"

    invoke-static {v3, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 128
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "extra_step"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 130
    iget-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->comingFromSettings:Z

    if-eqz v3, :cond_0

    .line 131
    const-string v3, "comming_from_settings"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 133
    :cond_0
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startActivity(Landroid/content/Intent;)V

    .line 134
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->finish()V

    .line 135
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x8

    .line 37
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v3, 0x7f03006d

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->setContentView(I)V

    .line 40
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v4, 0x7f08025c

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 42
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 44
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "comming_from_settings"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->comingFromSettings:Z

    .line 45
    iget-boolean v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->comingFromSettings:Z

    if-eqz v3, :cond_1

    .line 49
    const v3, 0x7f100197

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 50
    .local v0, "bottomBar":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 53
    :cond_0
    const v3, 0x7f100198

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 54
    .local v1, "bottomBarDivider":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 55
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 59
    .end local v0    # "bottomBar":Landroid/widget/RelativeLayout;
    .end local v1    # "bottomBarDivider":Landroid/view/View;
    :cond_1
    const v3, 0x7f1001a0

    const v4, 0x7f0201dd

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 60
    const v3, 0x7f1001a3

    const v4, 0x7f0201ed

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 61
    const v3, 0x7f1001a7

    const v4, 0x7f0201f2

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 62
    const v3, 0x7f1001aa

    const v4, 0x7f0201ee

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 63
    const v3, 0x7f1001ad

    const v4, 0x7f0201ec

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 64
    const v3, 0x7f1001b0

    const v4, 0x7f0201f0

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 65
    const v3, 0x7f1001b3

    const v4, 0x7f0201f3

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 66
    const v3, 0x7f1001b6

    const v4, 0x7f0201eb

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 67
    const v3, 0x7f1001b9

    const v4, 0x7f0201f1

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->loadImage(II)V

    .line 68
    return-void
.end method

.method public onInstallMediumOrTallMountClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 94
    const v0, 0x7f030072

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 95
    return-void
.end method

.method public onInstallShortMountClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 90
    const v0, 0x7f030076

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 91
    return-void
.end method

.method public onInstallingDialClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 119
    const v0, 0x7f03006e

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 120
    return-void
.end method

.method public onLensCheckClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 98
    const v0, 0x7f030070

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 99
    return-void
.end method

.method public onLookDownClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 155
    const v1, 0x7f100199

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 156
    .local v0, "sv":Landroid/widget/ScrollView;
    if-eqz v0, :cond_0

    .line 157
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollBy(II)V

    .line 159
    :cond_0
    return-void
.end method

.method public onNextClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 138
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 139
    .local v1, "settingsPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "finished_install"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 141
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->comingFromSettings:Z

    if-eqz v2, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->finish()V

    .line 152
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->weHaveCarInfo()Z

    move-result v2

    if-nez v2, :cond_1

    .line 145
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_next_step"

    const-string v3, "app_setup"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 149
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-static {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->goToAppSetup(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onOverviewClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 86
    const v0, 0x7f030074

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 87
    return-void
.end method

.method public onPickCableClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startActivity(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 72
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 74
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "box"

    const-string v4, "Old_Box"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "box":Ljava/lang/String;
    const-string v2, "New_Box"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    const v2, 0x7f1001a6

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 77
    .local v1, "card":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 78
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 82
    .end local v1    # "card":Landroid/view/View;
    :cond_0
    const-string v2, "Installation_Finished"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public onSecuringMountClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 102
    const v0, 0x7f030075

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 103
    return-void
.end method

.method public onTidyingUpClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 111
    const v0, 0x7f030077

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 112
    return-void
.end method

.method public onTurnOnClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 115
    const v0, 0x7f030078

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;->startInstallActivityAtThisStep(I)V

    .line 116
    return-void
.end method
