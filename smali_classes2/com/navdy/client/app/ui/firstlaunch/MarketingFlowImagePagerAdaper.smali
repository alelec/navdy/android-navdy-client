.class public Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;
.super Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;
.source "MarketingFlowImagePagerAdaper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "images"    # [I
    .param p3, "imageCache"    # Lcom/navdy/client/app/framework/util/ImageCache;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 19
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;->imageResourceIds:[I

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 28
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03007c

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 29
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 40
    .end local v0    # "imageView":Landroid/widget/ImageView;
    :goto_0
    return-object v0

    .line 33
    .restart local v0    # "imageView":Landroid/widget/ImageView;
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;->imageResourceIds:[I

    array-length v1, v1

    if-lt p2, v1, :cond_1

    .line 34
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 39
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 36
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowImagePagerAdaper;->imageResourceIds:[I

    aget v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
