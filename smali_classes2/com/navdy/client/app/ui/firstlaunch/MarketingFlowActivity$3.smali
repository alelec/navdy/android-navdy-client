.class Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;
.super Ljava/lang/Object;
.source "MarketingFlowActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field gotHandY:Z

.field handPivotY:F

.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;

.field final synthetic val$bgPager:Landroid/support/v4/view/ViewPager;

.field final synthetic val$buyOne:Landroid/widget/TextView;

.field final synthetic val$hand:Landroid/widget/ImageView;

.field final synthetic val$hudUiPager:Landroid/support/v4/view/ViewPager;

.field final synthetic val$logo:Landroid/widget/ImageView;

.field final synthetic val$pagination:Landroid/widget/ImageView;

.field final synthetic val$start:Landroid/widget/Button;

.field final synthetic val$textPager:Landroid/support/v4/view/ViewPager;

.field final synthetic val$totalTranslationY:F


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;Landroid/widget/ImageView;Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/ViewPager;FLandroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/Button;Landroid/support/v4/view/ViewPager;Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$bgPager:Landroid/support/v4/view/ViewPager;

    iput p5, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$totalTranslationY:F

    iput-object p6, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$logo:Landroid/widget/ImageView;

    iput-object p7, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$buyOne:Landroid/widget/TextView;

    iput-object p8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$start:Landroid/widget/Button;

    iput-object p9, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hudUiPager:Landroid/support/v4/view/ViewPager;

    iput-object p10, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$pagination:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->gotHandY:Z

    .line 142
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->handPivotY:F

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 217
    if-nez p1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 219
    .local v0, "position":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$bgPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 221
    .end local v0    # "position":I
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 12
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    const/high16 v11, 0x42c80000    # 100.0f

    .line 146
    iget-boolean v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->gotHandY:Z

    if-nez v8, :cond_0

    .line 147
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->gotHandY:Z

    .line 148
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getPivotY()F

    move-result v8

    iput v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->handPivotY:F

    .line 150
    :cond_0
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    iget v9, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->handPivotY:F

    iget-object v10, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 152
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8}, Landroid/support/v4/view/ViewPager;->getScrollX()I

    move-result v4

    .line 155
    .local v4, "scrollX":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$bgPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8, v4}, Landroid/support/v4/view/ViewPager;->setScrollX(I)V

    .line 158
    const/4 v8, 0x2

    if-lt p1, v8, :cond_1

    const/4 v8, 0x4

    if-gt p1, v8, :cond_1

    .line 159
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v8

    mul-int/lit8 v8, v8, 0x3

    int-to-float v2, v8

    .line 160
    .local v2, "scrollWidthUntilHand":F
    int-to-float v8, v4

    sub-float/2addr v8, v2

    mul-float/2addr v8, v11

    iget-object v9, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v0, v8, v9

    .line 161
    .local v0, "percentage":F
    const/high16 v8, -0x3d4c0000    # -90.0f

    mul-float/2addr v8, v0

    div-float v1, v8, v11

    .line 162
    .local v1, "rotation":F
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 166
    .end local v0    # "percentage":F
    .end local v1    # "rotation":F
    .end local v2    # "scrollWidthUntilHand":F
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->access$000()[I

    move-result-object v8

    array-length v8, v8

    add-int/lit8 v8, v8, -0x2

    if-lt p1, v8, :cond_2

    .line 167
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v8

    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->access$000()[I

    move-result-object v9

    array-length v9, v9

    add-int/lit8 v9, v9, -0x2

    mul-int/2addr v8, v9

    int-to-float v3, v8

    .line 168
    .local v3, "scrollWidthUntilLogo":F
    int-to-float v8, v4

    sub-float/2addr v8, v3

    mul-float/2addr v8, v11

    iget-object v9, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$textPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v9}, Landroid/support/v4/view/ViewPager;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v0, v8, v9

    .line 169
    .restart local v0    # "percentage":F
    iget v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$totalTranslationY:F

    mul-float/2addr v8, v0

    div-float v6, v8, v11

    .line 170
    .local v6, "translation":F
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$logo:Landroid/widget/ImageView;

    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 171
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$buyOne:Landroid/widget/TextView;

    iget v9, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$totalTranslationY:F

    neg-float v9, v9

    add-float/2addr v9, v6

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 172
    const/high16 v8, 0x42a00000    # 80.0f

    invoke-static {v8}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v8

    int-to-float v5, v8

    .line 173
    .local v5, "totalTranslationY2":F
    mul-float v8, v0, v5

    const/high16 v9, -0x3d380000    # -100.0f

    div-float v7, v8, v9

    .line 174
    .local v7, "translation2":F
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$start:Landroid/widget/Button;

    invoke-virtual {v8, v7}, Landroid/widget/Button;->setTranslationY(F)V

    .line 176
    .end local v0    # "percentage":F
    .end local v3    # "scrollWidthUntilLogo":F
    .end local v5    # "totalTranslationY2":F
    .end local v6    # "translation":F
    .end local v7    # "translation2":F
    :cond_2
    return-void
.end method

.method public onPageSelected(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hudUiPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 181
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$pagination:Landroid/widget/ImageView;

    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->access$100()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;->access$000()[I

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v0, v0, -0x2

    if-lt p1, v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$start:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 195
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 198
    const-string v0, "Meet_Navdy"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 213
    :goto_2
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$hand:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity$3;->val$start:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 201
    :pswitch_0
    const-string v0, "Never_Miss_A_Turn"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_2

    .line 204
    :pswitch_1
    const-string v0, "Stay_Connected"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_2

    .line 207
    :pswitch_2
    const-string v0, "Effortless_Control"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_2

    .line 210
    :pswitch_3
    const-string v0, "Get_Started"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_2

    .line 195
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
