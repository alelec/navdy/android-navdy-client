.class public Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "AppSetupProfileFragment.java"


# instance fields
.field private customerPrefs:Landroid/content/SharedPreferences;

.field private emailInput:Landroid/widget/EditText;

.field private nameInput:Landroid/widget/EditText;

.field private rootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getNameText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRootView()Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 32
    const v2, 0x7f030066

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    .line 34
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->customerPrefs:Landroid/content/SharedPreferences;

    .line 37
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    const v3, 0x7f10016f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    .line 38
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    const v3, 0x7f100171

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->emailInput:Landroid/widget/EditText;

    .line 40
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 41
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v2, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 42
    check-cast v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .line 43
    .local v1, "appSetupActivity":Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;
    invoke-virtual {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->doProfileOnCreate(Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;)V

    .line 46
    .end local v1    # "appSetupActivity":Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->emailInput:Landroid/widget/EditText;

    if-nez v2, :cond_2

    .line 48
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    .line 50
    :goto_0
    return-object v2

    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->rootView:Landroid/view/View;

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 59
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onResume()V

    .line 61
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v3, "fname"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "fullName":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v3, "email"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "email":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->emailInput:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    .line 69
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :cond_1
    return-void
.end method
