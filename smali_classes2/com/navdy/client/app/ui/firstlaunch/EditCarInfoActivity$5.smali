.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$5;
.super Ljava/lang/Object;
.source "EditCarInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$5;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 437
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isUsingDefaultObdScanSetting()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setDefaultObdSettingDependingOnBlacklistAsync()V

    .line 441
    :cond_0
    return-void
.end method
