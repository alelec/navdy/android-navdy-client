.class Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;
.super Ljava/lang/Object;
.source "InstallActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 91
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 76
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 80
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->access$000(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 82
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->access$100(Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;)Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getScreenAtPosition(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    .local v0, "screen":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 86
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "trigger system gc"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 87
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 88
    return-void
.end method
