.class final Lcom/navdy/client/app/ui/settings/SettingsUtils$5;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/SettingsUtils;->setDefaultObdSettingDependingOnBlacklistAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 991
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 993
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v0

    .line 994
    .local v0, "obdScanSettingString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->carIsBlacklistedForObdData()Z

    move-result v2

    if-nez v2, :cond_0

    .line 995
    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v0

    .line 997
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OBD Setting changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1000
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "hud_obd_on_enabled"

    .line 1001
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1002
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1003
    const-string v2, "Profile_Settings_Changed"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 1004
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementDriverProfileSerial()V

    .line 1005
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    .line 1006
    return-void
.end method
