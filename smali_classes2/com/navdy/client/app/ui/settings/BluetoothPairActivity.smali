.class public Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "BluetoothPairActivity.java"


# static fields
.field public static final bluetoothDialogTag:Ljava/lang/String; = "BLUETOOTH_DIALOG_FRAGMENT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "::onCreate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 21
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 25
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    new-instance v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;-><init>()V

    .line 26
    .local v0, "bluetoothFramelayoutFragment":Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "BLUETOOTH_DIALOG_FRAGMENT"

    invoke-virtual {v2, v0, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 27
    new-instance v2, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 36
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 42
    const-string v0, "Settings_Bluetooth_Pairing"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 43
    return-void
.end method
