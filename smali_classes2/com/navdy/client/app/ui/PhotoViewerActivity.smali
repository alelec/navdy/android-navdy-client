.class public Lcom/navdy/client/app/ui/PhotoViewerActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "PhotoViewerActivity.java"


# static fields
.field public static final EXTRA_FILE_PATH:Ljava/lang/String; = "EXTRA_FILE_PATH"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v4, 0x7f0300cf

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 29
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "EXTRA_FILE_PATH"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "filePath":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 31
    iget-object v4, p0, Lcom/navdy/client/app/ui/PhotoViewerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Image path not specified !"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->finish()V

    .line 51
    :goto_0
    return-void

    .line 36
    :cond_0
    const v4, 0x7f10007b

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 37
    .local v2, "image":Landroid/widget/ImageView;
    if-nez v2, :cond_1

    .line 38
    iget-object v4, p0, Lcom/navdy/client/app/ui/PhotoViewerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Image layout element not found !"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->finish()V

    goto :goto_0

    .line 43
    :cond_1
    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->getBitmapFromInternalStorage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 44
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 45
    iget-object v4, p0, Lcom/navdy/client/app/ui/PhotoViewerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to get a bitmap from the filePath: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " !"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 46
    const v4, 0x7f0201e7

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onDoneClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->finish()V

    .line 61
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 56
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/PhotoViewerActivity;->hideSystemUI()V

    .line 57
    return-void
.end method
