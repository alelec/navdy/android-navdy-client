.class Lcom/navdy/client/app/ui/search/SearchActivity$1;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 179
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$000(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 181
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 182
    .local v0, "context":Landroid/content/Context;
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 183
    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    sget-object v2, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$200(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/location/FusedLocationProviderApi;->getLastLocation(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/location/Location;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$102(Lcom/navdy/client/app/ui/search/SearchActivity;Landroid/location/Location;)Landroid/location/Location;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$300(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 188
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$1;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 189
    return-void
.end method
