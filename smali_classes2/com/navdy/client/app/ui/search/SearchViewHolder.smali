.class Lcom/navdy/client/app/ui/search/SearchViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SearchViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;
    }
.end annotation


# static fields
.field public static final DIVIDER_HEIGHT:I


# instance fields
.field protected bottomItemDivider:Landroid/view/View;

.field private details:Landroid/widget/TextView;

.field private distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

.field private image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

.field protected itemView:Landroid/view/View;

.field protected navButton:Landroid/widget/ImageButton;

.field private price:Landroid/widget/TextView;

.field protected row:Landroid/view/View;

.field private title:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->DIVIDER_HEIGHT:I

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 47
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->itemView:Landroid/view/View;

    .line 49
    const v0, 0x7f100360

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    if-nez v0, :cond_0

    .line 51
    const v0, 0x7f10035c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    .line 53
    :cond_0
    const v0, 0x7f100363

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->title:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f100364

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f100365

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    .line 56
    const v0, 0x7f100366

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->price:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f100361

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    .line 58
    const v0, 0x7f1002f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    .line 59
    const v0, 0x7f10035f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    .line 60
    return-void
.end method

.method private changeDividerAlignment(Z)V
    .locals 4
    .param p1, "showShort"    # Z

    .prologue
    const v3, 0x7f100362

    .line 143
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 144
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    sget v2, Lcom/navdy/client/app/ui/search/SearchViewHolder;->DIVIDER_HEIGHT:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 147
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_0

    .line 148
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 149
    const/16 v1, 0x12

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 151
    :cond_0
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 152
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 155
    .end local v0    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    return-void
.end method


# virtual methods
.method hideBottomDivider()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->bottomItemDivider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_0
    return-void
.end method

.method hideNavButton()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 121
    return-void
.end method

.method public setDetails(Landroid/text/Spannable;)V
    .locals 2
    .param p1, "text"    # Landroid/text/Spannable;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDetails(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 82
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setDetails(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "colorResource"    # I

    .prologue
    .line 76
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->details:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setDistance(D)V
    .locals 5
    .param p1, "distanceInMeters"    # D

    .prologue
    .line 92
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    const-wide/16 v2, 0x0

    cmpg-double v0, p1, v2

    if-gtz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 94
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->clearInitials()V

    .line 108
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(Landroid/graphics/Bitmap;)V

    .line 109
    return-void
.end method

.method public setImage(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;Z)V
    .locals 2
    .param p1, "searchItem"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    .param p2, "isAutocomplete"    # Z

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->clearInitials()V

    .line 103
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {p1, p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->getRes(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImageResource(I)V

    .line 104
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->image:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method setPrice(Ljava/lang/String;)V
    .locals 2
    .param p1, "priceString"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->price:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->price:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Landroid/text/Spannable;)V
    .locals 1
    .param p1, "text"    # Landroid/text/Spannable;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public setTitle(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "colorResource"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->title:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    .end local p1    # "text":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->title:Landroid/widget/TextView;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    return-void
.end method

.method showDeleteButton()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    const v1, 0x7f02014f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 126
    return-void
.end method

.method showLongDivider()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->changeDividerAlignment(Z)V

    .line 140
    return-void
.end method

.method showNavButton()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 117
    return-void
.end method

.method showShortDivider()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->changeDividerAlignment(Z)V

    .line 136
    return-void
.end method
