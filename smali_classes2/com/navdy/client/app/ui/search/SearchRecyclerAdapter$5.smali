.class Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;
.super Ljava/lang/Object;
.source "SearchRecyclerAdapter.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addMarker(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

.field final synthetic val$destinationIndex:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iput p2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->val$destinationIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 10
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 1022
    iget-object v5, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iget v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->val$destinationIndex:I

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDestinationAt(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 1024
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v0, :cond_0

    .line 1025
    invoke-static {}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to retrieve a destination from position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->val$destinationIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1051
    :goto_0
    return-void

    .line 1029
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1030
    invoke-static {}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to retrieve valid coordinates for the destination at position: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->val$destinationIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1034
    :cond_1
    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 1036
    .local v4, "mo":Lcom/google/android/gms/maps/model/MarkerOptions;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1037
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v8, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1042
    .local v2, "latlong":Lcom/google/android/gms/maps/model/LatLng;
    :goto_1
    invoke-virtual {v4, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1044
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->draggable(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1045
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getUnselectedPinAsset()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v1

    .line 1046
    .local v1, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    invoke-virtual {v4, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1049
    invoke-virtual {p1, v4}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v3

    .line 1050
    .local v3, "marker":Lcom/google/android/gms/maps/model/Marker;
    iget-object v5, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-static {v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$300(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->getId()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;->val$destinationIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1039
    .end local v1    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .end local v2    # "latlong":Lcom/google/android/gms/maps/model/LatLng;
    .end local v3    # "marker":Lcom/google/android/gms/maps/model/Marker;
    :cond_2
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v8, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .restart local v2    # "latlong":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_1
.end method
