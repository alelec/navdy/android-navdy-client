.class public final enum Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;
.super Ljava/lang/Enum;
.source "SearchViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PhotoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

.field public static final enum CONTACT:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

.field public static final enum RESOURCE_IMAGE:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    const-string v1, "RESOURCE_IMAGE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->RESOURCE_IMAGE:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    .line 42
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->CONTACT:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->RESOURCE_IMAGE:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->CONTACT:Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/search/SearchViewHolder$PhotoType;

    return-object v0
.end method
