.class Lcom/navdy/client/app/ui/search/SearchActivity$18;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 1084
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 3
    .param p1, "ids"    # [Ljava/lang/Integer;

    .prologue
    .line 1094
    if-eqz p1, :cond_0

    array-length v1, p1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1095
    :cond_0
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1099
    :goto_0
    return-object v1

    .line 1097
    :cond_1
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->removeFromSearchHistory(I)I

    move-result v0

    .line 1098
    .local v0, "id":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1000(Lcom/navdy/client/app/ui/search/SearchActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setUpServicesAndHistory(Z)V

    .line 1099
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1084
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$18;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "nbRows"    # Ljava/lang/Integer;

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 1106
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    .line 1107
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 1108
    const v0, 0x7f0804d8

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 1110
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1084
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$18;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1087
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1088
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->showProgressDialog()V

    .line 1089
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$18;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 1090
    return-void
.end method
