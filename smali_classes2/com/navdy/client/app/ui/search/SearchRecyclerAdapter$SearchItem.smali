.class Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
.super Ljava/lang/Object;
.source "SearchRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SearchItem"
.end annotation


# instance fields
.field public contact:Lcom/navdy/client/app/framework/models/ContactModel;

.field public destination:Lcom/navdy/client/app/framework/models/Destination;

.field public distance:Ljava/lang/Double;

.field imageEnum:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field price:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "distance"    # Ljava/lang/Double;
    .param p3, "price"    # Ljava/lang/String;
    .param p4, "imageEnum"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    .param p5, "contact"    # Lcom/navdy/client/app/framework/models/ContactModel;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 98
    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->distance:Ljava/lang/Double;

    .line 99
    iput-object p3, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->price:Ljava/lang/String;

    .line 100
    iput-object p4, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->imageEnum:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 101
    iput-object p5, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->contact:Lcom/navdy/client/app/framework/models/ContactModel;

    .line 102
    return-void
.end method


# virtual methods
.method getRes(Z)I
    .locals 3
    .param p1, "isAutocomplete"    # Z

    .prologue
    const/4 v0, 0x0

    .line 105
    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$6;->$SwitchMap$com$navdy$client$app$ui$search$SearchRecyclerAdapter$ImageEnum:[I

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->imageEnum:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 120
    :goto_0
    :pswitch_0
    return v0

    .line 107
    :pswitch_1
    if-eqz p1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForAutoComplete()I

    move-result v0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForSearchResults()I

    move-result v0

    goto :goto_0

    .line 112
    :pswitch_2
    const v0, 0x7f0200fe

    goto :goto_0

    .line 117
    :pswitch_3
    const v0, 0x7f0202c6

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
