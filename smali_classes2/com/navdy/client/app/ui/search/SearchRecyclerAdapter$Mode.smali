.class final enum Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;
.super Ljava/lang/Enum;
.source "SearchRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

.field public static final enum AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

.field public static final enum EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

.field public static final enum FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .line 82
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    const-string v1, "AUTO_COMPLETE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .line 83
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    const-string v1, "FULL_SEARCH"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .line 80
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    const-class v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    return-object v0
.end method
