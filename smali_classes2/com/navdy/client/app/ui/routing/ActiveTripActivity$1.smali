.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$1;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$1;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 2
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 112
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$1;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$000(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "HereMapFragment failed to initialize"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 113
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 1
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 107
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->setTrafficInfoVisible(Z)Lcom/here/android/mpa/mapping/Map;

    .line 108
    return-void
.end method
