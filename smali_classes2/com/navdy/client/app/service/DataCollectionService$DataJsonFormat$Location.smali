.class Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat$Location;
.super Ljava/lang/Object;
.source "DataCollectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Location"
.end annotation


# static fields
.field static final ACCURACY:Ljava/lang/String; = "accuracy"

.field static final ALTITUDE:Ljava/lang/String; = "altitude"

.field static final HEADING:Ljava/lang/String; = "heading"

.field static final LATITUDE:Ljava/lang/String; = "latitude"

.field static final LONGITUDE:Ljava/lang/String; = "longitude"

.field static final SPEED:Ljava/lang/String; = "speed"

.field static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field static final TRIP_DISTANCE:Ljava/lang/String; = "trip-distance"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
