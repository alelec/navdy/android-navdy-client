.class public Lcom/navdy/client/app/providers/NavdyContentProvider;
.super Landroid/content/ContentProvider;
.source "NavdyContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;,
        Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;,
        Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;
    }
.end annotation


# static fields
.field public static final CREATE_DESTINATIONS_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS destinations ( _id INTEGER PRIMARY KEY, place_id TEXT, last_place_id_refresh INTEGER, displat REAL, displong REAL, navlat REAL, navlong REAL, place_name TEXT NOT NULL, address TEXT NOT NULL, street_number TEXT, street_name TEXT, city TEXT, state TEXT, zip_code TEXT, country TEXT, countryCode TEXT, do_not_suggest INT, last_routed_date INTEGER, favorite_label TEXT, favorite_listing_order INTEGER, is_special INTEGER, place_detail_json TEXT, precision_level INTEGER, type INTEGER, contact_lookup_key TEXT, last_known_contact_id INTEGER, last_contact_lookup INTEGER);"

.field public static final CREATE_DESTINATION_CACHE_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);"

.field public static final CREATE_PLAYLISTS_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS playlists ( playlist_id INTEGER, playlist_name STRING );"

.field public static final CREATE_PLAYLIST_MEMBERS_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS playlist_members ( playlist_id INTEGER, SourceId STRING, artist STRING, album STRING, title STRING );"

.field public static final CREATE_SEARCH_HISTORY_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS search_history ( _id INTEGER PRIMARY KEY, last_searched_on INTEGER, query TEXT);"

.field public static final CREATE_TRIPS_TABLE:Ljava/lang/String; = " CREATE TABLE IF NOT EXISTS trips ( _id INTEGER PRIMARY KEY, trip_number INTEGER, start_time INTEGER, start_time_zone_n_dst INTEGER, start_odometer INTEGER, start_lat REAL, start_long REAL, end_time INTEGER, end_odometer INTEGER, end_lat REAL, end_long REAL, destination_id INTEGER, arrived_at_destination INTEGER);"

.field private static destinationCacheValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static destinationsValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static playlistMembersValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static playlistsValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static searchHistoryValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sqlDB:Landroid/database/sqlite/SQLiteDatabase;

.field private static tripsValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 144
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/providers/NavdyContentProvider;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    .line 172
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    .line 174
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "destinations"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 178
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "trips"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "search_history"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 186
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "destination_cache"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 190
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "playlists"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 194
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    const-string v2, "playlist_members"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 198
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 143
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->goThroughAllDestinationBlobsAndSetType(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method static synthetic access$200(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 143
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->goThroughAllDestinationBlobsAndSetCountryCode(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method static synthetic access$300(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 143
    invoke-static {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->goThroughAllFavoriteDestinationsAndLinkContacts(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    return-void
.end method

.method public static addPlaylistMemberToDb(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "playlistId"    # I
    .param p1, "sourceId"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;
    .param p3, "album"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 1674
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1675
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1676
    .local v1, "memberContentValues":Landroid/content/ContentValues;
    const-string v2, "playlist_id"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1677
    const-string v2, "SourceId"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678
    const-string v2, "artist"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679
    const-string v2, "album"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1680
    const-string v2, "title"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1682
    return-void
.end method

.method public static addPlaylistToDb(ILjava/lang/String;)V
    .locals 4
    .param p0, "playlistId"    # I
    .param p1, "playlistName"    # Ljava/lang/String;

    .prologue
    .line 1666
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1667
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 1668
    .local v1, "playlistContentValues":Landroid/content/ContentValues;
    const-string v2, "playlist_id"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1669
    const-string v2, "playlist_name"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1671
    return-void
.end method

.method public static addToCacheIfNotAlreadyIn(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 5
    .param p0, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1511
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1535
    :cond_0
    :goto_0
    return-void

    .line 1515
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1516
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1517
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1519
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 1523
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1524
    const/4 p1, 0x0

    .line 1527
    :cond_2
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CACHE: addToCacheIfNotAlreadyIn: address = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " destination = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1530
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheEntryIfExists(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/DestinationCacheEntry;

    move-result-object v2

    if-nez v2, :cond_3

    .line 1531
    invoke-static {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->insertNewDestinationInCache(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0

    .line 1533
    :cond_3
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "CACHE: Location already present in the cache so will not add it."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static addToSearchHistory(Ljava/lang/String;)I
    .locals 14
    .param p0, "query"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x1

    .line 1375
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1376
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    .line 1377
    .local v7, "context":Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1380
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 1381
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0801d5

    .line 1382
    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f08032f

    .line 1383
    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0801d2

    .line 1384
    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0800c5

    .line 1385
    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v12

    .line 1418
    :goto_0
    return v1

    .line 1390
    :cond_1
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1391
    .local v6, "contentValues":Landroid/content/ContentValues;
    const-string v1, "last_searched_on"

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1394
    const/4 v8, 0x0

    .line 1396
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_PROJECTION:[Ljava/lang/String;

    const-string v3, "lower(query)=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1400
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    .line 1396
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1402
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1403
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1404
    .local v10, "idIndex":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1407
    .local v9, "id":I
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 1410
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1407
    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1413
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v9    # "id":I
    .end local v10    # "idIndex":I
    :cond_2
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1417
    const-string v1, "query"

    invoke-virtual {v6, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v11

    goto :goto_0

    .line 1413
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1

    :cond_3
    move v1, v12

    .line 1418
    goto :goto_0
.end method

.method private static cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 856
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 861
    .end local p0    # "address":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 859
    .restart local p0    # "address":Ljava/lang/String;
    :cond_0
    const-string v0, "[,\n\t]"

    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " +"

    const-string v2, " "

    .line 860
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 861
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static clearCache()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1623
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1624
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getSqlDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1625
    .local v1, "database":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "destination_cache"

    invoke-virtual {v1, v2, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1626
    const-string v2, " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1627
    return-void
.end method

.method public static deleteAllPlaylists()V
    .locals 4
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1686
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1687
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1688
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1689
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1690
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1691
    return-void
.end method

.method public static deletePlaylist(I)V
    .locals 6
    .param p0, "playlistId"    # I
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1695
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1696
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 1697
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1698
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    const-string v3, "playlist_id = ?"

    .line 1699
    .local v3, "query":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    .line 1700
    .local v0, "args":[Ljava/lang/String;
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v4, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1701
    sget-object v4, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v4, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1702
    return-void
.end method

.method public static getCacheEntryIfExists(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/DestinationCacheEntry;
    .locals 13
    .param p0, "location"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 1442
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CACHE: Checking the cache for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1443
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1444
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "CACHE: Can\'t lookup entries for a null location!"

    invoke-virtual {v1, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v2, v10

    .line 1485
    :goto_0
    return-object v2

    .line 1448
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1449
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 1450
    .local v8, "context":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1452
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_1

    .line 1453
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "CACHE: Unable to get a content resolver!"

    invoke-virtual {v1, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v2, v10

    .line 1454
    goto :goto_0

    .line 1458
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->removeOutOfDateCacheEntries()V

    .line 1461
    const/4 v9, 0x0

    .line 1463
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_PROJECTION:[Ljava/lang/String;

    const-string v3, "location=?"

    const/4 v11, 0x1

    new-array v4, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p0, v4, v11

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1470
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1471
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1472
    .local v3, "id":I
    const-string v1, "last_response_date"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1473
    .local v4, "lastResponseDate":J
    const-string v1, "location"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1474
    .local v6, "locationString":Ljava/lang/String;
    const-string v1, "destination_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1476
    .local v7, "destinationId":I
    new-instance v2, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;

    invoke-direct/range {v2 .. v7}, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;-><init>(IJLjava/lang/String;I)V

    .line 1477
    .local v2, "dce":Lcom/navdy/client/app/framework/models/DestinationCacheEntry;
    invoke-static {v7}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    iput-object v1, v2, Lcom/navdy/client/app/framework/models/DestinationCacheEntry;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1478
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CACHE: Found this cache entry for this location: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1482
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .end local v2    # "dce":Lcom/navdy/client/app/framework/models/DestinationCacheEntry;
    .end local v3    # "id":I
    .end local v4    # "lastResponseDate":J
    .end local v6    # "locationString":Ljava/lang/String;
    .end local v7    # "destinationId":I
    :cond_2
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1484
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CACHE: Couldn\'t find any cache entry for this location: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    move-object v2, v10

    .line 1485
    goto/16 :goto_0

    .line 1482
    :catchall_0
    move-exception v1

    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method public static getCacheKeysForDestinationId(I)Ljava/util/ArrayList;
    .locals 3
    .param p0, "destinationId"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 735
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 736
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 737
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-static {v0, p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2
.end method

.method private static getCacheKeysForDestinationId(Landroid/content/ContentResolver;I)Ljava/util/ArrayList;
    .locals 1
    .param p0, "cr"    # Landroid/content/ContentResolver;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destinationId"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 762
    new-instance v0, Lcom/navdy/client/app/providers/NavdyContentProvider$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/providers/NavdyContentProvider$2;-><init>(Landroid/content/ContentResolver;)V

    invoke-static {v0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static getCacheKeysForDestinationId(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/ArrayList;
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destinationId"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 750
    new-instance v0, Lcom/navdy/client/app/providers/NavdyContentProvider$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/providers/NavdyContentProvider$1;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static getCacheKeysForDestinationId(Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;I)Ljava/util/ArrayList;
    .locals 10
    .param p0, "cq"    # Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destinationId"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 774
    if-gez p1, :cond_0

    .line 775
    sget-object v8, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "CACHE: Can\'t lookup entries for a negative ID!"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 776
    const/4 v0, 0x0

    .line 803
    :goto_0
    return-object v0

    .line 779
    :cond_0
    const/4 v0, 0x0

    .line 782
    .local v0, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 784
    .local v2, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x1

    :try_start_0
    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "location"

    aput-object v9, v5, v8

    .line 785
    .local v5, "projection":[Ljava/lang/String;
    const-string v6, "destination_id=?"

    .line 786
    .local v6, "selection":Ljava/lang/String;
    const/4 v8, 0x1

    new-array v7, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 788
    .local v7, "selectionArgs":[Ljava/lang/String;
    const-string v8, "destination_id=?"

    invoke-interface {p0, v5, v8, v7}, Lcom/navdy/client/app/providers/NavdyContentProvider$CacheQuerier;->runQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 790
    if-eqz v2, :cond_2

    .line 791
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 793
    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v1, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 794
    const-string v8, "location"

    invoke-static {v2, v8}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 795
    .local v4, "locationString":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 798
    .end local v4    # "locationString":Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v0, v1

    .line 799
    .end local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    .restart local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v3, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_2
    sget-object v8, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "CACHE: Something went wrong while trying to query the HERE cache!"

    invoke-virtual {v8, v9, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 801
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "projection":[Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .end local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v5    # "projection":[Ljava/lang/String;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v8

    :goto_3
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "projection":[Ljava/lang/String;
    .restart local v6    # "selection":Ljava/lang/String;
    .restart local v7    # "selectionArgs":[Ljava/lang/String;
    :catchall_1
    move-exception v8

    move-object v0, v1

    .end local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_3

    .line 798
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v6    # "selection":Ljava/lang/String;
    .end local v7    # "selectionArgs":[Ljava/lang/String;
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method private static getContactsWithoutLookupKey(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 694
    const/4 v10, 0x0

    .line 695
    .local v10, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v8, 0x0

    .line 697
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "destinations"

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "place_name"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "address"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "street_number"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "street_name"

    aput-object v3, v2, v0

    const-string v3, "type = ? AND contact_lookup_key is null"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v5, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 705
    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination$Type;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    .line 697
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 707
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 708
    :cond_0
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Unable to get a destination cursor for contact linking upgrade!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 727
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v0, v13

    .line 729
    :goto_0
    return-object v0

    .line 712
    :cond_1
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " destination(s)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 713
    new-instance v11, Ljava/util/ArrayList;

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 715
    .end local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .local v11, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :goto_1
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 716
    new-instance v9, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v9}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 717
    .local v9, "d":Lcom/navdy/client/app/framework/models/Destination;
    const-string v0, "_id"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 718
    const-string v0, "place_name"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 719
    const-string v0, "address"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 720
    const-string v0, "street_number"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    .line 721
    const-string v0, "street_name"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    .line 722
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 724
    .end local v9    # "d":Lcom/navdy/client/app/framework/models/Destination;
    :catch_0
    move-exception v12

    move-object v10, v11

    .line 725
    .end local v11    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .local v12, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a destination cursor for contact linking upgrade!"

    invoke-virtual {v0, v1, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 727
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .end local v12    # "e":Ljava/lang/Exception;
    :goto_3
    move-object v0, v10

    .line 729
    goto :goto_0

    .line 727
    .end local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v11    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_2
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v10, v11

    .line 728
    .end local v11    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    goto :goto_3

    .line 727
    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0

    .end local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v11    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :catchall_1
    move-exception v0

    move-object v10, v11

    .end local v11    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    .restart local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    goto :goto_4

    .line 724
    :catch_1
    move-exception v12

    goto :goto_2
.end method

.method private getContentResolverSafely()Landroid/content/ContentResolver;
    .locals 2

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 415
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 416
    const/4 v1, 0x0

    .line 418
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto :goto_0
.end method

.method private getContentUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1, "baseUri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 346
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    .line 358
    :goto_0
    return-object v0

    .line 350
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 352
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 354
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 356
    :pswitch_4
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 358
    :pswitch_5
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 346
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getDestinationCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 879
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getDestinationCursor(Landroid/util/Pair;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 883
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v0, "_id ASC"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationCursor(Landroid/util/Pair;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getDestinationCursor(Landroid/util/Pair;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 888
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 889
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 891
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 892
    const/4 v1, 0x0

    .line 895
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public static getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;
    .locals 40
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "position"    # I

    .prologue
    .line 904
    if-eqz p0, :cond_1

    invoke-interface/range {p0 .. p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v32

    if-eqz v32, :cond_1

    .line 905
    const-string v32, "_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 906
    .local v4, "id":I
    const-string v32, "place_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 907
    .local v5, "placeId":Ljava/lang/String;
    const-string v32, "last_place_id_refresh"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 908
    .local v6, "lastPlaceIdRefresh":J
    const-string v32, "displat"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v8

    .line 909
    .local v8, "displayLat":D
    const-string v32, "displong"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 910
    .local v10, "displayLong":D
    const-string v32, "navlat"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 911
    .local v12, "navigationLat":D
    const-string v32, "navlong"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    .line 912
    .local v14, "navigationLong":D
    const-string v32, "place_name"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 913
    .local v16, "name":Ljava/lang/String;
    const-string v32, "address"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 914
    .local v17, "address":Ljava/lang/String;
    const-string v32, "street_number"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 915
    .local v18, "streetNumber":Ljava/lang/String;
    const-string v32, "street_name"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 916
    .local v19, "streetName":Ljava/lang/String;
    const-string v32, "city"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 917
    .local v20, "city":Ljava/lang/String;
    const-string v32, "state"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 918
    .local v21, "state":Ljava/lang/String;
    const-string v32, "zip_code"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 919
    .local v22, "zipCode":Ljava/lang/String;
    const-string v32, "country"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 920
    .local v23, "country":Ljava/lang/String;
    const-string v32, "countryCode"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 922
    .local v24, "countryCode":Ljava/lang/String;
    const-string v32, "do_not_suggest"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_0

    const/16 v25, 0x1

    .line 924
    .local v25, "doNotSuggest":Z
    :goto_0
    const-string v32, "last_routed_date"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 926
    .local v26, "lastRoutedDate":J
    const-string v32, "favorite_label"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 927
    .local v28, "favoriteLabel":Ljava/lang/String;
    const-string v32, "favorite_listing_order"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v29

    .line 928
    .local v29, "favoriteOrder":I
    const-string v32, "is_special"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 930
    .local v30, "favoriteType":I
    const-string v32, "place_detail_json"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 931
    .local v31, "placeDetailJson":Ljava/lang/String;
    const-string v32, "precision_level"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 932
    .local v2, "precisionLevel":I
    const-string v32, "type"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v39

    .line 934
    .local v39, "type":I
    const-string v32, "contact_lookup_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 935
    .local v34, "contactLookupKey":Ljava/lang/String;
    const-string v32, "last_known_contact_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v35

    .line 936
    .local v35, "lastKnownContactId":J
    const-string v32, "last_contact_lookup"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v37

    .line 938
    .local v37, "lastContactLookup":J
    new-instance v3, Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v2}, Lcom/navdy/client/app/framework/models/Destination$Precision;->get(I)Lcom/navdy/client/app/framework/models/Destination$Precision;

    move-result-object v32

    invoke-static/range {v39 .. v39}, Lcom/navdy/client/app/framework/models/Destination$Type;->get(I)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v33

    invoke-direct/range {v3 .. v38}, Lcom/navdy/client/app/framework/models/Destination;-><init>(ILjava/lang/String;JDDDDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLjava/lang/String;IILjava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/models/Destination$Type;Ljava/lang/String;JJ)V

    .line 939
    .local v3, "d":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v4}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(I)Ljava/util/ArrayList;

    move-result-object v32

    move-object/from16 v0, v32

    iput-object v0, v3, Lcom/navdy/client/app/framework/models/Destination;->rawAddressVariations:Ljava/util/ArrayList;

    .line 942
    .end local v2    # "precisionLevel":I
    .end local v3    # "d":Lcom/navdy/client/app/framework/models/Destination;
    .end local v4    # "id":I
    .end local v5    # "placeId":Ljava/lang/String;
    .end local v6    # "lastPlaceIdRefresh":J
    .end local v8    # "displayLat":D
    .end local v10    # "displayLong":D
    .end local v12    # "navigationLat":D
    .end local v14    # "navigationLong":D
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "address":Ljava/lang/String;
    .end local v18    # "streetNumber":Ljava/lang/String;
    .end local v19    # "streetName":Ljava/lang/String;
    .end local v20    # "city":Ljava/lang/String;
    .end local v21    # "state":Ljava/lang/String;
    .end local v22    # "zipCode":Ljava/lang/String;
    .end local v23    # "country":Ljava/lang/String;
    .end local v24    # "countryCode":Ljava/lang/String;
    .end local v25    # "doNotSuggest":Z
    .end local v26    # "lastRoutedDate":J
    .end local v28    # "favoriteLabel":Ljava/lang/String;
    .end local v29    # "favoriteOrder":I
    .end local v30    # "favoriteType":I
    .end local v31    # "placeDetailJson":Ljava/lang/String;
    .end local v34    # "contactLookupKey":Ljava/lang/String;
    .end local v35    # "lastKnownContactId":J
    .end local v37    # "lastContactLookup":J
    .end local v39    # "type":I
    :goto_1
    return-object v3

    .line 922
    .restart local v4    # "id":I
    .restart local v5    # "placeId":Ljava/lang/String;
    .restart local v6    # "lastPlaceIdRefresh":J
    .restart local v8    # "displayLat":D
    .restart local v10    # "displayLong":D
    .restart local v12    # "navigationLat":D
    .restart local v14    # "navigationLong":D
    .restart local v16    # "name":Ljava/lang/String;
    .restart local v17    # "address":Ljava/lang/String;
    .restart local v18    # "streetNumber":Ljava/lang/String;
    .restart local v19    # "streetName":Ljava/lang/String;
    .restart local v20    # "city":Ljava/lang/String;
    .restart local v21    # "state":Ljava/lang/String;
    .restart local v22    # "zipCode":Ljava/lang/String;
    .restart local v23    # "country":Ljava/lang/String;
    .restart local v24    # "countryCode":Ljava/lang/String;
    :cond_0
    const/16 v25, 0x0

    goto/16 :goto_0

    .line 942
    .end local v4    # "id":I
    .end local v5    # "placeId":Ljava/lang/String;
    .end local v6    # "lastPlaceIdRefresh":J
    .end local v8    # "displayLat":D
    .end local v10    # "displayLong":D
    .end local v12    # "navigationLat":D
    .end local v14    # "navigationLong":D
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "address":Ljava/lang/String;
    .end local v18    # "streetNumber":Ljava/lang/String;
    .end local v19    # "streetName":Ljava/lang/String;
    .end local v20    # "city":Ljava/lang/String;
    .end local v21    # "state":Ljava/lang/String;
    .end local v22    # "zipCode":Ljava/lang/String;
    .end local v23    # "country":Ljava/lang/String;
    .end local v24    # "countryCode":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 3
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/client/app/framework/models/Destination;"
        }
    .end annotation

    .prologue
    .line 1105
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 1106
    .local v1, "destinationItem":Lcom/navdy/client/app/framework/models/Destination;
    const/4 v0, 0x0

    .line 1108
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    .line 1109
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1111
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1113
    return-object v1

    .line 1111
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method public static getDestinationsFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p0, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1033
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "query: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1035
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    const-string v4, "%"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1037
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1038
    .local v10, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v7, 0x0

    .line 1040
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1041
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1042
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1043
    const/4 v10, 0x0

    .line 1066
    .end local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1069
    :goto_0
    return-object v10

    .line 1045
    .restart local v10    # "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    const-string v3, "place_name like ? OR favorite_label like ? OR address like ?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    aput-object p0, v4, v5

    const/4 v5, 0x2

    aput-object p0, v4, v5

    const-string v5, "place_name ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1054
    if-eqz v7, :cond_2

    .line 1056
    :cond_1
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1057
    const-string v1, "_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1058
    .local v9, "destinationId":I
    invoke-static {v9}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v8

    .line 1059
    .local v8, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v8, :cond_1

    .line 1060
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v1

    iput v1, v8, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 1061
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1066
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v6    # "context":Landroid/content/Context;
    .end local v8    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v9    # "destinationId":I
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1

    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v6    # "context":Landroid/content/Context;
    :cond_2
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1068
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDestinationsFromSearchQuery: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFavoriteItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/client/app/framework/models/Destination;"
        }
    .end annotation

    .prologue
    .line 1237
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->restrictSelectionToFavorites(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p0

    .line 1238
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method public static getFavoritesCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1176
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getFavoritesCursor(Landroid/util/Pair;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1180
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->restrictSelectionToFavorites(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p0

    .line 1181
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1182
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1184
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1185
    const/4 v1, 0x0

    .line 1188
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const-string v5, "destinations.favorite_listing_order ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public static getGroupedTripsCursor()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1287
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1288
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1290
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1294
    :goto_0
    return-object v4

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "count(_id) as occurrences"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "destination_id"

    aput-object v5, v2, v3

    const-string v3, "destination_id != 0) group by (destination_id"

    const-string v5, "occurrences DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0
.end method

.method public static getHome()Lcom/navdy/client/app/framework/models/Destination;
    .locals 5

    .prologue
    .line 1211
    new-instance v0, Landroid/util/Pair;

    const-string v1, "is_special ==  ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, -0x3

    .line 1214
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1211
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoriteItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method private static getMatchingContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Ljava/util/List;Ljava/util/ArrayList;)Lcom/navdy/client/app/framework/models/ContactModel;
    .locals 10
    .param p0, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "streetNumber"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "streetName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "contentResolver"    # Landroid/content/ContentResolver;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/client/app/framework/models/ContactModel;"
        }
    .end annotation

    .prologue
    .line 808
    .local p4, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    .local p5, "cachedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 809
    .local v2, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    iget-object v7, v2, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    iget-wide v8, v2, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    invoke-static {p3, v7, v8, v9}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddressFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)Ljava/util/List;

    move-result-object v0

    .line 810
    .local v0, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/models/ContactModel;->addAddresses(Ljava/util/List;)V

    .line 811
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/models/Address;

    .line 812
    .local v3, "contactAddress":Lcom/navdy/client/app/framework/models/Address;
    invoke-static {p0, p1, p2, v3}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isMatchingContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/models/Address;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 830
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v2    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v3    # "contactAddress":Lcom/navdy/client/app/framework/models/Address;
    :goto_0
    return-object v2

    .line 815
    .restart local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .restart local v2    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .restart local v3    # "contactAddress":Lcom/navdy/client/app/framework/models/Address;
    :cond_2
    if-eqz p5, :cond_1

    .line 819
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Address;->getFullAddress()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 820
    .local v5, "fullAddress":Ljava/lang/String;
    iget-object v8, v3, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    invoke-static {v8}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 822
    .local v4, "contactStreet":Ljava/lang/String;
    invoke-virtual {p5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 823
    .local v1, "cachedAddress":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 824
    invoke-static {v1, v5, v4}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isSomewhatMatchingAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    goto :goto_0

    .line 830
    .end local v0    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v1    # "cachedAddress":Ljava/lang/String;
    .end local v2    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v3    # "contactAddress":Lcom/navdy/client/app/framework/models/Address;
    .end local v4    # "contactStreet":Ljava/lang/String;
    .end local v5    # "fullAddress":Ljava/lang/String;
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMaxTripId()J
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v3, 0x0

    .line 1264
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1265
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1267
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1282
    :goto_0
    return-wide v8

    .line 1270
    :cond_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "MAX(_id)"

    aput-object v1, v2, v3

    .line 1271
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1273
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1274
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1275
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1276
    .local v8, "maxID":J
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1280
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v8    # "maxID":J
    :cond_1
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method public static getNonStalePlaceDetailInfoFor(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "placeId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1123
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestinationWithPlaceId(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 1124
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isPlaceDetailsInfoStale()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1125
    :cond_0
    const/4 v1, 0x0

    .line 1127
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, v0, Lcom/navdy/client/app/framework/models/Destination;->placeDetailJson:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getPlaylistMembersCursor(I)Landroid/database/Cursor;
    .locals 7
    .param p0, "playlistId"    # I
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1646
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1647
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1648
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1650
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1657
    :goto_0
    return-object v5

    .line 1654
    :cond_0
    const-string v3, "playlist_id = ?"

    .line 1655
    .local v3, "query":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 1657
    .local v4, "args":[Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0
.end method

.method public static getPlaylistsCursor()Landroid/database/Cursor;
    .locals 7
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1632
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1633
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1634
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1636
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_PROJECTION:[Ljava/lang/String;

    const-string v4, "playlist_name"

    .line 1641
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getOrderString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    .line 1636
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public static getRecentItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/client/app/framework/models/Destination;"
        }
    .end annotation

    .prologue
    .line 1169
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->restrictSelectionToRecents(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p0

    .line 1170
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method public static getRecentsCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1135
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getRecentsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getRecentsCursor(Landroid/util/Pair;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1139
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->restrictSelectionToRecents(Landroid/util/Pair;)Landroid/util/Pair;

    move-result-object p0

    .line 1141
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1142
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1144
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1145
    const/4 v1, 0x0

    .line 1148
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const-string v5, "last_routed_date DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public static getSearchHistoryCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1352
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getSearchHistoryCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getSearchHistoryCursor(Landroid/util/Pair;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1357
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1358
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1359
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1361
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1362
    const/4 v1, 0x0

    .line 1365
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const-string v5, "last_searched_on DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public static getSearchHistoryFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 11
    .param p0, "constraint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1074
    .local v8, "pastQueries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 1076
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    .line 1077
    .local v7, "context":Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1078
    .local v0, "cr":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1079
    const/4 v8, 0x0

    .line 1093
    .end local v8    # "pastQueries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1095
    :goto_0
    return-object v8

    .line 1081
    .restart local v8    # "pastQueries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "query"

    aput-object v4, v2, v3

    const-string v3, "query like ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const-string v5, "last_searched_on DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1089
    :goto_1
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1090
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1093
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v7    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1

    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v7    # "context":Landroid/content/Context;
    :cond_1
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method public static declared-synchronized getSqlDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    const-class v2, Lcom/navdy/client/app/providers/NavdyContentProvider;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v1, :cond_0

    .line 386
    new-instance v0, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;-><init>(Landroid/content/Context;)V

    .line 387
    .local v0, "dbHelper":Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;
    invoke-virtual {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sput-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 389
    .end local v0    # "dbHelper":Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;
    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 385
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private getTableName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 306
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :pswitch_0
    const-string v0, "destinations"

    .line 318
    :goto_0
    return-object v0

    .line 310
    :pswitch_1
    const-string v0, "trips"

    goto :goto_0

    .line 312
    :pswitch_2
    const-string v0, "search_history"

    goto :goto_0

    .line 314
    :pswitch_3
    const-string v0, "destination_cache"

    goto :goto_0

    .line 316
    :pswitch_4
    const-string v0, "playlists"

    goto :goto_0

    .line 318
    :pswitch_5
    const-string v0, "playlist_members"

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getTableNameForQuery(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 326
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 340
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :pswitch_0
    const-string v0, "destinations"

    .line 338
    :goto_0
    return-object v0

    .line 330
    :pswitch_1
    const-string v0, "trips"

    goto :goto_0

    .line 332
    :pswitch_2
    const-string v0, "search_history"

    goto :goto_0

    .line 334
    :pswitch_3
    const-string v0, "destination_cache"

    goto :goto_0

    .line 336
    :pswitch_4
    const-string v0, "playlists"

    goto :goto_0

    .line 338
    :pswitch_5
    const-string v0, "playlist_members"

    goto :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getThisDestination(I)Lcom/navdy/client/app/framework/models/Destination;
    .locals 6
    .param p0, "destinationId"    # I

    .prologue
    .line 951
    :try_start_0
    new-instance v1, Landroid/util/Pair;

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 953
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 954
    .local v1, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 957
    .end local v1    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 955
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Exception while parsing destination ID"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 957
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 10
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const/4 v1, 0x0

    .line 985
    if-nez p0, :cond_0

    move-object p0, v1

    .line 1012
    .local v6, "c":Landroid/database/Cursor;
    :goto_0
    return-object p0

    .line 988
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    const/4 v6, 0x0

    .line 990
    .restart local v6    # "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    .line 991
    .local v8, "context":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 992
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->getComparisonSelectionClause()Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 994
    .local v7, "comparisonSelectionClause":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    if-nez v7, :cond_1

    .line 1010
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 998
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1004
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1005
    const/4 v1, 0x0

    invoke-static {v6, v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p0

    .line 1010
    :cond_2
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 1007
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v7    # "comparisonSelectionClause":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v8    # "context":Landroid/content/Context;
    :catch_0
    move-exception v9

    .line 1008
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Destination not found in db for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1010
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method public static getThisDestination(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 5
    .param p0, "destinationId"    # Ljava/lang/String;

    .prologue
    .line 967
    :try_start_0
    new-instance v1, Landroid/util/Pair;

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 970
    .local v1, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 973
    .end local v1    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 971
    :catch_0
    move-exception v0

    .line 972
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Exception while parsing destination ID"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 973
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getThisDestinationWithPlaceId(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 4
    .param p0, "placeId"    # Ljava/lang/String;

    .prologue
    .line 1020
    new-instance v0, Landroid/util/Pair;

    const-string v1, "place_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1023
    .local v0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    return-object v1
.end method

.method public static getThisTrip(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Trip;
    .locals 5
    .param p0, "tripNumber"    # Ljava/lang/String;

    .prologue
    .line 1330
    new-instance v0, Landroid/util/Pair;

    const-string v1, "trip_number=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1332
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1333
    .local v0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Trip;

    move-result-object v1

    return-object v1
.end method

.method public static getTripFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Trip;
    .locals 3
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/client/app/framework/models/Trip;"
        }
    .end annotation

    .prologue
    .line 1338
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 1339
    .local v1, "tripsItem":Lcom/navdy/client/app/framework/models/Trip;
    const/4 v0, 0x0

    .line 1341
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    .line 1342
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1344
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1346
    return-object v1

    .line 1344
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2
.end method

.method public static getTripsCursor()Landroid/database/Cursor;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1244
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getTripsCursor(Landroid/util/Pair;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1248
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1249
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1251
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1252
    const/4 v1, 0x0

    .line 1255
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_PROJECTION:[Ljava/lang/String;

    iget-object v3, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    const-string v5, "start_time DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method

.method public static getTripsItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Trip;
    .locals 24
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "position"    # I

    .prologue
    .line 1307
    if-eqz p0, :cond_0

    invoke-interface/range {p0 .. p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1308
    const-string v2, "_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1309
    .local v3, "id":I
    const-string v2, "trip_number"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1310
    .local v4, "tripNumber":J
    const-string v2, "start_time"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1311
    .local v6, "startTime":J
    const-string v2, "start_time_zone_n_dst"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1312
    .local v8, "offset":I
    const-string v2, "start_odometer"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1313
    .local v9, "startOdometer":I
    const-string v2, "start_lat"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v10

    .line 1314
    .local v10, "startLat":D
    const-string v2, "start_long"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v12

    .line 1315
    .local v12, "startLong":D
    const-string v2, "end_time"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1316
    .local v14, "endTime":J
    const-string v2, "end_odometer"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1317
    .local v16, "endOdometer":I
    const-string v2, "end_lat"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v17

    .line 1318
    .local v17, "endLat":D
    const-string v2, "end_long"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v19

    .line 1319
    .local v19, "endLong":D
    const-string v2, "arrived_at_destination"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    .line 1320
    .local v21, "arrivedAtDestination":J
    const-string v2, "destination_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 1321
    .local v23, "destinationId":I
    new-instance v2, Lcom/navdy/client/app/framework/models/Trip;

    invoke-direct/range {v2 .. v23}, Lcom/navdy/client/app/framework/models/Trip;-><init>(IJJIIDDJIDDJI)V

    .line 1323
    .end local v3    # "id":I
    .end local v4    # "tripNumber":J
    .end local v6    # "startTime":J
    .end local v8    # "offset":I
    .end local v9    # "startOdometer":I
    .end local v10    # "startLat":D
    .end local v12    # "startLong":D
    .end local v14    # "endTime":J
    .end local v16    # "endOdometer":I
    .end local v17    # "endLat":D
    .end local v19    # "endLong":D
    .end local v21    # "arrivedAtDestination":J
    .end local v23    # "destinationId":I
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getValues(Landroid/net/Uri;)Ljava/util/HashMap;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 380
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->destinationsValues:Ljava/util/HashMap;

    .line 378
    :goto_0
    return-object v0

    .line 370
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->tripsValues:Ljava/util/HashMap;

    goto :goto_0

    .line 372
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->searchHistoryValues:Ljava/util/HashMap;

    goto :goto_0

    .line 374
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->destinationCacheValues:Ljava/util/HashMap;

    goto :goto_0

    .line 376
    :pswitch_4
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->playlistsValues:Ljava/util/HashMap;

    goto :goto_0

    .line 378
    :pswitch_5
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->playlistMembersValues:Ljava/util/HashMap;

    goto :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getWork()Lcom/navdy/client/app/framework/models/Destination;
    .locals 5

    .prologue
    .line 1220
    new-instance v0, Landroid/util/Pair;

    const-string v1, "is_special ==  ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, -0x2

    .line 1223
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1220
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoriteItemFromSelection(Landroid/util/Pair;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method private static goThroughAllDestinationBlobsAndSetCountryCode(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 23
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 587
    const/4 v14, 0x0

    .line 590
    .local v14, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "destinations"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "place_detail_json"

    aput-object v5, v4, v2

    const-string v5, "place_detail_json <> \'\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 594
    if-nez v14, :cond_1

    .line 595
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to get a destination cursor for country code upgrade!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 639
    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 641
    :goto_0
    return-void

    .line 625
    .local v11, "addressComponents":Lorg/json/JSONArray;
    .local v13, "countryCode":Ljava/lang/String;
    .local v16, "id":I
    .local v17, "json":Ljava/lang/String;
    .local v18, "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :try_start_1
    invoke-static {v13}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 626
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 627
    .local v12, "contentValues":Landroid/content/ContentValues;
    const-string v2, "countryCode"

    invoke-virtual {v12, v2, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const-string v2, "destinations"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 632
    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 629
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599
    .end local v11    # "addressComponents":Lorg/json/JSONArray;
    .end local v12    # "contentValues":Landroid/content/ContentValues;
    .end local v13    # "countryCode":Ljava/lang/String;
    .end local v16    # "id":I
    .end local v17    # "json":Ljava/lang/String;
    .end local v18    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    :goto_1
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 600
    const-string v2, "_id"

    invoke-static {v14, v2}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v16

    .line 601
    .restart local v16    # "id":I
    const-string v2, "place_detail_json"

    invoke-static {v14, v2}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v17

    .line 604
    .restart local v17    # "json":Ljava/lang/String;
    :try_start_3
    new-instance v18, Lorg/json/JSONObject;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 605
    .restart local v18    # "jsonObject":Lorg/json/JSONObject;
    const-string v2, "address_components"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v11

    .line 606
    .restart local v11    # "addressComponents":Lorg/json/JSONArray;
    const/4 v13, 0x0

    .line 607
    .restart local v13    # "countryCode":Ljava/lang/String;
    if-eqz v11, :cond_0

    .line 608
    const/4 v10, 0x0

    .local v10, "aci":I
    :goto_2
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-ge v10, v2, :cond_0

    .line 610
    :try_start_4
    invoke-virtual {v11, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    .line 611
    .local v19, "obj":Lorg/json/JSONObject;
    const-string v2, "types"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v22

    .line 612
    .local v22, "types":Lorg/json/JSONArray;
    const/16 v20, 0x0

    .local v20, "ti":I
    :goto_3
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v20

    if-ge v0, v2, :cond_2

    .line 613
    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 614
    .local v21, "type":Ljava/lang/String;
    const-string v2, "country"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 615
    const-string v2, "short_name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v13

    .line 608
    .end local v19    # "obj":Lorg/json/JSONObject;
    .end local v20    # "ti":I
    .end local v21    # "type":Ljava/lang/String;
    .end local v22    # "types":Lorg/json/JSONArray;
    :cond_2
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 612
    .restart local v19    # "obj":Lorg/json/JSONObject;
    .restart local v20    # "ti":I
    .restart local v21    # "type":Ljava/lang/String;
    .restart local v22    # "types":Lorg/json/JSONArray;
    :cond_3
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 634
    .end local v10    # "aci":I
    .end local v11    # "addressComponents":Lorg/json/JSONArray;
    .end local v13    # "countryCode":Ljava/lang/String;
    .end local v18    # "jsonObject":Lorg/json/JSONObject;
    .end local v19    # "obj":Lorg/json/JSONObject;
    .end local v20    # "ti":I
    .end local v21    # "type":Ljava/lang/String;
    .end local v22    # "types":Lorg/json/JSONArray;
    :catch_0
    move-exception v15

    .line 635
    .local v15, "e":Lorg/json/JSONException;
    :try_start_5
    invoke-virtual {v15}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 639
    .end local v15    # "e":Lorg/json/JSONException;
    .end local v16    # "id":I
    .end local v17    # "json":Ljava/lang/String;
    :catchall_0
    move-exception v2

    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v2

    :cond_4
    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 619
    .restart local v10    # "aci":I
    .restart local v11    # "addressComponents":Lorg/json/JSONArray;
    .restart local v13    # "countryCode":Ljava/lang/String;
    .restart local v16    # "id":I
    .restart local v17    # "json":Ljava/lang/String;
    .restart local v18    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v2

    goto :goto_4
.end method

.method private static goThroughAllDestinationBlobsAndSetType(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 549
    const/4 v10, 0x0

    .line 552
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "destinations"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "_id"

    aput-object v4, v3, v1

    const/4 v1, 0x1

    const-string v4, "place_detail_json"

    aput-object v4, v3, v1

    const-string v4, "place_detail_json <> \'\'"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 556
    if-nez v10, :cond_0

    .line 557
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to get a destination cursor for destination type upgrade!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 584
    :goto_0
    return-void

    .line 561
    :cond_0
    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562
    const-string v1, "_id"

    invoke-static {v10, v1}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v13

    .line 563
    .local v13, "id":I
    const-string v1, "place_detail_json"

    invoke-static {v10, v1}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    .line 566
    .local v14, "json":Ljava/lang/String;
    :try_start_2
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15, v14}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 567
    .local v15, "obj":Lorg/json/JSONObject;
    const-string v1, "types"

    invoke-virtual {v15, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v16

    .line 568
    .local v16, "types":Lorg/json/JSONArray;
    invoke-static/range {v16 .. v16}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->parseTypes(Lorg/json/JSONArray;)Lcom/navdy/client/app/framework/models/Destination$Type;

    move-result-object v11

    .line 570
    .local v11, "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 571
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v1, "type"

    invoke-virtual {v11}, Lcom/navdy/client/app/framework/models/Destination$Type;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 573
    const-string v1, "destinations"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 576
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 573
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 577
    .end local v9    # "contentValues":Landroid/content/ContentValues;
    .end local v11    # "destinationType":Lcom/navdy/client/app/framework/models/Destination$Type;
    .end local v15    # "obj":Lorg/json/JSONObject;
    .end local v16    # "types":Lorg/json/JSONArray;
    :catch_0
    move-exception v12

    .line 578
    .local v12, "e":Lorg/json/JSONException;
    :try_start_3
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 582
    .end local v12    # "e":Lorg/json/JSONException;
    .end local v13    # "id":I
    .end local v14    # "json":Ljava/lang/String;
    :catchall_0
    move-exception v1

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1

    :cond_1
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private static goThroughAllFavoriteDestinationsAndLinkContacts(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 14
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 645
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "goThroughAllFavoriteDestinationsAndLinkContacts"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 647
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContactsWithoutLookupKey(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;

    move-result-object v8

    .line 649
    .local v8, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 650
    :cond_0
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Found no contact destination without contact lookup key"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 690
    :cond_1
    return-void

    .line 654
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 656
    .local v3, "contentResolver":Landroid/content/ContentResolver;
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/client/app/framework/models/Destination;

    .line 657
    .local v7, "d":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v0, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 658
    invoke-static {v0, v3}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsPreferablyWithPhoneNumber(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v4

    .line 660
    .local v4, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 661
    :cond_3
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Looking up "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned 0 contacts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 664
    :cond_4
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Looking up "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " contacts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 667
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 668
    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 674
    .local v9, "matchedContact":Lcom/navdy/client/app/framework/models/ContactModel;
    :goto_1
    if-eqz v9, :cond_6

    .line 675
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Matching contact for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 677
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 678
    .local v6, "contentValues":Landroid/content/ContentValues;
    const-string v0, "contact_lookup_key"

    iget-object v1, v9, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    const-string v0, "last_known_contact_id"

    iget-wide v12, v9, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 680
    const-string v0, "last_contact_lookup"

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 682
    const-string v0, "destinations"

    const-string v1, "_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v11, 0x0

    iget v12, v7, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 685
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v11

    .line 682
    invoke-virtual {p0, v0, v6, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 670
    .end local v6    # "contentValues":Landroid/content/ContentValues;
    .end local v9    # "matchedContact":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_5
    iget v0, v7, Lcom/navdy/client/app/framework/models/Destination;->id:I

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getCacheKeysForDestinationId(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 671
    .local v5, "cachedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, v7, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    iget-object v1, v7, Lcom/navdy/client/app/framework/models/Destination;->streetNumber:Ljava/lang/String;

    iget-object v2, v7, Lcom/navdy/client/app/framework/models/Destination;->streetName:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getMatchingContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;Ljava/util/List;Ljava/util/ArrayList;)Lcom/navdy/client/app/framework/models/ContactModel;

    move-result-object v9

    .restart local v9    # "matchedContact":Lcom/navdy/client/app/framework/models/ContactModel;
    goto :goto_1

    .line 687
    .end local v5    # "cachedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No matching contact for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static insertNewDestinationInCache(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 9
    .param p0, "location"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1539
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return-void

    .line 1543
    :cond_1
    sget-object v6, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CACHE: Inserting the following destination in the cache: location = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | destination = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1545
    if-eqz p1, :cond_2

    iget v6, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-gtz v6, :cond_2

    .line 1546
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Trying to insert a non null destination that has no ID! This should never happen"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1549
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1550
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 1551
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1553
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 1557
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->removeOldestEntryIfCacheMaxSizeExceeded()V

    .line 1559
    if-eqz p1, :cond_3

    iget v3, p1, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 1560
    .local v3, "destinationId":I
    :goto_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1561
    .local v1, "contentValues":Landroid/content/ContentValues;
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 1562
    .local v4, "now":J
    const-string v6, "last_response_date"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1563
    const-string v6, "location"

    invoke-virtual {v1, v6, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    const-string v6, "destination_id"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1566
    sget-object v6, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 1559
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v3    # "destinationId":I
    .end local v4    # "now":J
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public static isHomeSet()Z
    .locals 1

    .prologue
    .line 1229
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getHome()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isMatchingContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/models/Address;)Z
    .locals 5
    .param p0, "address"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "streetNumber"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "streetName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "contactAddress"    # Lcom/navdy/client/app/framework/models/Address;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 837
    invoke-virtual {p3}, Lcom/navdy/client/app/framework/models/Address;->getFullAddress()Ljava/lang/String;

    move-result-object v1

    .line 838
    .local v1, "fullAddress":Ljava/lang/String;
    iget-object v0, p3, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    .line 840
    .local v0, "contactStreet":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Comparing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 841
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Comparing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 843
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 844
    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 845
    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->cleanUpAddressForComparison(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 847
    invoke-static {p0, v1, v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->isSomewhatMatchingAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 848
    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    invoke-static {v0, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 850
    :cond_0
    invoke-static {v1, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 851
    invoke-static {v1, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isSomewhatMatchingAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "cachedAddress"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "fullAddress"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "contactStreet"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 869
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Comparing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 870
    invoke-static {p1, p0}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 871
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 872
    invoke-static {p0, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->containsAfterTrim(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWorkSet()Z
    .locals 1

    .prologue
    .line 1233
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getWork()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyChangeSafely(Landroid/net/Uri;)V
    .locals 2
    .param p1, "_uri"    # Landroid/net/Uri;

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContentResolverSafely()Landroid/content/ContentResolver;

    move-result-object v0

    .line 423
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 424
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 426
    :cond_0
    return-void
.end method

.method public static removeFromSearchHistory(I)I
    .locals 7
    .param p0, "id"    # I
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1424
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1425
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1426
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1428
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1429
    const/4 v2, -0x1

    .line 1432
    :goto_0
    return v2

    :cond_0
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1434
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1432
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method private static removeOldestEntryIfCacheMaxSizeExceeded()V
    .locals 14
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1571
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1572
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    .line 1573
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1575
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1605
    :goto_0
    return-void

    .line 1580
    :cond_0
    const/4 v8, 0x0

    .line 1582
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "last_response_date ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1589
    if-eqz v8, :cond_3

    .line 1590
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 1591
    .local v7, "count":I
    int-to-long v2, v7

    const-wide/16 v4, 0x3e8

    sub-long v12, v2, v4

    .line 1592
    .local v12, "nbEntriesToRemove":J
    const-wide/16 v2, 0x0

    cmp-long v1, v12, v2

    if-lez v1, :cond_1

    .line 1593
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CACHE: Too many entries in the cache. Removing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1595
    :cond_1
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    int-to-long v2, v9

    cmp-long v1, v2, v12

    if-gez v1, :cond_3

    .line 1596
    invoke-interface {v8, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1597
    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1598
    .local v10, "id":I
    invoke-static {v10}, Lcom/navdy/client/app/providers/NavdyContentProvider;->removeThisCacheEntry(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1595
    .end local v10    # "id":I
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1603
    .end local v7    # "count":I
    .end local v9    # "i":I
    .end local v12    # "nbEntriesToRemove":J
    :cond_3
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method private static removeOutOfDateCacheEntries()V
    .locals 12
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1490
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1491
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1492
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1494
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_1

    .line 1507
    :cond_0
    :goto_0
    return-void

    .line 1498
    :cond_1
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 1499
    .local v6, "now":J
    sget-wide v8, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->MAX_DESTINATION_CACHE_AGE:J

    sub-long v2, v6, v8

    .line 1500
    .local v2, "limit":J
    sget-object v5, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    const-string v8, "last_response_date<? AND destination_id = -1"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    .line 1503
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 1500
    invoke-virtual {v0, v5, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1504
    .local v4, "nbDeleted":I
    if-lez v4, :cond_0

    .line 1505
    sget-object v5, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CACHE: Removed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " old entry(s) from the cache"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static removeThisCacheEntry(I)V
    .locals 7
    .param p0, "id"    # I
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1609
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 1610
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1611
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1613
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-nez v0, :cond_0

    .line 1620
    :goto_0
    return-void

    .line 1617
    :cond_0
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 1619
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1617
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static restrictSelectionToFavorites(Landroid/util/Pair;)Landroid/util/Pair;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1200
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v0, "is_special <> 0"

    .line 1201
    .local v0, "isFavoriteClause":Ljava/lang/String;
    if-eqz p0, :cond_0

    iget-object v2, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1202
    new-instance v1, Landroid/util/Pair;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is_special <> 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .end local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .local v1, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    move-object p0, v1

    .line 1207
    .end local v1    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :goto_0
    return-object p0

    .line 1205
    :cond_0
    new-instance p0, Landroid/util/Pair;

    .end local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v2, "is_special <> 0"

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    goto :goto_0
.end method

.method public static restrictSelectionToRecents(Landroid/util/Pair;)Landroid/util/Pair;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1158
    .local p0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    iget-object v1, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1159
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "last_routed_date"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> 0)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .end local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .local v0, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    move-object p0, v0

    .line 1164
    .end local v0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :goto_0
    return-object p0

    .line 1162
    :cond_0
    new-instance p0, Landroid/util/Pair;

    .end local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    const-string v1, "last_routed_date <> 0"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local p0    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 445
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTableName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 446
    .local v1, "tableName":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 447
    .local v0, "rowsDeleted":I
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->notifyChangeSafely(Landroid/net/Uri;)V

    .line 448
    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 286
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 300
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/destinations"

    .line 298
    :goto_0
    return-object v0

    .line 290
    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/trips"

    goto :goto_0

    .line 292
    :pswitch_2
    const-string v0, "vnd.android.cursor.dir/search_history"

    goto :goto_0

    .line 294
    :pswitch_3
    const-string v0, "vnd.android.cursor.dir/destination_cache"

    goto :goto_0

    .line 296
    :pswitch_4
    const-string v0, "vnd.android.cursor.dir/playlists"

    goto :goto_0

    .line 298
    :pswitch_5
    const-string v0, "vnd.android.cursor.dir/playlist_members"

    goto :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 430
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTableName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "tableName":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5, v1, v4, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 432
    .local v2, "rowId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_0

    .line 433
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContentUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 434
    .local v0, "baseUri":Landroid/net/Uri;
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 435
    .local v4, "uri_to_new_data":Landroid/net/Uri;
    invoke-direct {p0, v4}, Lcom/navdy/client/app/providers/NavdyContentProvider;->notifyChangeSafely(Landroid/net/Uri;)V

    .line 440
    .end local v0    # "baseUri":Landroid/net/Uri;
    .end local v4    # "uri_to_new_data":Landroid/net/Uri;
    :goto_0
    return-object v4

    .line 438
    :cond_0
    sget-object v5, Lcom/navdy/client/app/providers/NavdyContentProvider;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Insert failed !"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getSqlDb(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 401
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 402
    .local v0, "queryBuilder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTableNameForQuery(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 403
    .local v9, "tableName":Ljava/lang/String;
    invoke-virtual {v0, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 404
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getValues(Landroid/net/Uri;)Ljava/util/HashMap;

    move-result-object v10

    .line 405
    .local v10, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v0, v10}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 407
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 409
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-direct {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getContentResolverSafely()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 410
    return-object v8
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 453
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTableName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 454
    .local v1, "tableName":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/providers/NavdyContentProvider;->sqlDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 455
    .local v0, "rowsUpdated":I
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->notifyChangeSafely(Landroid/net/Uri;)V

    .line 456
    return v0
.end method
