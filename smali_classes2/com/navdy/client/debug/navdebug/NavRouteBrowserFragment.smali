.class public Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
.super Landroid/app/ListFragment;
.source "NavRouteBrowserFragment.java"

# interfaces
.implements Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# static fields
.field private static final DEFAULT_DESTINATION:[D

.field private static final DEFAULT_START_POINT:[D

.field public static final EXTRA_COORDS_END:Ljava/lang/String; = "dest_coords"

.field public static final EXTRA_COORDS_START:Ljava/lang/String; = "start_coords"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mCalculatingRoute:Z

.field protected mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field private mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

.field protected mEngineAvailable:Z

.field protected mEngineInitialized:Z

.field protected mManeuverArrayAdapter:Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;

.field protected mManeuvers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;"
        }
    .end annotation
.end field

.field private mRoute:Lcom/here/android/mpa/routing/Route;

.field private mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 39
    new-array v0, v2, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->DEFAULT_START_POINT:[D

    .line 40
    new-array v0, v2, [D

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->DEFAULT_DESTINATION:[D

    return-void

    .line 39
    nop

    :array_0
    .array-data 8
        0x4048a1487768166aL    # 49.260024
        -0x3fa13f8d92fb19e7L    # -123.006984
    .end array-data

    .line 40
    :array_1
    .array-data 8
        0x40489fc89f40a287L    # 49.24831
        -0x3fa14147778dd617L    # -122.980013
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mRoute:Lcom/here/android/mpa/routing/Route;

    .line 73
    return-void
.end method

.method public static latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D
    .locals 4
    .param p0, "coordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 94
    const/4 v1, 0x2

    new-array v0, v1, [D

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 95
    .local v0, "geoCoordArray":[D
    return-object v0
.end method

.method public static newInstance(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
    .locals 4
    .param p0, "startCoord"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p1, "endCoord"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 57
    new-instance v1, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    invoke-direct {v1}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;-><init>()V

    .line 58
    .local v1, "fragment":Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    .local v0, "args":Landroid/os/Bundle;
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 60
    :cond_0
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "createIntentWithCoords: bad coordinates"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 61
    const/4 v1, 0x0

    .line 68
    .end local v1    # "fragment":Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
    :goto_0
    return-object v1

    .line 64
    .restart local v1    # "fragment":Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
    :cond_1
    const-string v2, "start_coords"

    invoke-static {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 65
    const-string v2, "dest_coords"

    invoke-static {p1}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 67
    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public buildRoute()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 172
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mRoute:Lcom/here/android/mpa/routing/Route;

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mCalculatingRoute:Z

    if-ne v3, v4, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-boolean v3, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEngineAvailable:Z

    if-eqz v3, :cond_0

    .line 180
    iput-boolean v4, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mCalculatingRoute:Z

    .line 181
    new-instance v0, Lcom/here/android/mpa/routing/RouteManager;

    invoke-direct {v0}, Lcom/here/android/mpa/routing/RouteManager;-><init>()V

    .line 183
    .local v0, "routeManager":Lcom/here/android/mpa/routing/RouteManager;
    new-instance v2, Lcom/here/android/mpa/routing/RoutePlan;

    invoke-direct {v2}, Lcom/here/android/mpa/routing/RoutePlan;-><init>()V

    .line 184
    .local v2, "routePlan":Lcom/here/android/mpa/routing/RoutePlan;
    new-instance v1, Lcom/here/android/mpa/routing/RouteOptions;

    invoke-direct {v1}, Lcom/here/android/mpa/routing/RouteOptions;-><init>()V

    .line 185
    .local v1, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setTransportMode(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 186
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 187
    invoke-virtual {v2, v1}, Lcom/here/android/mpa/routing/RoutePlan;->setRouteOptions(Lcom/here/android/mpa/routing/RouteOptions;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 189
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 190
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 192
    new-instance v3, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment$1;-><init>(Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/routing/RouteManager;->calculateRoute(Lcom/here/android/mpa/routing/RoutePlan;Lcom/here/android/mpa/routing/RouteManager$Listener;)Lcom/here/android/mpa/routing/RouteManager$Error;

    goto :goto_0
.end method

.method public initEngine()V
    .locals 3

    .prologue
    .line 119
    iget-boolean v1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEngineInitialized:Z

    if-eqz v1, :cond_0

    .line 120
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Already inited."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 125
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->getInstance()Lcom/here/android/mpa/common/MapEngine;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lcom/here/android/mpa/common/MapEngine;->init(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to init MapEngine"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected initializePosition()V
    .locals 2

    .prologue
    .line 145
    invoke-static {}, Lcom/here/android/mpa/common/PositioningManager;->getInstance()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v0

    .line 146
    .local v0, "positioningManager":Lcom/here/android/mpa/common/PositioningManager;
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->addListener(Ljava/lang/ref/WeakReference;)V

    .line 147
    invoke-virtual {v0}, Lcom/here/android/mpa/common/PositioningManager;->isActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    sget-object v1, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->GPS_NETWORK:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->start(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;)Z

    .line 150
    :cond_0
    return-void
.end method

.method public loadArguments()V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "start_coords"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    move-result-object v1

    .line 77
    .local v1, "startLatLon":[D
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "dest_coords"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    move-result-object v0

    .line 79
    .local v0, "endLatLon":[D
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eq v2, v4, :cond_1

    .line 80
    :cond_0
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing valid start coordinates. Falling back."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 81
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->DEFAULT_START_POINT:[D

    .line 84
    :cond_1
    if-eqz v0, :cond_2

    array-length v2, v0

    if-eq v2, v4, :cond_3

    .line 85
    :cond_2
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing end coordinates. Falling back."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->DEFAULT_DESTINATION:[D

    .line 89
    :cond_3
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    aget-wide v4, v1, v8

    aget-wide v6, v1, v9

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 90
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    aget-wide v4, v0, v8

    aget-wide v6, v0, v9

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 91
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->loadArguments()V

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->initEngine()V

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mManeuvers:Ljava/util/ArrayList;

    .line 108
    new-instance v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mManeuvers:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 109
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    const v0, 0x7f030093

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onEngineAvailable()V
    .locals 0

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->buildRoute()V

    .line 164
    return-void
.end method

.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 134
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne p1, v0, :cond_0

    .line 135
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "MapEngine initialized."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEngineInitialized:Z

    .line 137
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->initializePosition()V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MapEngine init completed with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPositionFixChanged(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "locationStatus"    # Lcom/here/android/mpa/common/PositioningManager$LocationStatus;

    .prologue
    .line 168
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPositionFixChanged: method:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public onPositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p3, "isMapMatched"    # Z

    .prologue
    .line 154
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPositionUpdated method: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 155
    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 156
    iget-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEngineAvailable:Z

    if-nez v0, :cond_0

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mEngineAvailable:Z

    .line 158
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->onEngineAvailable()V

    .line 160
    :cond_0
    return-void
.end method

.method protected setRoute(Lcom/here/android/mpa/routing/Route;)V
    .locals 3
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mRoute:Lcom/here/android/mpa/routing/Route;

    .line 218
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 219
    const-string v1, "No maneuvers in route."

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->showError(Ljava/lang/String;)V

    .line 227
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mManeuvers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 224
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->mManeuvers:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getManeuvers()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 225
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;

    .line 226
    .local v0, "adapter":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;
    invoke-virtual {v0}, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected showError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 230
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 232
    return-void
.end method
