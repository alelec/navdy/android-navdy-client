.class public Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "DebugActionsFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/DebugActionsFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/DebugActionsFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100232

    const-string v2, "field \'mTextViewCurrentSimSpeed\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/DebugActionsFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f10022e

    const-string v2, "method \'onShowScreenClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/DebugActionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const v1, 0x7f100231

    const-string v2, "method \'onShowScreenClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$2;-><init>(Lcom/navdy/client/debug/DebugActionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    const v1, 0x7f100230

    const-string v2, "method \'onShowScreenClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$3;-><init>(Lcom/navdy/client/debug/DebugActionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v1, 0x7f10022f

    const-string v2, "method \'onShowScreenClicked\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$4;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/DebugActionsFragment$$ViewInjector$4;-><init>(Lcom/navdy/client/debug/DebugActionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/DebugActionsFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/DebugActionsFragment;

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    .line 52
    return-void
.end method
