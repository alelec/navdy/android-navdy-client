.class Lcom/navdy/client/debug/SubmitTicketFragment$1;
.super Ljava/lang/Object;
.source "SubmitTicketFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/SubmitTicketFragment;->onSubmit(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/SubmitTicketFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/SubmitTicketFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 143
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 144
    .local v1, "applicationContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/PathManager;->getLogsFolder()Ljava/lang/String;

    move-result-object v3

    .line 146
    .local v3, "logsFolder":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$000()Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".txt"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 147
    .local v7, "fileName":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$102(Lcom/navdy/client/debug/SubmitTicketFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$100(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/service/library/util/LogUtils;->dumpLog(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    new-instance v0, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    sget-object v2, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    const-wide/16 v4, 0x3a98

    new-instance v6, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;

    invoke-direct {v6, p0, v3}, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment$1;Ljava/lang/String;)V

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 188
    .local v0, "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    invoke-virtual {v0}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;->pullFile()Z

    .line 189
    .end local v0    # "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v8

    .line 151
    .local v8, "t":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v6, 0x7f080160

    invoke-virtual {v5, v6}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error collecting device logs"

    invoke-virtual {v2, v4, v5, v6}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
