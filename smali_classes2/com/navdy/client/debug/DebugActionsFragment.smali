.class public Lcom/navdy/client/debug/DebugActionsFragment;
.super Landroid/app/ListFragment;
.source "DebugActionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/DebugActionsFragment$ListItem;
    }
.end annotation


# static fields
.field static final RANDOM_STREETS:[Ljava/lang/String;

.field private static final RETRY_INTERVAL_FOR_SUPPORT_TICKETS:J

.field static final SCREENS:[Lcom/navdy/service/library/events/ui/Screen;

.field private static final SIM_SPEED_CHANGE_STEP:I = 0x1

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;

.field static final screenMap:Landroid/util/SparseIntArray;

.field private static simulationSpeed:I


# instance fields
.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field protected mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

.field protected mLastDirectionIdx:I

.field protected mListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/DebugActionsFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field

.field mTextViewCurrentSimSpeed:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100232
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "TestActivity"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 57
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/debug/DebugActionsFragment;->RETRY_INTERVAL_FOR_SUPPORT_TICKETS:J

    .line 60
    const/16 v0, 0x12

    sput v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    .line 62
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Divisadero St."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Embarcadero St."

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MacArthur Blvd"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "El Camino Real"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Cesar Chavez St."

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "287 east alviso / milpitas"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->RANDOM_STREETS:[Ljava/lang/String;

    .line 335
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    .line 338
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    const v1, 0x7f10022e

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 339
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    const v1, 0x7f10022f

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_HYBRID_MAP:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 340
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    const v1, 0x7f100230

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 341
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    const v1, 0x7f100231

    sget-object v2, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/ui/Screen;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 344
    invoke-static {}, Lcom/navdy/service/library/events/ui/Screen;->values()[Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->SCREENS:[Lcom/navdy/service/library/events/ui/Screen;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 385
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mLastDirectionIdx:I

    .line 73
    return-void
.end method

.method private static changeSimulationSpeed(I)V
    .locals 3
    .param p0, "diff"    # I

    .prologue
    .line 403
    sget v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    add-int/2addr v0, p0

    sput v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    .line 404
    sget v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    if-gtz v0, :cond_0

    .line 405
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to set simulation speed to value less than or equal to 0 (reset to 1): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 407
    const/4 v0, 0x1

    sput v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    .line 409
    :cond_0
    return-void
.end method

.method public static getSimulationSpeed()I
    .locals 1

    .prologue
    .line 399
    sget v0, Lcom/navdy/client/debug/DebugActionsFragment;->simulationSpeed:I

    return v0
.end method

.method private lookupScreen(I)Lcom/navdy/service/library/events/ui/Screen;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 347
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->SCREENS:[Lcom/navdy/service/library/events/ui/Screen;

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment;->screenMap:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private onOTASendLargeFile()V
    .locals 1

    .prologue
    .line 300
    const-class v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 301
    return-void
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 378
    iget-object v1, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 380
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 381
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 383
    :cond_0
    return-void
.end method

.method private updateSimSpeed()V
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Simulation speed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " m/s "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x400f295f    # 2.2369f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MPH"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    return-void
.end method


# virtual methods
.method protected getRandomGoogleNowEvent()Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 372
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    .line 373
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v5, "AskNavdy"

    .line 374
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    move-object v4, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    move-object v10, v3

    move-object v11, v3

    move-object v13, v3

    invoke-direct/range {v0 .. v13}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-object v0
.end method

.method protected getRandomNavigationEvent()Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
    .locals 12

    .prologue
    .line 388
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment;->RANDOM_STREETS:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    .line 390
    .local v9, "randomStreetIndex":I
    iget v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mLastDirectionIdx:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/navdy/service/library/events/navigation/NavigationTurn;->values()[Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-result-object v1

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mLastDirectionIdx:I

    .line 391
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->RANDOM_STREETS:[Ljava/lang/String;

    aget-object v4, v0, v9

    .line 393
    .local v4, "randomStreet":Ljava/lang/String;
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    const-string v1, "current road"

    invoke-static {}, Lcom/navdy/service/library/events/navigation/NavigationTurn;->values()[Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-result-object v2

    iget v3, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mLastDirectionIdx:I

    aget-object v2, v2, v3

    const-string v3, "1 mi"

    const-string v5, "1:10"

    const-string v6, "10 mph"

    const/4 v7, 0x0

    const-wide/16 v10, 0x0

    .line 394
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/Long;)V

    return-object v0
.end method

.method protected getRandomNotificationEvent()Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 14

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 365
    const-string v3, "408-393-6782"

    .line 366
    .local v3, "phoneNumber":Ljava/lang/String;
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    .line 367
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_INCOMING_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v5, "Are you on the way?"

    .line 368
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, v4

    move-object v11, v3

    move-object v13, v4

    invoke-direct/range {v0 .. v13}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-object v0
.end method

.method public onBrowseVideosClicked()V
    .locals 1

    .prologue
    .line 304
    const-class v0, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 305
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->values()[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mListItems:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x1090016

    const v3, 0x1020014

    .line 84
    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->values()[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 83
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 85
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const v1, 0x7f030088

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 91
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 93
    new-instance v1, Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-direct {v1, v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    .line 95
    return-object v0
.end method

.method public onCustomNotificationClick()V
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCustomNotificationClick"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 267
    const-class v0, Lcom/navdy/client/debug/CustomNotificationFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 268
    return-void
.end method

.method public onFileTransferTest()V
    .locals 2

    .prologue
    .line 326
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onFileTransferTest"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 327
    const-class v0, Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 328
    return-void
.end method

.method public onHereNavigationClick()V
    .locals 1

    .prologue
    .line 308
    const-class v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 309
    return-void
.end method

.method public onHideContextMenu()V
    .locals 2

    .prologue
    .line 291
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onHideContextMenu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 292
    new-instance v0, Lcom/navdy/service/library/events/ui/DismissScreen;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 293
    return-void
.end method

.method public onHideOptionsView()V
    .locals 2

    .prologue
    .line 281
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onHideOptionsView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 282
    new-instance v0, Lcom/navdy/service/library/events/ui/DismissScreen;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 283
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 160
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 161
    invoke-virtual {p1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 163
    iget-object v1, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mListItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 165
    .local v0, "clickedItem":Lcom/navdy/client/debug/DebugActionsFragment$ListItem;
    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$1;->$SwitchMap$com$navdy$client$debug$DebugActionsFragment$ListItem:[I

    invoke-virtual {v0}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 244
    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unhandled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 247
    :goto_0
    :pswitch_0
    return-void

    .line 167
    :pswitch_1
    new-instance v1, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/connection/DisconnectRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 170
    :pswitch_2
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendNavigationEvent()V

    goto :goto_0

    .line 173
    :pswitch_3
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onHereNavigationClick()V

    goto :goto_0

    .line 176
    :pswitch_4
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onScreenTestingClick()V

    goto :goto_0

    .line 179
    :pswitch_5
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onBrowseVideosClicked()V

    goto :goto_0

    .line 182
    :pswitch_6
    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/DebugActionsFragment;->onSimSpeedChangeClick(I)V

    goto :goto_0

    .line 185
    :pswitch_7
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/DebugActionsFragment;->onSimSpeedChangeClick(I)V

    goto :goto_0

    .line 188
    :pswitch_8
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendGoogleNowEvent()V

    goto :goto_0

    .line 191
    :pswitch_9
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onCustomNotificationClick()V

    goto :goto_0

    .line 194
    :pswitch_a
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onShowOpionsView()V

    goto :goto_0

    .line 197
    :pswitch_b
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onHideOptionsView()V

    goto :goto_0

    .line 200
    :pswitch_c
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onShowContextMenu()V

    goto :goto_0

    .line 203
    :pswitch_d
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onHideContextMenu()V

    goto :goto_0

    .line 206
    :pswitch_e
    invoke-direct {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onOTASendLargeFile()V

    goto :goto_0

    .line 210
    :pswitch_f
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-class v2, Lcom/navdy/client/debug/MockedAddressPickerFragment;

    .line 209
    invoke-static {v1, v2}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    goto :goto_0

    .line 215
    :pswitch_10
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onShowDrivePlayback()V

    goto :goto_0

    .line 218
    :pswitch_11
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onStopDrivePlayback()V

    goto :goto_0

    .line 221
    :pswitch_12
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->onFileTransferTest()V

    goto :goto_0

    .line 224
    :pswitch_13
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_EXPERIMENTAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setFeatureMode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V

    goto :goto_0

    .line 227
    :pswitch_14
    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/DebugActionsFragment;->sendAudioStatusEvent(Z)V

    goto :goto_0

    .line 230
    :pswitch_15
    invoke-virtual {p0, v4}, Lcom/navdy/client/debug/DebugActionsFragment;->sendAudioStatusEvent(Z)V

    goto :goto_0

    .line 233
    :pswitch_16
    sget-wide v2, Lcom/navdy/client/debug/DebugActionsFragment;->RETRY_INTERVAL_FOR_SUPPORT_TICKETS:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->scheduleAlarmForSubmittingPendingTickets(J)V

    .line 234
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Scheduling alarm to submit tickets in 1 minutes"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 237
    :pswitch_17
    invoke-static {v3}, Lcom/zendesk/logger/Logger;->setLoggable(Z)V

    .line 238
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Zendesk SDK Logger enabled"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 241
    :pswitch_18
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/DebugActionsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onPause()V

    .line 101
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 102
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 109
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 111
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 113
    iget-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onResume()V

    .line 114
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->refreshUI()V

    .line 115
    return-void
.end method

.method public onScreenTestingClick()V
    .locals 2

    .prologue
    .line 271
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onScreenTestingClick"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 272
    const-class v0, Lcom/navdy/client/debug/ScreenTestingFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 273
    return-void
.end method

.method public onSendDismissClick()V
    .locals 2

    .prologue
    .line 262
    new-instance v0, Lcom/navdy/service/library/events/ui/DismissScreen;

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_NOTIFICATION:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 263
    return-void
.end method

.method public onShowContextMenu()V
    .locals 2

    .prologue
    .line 286
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onShowContextMenu"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 287
    new-instance v0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_CONTEXT_MENU:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 288
    return-void
.end method

.method public onShowDrivePlayback()V
    .locals 2

    .prologue
    .line 317
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onShowDrivePlayback"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 318
    const-class v0, Lcom/navdy/client/debug/DrivePlaybackFragment;

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->pushFullScreenFragment(Ljava/lang/Class;)V

    .line 319
    return-void
.end method

.method public onShowOpionsView()V
    .locals 2

    .prologue
    .line 276
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onShowOptionsView"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 277
    new-instance v0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_OPTIONS:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 278
    return-void
.end method

.method public onShowScreenClicked(Landroid/view/View;)V
    .locals 3
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10022e,
            0x7f100231,
            0x7f100230,
            0x7f10022f
        }
    .end annotation

    .prologue
    .line 352
    new-instance v1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/DebugActionsFragment;->lookupScreen(I)Lcom/navdy/service/library/events/ui/Screen;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    .line 353
    .local v0, "event":Lcom/navdy/service/library/events/ui/ShowScreen;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 354
    return-void
.end method

.method public onSimSpeedChangeClick(I)V
    .locals 0
    .param p1, "change"    # I

    .prologue
    .line 312
    invoke-static {p1}, Lcom/navdy/client/debug/DebugActionsFragment;->changeSimulationSpeed(I)V

    .line 313
    invoke-direct {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->updateSimSpeed()V

    .line 314
    return-void
.end method

.method public onStopDrivePlayback()V
    .locals 1

    .prologue
    .line 322
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->getInstance()Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->sendStopDrivePlaybackEvent()V

    .line 323
    return-void
.end method

.method protected pushFullScreenFragment(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p1, "fragmentClass":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Fragment;>;"
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 332
    return-void
.end method

.method protected refreshUI()V
    .locals 0

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->updateSimSpeed()V

    .line 358
    return-void
.end method

.method public sendAudioStatusEvent(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    .line 295
    new-instance v1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;-><init>()V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isConnected(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/AudioStatus$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->build()Lcom/navdy/service/library/events/audio/AudioStatus;

    move-result-object v0

    .line 296
    .local v0, "event":Lcom/navdy/service/library/events/audio/AudioStatus;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 297
    return-void
.end method

.method protected sendGoogleNowEvent()V
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getRandomGoogleNowEvent()Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 259
    return-void
.end method

.method protected sendNavigationEvent()V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getRandomNavigationEvent()Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 255
    return-void
.end method

.method protected sendNotificationEvent()V
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/navdy/client/debug/DebugActionsFragment;->getRandomNotificationEvent()Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/DebugActionsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 251
    return-void
.end method
