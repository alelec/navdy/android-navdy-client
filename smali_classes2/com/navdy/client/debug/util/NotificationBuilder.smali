.class public Lcom/navdy/client/debug/util/NotificationBuilder;
.super Ljava/lang/Object;
.source "NotificationBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildSmsNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 14
    .param p0, "contactName"    # Ljava/lang/String;
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "cannotReplyBack"    # Z

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    if-eqz p0, :cond_0

    move-object v3, p0

    :goto_0
    if-eqz p0, :cond_1

    move-object v4, p1

    .line 25
    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, ""

    :goto_2
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 32
    invoke-static/range {p3 .. p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    const/4 v13, 0x0

    move-object v11, p1

    invoke-direct/range {v0 .. v13}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-object v0

    :cond_0
    move-object v3, p1

    .line 21
    goto :goto_0

    :cond_1
    const-string v4, ""

    goto :goto_1

    :cond_2
    move-object/from16 v5, p2

    .line 25
    goto :goto_2
.end method
