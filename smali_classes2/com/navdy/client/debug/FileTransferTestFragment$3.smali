.class Lcom/navdy/client/debug/FileTransferTestFragment$3;
.super Ljava/lang/Object;
.source "FileTransferTestFragment.java"

# interfaces
.implements Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/FileTransferTestFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/FileTransferTestFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/FileTransferTestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 3
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 172
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/FileTransferTestFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transfer Error :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-static {v0, p2}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$400(Lcom/navdy/client/debug/FileTransferTestFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/FileTransferTestFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    invoke-static {}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error in file transfer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/FileTransferTestFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-static {v0}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$300(Lcom/navdy/client/debug/FileTransferTestFragment;)V

    goto :goto_0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    iget-object v0, v0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    .line 163
    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    .line 162
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment$3;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$400(Lcom/navdy/client/debug/FileTransferTestFragment;Ljava/lang/String;)V

    goto :goto_0
.end method
