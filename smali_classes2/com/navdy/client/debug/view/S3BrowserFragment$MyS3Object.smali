.class Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
.super Ljava/lang/Object;
.source "S3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyS3Object"
.end annotation


# instance fields
.field private final isFolder:Z

.field private final key:Ljava/lang/String;

.field private final size:J


# direct methods
.method constructor <init>(Lcom/amazonaws/services/s3/model/S3ObjectSummary;)V
    .locals 2
    .param p1, "summary"    # Lcom/amazonaws/services/s3/model/S3ObjectSummary;

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/S3ObjectSummary;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->key:Ljava/lang/String;

    .line 291
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/S3ObjectSummary;->getSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->size:J

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->isFolder:Z

    .line 293
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "commonPrefix"    # Ljava/lang/String;

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->key:Ljava/lang/String;

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->isFolder:Z

    .line 286
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->size:J

    .line 287
    return-void
.end method


# virtual methods
.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 304
    iget-wide v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->size:J

    return-wide v0
.end method

.method public isFolder()Z
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->isFolder:Z

    return v0
.end method
