.class Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$2;
.super Ljava/lang/Object;
.source "MockLocationTransmitter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$2;->this$0:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/location/Location;Landroid/location/Location;)I
    .locals 5
    .param p1, "lhs"    # Landroid/location/Location;
    .param p2, "rhs"    # Landroid/location/Location;

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    .line 94
    .local v0, "lh":J
    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    .line 95
    .local v2, "rh":J
    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v4, -0x1

    :goto_0
    return v4

    :cond_0
    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 90
    check-cast p1, Landroid/location/Location;

    check-cast p2, Landroid/location/Location;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$2;->compare(Landroid/location/Location;Landroid/location/Location;)I

    move-result v0

    return v0
.end method
