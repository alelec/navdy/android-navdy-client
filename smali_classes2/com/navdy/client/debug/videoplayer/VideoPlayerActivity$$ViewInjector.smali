.class public Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "VideoPlayerActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f1000d9

    const-string v2, "field \'videoView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/VideoView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    .line 12
    const v1, 0x7f1000da

    const-string v2, "field \'downloadStatus\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadStatus:Landroid/widget/TextView;

    .line 14
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    .line 18
    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadStatus:Landroid/widget/TextView;

    .line 19
    return-void
.end method
