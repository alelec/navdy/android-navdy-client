.class final enum Lcom/navdy/client/debug/DebugActionsFragment$ListItem;
.super Ljava/lang/Enum;
.source "DebugActionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/DebugActionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/DebugActionsFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum BROWSE_VIDEOS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum DECREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum DUMMY_EVENT0:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum DUMMY_EVENT1:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum ENABLE_EXPERIMENTAL_FEATURE_MODE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum ENABLE_ZENDESK_LOGGING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum FILE_TRANSFER_PERF_TEST:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum HERE_NAVIGATION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum HIDE_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum HIDE_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum INCREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum MOCK_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum NAV_CORD_TEST_SUITE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum NOTIFICATION_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum OTA_SEND_LARGE_FILE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum RESET_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SCREEN_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SEND_GOOGLE_NOW_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SEND_HEAD_UNIT_CONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SEND_HEAD_UNIT_DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SEND_NAVIGATION_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SET_RETRY_TIME_FOR_SUPPORT_TICKETS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SHOW_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum SHOW_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

.field public static final enum STOP_DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "DISCONNECT"

    const-string v2, "Disconnect"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 119
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SCREEN_TESTING"

    const-string v2, "Screen testing"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SCREEN_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 120
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SEND_NAVIGATION_EVENT"

    const-string v2, "Send navigation event"

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_NAVIGATION_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 121
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "BROWSE_VIDEOS"

    const-string v2, "Browse Videos"

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->BROWSE_VIDEOS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 122
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "DUMMY_EVENT0"

    const-string v2, " "

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DUMMY_EVENT0:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 123
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "HERE_NAVIGATION"

    const/4 v2, 0x5

    const-string v3, "Here Nav Test"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HERE_NAVIGATION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 124
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SEND_GOOGLE_NOW_EVENT"

    const/4 v2, 0x6

    const-string v3, "Send google now notification event"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_GOOGLE_NOW_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 125
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "DUMMY_EVENT1"

    const/4 v2, 0x7

    const-string v3, " "

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DUMMY_EVENT1:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 126
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "INCREASE_SIM_SPEED"

    const/16 v2, 0x8

    const-string v3, "Increase simulation speed +1"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->INCREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 127
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "DECREASE_SIM_SPEED"

    const/16 v2, 0x9

    const-string v3, "Decrease simulation speed -1"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DECREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 128
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "MOCK_CURRENT_POSITION"

    const/16 v2, 0xa

    const-string v3, "Mock current GPS position"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->MOCK_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 129
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "RESET_CURRENT_POSITION"

    const/16 v2, 0xb

    const-string v3, "Reset current GPS position"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->RESET_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 130
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "NOTIFICATION_TESTING"

    const/16 v2, 0xc

    const-string v3, "Notification Testing"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->NOTIFICATION_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 131
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SHOW_OPTIONS_VIEW"

    const/16 v2, 0xd

    const-string v3, "Show Options View"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SHOW_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 132
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "HIDE_OPTIONS_VIEW"

    const/16 v2, 0xe

    const-string v3, "Hide Options View"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HIDE_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 133
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SHOW_CONTEXT_MENU"

    const/16 v2, 0xf

    const-string v3, "Show Context Menu"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SHOW_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 134
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "HIDE_CONTEXT_MENU"

    const/16 v2, 0x10

    const-string v3, "Hide Context Menu"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HIDE_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 135
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "OTA_SEND_LARGE_FILE"

    const/16 v2, 0x11

    const-string v3, "Send Large File (OTA test)"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->OTA_SEND_LARGE_FILE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 136
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "DRIVE_PLAYBACK"

    const/16 v2, 0x12

    const-string v3, "Drive Playback"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 137
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "STOP_DRIVE_PLAYBACK"

    const/16 v2, 0x13

    const-string v3, "Stop Drive Playback"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->STOP_DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 138
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "FILE_TRANSFER_PERF_TEST"

    const/16 v2, 0x14

    const-string v3, "File Transfer Performance Test"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->FILE_TRANSFER_PERF_TEST:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 139
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "ENABLE_EXPERIMENTAL_FEATURE_MODE"

    const/16 v2, 0x15

    const-string v3, "Enable Experimental Feature Mode"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->ENABLE_EXPERIMENTAL_FEATURE_MODE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 140
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SEND_HEAD_UNIT_CONNECT"

    const/16 v2, 0x16

    const-string v3, "Send head unit connection event"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_HEAD_UNIT_CONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 141
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SEND_HEAD_UNIT_DISCONNECT"

    const/16 v2, 0x17

    const-string v3, "Send head unit disconnect event"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_HEAD_UNIT_DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 142
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "SET_RETRY_TIME_FOR_SUPPORT_TICKETS"

    const/16 v2, 0x18

    const-string v3, "Reschedule Support Ticket Alarm To 1 Min"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SET_RETRY_TIME_FOR_SUPPORT_TICKETS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 143
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "ENABLE_ZENDESK_LOGGING"

    const/16 v2, 0x19

    const-string v3, "Enables Zendesk SDK Logger"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->ENABLE_ZENDESK_LOGGING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 144
    new-instance v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    const-string v1, "NAV_CORD_TEST_SUITE"

    const/16 v2, 0x1a

    const-string v3, "Navigation Coord Test Suite"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->NAV_CORD_TEST_SUITE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    .line 117
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SCREEN_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_NAVIGATION_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->BROWSE_VIDEOS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DUMMY_EVENT0:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HERE_NAVIGATION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_GOOGLE_NOW_EVENT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DUMMY_EVENT1:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->INCREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DECREASE_SIM_SPEED:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->MOCK_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->RESET_CURRENT_POSITION:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->NOTIFICATION_TESTING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SHOW_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HIDE_OPTIONS_VIEW:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SHOW_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->HIDE_CONTEXT_MENU:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->OTA_SEND_LARGE_FILE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->STOP_DRIVE_PLAYBACK:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->FILE_TRANSFER_PERF_TEST:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->ENABLE_EXPERIMENTAL_FEATURE_MODE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_HEAD_UNIT_CONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SEND_HEAD_UNIT_DISCONNECT:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->SET_RETRY_TIME_FOR_SUPPORT_TICKETS:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->ENABLE_ZENDESK_LOGGING:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->NAV_CORD_TEST_SUITE:Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "item"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 148
    iput-object p3, p0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->mText:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/DebugActionsFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 117
    const-class v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/DebugActionsFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/navdy/client/debug/DebugActionsFragment$ListItem;->mText:Ljava/lang/String;

    return-object v0
.end method
