.class public final Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public identifier:Ljava/lang/String;

.field public photo:Lokio/ByteString;

.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoUpdate;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 74
    if-nez p1, :cond_0

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->identifier:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo:Lokio/ByteString;

    .line 77
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/photo/PhotoUpdate;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->checkRequiredFields()V

    .line 98
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdate;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdate;

    move-result-object v0

    return-object v0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->identifier:Ljava/lang/String;

    .line 82
    return-object p0
.end method

.method public photo(Lokio/ByteString;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    .locals 0
    .param p1, "photo"    # Lokio/ByteString;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo:Lokio/ByteString;

    .line 87
    return-object p0
.end method

.method public photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    .locals 0
    .param p1, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 92
    return-object p0
.end method
