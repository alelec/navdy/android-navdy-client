.class public final enum Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;
.super Ljava/lang/Enum;
.source "DialBondRequest.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/dial/DialBondRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

.field public static final enum DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

.field public static final enum DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

.field public static final enum DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 79
    new-instance v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    const-string v1, "DIAL_BOND"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 83
    new-instance v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    const-string v1, "DIAL_CLEAR_BOND"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 87
    new-instance v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    const-string v1, "DIAL_REBOND"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 74
    new-array v0, v5, [Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->$VALUES:[Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 92
    iput p3, p0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->value:I

    .line 93
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->$VALUES:[Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->value:I

    return v0
.end method
