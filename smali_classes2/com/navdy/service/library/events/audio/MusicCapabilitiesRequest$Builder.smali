.class public final Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCapabilitiesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;)V
    .locals 0
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 34
    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCapabilitiesRequest;

    move-result-object v0

    return-object v0
.end method
