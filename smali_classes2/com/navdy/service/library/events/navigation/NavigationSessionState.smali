.class public final enum Lcom/navdy/service/library/events/navigation/NavigationSessionState;
.super Ljava/lang/Enum;
.source "NavigationSessionState.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionState;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_ARRIVED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_ERROR:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_LOCATION_SERVICES_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_REROUTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final enum NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_ENGINE_NOT_READY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_LOCATION_SERVICES_NOT_READY"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_LOCATION_SERVICES_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_STOPPED"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_STARTED"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_PAUSED"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 32
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ERROR:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_REROUTED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_REROUTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 40
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_ENGINE_READY"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 44
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const-string v1, "NAV_SESSION_ARRIVED"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ARRIVED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 7
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_LOCATION_SERVICES_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ERROR:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_REROUTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ARRIVED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->value:I

    .line 50
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/navigation/NavigationSessionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->value:I

    return v0
.end method
