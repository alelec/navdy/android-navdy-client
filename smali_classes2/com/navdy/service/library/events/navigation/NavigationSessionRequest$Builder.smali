.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public originDisplay:Ljava/lang/Boolean;

.field public routeId:Ljava/lang/String;

.field public simulationSpeed:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 105
    if-nez p1, :cond_0

    .line 111
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 107
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->label:Ljava/lang/String;

    .line 108
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->routeId:Ljava/lang/String;

    .line 109
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->simulationSpeed:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->simulationSpeed:Ljava/lang/Integer;

    .line 110
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;->originDisplay:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->checkRequiredFields()V

    .line 156
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->label:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public newState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .locals 0
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->newState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 118
    return-object p0
.end method

.method public originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .locals 0
    .param p1, "originDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    .line 150
    return-object p0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->routeId:Ljava/lang/String;

    .line 134
    return-object p0
.end method

.method public simulationSpeed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;
    .locals 0
    .param p1, "simulationSpeed"    # Ljava/lang/Integer;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest$Builder;->simulationSpeed:Ljava/lang/Integer;

    .line 142
    return-object p0
.end method
