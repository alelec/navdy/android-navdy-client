.class public final enum Lcom/navdy/service/library/events/RequestStatus;
.super Ljava/lang/Enum;
.source "RequestStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/RequestStatus;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

.field public static final enum REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_SUCCESS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_NOT_READY"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_NO_LOCATION_SERVICE"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_SERVICE_ERROR"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_INVALID_REQUEST"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    .line 32
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_INVALID_STATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_UNKNOWN_ERROR"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    .line 40
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_VERSION_IS_CURRENT"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    .line 44
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_NOT_AVAILABLE"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    .line 48
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_ALREADY_IN_PROGRESS"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 52
    new-instance v0, Lcom/navdy/service/library/events/RequestStatus;

    const-string v1, "REQUEST_CANCELLED"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/RequestStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    .line 7
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/navdy/service/library/events/RequestStatus;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_READY:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NO_LOCATION_SERVICE:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_REQUEST:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_INVALID_STATE:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_NOT_AVAILABLE:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_ALREADY_IN_PROGRESS:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/RequestStatus;->$VALUES:[Lcom/navdy/service/library/events/RequestStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/navdy/service/library/events/RequestStatus;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/RequestStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/RequestStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/RequestStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/RequestStatus;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->$VALUES:[Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/RequestStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/RequestStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/navdy/service/library/events/RequestStatus;->value:I

    return v0
.end method
