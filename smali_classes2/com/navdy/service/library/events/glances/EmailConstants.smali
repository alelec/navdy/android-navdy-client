.class public final enum Lcom/navdy/service/library/events/glances/EmailConstants;
.super Ljava/lang/Enum;
.source "EmailConstants.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/EmailConstants;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_TO_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

.field public static final enum EMAIL_TO_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_FROM_EMAIL"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_FROM_NAME"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_TO_EMAIL"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_TO_NAME"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_SUBJECT"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    const-string v1, "EMAIL_BODY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/glances/EmailConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/EmailConstants;

    sget-object v1, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/EmailConstants;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/navdy/service/library/events/glances/EmailConstants;->value:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/EmailConstants;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/EmailConstants;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/EmailConstants;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/glances/EmailConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/EmailConstants;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/EmailConstants;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/navdy/service/library/events/glances/EmailConstants;->value:I

    return v0
.end method
