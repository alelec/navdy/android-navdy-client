.class public final Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LocalPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/LocalPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/LocalPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public manualZoom:Ljava/lang/Boolean;

.field public manualZoomLevel:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 83
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/LocalPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 87
    if-nez p1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoom:Ljava/lang/Boolean;

    .line 89
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoomLevel:Ljava/lang/Float;

    .line 90
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/LocalPreferences;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/LocalPreferences;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;Lcom/navdy/service/library/events/preferences/LocalPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/LocalPreferences;

    move-result-object v0

    return-object v0
.end method

.method public clockFormat(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
    .locals 0
    .param p1, "clockFormat"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 114
    return-object p0
.end method

.method public manualZoom(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
    .locals 0
    .param p1, "manualZoom"    # Ljava/lang/Boolean;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoom:Ljava/lang/Boolean;

    .line 98
    return-object p0
.end method

.method public manualZoomLevel(Ljava/lang/Float;)Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
    .locals 0
    .param p1, "manualZoomLevel"    # Ljava/lang/Float;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoomLevel:Ljava/lang/Float;

    .line 106
    return-object p0
.end method
