.class public final enum Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
.super Ljava/lang/Enum;
.source "DriverProfilePreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialLongPressAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

.field public static final enum DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

.field public static final enum DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 556
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    const-string v1, "DIAL_LONG_PRESS_VOICE_ASSISTANT"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 557
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    const-string v1, "DIAL_LONG_PRESS_PLACE_SEARCH"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 554
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 562
    iput p3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->value:I

    .line 563
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 554
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .locals 1

    .prologue
    .line 554
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 567
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->value:I

    return v0
.end method
