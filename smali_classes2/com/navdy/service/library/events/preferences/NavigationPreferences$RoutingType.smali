.class public final enum Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;
.super Ljava/lang/Enum;
.source "NavigationPreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/NavigationPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RoutingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

.field public static final enum ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

.field public static final enum ROUTING_SHORTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 311
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    const-string v1, "ROUTING_FASTEST"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 312
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    const-string v1, "ROUTING_SHORTEST"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_SHORTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 309
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_SHORTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->$VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 316
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 317
    iput p3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->value:I

    .line 318
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 309
    const-class v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->$VALUES:[Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->value:I

    return v0
.end method
