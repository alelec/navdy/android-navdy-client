.class public final enum Lcom/navdy/service/library/events/file/FileType;
.super Ljava/lang/Enum;
.source "FileType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/file/FileType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/file/FileType;

.field public static final enum FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

.field public static final enum FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

.field public static final enum FILE_TYPE_PERF_TEST:Lcom/navdy/service/library/events/file/FileType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/file/FileType;

    const-string v1, "FILE_TYPE_OTA"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/file/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/file/FileType;

    const-string v1, "FILE_TYPE_LOGS"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/file/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/file/FileType;

    const-string v1, "FILE_TYPE_PERF_TEST"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/file/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_PERF_TEST:Lcom/navdy/service/library/events/file/FileType;

    .line 7
    new-array v0, v5, [Lcom/navdy/service/library/events/file/FileType;

    sget-object v1, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_LOGS:Lcom/navdy/service/library/events/file/FileType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_PERF_TEST:Lcom/navdy/service/library/events/file/FileType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/file/FileType;->$VALUES:[Lcom/navdy/service/library/events/file/FileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/navdy/service/library/events/file/FileType;->value:I

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/file/FileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/file/FileType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/file/FileType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->$VALUES:[Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/file/FileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/file/FileType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/navdy/service/library/events/file/FileType;->value:I

    return v0
.end method
