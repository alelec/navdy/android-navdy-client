.class public final Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AutoCompleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/AutoCompleteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/AutoCompleteResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public partialSearch:Ljava/lang/String;

.field public results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 96
    if-nez p1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->partialSearch:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->partialSearch:Ljava/lang/String;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 99
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 100
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->results:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->results:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/AutoCompleteResponse;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->checkRequiredFields()V

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/AutoCompleteResponse;-><init>(Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;Lcom/navdy/service/library/events/places/AutoCompleteResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->build()Lcom/navdy/service/library/events/places/AutoCompleteResponse;

    move-result-object v0

    return-object v0
.end method

.method public partialSearch(Ljava/lang/String;)Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    .locals 0
    .param p1, "partialSearch"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->partialSearch:Ljava/lang/String;

    .line 108
    return-object p0
.end method

.method public results(Ljava/util/List;)Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->results:Ljava/util/List;

    .line 132
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 116
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 124
    return-object p0
.end method
