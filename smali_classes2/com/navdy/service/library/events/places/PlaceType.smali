.class public final enum Lcom/navdy/service/library/events/places/PlaceType;
.super Ljava/lang/Enum;
.source "PlaceType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/places/PlaceType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_AIRPORT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_BANK:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_BAR:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_CALENDAR_EVENT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_CONTACT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_ENTERTAINMENT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_GYM:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_PARK:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_SCHOOL:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_TRANSIT:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final enum PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_ATM"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_BANK"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BANK:Lcom/navdy/service/library/events/places/PlaceType;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_SCHOOL"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_SCHOOL:Lcom/navdy/service/library/events/places/PlaceType;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_STORE"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_GAS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_AIRPORT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_AIRPORT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_PARKING"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    .line 17
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_TRANSIT"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_TRANSIT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_BAR"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BAR:Lcom/navdy/service/library/events/places/PlaceType;

    .line 19
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_ENTERTAINMENT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ENTERTAINMENT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_HOSPITAL"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    .line 21
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_COFFEE"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    .line 22
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_RESTAURANT"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 23
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_GYM"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GYM:Lcom/navdy/service/library/events/places/PlaceType;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_PARK"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARK:Lcom/navdy/service/library/events/places/PlaceType;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_CONTACT"

    const/16 v2, 0x10

    const/16 v3, 0x3e9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CONTACT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 29
    new-instance v0, Lcom/navdy/service/library/events/places/PlaceType;

    const-string v1, "PLACE_TYPE_CALENDAR_EVENT"

    const/16 v2, 0x11

    const/16 v3, 0x3ea

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlaceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CALENDAR_EVENT:Lcom/navdy/service/library/events/places/PlaceType;

    .line 7
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/navdy/service/library/events/places/PlaceType;

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ATM:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BANK:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_SCHOOL:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_STORE:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GAS:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_AIRPORT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARKING:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_TRANSIT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_BAR:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_ENTERTAINMENT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_HOSPITAL:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_COFFEE:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_RESTAURANT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_GYM:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_PARK:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CONTACT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_CALENDAR_EVENT:Lcom/navdy/service/library/events/places/PlaceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/places/PlaceType;->$VALUES:[Lcom/navdy/service/library/events/places/PlaceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/navdy/service/library/events/places/PlaceType;->value:I

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/places/PlaceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/places/PlaceType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/places/PlaceType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->$VALUES:[Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/places/PlaceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/places/PlaceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/navdy/service/library/events/places/PlaceType;->value:I

    return v0
.end method
