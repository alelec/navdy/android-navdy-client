.class public final Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DestinationSelectedRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/DestinationSelectedRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public destination:Lcom/navdy/service/library/events/destination/Destination;

.field public request_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 71
    if-nez p1, :cond_0

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->request_id:Ljava/lang/String;

    .line 73
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;-><init>(Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;Lcom/navdy/service/library/events/places/DestinationSelectedRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->build()Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    move-result-object v0

    return-object v0
.end method

.method public destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 89
    return-object p0
.end method

.method public request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedRequest$Builder;->request_id:Ljava/lang/String;

    .line 81
    return-object p0
.end method
