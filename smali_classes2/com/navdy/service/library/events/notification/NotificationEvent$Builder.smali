.class public final Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;"
        }
    .end annotation
.end field

.field public appId:Ljava/lang/String;

.field public appName:Ljava/lang/String;

.field public cannotReplyBack:Ljava/lang/Boolean;

.field public category:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public iconResourceName:Ljava/lang/String;

.field public id:Ljava/lang/Integer;

.field public imageResourceName:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public photo:Lokio/ByteString;

.field public sourceIdentifier:Ljava/lang/String;

.field public subtitle:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 186
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationEvent;

    .prologue
    .line 189
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 190
    if-nez p1, :cond_0

    .line 204
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->id:Ljava/lang/Integer;

    .line 192
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 193
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->title:Ljava/lang/String;

    .line 194
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->subtitle:Ljava/lang/String;

    .line 195
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->message:Ljava/lang/String;

    .line 196
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appId:Ljava/lang/String;

    .line 197
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/notification/NotificationEvent;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->actions:Ljava/util/List;

    .line 198
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->photo:Lokio/ByteString;

    .line 199
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->iconResourceName:Ljava/lang/String;

    .line 200
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->imageResourceName:Ljava/lang/String;

    .line 201
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->sourceIdentifier:Ljava/lang/String;

    .line 202
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->cannotReplyBack:Ljava/lang/Boolean;

    .line 203
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public actions(Ljava/util/List;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;)",
            "Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationAction;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->actions:Ljava/util/List;

    .line 244
    return-object p0
.end method

.method public appId(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appId:Ljava/lang/String;

    .line 236
    return-object p0
.end method

.method public appName(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appName:Ljava/lang/String;

    .line 291
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/notification/NotificationEvent;
    .locals 2

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->checkRequiredFields()V

    .line 297
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;Lcom/navdy/service/library/events/notification/NotificationEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v0

    return-object v0
.end method

.method public cannotReplyBack(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "cannotReplyBack"    # Ljava/lang/Boolean;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->cannotReplyBack:Ljava/lang/Boolean;

    .line 283
    return-object p0
.end method

.method public category(Lcom/navdy/service/library/events/notification/NotificationCategory;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "category"    # Lcom/navdy/service/library/events/notification/NotificationCategory;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 213
    return-object p0
.end method

.method public iconResourceName(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "iconResourceName"    # Ljava/lang/String;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->iconResourceName:Ljava/lang/String;

    .line 260
    return-object p0
.end method

.method public id(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/Integer;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->id:Ljava/lang/Integer;

    .line 208
    return-object p0
.end method

.method public imageResourceName(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "imageResourceName"    # Ljava/lang/String;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->imageResourceName:Ljava/lang/String;

    .line 265
    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->message:Ljava/lang/String;

    .line 228
    return-object p0
.end method

.method public photo(Lokio/ByteString;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "photo"    # Lokio/ByteString;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->photo:Lokio/ByteString;

    .line 252
    return-object p0
.end method

.method public sourceIdentifier(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "sourceIdentifier"    # Ljava/lang/String;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->sourceIdentifier:Ljava/lang/String;

    .line 274
    return-object p0
.end method

.method public subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "subtitle"    # Ljava/lang/String;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->subtitle:Ljava/lang/String;

    .line 223
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->title:Ljava/lang/String;

    .line 218
    return-object p0
.end method
