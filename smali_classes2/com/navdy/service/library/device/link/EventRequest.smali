.class public Lcom/navdy/service/library/device/link/EventRequest;
.super Ljava/lang/Object;
.source "EventRequest.java"


# instance fields
.field public final eventCompleteHandler:Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

.field public final eventData:[B

.field private final handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;[BLcom/navdy/service/library/device/RemoteDevice$PostEventHandler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "eventData"    # [B
    .param p3, "eventCompleteHandler"    # Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/navdy/service/library/device/link/EventRequest;->eventData:[B

    .line 19
    iput-object p3, p0, Lcom/navdy/service/library/device/link/EventRequest;->eventCompleteHandler:Lcom/navdy/service/library/device/RemoteDevice$PostEventHandler;

    .line 20
    iput-object p1, p0, Lcom/navdy/service/library/device/link/EventRequest;->handler:Landroid/os/Handler;

    .line 21
    return-void
.end method


# virtual methods
.method public callCompletionHandlers(Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/service/library/device/link/EventRequest;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/service/library/device/link/EventRequest$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/service/library/device/link/EventRequest$1;-><init>(Lcom/navdy/service/library/device/link/EventRequest;Lcom/navdy/service/library/device/RemoteDevice$PostEventStatus;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 32
    return-void
.end method
