.class public Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;
.super Ljava/lang/Object;
.source "BTDeviceBroadcaster.java"

# interfaces
.implements Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;


# static fields
.field public static final DISCOVERY_TIMEOUT:I = 0x12c

.field public static final DISPLAY_NAME:Ljava/lang/String; = " Navdy Display"

.field public static final DISPLAY_PATTERN:Ljava/util/regex/Pattern;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private adapter:Landroid/bluetooth/BluetoothAdapter;

.field private started:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 24
    const-string v0, "navdy|hud"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->DISPLAY_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->adapter:Landroid/bluetooth/BluetoothAdapter;

    .line 28
    return-void
.end method

.method private ensureDisplayLike()V
    .locals 4

    .prologue
    .line 70
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->isDisplay(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->adapter:Landroid/bluetooth/BluetoothAdapter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Navdy Display"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->setName(Ljava/lang/String;)Z

    .line 75
    :cond_0
    return-void
.end method

.method public static isDisplay(Ljava/lang/String;)Z
    .locals 2
    .param p0, "bluetoothName"    # Ljava/lang/String;

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->DISPLAY_PATTERN:Ljava/util/regex/Pattern;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    return v0
.end method

.method private setScanMode(II)V
    .locals 7
    .param p1, "mode"    # I
    .param p2, "timeout"    # I

    .prologue
    .line 61
    :try_start_0
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->adapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 62
    .local v0, "classBtAdapter":Ljava/lang/Class;
    const-string v3, "setScanMode"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 63
    .local v2, "methodSetScanMode":Ljava/lang/reflect/Method;
    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->adapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v0    # "classBtAdapter":Ljava/lang/Class;
    .end local v2    # "methodSetScanMode":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enableDiscovery failed - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public start()Z
    .locals 2

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->started:Z

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->started:Z

    .line 44
    const/16 v0, 0x17

    const/16 v1, 0x12c

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->setScanMode(II)V

    .line 46
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->started:Z

    return v0
.end method

.method public stop()Z
    .locals 2

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->started:Z

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->started:Z

    .line 54
    const/16 v0, 0x14

    const/16 v1, 0x12c

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->setScanMode(II)V

    .line 56
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
