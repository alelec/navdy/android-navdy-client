.class public abstract Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;
.super Lcom/navdy/service/library/util/Listenable;
.source "RemoteDeviceScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$Listener;,
        Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$ScanEventDispatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/navdy/service/library/util/Listenable",
        "<",
        "Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$Listener;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/service/library/util/Listenable;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->mContext:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method protected dispatchOnDiscovered(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 2
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 55
    const/4 v1, 0x1

    new-array v0, v1, [Lcom/navdy/service/library/device/connection/ConnectionInfo;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 56
    .local v0, "connectionArray":[Lcom/navdy/service/library/device/connection/ConnectionInfo;
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->dispatchOnDiscovered(Ljava/util/List;)V

    .line 57
    return-void
.end method

.method protected dispatchOnDiscovered(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    new-instance v0, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$3;-><init>(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 66
    return-void
.end method

.method protected dispatchOnLost(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    new-instance v0, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$4;-><init>(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 75
    return-void
.end method

.method protected dispatchOnScanStarted()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$2;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$2;-><init>(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 52
    return-void
.end method

.method protected dispatchOnScanStopped()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner$1;-><init>(Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;)V

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/RemoteDeviceScanner;->dispatchToListeners(Lcom/navdy/service/library/util/Listenable$EventDispatcher;)V

    .line 43
    return-void
.end method

.method public abstract startScan()Z
.end method

.method public abstract stopScan()Z
.end method
