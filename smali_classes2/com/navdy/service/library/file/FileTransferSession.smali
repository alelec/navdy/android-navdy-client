.class public Lcom/navdy/service/library/file/FileTransferSession;
.super Ljava/lang/Object;
.source "FileTransferSession.java"


# static fields
.field public static final CHUNK_SIZE:I = 0x20000

.field public static final PULL_CHUNK_SIZE:I = 0x4000

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mAlreadyTransferred:J

.field mDataCheckSum:Ljava/security/MessageDigest;

.field mDestinationFolder:Ljava/lang/String;

.field mExpectedChunk:I

.field mFile:Ljava/io/File;

.field mFileName:Ljava/lang/String;

.field private mFileReader:Ljava/io/FileInputStream;

.field mFileSize:J

.field private mFileType:Lcom/navdy/service/library/events/file/FileType;

.field private mIsTestTransfer:Z

.field private mLastActivity:J

.field private mPullRequest:Z

.field private mReadBuffer:[B

.field mTotalBytesTransferred:J

.field mTransferId:I

.field private mWaitForAcknowledgements:Z

.field negotiatedOffset:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/file/FileTransferSession;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "mTransferId"    # I

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    .line 41
    iput v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J

    .line 46
    iput-boolean v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mPullRequest:Z

    .line 55
    iput-boolean v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mIsTestTransfer:Z

    .line 61
    iput p1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    .line 62
    return-void
.end method

.method private declared-synchronized verifyTestDataChunk(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileTransferData;Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;)Lcom/navdy/service/library/events/file/FileTransferStatus;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Lcom/navdy/service/library/events/file/FileTransferData;
    .param p3, "responseBuilder"    # Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    if-eq v1, v2, :cond_0

    .line 237
    sget-object v1, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal chunk,  Expected :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", received:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 238
    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 264
    :goto_0
    monitor-exit p0

    return-object v1

    .line 240
    :cond_0
    :try_start_1
    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 241
    iget-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTotalBytesTransferred:J

    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->size()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTotalBytesTransferred:J

    .line 242
    iget-object v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDataCheckSum:Ljava/security/MessageDigest;

    iget-object v2, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 245
    :cond_1
    iget-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTotalBytesTransferred:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 246
    iget v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 247
    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 248
    iget-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTotalBytesTransferred:J

    iget-wide v4, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 249
    sget-object v1, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Not the last chunk, data missing. Did not receive full file"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 250
    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v1

    goto :goto_0

    .line 252
    :cond_2
    iget-object v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDataCheckSum:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "finalChecksum":Ljava/lang/String;
    iget-object v1, p2, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 254
    sget-object v1, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received last chunk, Transfer complete, final data size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 255
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 264
    .end local v0    # "finalChecksum":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v1

    goto/16 :goto_0

    .line 257
    .restart local v0    # "finalChecksum":Ljava/lang/String;
    :cond_4
    sget-object v1, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\""

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 258
    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {p3, v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto/16 :goto_0

    .line 236
    .end local v0    # "finalChecksum":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized appendChunk(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileTransferData;)Lcom/navdy/service/library/events/file/FileTransferStatus;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Lcom/navdy/service/library/events/file/FileTransferData;

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J

    .line 273
    const/4 v4, 0x0

    .line 275
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    new-instance v7, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    invoke-direct {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;-><init>()V

    iget v8, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    .line 276
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    const-wide/16 v8, 0x0

    .line 277
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    const/4 v8, 0x0

    .line 278
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    const/4 v8, 0x0

    .line 279
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    iget-object v8, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    .line 280
    invoke-virtual {v7, v8}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v6

    .line 281
    .local v6, "responseBuilder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    iget-boolean v7, p0, Lcom/navdy/service/library/file/FileTransferSession;->mIsTestTransfer:Z

    if-eqz v7, :cond_0

    .line 282
    invoke-direct {p0, p1, p2, v6}, Lcom/navdy/service/library/file/FileTransferSession;->verifyTestDataChunk(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileTransferData;Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;)Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 327
    :goto_0
    monitor-exit p0

    return-object v7

    .line 284
    :cond_0
    const/4 v1, 0x0

    .line 285
    .local v1, "error":Lcom/navdy/service/library/events/file/FileTransferError;
    :try_start_1
    new-instance v2, Ljava/io/File;

    iget-object v7, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    invoke-direct {v2, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 287
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Session not initiated "

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 288
    sget-object v7, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v7

    goto :goto_0

    .line 290
    :cond_1
    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget v8, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    if-eq v7, v8, :cond_2

    .line 291
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Illegal chunk,  Expected :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", received:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 292
    sget-object v7, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    goto :goto_0

    .line 295
    :cond_2
    :try_start_2
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Starting to write chunk %d into file %s from folder %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p2, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    .line 297
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    .line 295
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 298
    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    if-eqz v7, :cond_3

    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v7}, Lokio/ByteString;->size()I

    move-result v7

    if-lez v7, :cond_3

    .line 299
    new-instance v5, Ljava/io/FileOutputStream;

    const/4 v7, 0x1

    invoke-direct {v5, v2, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 300
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .local v5, "outputStream":Ljava/io/FileOutputStream;
    :try_start_3
    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v7}, Lokio/ByteString;->toByteArray()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-object v4, v5

    .line 303
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :cond_3
    :try_start_4
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .line 304
    iget v7, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 305
    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 306
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_4

    .line 307
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Not the last chunk, data missing. Did not receive full file"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 308
    sget-object v7, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v7

    .line 324
    :try_start_5
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 325
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 272
    .end local v1    # "error":Lcom/navdy/service/library/events/file/FileTransferError;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .end local v6    # "responseBuilder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 310
    .restart local v1    # "error":Lcom/navdy/service/library/events/file/FileTransferError;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "responseBuilder":Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    :cond_4
    :try_start_6
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 311
    .local v3, "finalChecksumForTheFile":Ljava/lang/String;
    iget-object v7, p2, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 312
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Written last chunk to the file , Transfer complete, final file size "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 313
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 324
    .end local v3    # "finalChecksumForTheFile":Ljava/lang/String;
    :cond_5
    :try_start_7
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 325
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 327
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v7

    goto/16 :goto_0

    .line 315
    .restart local v3    # "finalChecksumForTheFile":Ljava/lang/String;
    :cond_6
    :try_start_8
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\""

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p2, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 316
    sget-object v7, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v7

    .line 324
    :try_start_9
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 325
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 320
    .end local v3    # "finalChecksumForTheFile":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    :try_start_a
    sget-object v7, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Error writing the chunk to the file"

    invoke-virtual {v7, v8, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 322
    sget-object v7, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v7

    .line 324
    :try_start_b
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 325
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 324
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v7

    :goto_2
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->fileSync(Ljava/io/FileOutputStream;)V

    .line 325
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 324
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 320
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v0

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public declared-synchronized endSession(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deleteFile"    # Z

    .prologue
    .line 368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileReader:Ljava/io/FileInputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 369
    iget-boolean v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mPullRequest:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 370
    iget-object v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 372
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    monitor-exit p0

    return-void

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFileType()Lcom/navdy/service/library/events/file/FileType;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    return-object v0
.end method

.method public declared-synchronized getLastActivity()J
    .locals 2

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNextChunk()Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    add-int/lit8 v9, v1, 0x1

    iput v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 338
    .local v1, "chunkId":I
    const/16 v2, 0x4000

    .line 339
    .local v2, "chunkSize":I
    iget-wide v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    iget-wide v12, p0, Lcom/navdy/service/library/file/FileTransferSession;->mAlreadyTransferred:J

    sub-long v6, v10, v12

    .line 340
    .local v6, "remainingBytes":J
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-gez v9, :cond_0

    .line 341
    new-instance v9, Ljava/io/EOFException;

    const-string v10, "End of file, no more chunk"

    invoke-direct {v9, v10}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    .end local v1    # "chunkId":I
    .end local v2    # "chunkSize":I
    .end local v6    # "remainingBytes":J
    :catch_0
    move-exception v8

    .line 360
    .local v8, "t":Ljava/lang/Throwable;
    :try_start_1
    sget-object v9, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Exception while reading the next chunk "

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    iget-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileReader:Ljava/io/FileInputStream;

    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 363
    const/4 v9, 0x0

    .end local v8    # "t":Ljava/lang/Throwable;
    :goto_0
    monitor-exit p0

    return-object v9

    .line 343
    .restart local v1    # "chunkId":I
    .restart local v2    # "chunkSize":I
    .restart local v6    # "remainingBytes":J
    :cond_0
    move v5, v2

    .line 344
    .local v5, "nextChunkSize":I
    const/4 v4, 0x0

    .line 345
    .local v4, "lastChunk":Z
    int-to-long v10, v2

    cmp-long v9, v6, v10

    if-gez v9, :cond_1

    .line 346
    long-to-int v5, v6

    .line 347
    const/4 v4, 0x1

    .line 349
    :cond_1
    :try_start_2
    iget-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B

    array-length v9, v9

    if-eq v9, v5, :cond_3

    .line 350
    :cond_2
    new-array v9, v5, [B

    iput-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B

    .line 354
    :cond_3
    iget-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileReader:Ljava/io/FileInputStream;

    iget-object v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B

    invoke-virtual {v9, v10}, Ljava/io/FileInputStream;->read([B)I

    move-result v0

    .line 355
    .local v0, "bytesRead":I
    iget-wide v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mAlreadyTransferred:J

    int-to-long v12, v0

    add-long/2addr v10, v12

    iput-wide v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mAlreadyTransferred:J

    .line 356
    new-instance v9, Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    invoke-direct {v9}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;-><init>()V

    iget v10, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    .line 357
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v3

    .line 358
    .local v3, "fileTransferDataBuilder":Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    iget-object v9, p0, Lcom/navdy/service/library/file/FileTransferSession;->mReadBuffer:[B

    invoke-static {v9}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->dataBytes(Lokio/ByteString;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v9

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->lastChunk(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferData;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    goto :goto_0

    .line 337
    .end local v0    # "bytesRead":I
    .end local v1    # "chunkId":I
    .end local v2    # "chunkSize":I
    .end local v3    # "fileTransferDataBuilder":Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .end local v4    # "lastChunk":Z
    .end local v5    # "nextChunkSize":I
    .end local v6    # "remainingBytes":J
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9
.end method

.method public declared-synchronized handleFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)Z
    .locals 4
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 268
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mAlreadyTransferred:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized initFileTransfer(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/navdy/service/library/events/file/FileTransferResponse;
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "destinationFolder"    # Ljava/lang/String;
    .param p4, "destinationFileName"    # Ljava/lang/String;
    .param p5, "size"    # J
    .param p7, "offsetRequested"    # J
    .param p9, "checkSum"    # Ljava/lang/String;
    .param p10, "override"    # Z

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 106
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J

    .line 107
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :try_start_2
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    .line 109
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    .line 110
    move-wide/from16 v0, p5

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    .line 111
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/navdy/service/library/file/FileTransferSession;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 112
    monitor-enter p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 113
    const/16 v20, 0x0

    :try_start_3
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 114
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 115
    :try_start_4
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v6, "destinationDirectory":Ljava/io/File;
    const/16 v16, 0x0

    .line 117
    .local v16, "responseCheckSum":Ljava/lang/String;
    new-instance v4, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v4}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    .line 119
    .local v4, "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 120
    move-object/from16 v0, p0

    iget v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 122
    const-wide/16 v14, 0x0

    .line 123
    .local v14, "localOffset":J
    const-wide/16 v10, 0x0

    .line 124
    .local v10, "finalOffset":J
    :try_start_5
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p4

    invoke-direct {v8, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 125
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 126
    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v20

    if-nez v20, :cond_0

    .line 128
    sget-object v20, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v20

    .line 205
    .end local v8    # "file":Ljava/io/File;
    :goto_0
    monitor-exit p0

    return-object v20

    .line 107
    .end local v4    # "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .end local v6    # "destinationDirectory":Ljava/io/File;
    .end local v10    # "finalOffset":J
    .end local v14    # "localOffset":J
    .end local v16    # "responseCheckSum":Ljava/lang/String;
    :catchall_0
    move-exception v20

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v20
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 105
    :catchall_1
    move-exception v20

    monitor-exit p0

    throw v20

    .line 114
    :catchall_2
    move-exception v20

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v20
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 130
    .restart local v4    # "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .restart local v6    # "destinationDirectory":Ljava/io/File;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v10    # "finalOffset":J
    .restart local v14    # "localOffset":J
    .restart local v16    # "responseCheckSum":Ljava/lang/String;
    :cond_0
    :try_start_a
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 131
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Requested offset :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-wide/from16 v1, p7

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " , Already existing file size:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", override :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p10

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 132
    cmp-long v20, p7, v14

    if-gtz v20, :cond_4

    .line 134
    if-eqz p10, :cond_3

    move-wide/from16 v10, p7

    .line 136
    :goto_1
    const-wide/16 v20, 0x0

    cmp-long v20, v10, v20

    if-lez v20, :cond_1

    .line 138
    move-wide/from16 v0, p7

    invoke-static {v8, v0, v1}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v13

    .line 139
    .local v13, "hash":Ljava/lang/String;
    move-object/from16 v0, p9

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 141
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "Checksum mismatch, local file does not match remote file"

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 142
    const-wide/16 v10, 0x0

    .line 149
    .end local v13    # "hash":Ljava/lang/String;
    :cond_1
    :goto_2
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "New proposed offset :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 150
    sub-long v20, v14, v10

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_2

    .line 152
    const/4 v9, 0x0

    .line 153
    .local v9, "fos":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 155
    .local v5, "channel":Ljava/nio/channels/FileChannel;
    :try_start_b
    new-instance v12, Ljava/io/FileOutputStream;

    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-direct {v12, v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 156
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .local v12, "fos":Ljava/io/FileOutputStream;
    :try_start_c
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 157
    invoke-virtual {v5, v10, v11}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 162
    :try_start_d
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 163
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 165
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Truncated the file , previous size :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", new size :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 178
    .end local v5    # "channel":Ljava/nio/channels/FileChannel;
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    .line 179
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Final offset proposed "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 182
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v20, v0

    sub-long v18, p5, v20

    .line 183
    .local v18, "totalSize":J
    invoke-static/range {p3 .. p3}, Lcom/navdy/service/library/util/IOUtils;->getFreeSpace(Ljava/lang/String;)J

    move-result-wide v20

    cmp-long v20, v20, v18

    if-gtz v20, :cond_6

    .line 185
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Not enough space to write the file, space required :"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 186
    sget-object v20, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-result-object v20

    goto/16 :goto_0

    .end local v18    # "totalSize":J
    :cond_3
    move-wide v10, v14

    .line 134
    goto/16 :goto_1

    .line 147
    :cond_4
    move-wide v10, v14

    goto/16 :goto_2

    .line 158
    .restart local v5    # "channel":Ljava/nio/channels/FileChannel;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v17

    .line 159
    .local v17, "t":Ljava/lang/Throwable;
    :goto_3
    :try_start_e
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Exception truncating the file to offset "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 160
    sget-object v20, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    move-result-object v20

    .line 162
    :try_start_f
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 163
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_0

    .line 195
    .end local v5    # "channel":Ljava/nio/channels/FileChannel;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v17    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v7

    .line 196
    .local v7, "e":Ljava/lang/Exception;
    :try_start_10
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "Error processing FileTransferRequest "

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 197
    sget-object v20, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    move-result-object v20

    goto/16 :goto_0

    .line 162
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v5    # "channel":Ljava/nio/channels/FileChannel;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v20

    :goto_4
    :try_start_11
    invoke-static {v9}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 163
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v20

    .line 170
    .end local v5    # "channel":Ljava/nio/channels/FileChannel;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    :cond_5
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "File does not exists, creating new file"

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 171
    const-wide/16 v10, 0x0

    .line 173
    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    move-result v20

    if-nez v20, :cond_2

    .line 174
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "Permission denied"

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 175
    sget-object v20, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v20

    goto/16 :goto_0

    .line 189
    .restart local v18    # "totalSize":J
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v20, v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    cmp-long v20, v20, p7

    if-nez v20, :cond_9

    .line 190
    move-object/from16 v16, p9

    .line 199
    :cond_7
    :goto_5
    const/high16 v20, 0x20000

    :try_start_12
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 200
    sget-object v20, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "FileTransferRequest successful"

    invoke-virtual/range {v20 .. v21}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    const/16 v20, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 202
    if-eqz v16, :cond_8

    .line 203
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->checksum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 205
    :cond_8
    invoke-virtual {v4}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result-object v20

    goto/16 :goto_0

    .line 191
    :cond_9
    :try_start_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_7

    .line 192
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-static {v8, v0, v1}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;J)Ljava/lang/String;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    move-result-object v16

    goto :goto_5

    .line 162
    .end local v18    # "totalSize":J
    .restart local v5    # "channel":Ljava/nio/channels/FileChannel;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    :catchall_4
    move-exception v20

    move-object v9, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 158
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v17

    move-object v9, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_3
.end method

.method public declared-synchronized initPull(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/file/FileTransferResponse;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "destinationFolder"    # Ljava/lang/String;
    .param p4, "sourceFileName"    # Ljava/lang/String;
    .param p5, "supportsFlowControl"    # Z

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    .line 77
    .local v0, "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    iput-boolean p5, p0, Lcom/navdy/service/library/file/FileTransferSession;->mWaitForAcknowledgements:Z

    .line 78
    iput-object p2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 79
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->supportsAcks(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 81
    invoke-virtual {v0, p4}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 83
    :try_start_1
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 84
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J

    .line 85
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    :try_start_3
    iput-object p3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    .line 87
    iput-object p4, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    .line 88
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    invoke-direct {v2, p3, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    .line 89
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mPullRequest:Z

    .line 90
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    .line 92
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileReader:Ljava/io/FileInputStream;

    .line 93
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    const/16 v3, 0x4000

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    iget v3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v2

    .line 99
    :goto_0
    monitor-exit p0

    return-object v2

    .line 85
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v2
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 98
    :catch_0
    move-exception v1

    .line 99
    .local v1, "t":Ljava/lang/Throwable;
    const/4 v2, 0x0

    :try_start_6
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v2

    goto :goto_0

    .line 96
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    const/4 v2, 0x0

    :try_start_7
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v2

    goto :goto_0

    .line 76
    .end local v0    # "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized initTestData(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;J)Lcom/navdy/service/library/events/file/FileTransferResponse;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "size"    # J

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mLastActivity:J

    .line 211
    const-string v2, ""

    iput-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    .line 212
    iput-object p3, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    .line 213
    iput-wide p4, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileSize:J

    .line 214
    iput-object p2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mFileType:Lcom/navdy/service/library/events/file/FileType;

    .line 215
    const/4 v2, 0x0

    iput v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mExpectedChunk:I

    .line 216
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTotalBytesTransferred:J

    .line 217
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    .line 218
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mIsTestTransfer:Z

    .line 220
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    .line 221
    .local v0, "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    invoke-virtual {v0, p2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 222
    const/high16 v2, 0x20000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/service/library/file/FileTransferSession;->negotiatedOffset:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 223
    iget v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :try_start_1
    const-string v2, "MD5"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/service/library/file/FileTransferSession;->mDataCheckSum:Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    :try_start_2
    sget-object v2, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "FileTransferRequest successful"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 231
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .line 232
    invoke-virtual {v0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    :goto_0
    monitor-exit p0

    return-object v2

    .line 226
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_3
    sget-object v2, Lcom/navdy/service/library/file/FileTransferSession;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "unable to create MD5 message digest"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 228
    sget-object v2, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 210
    .end local v0    # "builder":Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public isFlowControlEnabled()Z
    .locals 1

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/navdy/service/library/file/FileTransferSession;->mWaitForAcknowledgements:Z

    return v0
.end method
