.class Lcom/nimbusds/jose/crypto/ECDSA;
.super Ljava/lang/Object;
.source "ECDSA.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSignatureByteArrayLength(Lcom/nimbusds/jose/JWSAlgorithm;)I
    .locals 2
    .param p0, "alg"    # Lcom/nimbusds/jose/JWSAlgorithm;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 126
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const/16 v0, 0x40

    .line 136
    :goto_0
    return v0

    .line 130
    :cond_0
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    const/16 v0, 0x60

    goto :goto_0

    .line 134
    :cond_1
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    const/16 v0, 0x84

    goto :goto_0

    .line 140
    :cond_2
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    .line 142
    sget-object v1, Lcom/nimbusds/jose/crypto/ECDSAProvider;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    .line 140
    invoke-static {p0, v1}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWSAlgorithm(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getSignerAndVerifier(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/security/Provider;)Ljava/security/Signature;
    .locals 5
    .param p0, "alg"    # Lcom/nimbusds/jose/JWSAlgorithm;
    .param p1, "jcaProvider"    # Ljava/security/Provider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v2, Lcom/nimbusds/jose/JWSAlgorithm;->ES256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v2}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 88
    const-string v1, "SHA256withECDSA"

    .line 101
    .local v1, "jcaAlg":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_3

    .line 102
    :try_start_0
    invoke-static {v1, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 104
    :goto_1
    return-object v2

    .line 89
    .end local v1    # "jcaAlg":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/nimbusds/jose/JWSAlgorithm;->ES384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v2}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const-string v1, "SHA384withECDSA"

    .line 91
    .restart local v1    # "jcaAlg":Ljava/lang/String;
    goto :goto_0

    .end local v1    # "jcaAlg":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/nimbusds/jose/JWSAlgorithm;->ES512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v2}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 92
    const-string v1, "SHA512withECDSA"

    .line 93
    .restart local v1    # "jcaAlg":Ljava/lang/String;
    goto :goto_0

    .line 94
    .end local v1    # "jcaAlg":Ljava/lang/String;
    :cond_2
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    .line 97
    sget-object v3, Lcom/nimbusds/jose/crypto/ECDSAProvider;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    .line 95
    invoke-static {p0, v3}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWSAlgorithm(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    .line 94
    invoke-direct {v2, v3}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 104
    .restart local v1    # "jcaAlg":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_1

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported ECDSA algorithm: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static resolveAlgorithm(Lcom/nimbusds/jose/jwk/ECKey$Curve;)Lcom/nimbusds/jose/JWSAlgorithm;
    .locals 3
    .param p0, "curve"    # Lcom/nimbusds/jose/jwk/ECKey$Curve;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 55
    if-nez p0, :cond_0

    .line 56
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "The EC key curve is not supported, must be P256, P384 or P521"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    sget-object v0, Lcom/nimbusds/jose/jwk/ECKey$Curve;->P_256:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v0, p0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES256:Lcom/nimbusds/jose/JWSAlgorithm;

    .line 62
    :goto_0
    return-object v0

    .line 59
    :cond_1
    sget-object v0, Lcom/nimbusds/jose/jwk/ECKey$Curve;->P_384:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v0, p0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES384:Lcom/nimbusds/jose/JWSAlgorithm;

    goto :goto_0

    .line 61
    :cond_2
    sget-object v0, Lcom/nimbusds/jose/jwk/ECKey$Curve;->P_521:Lcom/nimbusds/jose/jwk/ECKey$Curve;

    invoke-virtual {v0, p0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62
    sget-object v0, Lcom/nimbusds/jose/JWSAlgorithm;->ES512:Lcom/nimbusds/jose/JWSAlgorithm;

    goto :goto_0

    .line 64
    :cond_3
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected curve: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static resolveAlgorithm(Ljava/security/interfaces/ECKey;)Lcom/nimbusds/jose/JWSAlgorithm;
    .locals 2
    .param p0, "ecKey"    # Ljava/security/interfaces/ECKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-interface {p0}, Ljava/security/interfaces/ECKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    .line 38
    .local v0, "ecParameterSpec":Ljava/security/spec/ECParameterSpec;
    invoke-static {v0}, Lcom/nimbusds/jose/jwk/ECKey$Curve;->forECParameterSpec(Ljava/security/spec/ECParameterSpec;)Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v1

    invoke-static {v1}, Lcom/nimbusds/jose/crypto/ECDSA;->resolveAlgorithm(Lcom/nimbusds/jose/jwk/ECKey$Curve;)Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v1

    return-object v1
.end method

.method public static transcodeSignatureToConcat([BI)[B
    .locals 11
    .param p0, "derSignature"    # [B
    .param p1, "outputLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 161
    array-length v7, p0

    const/16 v8, 0x8

    if-lt v7, v8, :cond_0

    const/4 v7, 0x0

    aget-byte v7, p0, v7

    const/16 v8, 0x30

    if-eq v7, v8, :cond_1

    .line 162
    :cond_0
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    const-string v8, "Invalid  ECDSA signature format"

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 166
    :cond_1
    aget-byte v7, p0, v9

    if-lez v7, :cond_5

    .line 167
    const/4 v3, 0x2

    .line 174
    .local v3, "offset":I
    :goto_0
    add-int/lit8 v7, v3, 0x1

    aget-byte v4, p0, v7

    .line 177
    .local v4, "rLength":B
    move v1, v4

    .local v1, "i":I
    :goto_1
    if-lez v1, :cond_2

    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    sub-int/2addr v7, v1

    aget-byte v7, p0, v7

    if-eqz v7, :cond_7

    .line 181
    :cond_2
    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    add-int/lit8 v7, v7, 0x1

    aget-byte v6, p0, v7

    .line 184
    .local v6, "sLength":B
    move v2, v6

    .local v2, "j":I
    :goto_2
    if-lez v2, :cond_3

    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    add-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v6

    sub-int/2addr v7, v2

    aget-byte v7, p0, v7

    if-eqz v7, :cond_8

    .line 188
    :cond_3
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 189
    .local v5, "rawLen":I
    div-int/lit8 v7, p1, 0x2

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 191
    add-int/lit8 v7, v3, -0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    array-length v8, p0

    sub-int/2addr v8, v3

    if-ne v7, v8, :cond_4

    .line 192
    add-int/lit8 v7, v3, -0x1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    add-int/lit8 v8, v4, 0x2

    add-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v6

    if-ne v7, v8, :cond_4

    .line 193
    aget-byte v7, p0, v3

    if-ne v7, v10, :cond_4

    .line 194
    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    aget-byte v7, p0, v7

    if-eq v7, v10, :cond_9

    .line 195
    :cond_4
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    const-string v8, "Invalid  ECDSA signature format"

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 168
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "offset":I
    .end local v4    # "rLength":B
    .end local v5    # "rawLen":I
    .end local v6    # "sLength":B
    :cond_5
    aget-byte v7, p0, v9

    const/16 v8, -0x7f

    if-ne v7, v8, :cond_6

    .line 169
    const/4 v3, 0x3

    .line 170
    .restart local v3    # "offset":I
    goto :goto_0

    .line 171
    .end local v3    # "offset":I
    :cond_6
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    const-string v8, "Invalid  ECDSA signature format"

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 177
    .restart local v1    # "i":I
    .restart local v3    # "offset":I
    .restart local v4    # "rLength":B
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 184
    .restart local v2    # "j":I
    .restart local v6    # "sLength":B
    :cond_8
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 198
    .restart local v5    # "rawLen":I
    :cond_9
    mul-int/lit8 v7, v5, 0x2

    new-array v0, v7, [B

    .line 200
    .local v0, "concatSignature":[B
    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    sub-int/2addr v7, v1

    sub-int v8, v5, v1

    invoke-static {p0, v7, v0, v8, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    add-int/lit8 v7, v3, 0x2

    add-int/2addr v7, v4

    add-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v6

    sub-int/2addr v7, v2

    mul-int/lit8 v8, v5, 0x2

    sub-int/2addr v8, v2

    invoke-static {p0, v7, v0, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 203
    return-object v0
.end method

.method public static transcodeSignatureToDER([B)[B
    .locals 12
    .param p0, "jwsSignature"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    .line 225
    array-length v9, p0

    div-int/lit8 v8, v9, 0x2

    .line 229
    .local v8, "rawLen":I
    move v1, v8

    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_0

    sub-int v9, v8, v1

    aget-byte v9, p0, v9

    if-eqz v9, :cond_4

    .line 233
    :cond_0
    move v2, v1

    .line 235
    .local v2, "j":I
    sub-int v9, v8, v1

    aget-byte v9, p0, v9

    if-gez v9, :cond_1

    .line 236
    add-int/lit8 v2, v2, 0x1

    .line 241
    :cond_1
    move v3, v8

    .local v3, "k":I
    :goto_1
    if-lez v3, :cond_2

    mul-int/lit8 v9, v8, 0x2

    sub-int/2addr v9, v3

    aget-byte v9, p0, v9

    if-eqz v9, :cond_5

    .line 245
    :cond_2
    move v4, v3

    .line 247
    .local v4, "l":I
    mul-int/lit8 v9, v8, 0x2

    sub-int/2addr v9, v3

    aget-byte v9, p0, v9

    if-gez v9, :cond_3

    .line 248
    add-int/lit8 v4, v4, 0x1

    .line 251
    :cond_3
    add-int/lit8 v9, v2, 0x2

    add-int/lit8 v9, v9, 0x2

    add-int v5, v9, v4

    .line 253
    .local v5, "len":I
    const/16 v9, 0xff

    if-le v5, v9, :cond_6

    .line 254
    new-instance v9, Lcom/nimbusds/jose/JOSEException;

    const-string v10, "Invalid ECDSA signature format"

    invoke-direct {v9, v10}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 229
    .end local v2    # "j":I
    .end local v3    # "k":I
    .end local v4    # "l":I
    .end local v5    # "len":I
    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 241
    .restart local v2    # "j":I
    .restart local v3    # "k":I
    :cond_5
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 261
    .restart local v4    # "l":I
    .restart local v5    # "len":I
    :cond_6
    const/16 v9, 0x80

    if-ge v5, v9, :cond_7

    .line 262
    add-int/lit8 v9, v2, 0x4

    add-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v4

    new-array v0, v9, [B

    .line 263
    .local v0, "derSignature":[B
    const/4 v6, 0x1

    .line 270
    .local v6, "offset":I
    :goto_2
    const/4 v9, 0x0

    const/16 v10, 0x30

    aput-byte v10, v0, v9

    .line 271
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "offset":I
    .local v7, "offset":I
    int-to-byte v9, v5

    aput-byte v9, v0, v6

    .line 272
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    aput-byte v11, v0, v7

    .line 273
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "offset":I
    .restart local v7    # "offset":I
    int-to-byte v9, v2

    aput-byte v9, v0, v6

    .line 275
    sub-int v9, v8, v1

    add-int v10, v7, v2

    sub-int/2addr v10, v1

    invoke-static {p0, v9, v0, v10, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 277
    add-int v6, v7, v2

    .line 279
    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "offset":I
    .restart local v7    # "offset":I
    aput-byte v11, v0, v6

    .line 280
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "offset":I
    .restart local v6    # "offset":I
    int-to-byte v9, v4

    aput-byte v9, v0, v7

    .line 282
    mul-int/lit8 v9, v8, 0x2

    sub-int/2addr v9, v3

    add-int v10, v6, v4

    sub-int/2addr v10, v3

    invoke-static {p0, v9, v0, v10, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 284
    return-object v0

    .line 265
    .end local v0    # "derSignature":[B
    .end local v6    # "offset":I
    :cond_7
    add-int/lit8 v9, v2, 0x5

    add-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v4

    new-array v0, v9, [B

    .line 266
    .restart local v0    # "derSignature":[B
    const/4 v9, 0x1

    const/16 v10, -0x7f

    aput-byte v10, v0, v9

    .line 267
    const/4 v6, 0x2

    .restart local v6    # "offset":I
    goto :goto_2
.end method
