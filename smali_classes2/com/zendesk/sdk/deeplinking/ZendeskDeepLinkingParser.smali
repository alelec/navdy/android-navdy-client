.class public Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;
.super Ljava/lang/Object;
.source "ZendeskDeepLinkingParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;,
        Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HelpCenterParser;,
        Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HttpParser;,
        Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$MailParser;,
        Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$DefaultParser;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskDeepLinkingParser"


# instance fields
.field private final mParserModuleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    new-instance v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HelpCenterParser;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HelpCenterParser;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    new-instance v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$MailParser;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$MailParser;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    new-instance v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HttpParser;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HttpParser;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    new-instance v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$DefaultParser;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$DefaultParser;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .locals 5
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v0, v2

    .line 86
    :goto_0
    return-object v0

    .line 79
    :cond_1
    iget-object v3, p0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;->mParserModuleList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;

    .line 80
    .local v1, "parserModule":Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;
    invoke-interface {v1, p1, p2}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;->parse(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 81
    .local v0, "parse":Landroid/content/Intent;
    if-eqz v0, :cond_2

    goto :goto_0

    .end local v0    # "parse":Landroid/content/Intent;
    .end local v1    # "parserModule":Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;
    :cond_3
    move-object v0, v2

    .line 86
    goto :goto_0
.end method
