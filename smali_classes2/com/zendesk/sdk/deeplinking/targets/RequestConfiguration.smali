.class public Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;
.super Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
.source "RequestConfiguration.java"


# instance fields
.field private final mRequestId:Ljava/lang/String;

.field private final mSubject:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V
    .locals 1
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "subject"    # Ljava/lang/String;
    .param p4, "fallBackActivity"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 16
    .local p3, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    sget-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Request:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    invoke-direct {p0, v0, p3, p4}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;-><init>(Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;Ljava/util/List;Landroid/content/Intent;)V

    .line 17
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->mRequestId:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->mSubject:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public bridge synthetic getBackStackActivities()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFallbackActivity()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->mRequestId:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/RequestConfiguration;->mSubject:Ljava/lang/String;

    return-object v0
.end method
