.class public Lcom/zendesk/sdk/rating/impl/SettingsRateMyAppRule;
.super Ljava/lang/Object;
.source "SettingsRateMyAppRule.java"

# interfaces
.implements Lcom/zendesk/sdk/rating/RateMyAppRule;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/SettingsRateMyAppRule;->mContext:Landroid/content/Context;

    .line 26
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public permitsShowOfDialog()Z
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/zendesk/sdk/rating/impl/SettingsRateMyAppRule;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 31
    const/4 v1, 0x0

    .line 35
    :goto_0
    return v1

    .line 34
    :cond_0
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 35
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isRateMyAppEnabled()Z

    move-result v1

    goto :goto_0
.end method
