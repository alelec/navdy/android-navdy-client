.class public Lcom/zendesk/sdk/rating/ui/FeedbackDialog;
.super Lcom/zendesk/sdk/ui/ZendeskDialog;
.source "FeedbackDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/rating/ui/FeedbackDialog$NullSafeFeedbackConnector;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mFeedBackConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

.field private mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/ZendeskDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    return-object v0
.end method

.method public static newInstance(Lcom/zendesk/sdk/feedback/FeedbackConnector;)Lcom/zendesk/sdk/rating/ui/FeedbackDialog;
    .locals 4
    .param p0, "connector"    # Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .prologue
    .line 63
    new-instance v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-direct {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;-><init>()V

    .line 65
    .local v0, "feedbackDialog":Lcom/zendesk/sdk/rating/ui/FeedbackDialog;
    if-nez p0, :cond_0

    .line 66
    sget-object v1, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Supplied connector was null, falling back to a null safe connector but no feedback will be sent!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    new-instance p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$NullSafeFeedbackConnector;

    .end local p0    # "connector":Lcom/zendesk/sdk/feedback/FeedbackConnector;
    invoke-direct {p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$NullSafeFeedbackConnector;-><init>()V

    .line 69
    .restart local p0    # "connector":Lcom/zendesk/sdk/feedback/FeedbackConnector;
    :cond_0
    invoke-virtual {v0, p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->setFeedBackConnector(Lcom/zendesk/sdk/feedback/FeedbackConnector;)V

    .line 71
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/ZendeskDialog;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->setRetainInstance(Z)V

    .line 78
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    sget v0, Lcom/zendesk/sdk/R$layout;->fragment_send_feedback:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 85
    .local v9, "dialog":Landroid/view/View;
    sget v0, Lcom/zendesk/sdk/R$id;->rma_feedback_issue_cancel_button:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 86
    .local v8, "cancelButton":Landroid/view/View;
    sget v0, Lcom/zendesk/sdk/R$id;->rma_feedback_issue_send_button:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 87
    .local v2, "sendButton":Landroid/view/View;
    sget v0, Lcom/zendesk/sdk/R$id;->rma_feedback_issue_edittext:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 88
    .local v3, "editText":Landroid/widget/EditText;
    sget v0, Lcom/zendesk/sdk/R$id;->rma_feedback_issue_progress:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    .line 90
    .local v4, "progressBar":Landroid/widget/ProgressBar;
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 91
    .local v7, "context":Landroid/content/Context;
    new-instance v6, Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-direct {v6, v7}, Lcom/zendesk/sdk/storage/RateMyAppStorage;-><init>(Landroid/content/Context;)V

    .line 93
    .local v6, "rateMyAppStorage":Lcom/zendesk/sdk/storage/RateMyAppStorage;
    if-eqz v8, :cond_0

    .line 94
    sget-object v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Inflating cancel button..."

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v11}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    new-instance v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$1;-><init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_0
    iget-object v5, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->mFeedBackConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .line 113
    .local v5, "connector":Lcom/zendesk/sdk/feedback/FeedbackConnector;
    invoke-virtual {v6}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->getSavedFeedback()Ljava/lang/String;

    move-result-object v10

    .line 115
    .local v10, "savedFeedback":Ljava/lang/String;
    invoke-static {v10}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116
    sget-object v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Saved feedback was found."

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v11}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    if-eqz v3, :cond_1

    .line 119
    invoke-virtual {v3, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_1
    if-eqz v2, :cond_2

    .line 123
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 128
    :cond_2
    if-eqz v2, :cond_3

    if-nez v3, :cond_5

    .line 129
    :cond_3
    sget-object v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Could not setOnClickListener because views were null"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v0, v1, v11}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    :goto_0
    if-eqz v3, :cond_4

    .line 198
    new-instance v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;

    invoke-direct {v0, p0, v2}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;-><init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;Landroid/view/View;)V

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 216
    :cond_4
    return-object v9

    .line 131
    :cond_5
    new-instance v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;-><init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;Landroid/view/View;Landroid/widget/EditText;Landroid/widget/ProgressBar;Lcom/zendesk/sdk/feedback/FeedbackConnector;Lcom/zendesk/sdk/storage/RateMyAppStorage;Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 223
    :cond_0
    invoke-super {p0}, Lcom/zendesk/sdk/ui/ZendeskDialog;->onDestroyView()V

    .line 224
    return-void
.end method

.method setFeedBackConnector(Lcom/zendesk/sdk/feedback/FeedbackConnector;)V
    .locals 0
    .param p1, "feedBackConnector"    # Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->mFeedBackConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .line 228
    return-void
.end method

.method public setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V
    .locals 0
    .param p1, "feedbackListener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    .line 59
    return-void
.end method
