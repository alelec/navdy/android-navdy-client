.class Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

.field final synthetic val$connector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$editText:Landroid/widget/EditText;

.field final synthetic val$progressBar:Landroid/widget/ProgressBar;

.field final synthetic val$rateMyAppStorage:Lcom/zendesk/sdk/storage/RateMyAppStorage;

.field final synthetic val$sendButton:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;Landroid/view/View;Landroid/widget/EditText;Landroid/widget/ProgressBar;Lcom/zendesk/sdk/feedback/FeedbackConnector;Lcom/zendesk/sdk/storage/RateMyAppStorage;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$sendButton:Landroid/view/View;

    iput-object p3, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$editText:Landroid/widget/EditText;

    iput-object p4, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$progressBar:Landroid/widget/ProgressBar;

    iput-object p5, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$connector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    iput-object p6, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$rateMyAppStorage:Lcom/zendesk/sdk/storage/RateMyAppStorage;

    iput-object p7, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v1}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v1}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionStarted()V

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$sendButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 140
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 141
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 143
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "feedback":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$connector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;

    invoke-direct {v3, p0, v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;-><init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;Ljava/lang/String;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/zendesk/sdk/feedback/FeedbackConnector;->sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V

    .line 193
    return-void
.end method
