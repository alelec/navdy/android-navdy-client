.class public Lcom/zendesk/sdk/model/push/PushRegistrationResponse;
.super Ljava/lang/Object;
.source "PushRegistrationResponse.java"


# instance fields
.field private active:Z

.field private createdAt:Ljava/util/Date;

.field private deviceType:Ljava/lang/String;

.field private id:I

.field private identifier:Ljava/lang/String;

.field private updatedAt:Ljava/util/Date;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdentifier()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/PushRegistrationResponse;->identifier:Ljava/lang/String;

    return-object v0
.end method
