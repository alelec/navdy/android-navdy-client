.class public Lcom/zendesk/sdk/model/request/UploadResponse;
.super Ljava/lang/Object;
.source "UploadResponse.java"


# instance fields
.field private attachment:Lcom/zendesk/sdk/model/request/Attachment;

.field private expiresAt:Ljava/util/Date;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttachment()Lcom/zendesk/sdk/model/request/Attachment;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UploadResponse;->attachment:Lcom/zendesk/sdk/model/request/Attachment;

    return-object v0
.end method

.method public getExpiresAt()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UploadResponse;->expiresAt:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/zendesk/sdk/model/request/UploadResponse;->expiresAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/UploadResponse;->token:Ljava/lang/String;

    return-object v0
.end method
