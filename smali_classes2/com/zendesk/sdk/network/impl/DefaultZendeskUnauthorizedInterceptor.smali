.class public Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;
.super Ljava/lang/Object;
.source "DefaultZendeskUnauthorizedInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/storage/SdkStorage;)V
    .locals 0
    .param p1, "sdkStorage"    # Lcom/zendesk/sdk/storage/SdkStorage;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    .line 22
    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 3
    .param p1, "chain"    # Lokhttp3/Interceptor$Chain;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v1

    invoke-interface {p1, v1}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v0

    .line 28
    .local v0, "response":Lokhttp3/Response;
    invoke-virtual {v0}, Lokhttp3/Response;->isSuccessful()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x191

    invoke-virtual {v0}, Lokhttp3/Response;->code()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;->onHttpUnauthorized()V

    .line 32
    :cond_0
    return-object v0
.end method

.method public onHttpUnauthorized()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;->sdkStorage:Lcom/zendesk/sdk/storage/SdkStorage;

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkStorage;->clearUserData()V

    .line 37
    return-void
.end method
