.class Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
.super Ljava/lang/Object;
.source "ApplicationScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/network/impl/ApplicationScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private final appId:Ljava/lang/String;

.field private final applicationContext:Landroid/content/Context;

.field private coppaEnabled:Z

.field private customFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation
.end field

.field private final developmentMode:Z

.field private locale:Ljava/util/Locale;

.field private final oAuthToken:Ljava/lang/String;

.field private sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

.field private ticketFormId:Ljava/lang/Long;

.field private final url:Ljava/lang/String;

.field private userAgentHeader:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "appId"    # Ljava/lang/String;
    .param p4, "oAuth"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->applicationContext:Landroid/content/Context;

    .line 233
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->url:Ljava/lang/String;

    .line 234
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->appId:Ljava/lang/String;

    .line 235
    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->oAuthToken:Ljava/lang/String;

    .line 237
    const/4 v0, 0x1

    .line 239
    .local v0, "devMode":Z
    :try_start_0
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->applicationContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/zendesk/sdk/util/StageDetectionUtil;->isDebug(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 243
    :goto_0
    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->developmentMode:Z

    .line 245
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->locale:Ljava/util/Locale;

    .line 246
    iput-boolean v5, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->coppaEnabled:Z

    .line 247
    new-instance v2, Lcom/zendesk/sdk/network/impl/DefaultSdkOptions;

    invoke-direct {v2}, Lcom/zendesk/sdk/network/impl/DefaultSdkOptions;-><init>()V

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    .line 248
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->ticketFormId:Ljava/lang/Long;

    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->customFields:Ljava/util/List;

    .line 251
    const-string v2, ""

    iput-object v2, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader:Ljava/lang/String;

    .line 252
    return-void

    .line 240
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ApplicationScope"

    const-string v3, "Unable to detect stage"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method constructor <init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V
    .locals 1
    .param p1, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->applicationContext:Landroid/content/Context;

    .line 256
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->url:Ljava/lang/String;

    .line 257
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getAppId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->appId:Ljava/lang/String;

    .line 258
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getOAuthToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->oAuthToken:Ljava/lang/String;

    .line 259
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->isDevelopmentMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->developmentMode:Z

    .line 260
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getLocale()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->locale:Ljava/util/Locale;

    .line 261
    invoke-static {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->access$1100(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->coppaEnabled:Z

    .line 262
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    .line 263
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getTicketFormId()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->ticketFormId:Ljava/lang/Long;

    .line 264
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getCustomFields()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->customFields:Ljava/util/List;

    .line 265
    invoke-virtual {p1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getUserAgentHeader()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader:Ljava/lang/String;

    .line 266
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->appId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->oAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->coppaEnabled:Z

    return v0
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->developmentMode:Z

    return v0
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Lcom/zendesk/sdk/network/SdkOptions;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    return-object v0
.end method

.method static synthetic access$800(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->ticketFormId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$900(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->customFields:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method build()Lcom/zendesk/sdk/network/impl/ApplicationScope;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 348
    new-instance v0, Lcom/zendesk/sdk/network/impl/ApplicationScope;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/zendesk/sdk/network/impl/ApplicationScope;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;Lcom/zendesk/sdk/network/impl/ApplicationScope$1;)V

    return-object v0
.end method

.method coppa(Z)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 0
    .param p1, "coppaEnabled"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 315
    iput-boolean p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->coppaEnabled:Z

    .line 316
    return-object p0
.end method

.method customFields(Ljava/util/List;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;)",
            "Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "customFields":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/CustomField;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->customFields:Ljava/util/List;

    .line 292
    return-object p0
.end method

.method locale(Ljava/util/Locale;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 303
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->locale:Ljava/util/Locale;

    .line 304
    return-object p0
.end method

.method sdkOptions(Lcom/zendesk/sdk/network/SdkOptions;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 0
    .param p1, "sdkOptions"    # Lcom/zendesk/sdk/network/SdkOptions;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 326
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->sdkOptions:Lcom/zendesk/sdk/network/SdkOptions;

    .line 327
    return-object p0
.end method

.method ticketFormId(Ljava/lang/Long;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 0
    .param p1, "ticketFormId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 277
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->ticketFormId:Ljava/lang/Long;

    .line 278
    return-object p0
.end method

.method userAgentHeader(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;
    .locals 0
    .param p1, "userAgentHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 337
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ApplicationScope$Builder;->userAgentHeader:Ljava/lang/String;

    .line 338
    return-object p0
.end method
