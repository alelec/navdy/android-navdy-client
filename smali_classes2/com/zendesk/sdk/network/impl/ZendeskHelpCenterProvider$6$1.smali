.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;)V
    .locals 2
    .param p1, "articlesListResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;

    .prologue
    .line 227
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;

    iget-object v1, v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-virtual {v1, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->asFlatArticleList(Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;)Ljava/util/List;

    move-result-object v0

    .line 229
    .local v0, "flatArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/FlatArticle;>;"
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;

    iget-object v1, v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;

    iget-object v1, v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v1, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 232
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 224
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6$1;->onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;)V

    return-void
.end method
