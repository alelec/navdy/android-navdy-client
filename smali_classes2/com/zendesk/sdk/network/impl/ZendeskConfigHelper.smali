.class Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;
.super Ljava/lang/Object;
.source "ZendeskConfigHelper.java"


# instance fields
.field private providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

.field private storageStore:Lcom/zendesk/sdk/storage/StorageStore;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ProviderStore;Lcom/zendesk/sdk/storage/StorageStore;)V
    .locals 0
    .param p1, "providerStore"    # Lcom/zendesk/sdk/network/impl/ProviderStore;
    .param p2, "storageStore"    # Lcom/zendesk/sdk/storage/StorageStore;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

    .line 21
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->storageStore:Lcom/zendesk/sdk/storage/StorageStore;

    .line 22
    return-void
.end method


# virtual methods
.method public getProviderStore()Lcom/zendesk/sdk/network/impl/ProviderStore;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->providerStore:Lcom/zendesk/sdk/network/impl/ProviderStore;

    return-object v0
.end method

.method public getStorageStore()Lcom/zendesk/sdk/storage/StorageStore;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskConfigHelper;->storageStore:Lcom/zendesk/sdk/storage/StorageStore;

    return-object v0
.end method
