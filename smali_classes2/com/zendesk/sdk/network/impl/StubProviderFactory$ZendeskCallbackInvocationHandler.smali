.class Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;
.super Ljava/lang/Object;
.source "StubProviderFactory.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/network/impl/StubProviderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ZendeskCallbackInvocationHandler"
.end annotation


# instance fields
.field private clazz:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 0
    .param p1, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;->clazz:Ljava/lang/Class;

    .line 58
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 63
    const/4 v3, 0x0

    .line 64
    .local v3, "zendeskCallback":Lcom/zendesk/service/ZendeskCallback;
    array-length v6, p3

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v1, p3, v4

    .line 65
    .local v1, "o":Ljava/lang/Object;
    instance-of v7, v1, Lcom/zendesk/service/ZendeskCallback;

    if-eqz v7, :cond_2

    move-object v3, v1

    .line 66
    check-cast v3, Lcom/zendesk/service/ZendeskCallback;

    .line 71
    .end local v1    # "o":Ljava/lang/Object;
    :cond_0
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/StubProviderFactory$ZendeskCallbackInvocationHandler;->clazz:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "providerName":Ljava/lang/String;
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Zendesk not initialised! You tried to call: %s#%s(...)"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v5

    const/4 v8, 0x1

    .line 73
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 72
    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "errorMsg":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    if-eqz v3, :cond_1

    .line 77
    new-instance v4, Lcom/zendesk/service/ErrorResponseAdapter;

    invoke-direct {v4, v0}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 80
    :cond_1
    const/4 v4, 0x0

    return-object v4

    .line 64
    .end local v0    # "errorMsg":Ljava/lang/String;
    .end local v2    # "providerName":Ljava/lang/String;
    .restart local v1    # "o":Ljava/lang/Object;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
