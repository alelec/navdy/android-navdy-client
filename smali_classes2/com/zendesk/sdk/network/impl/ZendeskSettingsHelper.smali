.class Lcom/zendesk/sdk/network/impl/ZendeskSettingsHelper;
.super Ljava/lang/Object;
.source "ZendeskSettingsHelper.java"

# interfaces
.implements Lcom/zendesk/sdk/network/SettingsHelper;


# instance fields
.field private baseProvider:Lcom/zendesk/sdk/network/BaseProvider;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSettingsHelper;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 18
    return-void
.end method


# virtual methods
.method public loadSetting(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/settings/SafeMobileSettings;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskSettingsHelper;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/BaseProvider;->getSdkSettings(Lcom/zendesk/service/ZendeskCallback;)V

    .line 23
    return-void
.end method
