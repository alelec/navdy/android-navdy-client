.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskHelpCenterService"

.field private static final NUMBER_PER_PAGE:I = 0x3e8


# instance fields
.field private final helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/HelpCenterService;)V
    .locals 0
    .param p1, "helpCenterService"    # Lcom/zendesk/sdk/network/HelpCenterService;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    .line 54
    return-void
.end method


# virtual methods
.method public deleteVote(Ljava/lang/String;Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "voteId"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    if-nez p2, :cond_1

    .line 337
    const-string v0, "The vote id was null, can not delete the vote"

    .line 339
    .local v0, "reason":Ljava/lang/String;
    const-string v1, "ZendeskHelpCenterService"

    const-string v2, "The vote id was null, can not delete the vote"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    if-eqz p3, :cond_0

    .line 342
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "The vote id was null, can not delete the vote"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 349
    .end local v0    # "reason":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, p2}, Lcom/zendesk/sdk/network/HelpCenterService;->deleteVote(Ljava/lang/String;Ljava/lang/Long;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v2, p3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    goto :goto_0
.end method

.method public downvoteArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "articleId"    # Ljava/lang/Long;
    .param p3, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;>;"
    if-nez p2, :cond_1

    .line 320
    const-string v0, "The article id was null, can not create down vote"

    .line 322
    .local v0, "reason":Ljava/lang/String;
    const-string v1, "ZendeskHelpCenterService"

    const-string v2, "The article id was null, can not create down vote"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    if-eqz p4, :cond_0

    .line 325
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "The article id was null, can not create down vote"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 332
    .end local v0    # "reason":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, p2, p3}, Lcom/zendesk/sdk/network/HelpCenterService;->downvoteArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v2, p4}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    goto :goto_0
.end method

.method public getArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "articleId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "include"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "localeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, v0, p2, p4}, Lcom/zendesk/sdk/network/HelpCenterService;->getArticle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v2, p5, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    .line 197
    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 205
    return-void
.end method

.method public getArticlesForSection(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 6
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "sectionId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "include"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x3e8

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/zendesk/sdk/network/HelpCenterService;->getArticles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;I)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v1, p5, v2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 122
    return-void
.end method

.method public getAttachments(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/AttachmentType;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 5
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "articleId"    # Ljava/lang/Long;
    .param p4, "type"    # Lcom/zendesk/sdk/model/helpcenter/AttachmentType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Ljava/lang/Long;",
            "Lcom/zendesk/sdk/model/helpcenter/AttachmentType;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Attachment;>;>;"
    if-nez p4, :cond_1

    .line 278
    const-string v0, "getAttachments() was called with null attachment type"

    .line 279
    .local v0, "error":Ljava/lang/String;
    const-string v2, "ZendeskHelpCenterService"

    const-string v3, "getAttachments() was called with null attachment type"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    if-eqz p5, :cond_0

    .line 282
    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "getAttachments() was called with null attachment type"

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5, v2}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 296
    .end local v0    # "error":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    invoke-static {p2}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "localeString":Ljava/lang/String;
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-virtual {p4}, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->getAttachmentType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v1, p3, v3}, Lcom/zendesk/sdk/network/HelpCenterService;->getAttachments(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v2

    new-instance v3, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v4, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$7;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$7;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v3, p5, v4}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v2, v3}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    goto :goto_0
.end method

.method public getCategories(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Category;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-static {p2}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/zendesk/sdk/network/HelpCenterService;->getCategories(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v1, p3, v2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 83
    return-void
.end method

.method public getCategoryById(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "categoryId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "localeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, v0, p2}, Lcom/zendesk/sdk/network/HelpCenterService;->getCategoryById(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$6;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$6;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v2, p4, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 263
    return-void
.end method

.method getHelp(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 12
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
    .param p3, "categoryIds"    # Ljava/lang/String;
    .param p4, "sectionIds"    # Ljava/lang/String;
    .param p5, "include"    # Ljava/lang/String;
    .param p6, "articlesPerPageLimit"    # I
    .param p7, "labelNames"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p8, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;>;"
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-static {p2}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const/16 v9, 0x3e8

    sget-object v2, Lcom/zendesk/sdk/model/helpcenter/SortBy;->CREATED_AT:Lcom/zendesk/sdk/model/helpcenter/SortBy;

    .line 63
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/SortBy;->getApiValue()Ljava/lang/String;

    move-result-object v10

    sget-object v2, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->DESCENDING:Lcom/zendesk/sdk/model/helpcenter/SortOrder;

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/SortOrder;->getApiValue()Ljava/lang/String;

    move-result-object v11

    move-object v2, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    .line 61
    invoke-interface/range {v1 .. v11}, Lcom/zendesk/sdk/network/HelpCenterService;->getHelp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    move-object/from16 v0, p8

    invoke-direct {v2, v0}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    .line 64
    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 65
    return-void
.end method

.method public getSectionById(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "sectionId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "localeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, v0, p2}, Lcom/zendesk/sdk/network/HelpCenterService;->getSectionById(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$5;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$5;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v2, p4, v3}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 242
    return-void
.end method

.method public getSectionsForCategory(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "categoryId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Section;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Section;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-interface {v0, p1, v1, p2, v2}, Lcom/zendesk/sdk/network/HelpCenterService;->getSections(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;I)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$2;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V

    invoke-direct {v1, p4, v2}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 102
    return-void
.end method

.method public getSuggestedArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "authorizationHeader"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "labelNames"    # Ljava/lang/String;
    .param p5, "category"    # Ljava/lang/Long;
    .param p6, "section"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p7, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleResponse;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 366
    .local v3, "localeString":Ljava/lang/String;
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/zendesk/sdk/network/HelpCenterService;->getSuggestedArticles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p7}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 367
    return-void
.end method

.method public listArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 10
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "labelNames"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "include"    # Ljava/lang/String;
    .param p5, "sortBy"    # Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;
    .param p7, "page"    # Ljava/lang/Integer;
    .param p8, "resultsPerPage"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p9, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "localeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    move-object v2, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-interface/range {v1 .. v9}, Lcom/zendesk/sdk/network/HelpCenterService;->listArticles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    move-object/from16 v0, p9

    invoke-direct {v2, v0}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    .line 150
    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 153
    return-void
.end method

.method matchArticleWithUsers(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 4
    .param p1, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/User;",
            ">;)",
            "Lcom/zendesk/sdk/model/helpcenter/Article;"
        }
    .end annotation

    .prologue
    .line 210
    .local p2, "users":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/User;>;"
    if-nez p1, :cond_1

    .line 211
    new-instance p1, Lcom/zendesk/sdk/model/helpcenter/Article;

    .end local p1    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    invoke-direct {p1}, Lcom/zendesk/sdk/model/helpcenter/Article;-><init>()V

    .line 220
    :cond_0
    :goto_0
    return-object p1

    .line 214
    .restart local p1    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/User;

    .line 215
    .local v0, "user":Lcom/zendesk/sdk/model/helpcenter/User;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/User;->getId()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/User;->getId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthorId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 216
    invoke-virtual {p1, v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->setAuthor(Lcom/zendesk/sdk/model/helpcenter/User;)V

    goto :goto_0
.end method

.method matchArticlesWithUsers(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/User;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "users":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/User;>;"
    .local p2, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 127
    .local v1, "mapOfUsers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/User;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/User;

    .line 128
    .local v3, "user":Lcom/zendesk/sdk/model/helpcenter/User;
    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/User;->getId()Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 131
    .end local v3    # "user":Lcom/zendesk/sdk/model/helpcenter/User;
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v2, "matchedArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 135
    .local v0, "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthorId()Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/User;

    .line 136
    .restart local v3    # "user":Lcom/zendesk/sdk/model/helpcenter/User;
    if-eqz v3, :cond_1

    .line 137
    invoke-virtual {v0, v3}, Lcom/zendesk/sdk/model/helpcenter/Article;->setAuthor(Lcom/zendesk/sdk/model/helpcenter/User;)V

    .line 140
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 143
    .end local v0    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    .end local v3    # "user":Lcom/zendesk/sdk/model/helpcenter/User;
    :cond_2
    return-object v2
.end method

.method public searchArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 11
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "include"    # Ljava/lang/String;
    .param p5, "labelNames"    # Ljava/lang/String;
    .param p6, "categoryIds"    # Ljava/lang/String;
    .param p7, "sectionIds"    # Ljava/lang/String;
    .param p8, "page"    # Ljava/lang/Integer;
    .param p9, "resultsPerPage"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p10, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticlesSearchResponse;>;"
    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 177
    .local v4, "localeString":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-interface/range {v1 .. v10}, Lcom/zendesk/sdk/network/HelpCenterService;->searchArticles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    move-object/from16 v0, p10

    invoke-direct {v2, v0}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    .line 178
    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 181
    return-void
.end method

.method public submitRecordArticleView(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "authorizationHeader"    # Ljava/lang/String;
    .param p2, "articleId"    # Ljava/lang/Long;
    .param p3, "locale"    # Ljava/util/Locale;
    .param p4, "recordArticleViewRequest"    # Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;
    .param p5    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 380
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-static {p3}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1, p4}, Lcom/zendesk/sdk/network/HelpCenterService;->submitRecordArticleView(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/zendesk/sdk/model/helpcenter/RecordArticleViewRequest;)Lretrofit2/Call;

    move-result-object v0

    new-instance v1, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v1, p5}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 382
    return-void
.end method

.method public upvoteArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "authorizationHeader"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "articleId"    # Ljava/lang/Long;
    .param p3, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;>;"
    if-nez p2, :cond_1

    .line 302
    const-string v0, "The article id was null, can not create up vote"

    .line 304
    .local v0, "reason":Ljava/lang/String;
    const-string v1, "ZendeskHelpCenterService"

    const-string v2, "The article id was null, can not create up vote"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 306
    if-eqz p4, :cond_0

    .line 307
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "The article id was null, can not create up vote"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 314
    .end local v0    # "reason":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->helpCenterService:Lcom/zendesk/sdk/network/HelpCenterService;

    invoke-interface {v1, p1, p2, p3}, Lcom/zendesk/sdk/network/HelpCenterService;->upvoteArticle(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Lretrofit2/Call;

    move-result-object v1

    new-instance v2, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;

    invoke-direct {v2, p4}, Lcom/zendesk/service/RetrofitZendeskCallbackAdapter;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v1, v2}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    goto :goto_0
.end method
