.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;)V
    .locals 2
    .param p1, "articleVoteResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;

    .prologue
    .line 378
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;->getVote()Lcom/zendesk/sdk/model/helpcenter/ArticleVote;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 381
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 375
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12$1;->onSuccess(Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;)V

    return-void
.end method
