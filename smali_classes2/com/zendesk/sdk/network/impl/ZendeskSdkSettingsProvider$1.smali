.class Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskSdkSettingsProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->getSettings(Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/settings/MobileSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$locale:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$locale:Ljava/lang/String;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/settings/MobileSettings;)V
    .locals 8
    .param p1, "result"    # Lcom/zendesk/sdk/model/settings/MobileSettings;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 63
    const-string v4, "ZendeskSdkSettingsProvider"

    const-string v5, "Successfully retrieved SDK Settings"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    new-instance v1, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-direct {v1, p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;-><init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    .line 66
    .local v1, "safeMobileSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$locale:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getHelpCenterLocale()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v2

    .line 68
    .local v0, "requestedLocaleIsNotAvailable":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 69
    const-string v4, "ZendeskSdkSettingsProvider"

    const-string v5, "No support for %s, Help Center is %s in your application settings"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$locale:Ljava/lang/String;

    aput-object v7, v6, v3

    .line 71
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isHelpCenterEnabled()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v2

    .line 69
    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;

    invoke-static {v2}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->setStoredSettings(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    .line 76
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v2, :cond_1

    .line 77
    const-string v2, "ZendeskSdkSettingsProvider"

    const-string v4, "Calling back with successful result"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v4, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v2, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 80
    :cond_1
    return-void

    .end local v0    # "requestedLocaleIsNotAvailable":Z
    :cond_2
    move v0, v3

    .line 66
    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p1, Lcom/zendesk/sdk/model/settings/MobileSettings;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskSdkSettingsProvider$1;->onSuccess(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    return-void
.end method
