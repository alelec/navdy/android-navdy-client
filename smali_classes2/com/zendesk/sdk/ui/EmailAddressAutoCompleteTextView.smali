.class public Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;
.super Landroid/widget/AutoCompleteTextView;
.source "EmailAddressAutoCompleteTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->initialise()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->initialise()V

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->isEmailValid(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private initialise()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 57
    const/4 v5, -0x1

    .line 58
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v6, v7}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v6

    if-eq v5, v6, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->isInEditMode()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 61
    :cond_0
    const-class v5, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "You must declare the GET_ACCOUNTS permission to use the auto-complete feature"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v4}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    :cond_1
    :goto_0
    new-instance v4, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;

    invoke-direct {v4, p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView$1;-><init>(Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;)V

    invoke-virtual {p0, v4}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 101
    return-void

    .line 66
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 67
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v3, "emailAddresses":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v5

    array-length v6, v5

    :goto_1
    if-ge v4, v6, :cond_4

    aget-object v0, v5, v4

    .line 71
    .local v0, "account":Landroid/accounts/Account;
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->isEmailValid(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 72
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 73
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 78
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 79
    new-instance v2, Landroid/widget/ArrayAdapter;

    .line 81
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x109000a

    invoke-direct {v2, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 85
    .local v2, "emailAddressAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method private isEmailValid(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "email"    # Ljava/lang/CharSequence;

    .prologue
    .line 109
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isInputValid()Z
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "input":Ljava/lang/String;
    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->isEmailValid(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
