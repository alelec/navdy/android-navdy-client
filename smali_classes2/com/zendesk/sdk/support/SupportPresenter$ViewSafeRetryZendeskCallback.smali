.class Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "SupportPresenter.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewSafeRetryZendeskCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
        ">;>;"
    }
.end annotation


# instance fields
.field private query:Ljava/lang/String;

.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportPresenter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportPresenter;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportPresenter;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    .line 185
    iput-object p2, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->query:Ljava/lang/String;

    .line 186
    return-void
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->query:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 208
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 210
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    sget-object v1, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    new-instance v2, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$2;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$2;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;)V

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/support/SupportMvp$View;->showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    .line 224
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$3;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$3;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;Lcom/zendesk/service/ErrorResponse;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 179
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 192
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->query:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/zendesk/sdk/support/SupportMvp$View;->showSearchResults(Ljava/util/List;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$100(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig;->isShowContactUsButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showContactUsButton()V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$1;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback$1;-><init>(Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
