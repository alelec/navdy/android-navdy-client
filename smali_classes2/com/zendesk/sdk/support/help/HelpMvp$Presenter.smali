.class public interface abstract Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;
.super Ljava/lang/Object;
.source "HelpMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Presenter"
.end annotation


# virtual methods
.method public abstract getItem(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
.end method

.method public abstract getItemCount()I
.end method

.method public abstract getItemForBinding(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getItemViewType(I)I
.end method

.method public abstract onAttached()V
.end method

.method public abstract onCategoryClick(Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)Z
.end method

.method public abstract onDetached()V
.end method

.method public abstract onSeeAllClick(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
.end method

.method public abstract setContentPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V
.end method
