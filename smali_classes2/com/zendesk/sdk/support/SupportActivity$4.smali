.class Lcom/zendesk/sdk/support/SupportActivity$4;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/SupportActivity;->showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportActivity;

.field final synthetic val$action:Lcom/zendesk/sdk/network/RetryAction;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportActivity;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportActivity$4;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    iput-object p2, p0, Lcom/zendesk/sdk/support/SupportActivity$4;->val$action:Lcom/zendesk/sdk/network/RetryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$4;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportActivity;->access$200(Lcom/zendesk/sdk/support/SupportActivity;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->dismiss()V

    .line 377
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$4;->val$action:Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/RetryAction;->onRetry()V

    .line 378
    return-void
.end method
