.class public Lcom/zendesk/sdk/support/SupportActivity$Builder;
.super Ljava/lang/Object;
.source "SupportActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final args:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    .line 434
    return-void
.end method


# virtual methods
.method intent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 542
    const-string v1, "SupportActivity"

    const-string v2, "intent: creating Intent"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 543
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/zendesk/sdk/support/SupportActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 544
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 545
    return-object v0
.end method

.method public show(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 531
    const-string v0, "SupportActivity"

    const-string v1, "show: showing SupportActivity"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/SupportActivity$Builder;->intent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 533
    return-void
.end method

.method public showContactUsButton(Z)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 2
    .param p1, "showContactUsButton"    # Z

    .prologue
    .line 475
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_show_contact_us_button"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 477
    return-object p0
.end method

.method public varargs withArticlesForCategoryIds([J)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 3
    .param p1, "categoryIds"    # [J

    .prologue
    .line 443
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_section_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    const-string v0, "SupportActivity"

    const-string v1, "Builder: sections have already been specified. Removing section IDs to set category IDs."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_section_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_category_ids"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 449
    return-object p0
.end method

.method public varargs withArticlesForSectionIds([J)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 3
    .param p1, "sectionIds"    # [J

    .prologue
    .line 459
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_category_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    if-eqz v0, :cond_0

    .line 460
    const-string v0, "SupportActivity"

    const-string v1, "Builder: categories have already been specified. Removing category IDs to set section IDs."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 461
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_category_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_section_ids"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 465
    return-object p0
.end method

.method public withCategoriesCollapsed(Z)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 2
    .param p1, "categoriesCollapsed"    # Z

    .prologue
    .line 521
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_categories_collapsed"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 522
    return-object p0
.end method

.method public withContactConfiguration(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 3
    .param p1, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .prologue
    .line 488
    if-eqz p1, :cond_0

    .line 489
    new-instance v0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    .end local p1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .local v0, "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    move-object p1, v0

    .line 492
    .end local v0    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .restart local p1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v2, "extra_contact_configuration"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 493
    return-object p0
.end method

.method public varargs withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/support/SupportActivity$Builder;
    .locals 2
    .param p1, "labelNames"    # [Ljava/lang/String;

    .prologue
    .line 503
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$Builder;->args:Landroid/os/Bundle;

    const-string v1, "extra_label_names"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 507
    :cond_0
    return-object p0
.end method
