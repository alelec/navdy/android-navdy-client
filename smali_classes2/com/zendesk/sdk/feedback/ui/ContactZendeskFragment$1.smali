.class final Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;
.super Ljava/lang/Object;
.source "ContactZendeskFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->bundleToZendeskFeedbackConfiguration(Landroid/os/Bundle;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$bundle:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;->val$bundle:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdditionalInfo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;->val$bundle:Landroid/os/Bundle;

    const-string v1, "EXTRA_CONFIGURATION_ADDITIONAL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestSubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;->val$bundle:Landroid/os/Bundle;

    const-string v1, "EXTRA_CONFIGURATION_SUBJECT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$1;->val$bundle:Landroid/os/Bundle;

    const-string v1, "EXTRA_CONFIGURATION_TAGS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
