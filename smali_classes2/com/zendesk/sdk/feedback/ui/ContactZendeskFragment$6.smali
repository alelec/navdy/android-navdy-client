.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;
.super Ljava/lang/Object;
.source "ContactZendeskFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setUpCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public allImagesUploaded(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 509
    .local p1, "responses":Ljava/util/Map;, "Ljava/util/Map<Ljava/io/File;Lcom/zendesk/sdk/model/request/UploadResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 510
    return-void
.end method

.method public imageUploadError(Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 7
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;
    .param p2, "file"    # Lcom/zendesk/belvedere/BelvedereResult;

    .prologue
    const/4 v6, 0x0

    .line 519
    invoke-static {}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Image upload error; Reason: %s, Status: %s, isNetworkError: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->isNetworkError()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$100(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v2

    invoke-static {v0, p2, p1, v1, v2}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->showAttachmentTryAgainDialog(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereResult;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V

    .line 521
    return-void
.end method

.method public imageUploaded(Lcom/zendesk/sdk/model/request/UploadResponse;Lcom/zendesk/belvedere/BelvedereResult;)V
    .locals 2
    .param p1, "uploadResponse"    # Lcom/zendesk/sdk/model/request/UploadResponse;
    .param p2, "file"    # Lcom/zendesk/belvedere/BelvedereResult;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$6;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$200(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    move-result-object v0

    invoke-virtual {p2}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentUploaded(Ljava/io/File;)V

    .line 515
    return-void
.end method
