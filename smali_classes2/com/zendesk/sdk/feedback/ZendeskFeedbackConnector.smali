.class public Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;
.super Ljava/lang/Object;
.source "ZendeskFeedbackConnector.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/FeedbackConnector;


# static fields
.field private static final FALLBACK_REQUEST_SUBJECT:Ljava/lang/String; = "App Ticket"

.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskFeedbackConnector"


# instance fields
.field private final additionalInfo:Ljava/lang/String;

.field private final additionalTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final customFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;"
        }
    .end annotation
.end field

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

.field private final metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

.field private final requestSubject:Ljava/lang/String;

.field private final ticketFormId:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lcom/zendesk/sdk/network/RequestProvider;Lcom/zendesk/sdk/storage/IdentityStorage;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "ticketFormId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "requestProvider"    # Lcom/zendesk/sdk/network/RequestProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "identityStorage"    # Lcom/zendesk/sdk/storage/IdentityStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/zendesk/sdk/network/RequestProvider;",
            "Lcom/zendesk/sdk/storage/IdentityStorage;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "additionalTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "customFields":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/CustomField;>;"
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    if-nez p2, :cond_0

    move-object v0, v1

    .line 102
    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->additionalInfo:Ljava/lang/String;

    .line 104
    if-nez p2, :cond_2

    if-nez p1, :cond_1

    const-string v0, "App Ticket"

    .line 110
    :goto_1
    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->requestSubject:Ljava/lang/String;

    .line 112
    if-nez p2, :cond_4

    move-object v0, v1

    .line 114
    :goto_2
    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->configurationTags:Ljava/util/List;

    .line 116
    if-nez p1, :cond_5

    .line 118
    :goto_3
    iput-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->metadata:Ljava/util/Map;

    .line 120
    iput-object p3, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->additionalTags:Ljava/util/List;

    .line 121
    iput-object p4, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->customFields:Ljava/util/List;

    .line 122
    iput-object p5, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->ticketFormId:Ljava/lang/Long;

    .line 123
    iput-object p6, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    .line 124
    iput-object p7, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 125
    return-void

    .line 102
    :cond_0
    invoke-interface {p2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_1
    sget v0, Lcom/zendesk/sdk/R$string;->contact_fragment_request_subject:I

    .line 107
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 108
    :cond_2
    invoke-interface {p2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getRequestSubject()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "App Ticket"

    goto :goto_1

    .line 110
    :cond_3
    invoke-interface {p2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getRequestSubject()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 114
    :cond_4
    invoke-interface {p2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getTags()Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 116
    :cond_5
    new-instance v0, Lcom/zendesk/sdk/model/DeviceInfo;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/model/DeviceInfo;-><init>(Landroid/content/Context;)V

    .line 118
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/DeviceInfo;->getDeviceInfoAsMap()Ljava/util/Map;

    move-result-object v1

    goto :goto_3
.end method

.method public static defaultConnector(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Ljava/util/List;)Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;"
        }
    .end annotation

    .prologue
    .line 70
    .local p2, "additionalTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 72
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getCustomFields()Ljava/util/List;

    move-result-object v4

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 73
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getTicketFormId()Ljava/lang/Long;

    move-result-object v5

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 74
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->requestProvider()Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v6

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 75
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;-><init>(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lcom/zendesk/sdk/network/RequestProvider;Lcom/zendesk/sdk/storage/IdentityStorage;)V

    return-object v0
.end method


# virtual methods
.method public isValid()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 130
    iget-object v6, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    if-eqz v6, :cond_1

    move v3, v4

    .line 131
    .local v3, "providerAvailable":Z
    :goto_0
    iget-object v6, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    invoke-interface {v6}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v6

    if-eqz v6, :cond_2

    move v1, v4

    .line 133
    .local v1, "hasIdentity":Z
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    move v2, v4

    .line 135
    .local v2, "isValidConnector":Z
    :goto_2
    if-nez v2, :cond_0

    .line 136
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Provider available: %s, identity is valid: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    .line 140
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v8, v4

    .line 137
    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "debugString":Ljava/lang/String;
    const-string v4, "ZendeskFeedbackConnector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Connector is invalid, unable to send feedback. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v6, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    .end local v0    # "debugString":Ljava/lang/String;
    :cond_0
    return v2

    .end local v1    # "hasIdentity":Z
    .end local v2    # "isValidConnector":Z
    .end local v3    # "providerAvailable":Z
    :cond_1
    move v3, v5

    .line 130
    goto :goto_0

    .restart local v3    # "providerAvailable":Z
    :cond_2
    move v1, v5

    .line 131
    goto :goto_1

    .restart local v1    # "hasIdentity":Z
    :cond_3
    move v2, v5

    .line 133
    goto :goto_2
.end method

.method public sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "feedback"    # Ljava/lang/String;
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p2, "attachments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CreateRequest;>;"
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    .line 152
    if-eqz p3, :cond_0

    .line 153
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Configuration is invalid, unable to send feedback."

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    const-string v1, "ZendeskFeedbackConnector"

    const-string v2, "Configuration is valid, attempting to send feedback..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    new-instance v0, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/CreateRequest;-><init>()V

    .line 162
    .local v0, "request":Lcom/zendesk/sdk/model/request/CreateRequest;
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->customFields:Ljava/util/List;

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->ticketFormId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->metadata:Ljava/util/Map;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->setCustomFieldsAndMetadata(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/util/List;Ljava/lang/Long;Ljava/util/Map;)V

    .line 163
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->setEmailFromIdentity(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/Identity;)V

    .line 164
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->requestSubject:Ljava/lang/String;

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->additionalInfo:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->setSubjectAndDescription(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->configurationTags:Ljava/util/List;

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->additionalTags:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->setTags(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/util/List;Ljava/util/List;)V

    .line 167
    if-eqz p2, :cond_2

    .line 168
    invoke-virtual {v0, p2}, Lcom/zendesk/sdk/model/request/CreateRequest;->setAttachments(Ljava/util/List;)V

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    new-instance v2, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;

    invoke-direct {v2, p0, p3, v0}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;-><init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;)V

    invoke-interface {v1, v0, v2}, Lcom/zendesk/sdk/network/RequestProvider;->createRequest(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method setCustomFieldsAndMetadata(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/util/List;Ljava/lang/Long;Ljava/util/Map;)V
    .locals 0
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "ticketFormId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/CustomField;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 198
    .local p2, "customFields":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/CustomField;>;"
    .local p4, "metadata":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1, p3}, Lcom/zendesk/sdk/model/request/CreateRequest;->setTicketFormId(Ljava/lang/Long;)V

    .line 199
    invoke-virtual {p1, p2}, Lcom/zendesk/sdk/model/request/CreateRequest;->setCustomFields(Ljava/util/List;)V

    .line 200
    invoke-virtual {p1, p4}, Lcom/zendesk/sdk/model/request/CreateRequest;->setMetadata(Ljava/util/Map;)V

    .line 201
    return-void
.end method

.method setEmailFromIdentity(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 2
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "identity"    # Lcom/zendesk/sdk/model/access/Identity;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 237
    instance-of v1, p2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 238
    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 240
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/model/request/CreateRequest;->setEmail(Ljava/lang/String;)V

    .line 244
    .end local v0    # "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    :cond_0
    return-void
.end method

.method setSubjectAndDescription(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "subject"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "feedback"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "additionalInfo"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 261
    invoke-virtual {p1, p2}, Lcom/zendesk/sdk/model/request/CreateRequest;->setSubject(Ljava/lang/String;)V

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 264
    .local v0, "descriptionBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-static {p4}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string v1, "ZendeskFeedbackConnector"

    const-string v2, "Additional info is present. Attaching to description."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "------------------------------"

    .line 271
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    .line 272
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    .line 273
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 274
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/model/request/CreateRequest;->setDescription(Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method setTags(Lcom/zendesk/sdk/model/request/CreateRequest;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "configurationTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "additionalTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 215
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/util/List;

    aput-object p2, v1, v3

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->combineLists([Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 220
    .local v0, "combinedTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "ZendeskFeedbackConnector"

    const-string v2, "Adding tags to feedback..."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    invoke-virtual {p1, v0}, Lcom/zendesk/sdk/model/request/CreateRequest;->setTags(Ljava/util/List;)V

    .line 224
    :cond_0
    return-void
.end method
