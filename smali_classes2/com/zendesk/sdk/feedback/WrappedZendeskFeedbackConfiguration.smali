.class public Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;
.super Ljava/lang/Object;
.source "WrappedZendeskFeedbackConfiguration.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;


# instance fields
.field private mAdditionalInfo:Ljava/lang/String;

.field private mRequestSubject:Ljava/lang/String;

.field private mTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V
    .locals 1
    .param p1, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-eqz p1, :cond_0

    .line 25
    invoke-interface {p1}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getTags()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mTags:Ljava/util/List;

    .line 26
    invoke-interface {p1}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mAdditionalInfo:Ljava/lang/String;

    .line 27
    invoke-interface {p1}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;->getRequestSubject()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mRequestSubject:Ljava/lang/String;

    .line 29
    :cond_0
    return-void
.end method


# virtual methods
.method public getAdditionalInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mAdditionalInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mRequestSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;->mTags:Ljava/util/List;

    return-object v0
.end method
