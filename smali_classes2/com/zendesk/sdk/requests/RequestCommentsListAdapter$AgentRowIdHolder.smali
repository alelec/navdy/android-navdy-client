.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AgentRowIdHolder;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"

# interfaces
.implements Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AgentRowIdHolder"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AgentRowIdHolder;-><init>()V

    return-void
.end method


# virtual methods
.method public getAttachmentsContainerId()I
    .locals 1

    .prologue
    .line 131
    sget v0, Lcom/zendesk/sdk/R$id;->view_request_agent_response_attachment_container:I

    return v0
.end method

.method public getAvatarId()I
    .locals 1

    .prologue
    .line 111
    sget v0, Lcom/zendesk/sdk/R$id;->view_request_agent_avatar_imageview:I

    return v0
.end method

.method public getContainerId()I
    .locals 1

    .prologue
    .line 106
    sget v0, Lcom/zendesk/sdk/R$layout;->row_agent_comment:I

    return v0
.end method

.method public getDateId()I
    .locals 1

    .prologue
    .line 126
    sget v0, Lcom/zendesk/sdk/R$id;->view_request_agent_comment_date:I

    return v0
.end method

.method public getNameId()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/zendesk/sdk/R$id;->view_request_agent_name_textview:I

    return v0
.end method

.method public getResponseId()I
    .locals 1

    .prologue
    .line 121
    sget v0, Lcom/zendesk/sdk/R$id;->view_request_agent_response_textview:I

    return v0
.end method
