.class public Lcom/zendesk/sdk/storage/StorageInjector;
.super Ljava/lang/Object;
.source "StorageInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static injectCachedHelpCenterSessionCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 68
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/StorageModule;->getHelpCenterSessionCache()Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v0

    return-object v0
.end method

.method public static injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/StorageModule;->getIdentityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v0

    return-object v0
.end method

.method public static injectCachedRequestStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 32
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/StorageModule;->getRequestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v0

    return-object v0
.end method

.method public static injectCachedSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 45
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/StorageModule;->getSdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v0

    return-object v0
.end method

.method public static injectCachedSdkStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkStorage;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 22
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/StorageModule;->getSdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v0

    return-object v0
.end method

.method static injectCachedStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 88
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectStorageModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/storage/StorageInjector$1;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/storage/StorageInjector$1;-><init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/util/ScopeCache;->get(Lcom/zendesk/sdk/util/DependencyProvider;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/storage/StorageModule;

    return-object v0
.end method

.method static injectHelpCenterSessionCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 64
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/ZendeskHelpCenterSessionCache;-><init>()V

    return-object v0
.end method

.method static injectIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 51
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;

    .line 52
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectApplicationContext(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Landroid/content/Context;

    move-result-object v1

    .line 53
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/storage/ZendeskIdentityStorage;-><init>(Landroid/content/Context;Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method static injectRequestStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 28
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;

    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectApplicationContext(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static injectSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 38
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;

    .line 39
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectApplicationContext(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Landroid/content/Context;

    move-result-object v1

    .line 40
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/storage/ZendeskSdkSettingsStorage;-><init>(Landroid/content/Context;Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method static injectSdkStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkStorage;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 18
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;-><init>()V

    return-object v0
.end method

.method static injectStorageModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageModule;
    .locals 6
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 74
    new-instance v0, Lcom/zendesk/sdk/storage/StorageModule;

    .line 75
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectSdkStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v1

    .line 76
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v2

    .line 77
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectRequestStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v3

    .line 78
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v4

    .line 79
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectHelpCenterSessionCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/storage/StorageModule;-><init>(Lcom/zendesk/sdk/storage/SdkStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/storage/RequestStorage;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V

    return-object v0
.end method

.method private static injectStorageModuleCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/ScopeCache;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/network/impl/ApplicationScope;",
            ")",
            "Lcom/zendesk/sdk/util/ScopeCache",
            "<",
            "Lcom/zendesk/sdk/storage/StorageModule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/zendesk/sdk/network/impl/ApplicationScope;->getStorageModuleCache()Lcom/zendesk/sdk/util/ScopeCache;

    move-result-object v0

    return-object v0
.end method

.method public static injectStorageStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StorageStore;
    .locals 6
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 98
    new-instance v0, Lcom/zendesk/sdk/storage/ZendeskStorageStore;

    .line 99
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedSdkStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v1

    .line 100
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedIdentityStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v2

    .line 101
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedRequestStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v3

    .line 102
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedSdkSettingsStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v4

    .line 103
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedHelpCenterSessionCache(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/storage/ZendeskStorageStore;-><init>(Lcom/zendesk/sdk/storage/SdkStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/storage/RequestStorage;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V

    return-object v0
.end method

.method public static injectStubStorageStore(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/StubStorageStore;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 108
    new-instance v0, Lcom/zendesk/sdk/storage/StubStorageStore;

    invoke-direct {v0}, Lcom/zendesk/sdk/storage/StubStorageStore;-><init>()V

    return-object v0
.end method
