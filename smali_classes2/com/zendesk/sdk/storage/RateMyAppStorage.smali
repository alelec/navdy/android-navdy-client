.class public Lcom/zendesk/sdk/storage/RateMyAppStorage;
.super Ljava/lang/Object;
.source "RateMyAppStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;


# static fields
.field private static final FEEDBACK_CONTENT:Ljava/lang/String; = "zd_content"

.field private static final LOG_TAG:Ljava/lang/String; = "RateMyAppStorage"

.field private static final PREFERENCES_FILE_NAME:Ljava/lang/String; = "zd_rma"

.field private static final RMA_PREFERENCES_DONT_SHOW_AGAIN_FOR_VERSION:Ljava/lang/String; = "rma_dont_show_again_for_version"

.field private static final RMA_PREFERENCES_NUMBER_OF_LAUNCHES:Ljava/lang/String; = "rma_number_of_launches"

.field private static final RMA_PREFERENCES_RATED_FOR_VERSION:Ljava/lang/String; = "rma_rated_for_version"

.field private static final RMA_PREFERENCES_SAVED_LAUNCH_TIME:Ljava/lang/String; = "rma_saved_launch_time"


# instance fields
.field private mAppVersion:Lcom/zendesk/sdk/model/AppVersion;

.field private mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    if-nez p1, :cond_0

    .line 36
    const-string v0, "RateMyAppStorage"

    const-string v1, "Context is null, storage will not work"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    :goto_0
    return-void

    .line 40
    :cond_0
    const-string v0, "zd_rma"

    .line 41
    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 43
    new-instance v0, Lcom/zendesk/sdk/model/AppVersion;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/model/AppVersion;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mAppVersion:Lcom/zendesk/sdk/model/AppVersion;

    .line 45
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .line 47
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->sdkStorage()Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/zendesk/sdk/storage/SdkStorage;->registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V

    goto :goto_0
.end method


# virtual methods
.method public clearUserData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 302
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 303
    const-string v0, "RateMyAppStorage"

    const-string v1, "deleteSavedFeedback() Storage is null, returning"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    :goto_0
    return-void

    .line 307
    :cond_0
    const-string v0, "RateMyAppStorage"

    const-string v1, "Deleting saved feedback"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "zd_content"

    .line 310
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 311
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public getCacheKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    const-string v0, "zd_rma"

    return-object v0
.end method

.method public getCanShowAfterTime()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 83
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    .line 84
    const-string v2, "RateMyAppStorage"

    const-string v3, "getCanShowAfterTime() Storage is null, returning current time"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 100
    :goto_0
    return-wide v2

    .line 88
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "rma_saved_launch_time"

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 90
    .local v0, "savedLaunchTime":J
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 92
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 94
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 95
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "rma_saved_launch_time"

    .line 96
    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 97
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 100
    :cond_1
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppDuration()J

    move-result-wide v2

    add-long/2addr v2, v0

    goto :goto_0
.end method

.method public getNumberOfLaunches()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 60
    :goto_0
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "rma_number_of_launches"

    .line 60
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public getSavedFeedback()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 261
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 263
    :goto_0
    return-object v0

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "zd_content"

    .line 263
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public incrementNumberOfLaunches()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 108
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 109
    const-string v1, "RateMyAppStorage"

    const-string v2, "incrementNumberOfLaunches() Storage is null, returning"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->getNumberOfLaunches()I

    move-result v0

    .line 114
    .local v0, "currentNumberOfLaunches":I
    add-int/lit8 v0, v0, 0x1

    .line 115
    const-string v1, "RateMyAppStorage"

    const-string v2, "Incrementing number of RMA launches"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 118
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "rma_number_of_launches"

    .line 119
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 120
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public isDontShowAgain()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 220
    iget-object v3, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v3, :cond_1

    .line 221
    const-string v3, "RateMyAppStorage"

    const-string v4, "isDontShowAgain() Storage is null, returning true"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    :cond_0
    :goto_0
    return v1

    .line 225
    :cond_1
    iget-object v3, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "rma_dont_show_again_for_version"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 226
    .local v0, "dontShowAgainVersion":I
    if-ne v0, v5, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public isLaunchTimeMet()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 155
    iget-object v5, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v5, :cond_1

    .line 156
    const-string v5, "RateMyAppStorage"

    const-string v6, "isLaunchTimeMet() Storage is null, returning false"

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :cond_0
    :goto_0
    return v4

    .line 160
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->getCanShowAfterTime()J

    move-result-wide v0

    .line 161
    .local v0, "canShowAfterTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 163
    .local v2, "currentTime":J
    const-string v5, "RateMyAppStorage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Current time is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and settings requires >= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    cmp-long v5, v2, v0

    if-ltz v5, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public isNumberOfLaunchesMet()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 136
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->hasHelpCenterSettings()Z

    move-result v2

    if-nez v2, :cond_1

    .line 137
    const-string v2, "RateMyAppStorage"

    const-string v3, "Settings are null"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    :cond_0
    :goto_0
    return v1

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->getNumberOfLaunches()I

    move-result v0

    .line 142
    .local v0, "currentNumberOfLaunches":I
    const-string v2, "RateMyAppStorage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current launches is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and settings requires >= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppVisits()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    int-to-long v2, v0

    iget-object v4, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppVisits()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRatedForCurrentVersion()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 175
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_1

    .line 176
    const-string v2, "RateMyAppStorage"

    const-string v3, "isRatedForCurrentVersion() Storage is null, returning true"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    const/4 v0, 0x1

    .line 188
    :cond_0
    :goto_0
    return v0

    .line 180
    :cond_1
    const/4 v0, 0x0

    .line 182
    .local v0, "isRatedForCurrentVersion":Z
    iget-object v2, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "rma_rated_for_version"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 184
    .local v1, "savedVersion":I
    if-eq v1, v4, :cond_0

    .line 185
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 292
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 293
    const-string v0, "RateMyAppStorage"

    const-string v1, "reset() Storage is null, returning"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public setDontShowAgain()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 239
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 240
    const-string v1, "RateMyAppStorage"

    const-string v2, "setDontShowSagain() Storage is null, returning"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 251
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mAppVersion:Lcom/zendesk/sdk/model/AppVersion;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/AppVersion;->getAppVersionCode()I

    move-result v0

    .line 245
    .local v0, "appVersionCode":I
    const-string v1, "RateMyAppStorage"

    const-string v2, "Setting don\'t show again for version code %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 248
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "rma_dont_show_again_for_version"

    .line 249
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 250
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public setRatedForCurrentVersion()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 199
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 200
    const-string v1, "RateMyAppStorage"

    const-string v2, "setRatedForCurrentVersion() Storage is null, returning"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mAppVersion:Lcom/zendesk/sdk/model/AppVersion;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/AppVersion;->getAppVersionCode()I

    move-result v0

    .line 205
    .local v0, "appVersionCode":I
    const-string v1, "RateMyAppStorage"

    const-string v2, "Setting rated for version code %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    iget-object v1, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 208
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "rma_rated_for_version"

    .line 209
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 210
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public setSavedFeedback(Ljava/lang/String;)V
    .locals 3
    .param p1, "feedback"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 274
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 275
    const-string v0, "RateMyAppStorage"

    const-string v1, "setSavedFeedback() Storage is null, returning"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    :goto_0
    return-void

    .line 279
    :cond_0
    const-string v0, "RateMyAppStorage"

    const-string v1, "Saving feedback to storage"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    iget-object v0, p0, Lcom/zendesk/sdk/storage/RateMyAppStorage;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "zd_content"

    .line 281
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 282
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
