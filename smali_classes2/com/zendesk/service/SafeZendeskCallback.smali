.class public Lcom/zendesk/service/SafeZendeskCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "SafeZendeskCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/zendesk/service/ZendeskCallback",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SafeZendeskCallback"


# instance fields
.field private final callback:Lcom/zendesk/service/ZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/ZendeskCallback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private cancelled:Z


# direct methods
.method public constructor <init>(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/zendesk/service/SafeZendeskCallback;, "Lcom/zendesk/service/SafeZendeskCallback<TT;>;"
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<TT;>;"
    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/zendesk/service/SafeZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->cancelled:Z

    .line 27
    return-void
.end method

.method public static from(Lcom/zendesk/service/ZendeskCallback;)Lcom/zendesk/service/SafeZendeskCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<TT;>;)",
            "Lcom/zendesk/service/SafeZendeskCallback",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<TT;>;"
    new-instance v0, Lcom/zendesk/service/SafeZendeskCallback;

    invoke-direct {v0, p0}, Lcom/zendesk/service/SafeZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 51
    .local p0, "this":Lcom/zendesk/service/SafeZendeskCallback;, "Lcom/zendesk/service/SafeZendeskCallback<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->cancelled:Z

    .line 52
    return-void
.end method

.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 40
    .local p0, "this":Lcom/zendesk/service/SafeZendeskCallback;, "Lcom/zendesk/service/SafeZendeskCallback<TT;>;"
    iget-boolean v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->cancelled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    const-string v0, "SafeZendeskCallback"

    invoke-static {v0, p1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/zendesk/service/SafeZendeskCallback;, "Lcom/zendesk/service/SafeZendeskCallback<TT;>;"
    .local p1, "result":Ljava/lang/Object;, "TT;"
    iget-boolean v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->cancelled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/zendesk/service/SafeZendeskCallback;->callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 36
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v0, "SafeZendeskCallback"

    const-string v1, "Operation was a success but callback is null or was cancelled"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
