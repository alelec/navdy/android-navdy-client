.class public Lcom/google/maps/internal/RateLimitExecutorService;
.super Ljava/lang/Object;
.source "RateLimitExecutorService.java"

# interfaces
.implements Ljava/util/concurrent/ExecutorService;
.implements Ljava/lang/Runnable;


# static fields
.field private static final DEFAULT_QUERIES_PER_SECOND:I = 0xa

.field private static final HALF_SECOND:I = 0x1f4

.field private static final SECOND:I = 0x3e8

.field private static final log:Ljava/util/logging/Logger;


# instance fields
.field private final delegate:Ljava/util/concurrent/ExecutorService;

.field private lastSentTime:J

.field private volatile minimumDelay:I

.field private volatile queriesPerSecond:I

.field private final queue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private sentTimes:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/maps/internal/RateLimitExecutorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/maps/internal/RateLimitExecutorService;->log:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string v8, "Rate Limited Dispatcher"

    invoke-static {v8, v9}, Lcom/google/maps/internal/RateLimitExecutorService;->threadFactory(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    .line 51
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queue:Ljava/util/concurrent/BlockingQueue;

    .line 55
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    .line 56
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/maps/internal/RateLimitExecutorService;->lastSentTime:J

    .line 59
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/google/maps/internal/RateLimitExecutorService;->setQueriesPerSecond(I)V

    .line 60
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 61
    .local v0, "delayThread":Ljava/lang/Thread;
    invoke-virtual {v0, v9}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 62
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 63
    return-void
.end method

.method private static threadFactory(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "daemon"    # Z

    .prologue
    .line 119
    new-instance v0, Lcom/google/maps/internal/RateLimitExecutorService$1;

    invoke-direct {v0, p0, p1}, Lcom/google/maps/internal/RateLimitExecutorService$1;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method


# virtual methods
.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1
    .param p1, "l"    # J
    .param p3, "timeUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 131
    return-void
.end method

.method public invokeAll(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<TT;>;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "callables":Ljava/util/Collection;, "Ljava/util/Collection<+Ljava/util/concurrent/Callable<TT;>;>;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;
    .locals 2
    .param p2, "l"    # J
    .param p4, "timeUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<TT;>;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "callables":Ljava/util/Collection;, "Ljava/util/Collection<+Ljava/util/concurrent/Callable<TT;>;>;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public invokeAny(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "callables":Ljava/util/Collection;, "Ljava/util/Collection<+Ljava/util/concurrent/Callable<TT;>;>;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 2
    .param p2, "l"    # J
    .param p4, "timeUnit"    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;J",
            "Ljava/util/concurrent/TimeUnit;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "callables":Ljava/util/Collection;, "Ljava/util/Collection<+Ljava/util/concurrent/Callable<TT;>;>;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2, p3, p4}, Ljava/util/concurrent/ExecutorService;->invokeAny(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isShutdown()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public isTerminated()Z
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isTerminated()Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 14

    .prologue
    .line 83
    :goto_0
    :try_start_0
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v10}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v10

    if-nez v10, :cond_1

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 85
    .local v4, "now":J
    const-wide/16 v10, 0x3e8

    sub-long v6, v4, v10

    .line 87
    .local v6, "oneSecondAgo":J
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v10}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Runnable;

    .line 89
    .local v3, "r":Ljava/lang/Runnable;
    iget-wide v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->lastSentTime:J

    iget v12, p0, Lcom/google/maps/internal/RateLimitExecutorService;->minimumDelay:I

    int-to-long v12, v12

    add-long/2addr v10, v12

    sub-long v8, v10, v4

    .line 90
    .local v8, "requiredSeparationDelay":J
    const-wide/16 v10, 0x0

    cmp-long v10, v8, v10

    if-lez v10, :cond_0

    .line 91
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 95
    :cond_0
    :goto_1
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    if-lez v10, :cond_2

    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->peekFirst()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v10, v10, v6

    if-gez v10, :cond_2

    .line 96
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 113
    .end local v3    # "r":Ljava/lang/Runnable;
    .end local v4    # "now":J
    .end local v6    # "oneSecondAgo":J
    .end local v8    # "requiredSeparationDelay":J
    :catch_0
    move-exception v2

    .line 114
    .local v2, "ie":Ljava/lang/InterruptedException;
    sget-object v10, Lcom/google/maps/internal/RateLimitExecutorService;->log:Ljava/util/logging/Logger;

    sget-object v11, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v12, "Interrupted"

    invoke-virtual {v10, v11, v12, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 116
    .end local v2    # "ie":Ljava/lang/InterruptedException;
    :cond_1
    return-void

    .line 99
    .restart local v3    # "r":Ljava/lang/Runnable;
    .restart local v4    # "now":J
    .restart local v6    # "oneSecondAgo":J
    .restart local v8    # "requiredSeparationDelay":J
    :cond_2
    const-wide/16 v0, 0x0

    .line 100
    .local v0, "delay":J
    :try_start_1
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    if-lez v10, :cond_3

    .line 101
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->peekFirst()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    add-long/2addr v10, v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v0, v10, v12

    .line 104
    :cond_3
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v10

    iget v11, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queriesPerSecond:I

    if-lt v10, v11, :cond_4

    const-wide/16 v10, 0x0

    cmp-long v10, v0, v10

    if-gtz v10, :cond_5

    .line 105
    :cond_4
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v10, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 106
    iput-wide v4, p0, Lcom/google/maps/internal/RateLimitExecutorService;->lastSentTime:J

    .line 107
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->sentTimes:Ljava/util/LinkedList;

    iget-wide v12, p0, Lcom/google/maps/internal/RateLimitExecutorService;->lastSentTime:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 109
    :cond_5
    iget-object v10, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v10, v3}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public setQueriesPerSecond(I)V
    .locals 2
    .param p1, "maxQps"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queriesPerSecond:I

    .line 67
    const/16 v0, 0x1f4

    iget v1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queriesPerSecond:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->minimumDelay:I

    .line 68
    return-void
.end method

.method public setQueriesPerSecond(II)V
    .locals 4
    .param p1, "maxQps"    # I
    .param p2, "minimumInterval"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/google/maps/internal/RateLimitExecutorService;->queriesPerSecond:I

    .line 72
    iput p2, p0, Lcom/google/maps/internal/RateLimitExecutorService;->minimumDelay:I

    .line 74
    sget-object v0, Lcom/google/maps/internal/RateLimitExecutorService;->log:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x58

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Configuring rate limit at QPS: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", minimum delay "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms between requests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 138
    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 167
    .local p2, "t":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "tCallable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iget-object v0, p0, Lcom/google/maps/internal/RateLimitExecutorService;->delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method
