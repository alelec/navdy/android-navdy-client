.class public Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;
.super Ljava/lang/Object;
.source "DefaultJWEDecrypterFactory.java"

# interfaces
.implements Lcom/nimbusds/jose/proc/JWEDecrypterFactory;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final SUPPORTED_ALGORITHMS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/JWEAlgorithm;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 46
    .local v0, "algs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/JWEAlgorithm;>;"
    sget-object v2, Lcom/nimbusds/jose/crypto/RSADecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 47
    sget-object v2, Lcom/nimbusds/jose/crypto/ECDHDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 48
    sget-object v2, Lcom/nimbusds/jose/crypto/DirectDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 49
    sget-object v2, Lcom/nimbusds/jose/crypto/AESDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 50
    sget-object v2, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 51
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    .line 53
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 54
    .local v1, "encs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/EncryptionMethod;>;"
    sget-object v2, Lcom/nimbusds/jose/crypto/RSADecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 55
    sget-object v2, Lcom/nimbusds/jose/crypto/ECDHDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 56
    sget-object v2, Lcom/nimbusds/jose/crypto/DirectDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 57
    sget-object v2, Lcom/nimbusds/jose/crypto/AESDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 58
    sget-object v2, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 59
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    .line 60
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-direct {v0}, Lcom/nimbusds/jose/jca/JWEJCAContext;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    .line 29
    return-void
.end method


# virtual methods
.method public createJWEDecrypter(Lcom/nimbusds/jose/JWEHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWEDecrypter;
    .locals 10
    .param p1, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p2, "key"    # Ljava/security/Key;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v7, Lcom/nimbusds/jose/crypto/RSADecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 97
    sget-object v7, Lcom/nimbusds/jose/crypto/RSADecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 99
    instance-of v7, p2, Ljava/security/interfaces/RSAPrivateKey;

    if-nez v7, :cond_0

    .line 100
    new-instance v7, Lcom/nimbusds/jose/KeyTypeException;

    const-class v8, Ljava/security/interfaces/RSAPrivateKey;

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v7

    :cond_0
    move-object v6, p2

    .line 103
    check-cast v6, Ljava/security/interfaces/RSAPrivateKey;

    .line 105
    .local v6, "rsaPrivateKey":Ljava/security/interfaces/RSAPrivateKey;
    new-instance v2, Lcom/nimbusds/jose/crypto/RSADecrypter;

    invoke-direct {v2, v6}, Lcom/nimbusds/jose/crypto/RSADecrypter;-><init>(Ljava/security/interfaces/RSAPrivateKey;)V

    .line 165
    .end local v6    # "rsaPrivateKey":Ljava/security/interfaces/RSAPrivateKey;
    .local v2, "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    :goto_0
    invoke-interface {v2}, Lcom/nimbusds/jose/JWEDecrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    check-cast v7, Lcom/nimbusds/jose/jca/JWEJCAContext;

    iget-object v8, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->setSecureRandom(Ljava/security/SecureRandom;)V

    .line 166
    invoke-interface {v2}, Lcom/nimbusds/jose/JWEDecrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    check-cast v7, Lcom/nimbusds/jose/jca/JWEJCAContext;

    iget-object v8, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->setProvider(Ljava/security/Provider;)V

    .line 167
    invoke-interface {v2}, Lcom/nimbusds/jose/JWEDecrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    check-cast v7, Lcom/nimbusds/jose/jca/JWEJCAContext;

    iget-object v8, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->setKeyEncryptionProvider(Ljava/security/Provider;)V

    .line 168
    invoke-interface {v2}, Lcom/nimbusds/jose/JWEDecrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    check-cast v7, Lcom/nimbusds/jose/jca/JWEJCAContext;

    iget-object v8, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getMACProvider()Ljava/security/Provider;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->setMACProvider(Ljava/security/Provider;)V

    .line 169
    invoke-interface {v2}, Lcom/nimbusds/jose/JWEDecrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    check-cast v7, Lcom/nimbusds/jose/jca/JWEJCAContext;

    iget-object v8, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    invoke-virtual {v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getContentEncryptionProvider()Ljava/security/Provider;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/nimbusds/jose/jca/JWEJCAContext;->setContentEncryptionProvider(Ljava/security/Provider;)V

    .line 171
    return-object v2

    .line 107
    .end local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    :cond_1
    sget-object v7, Lcom/nimbusds/jose/crypto/ECDHDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 108
    sget-object v7, Lcom/nimbusds/jose/crypto/ECDHDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 110
    instance-of v7, p2, Ljava/security/interfaces/ECPrivateKey;

    if-nez v7, :cond_2

    .line 111
    new-instance v7, Lcom/nimbusds/jose/KeyTypeException;

    const-class v8, Ljava/security/interfaces/ECPrivateKey;

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v7

    :cond_2
    move-object v4, p2

    .line 114
    check-cast v4, Ljava/security/interfaces/ECPrivateKey;

    .line 115
    .local v4, "ecPrivateKey":Ljava/security/interfaces/ECPrivateKey;
    new-instance v2, Lcom/nimbusds/jose/crypto/ECDHDecrypter;

    invoke-direct {v2, v4}, Lcom/nimbusds/jose/crypto/ECDHDecrypter;-><init>(Ljava/security/interfaces/ECPrivateKey;)V

    .line 117
    .restart local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    goto :goto_0

    .end local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    .end local v4    # "ecPrivateKey":Ljava/security/interfaces/ECPrivateKey;
    :cond_3
    sget-object v7, Lcom/nimbusds/jose/crypto/DirectDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 118
    sget-object v7, Lcom/nimbusds/jose/crypto/DirectDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 120
    instance-of v7, p2, Ljavax/crypto/SecretKey;

    if-nez v7, :cond_4

    .line 121
    new-instance v7, Lcom/nimbusds/jose/KeyTypeException;

    const-class v8, Ljavax/crypto/SecretKey;

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v7

    :cond_4
    move-object v1, p2

    .line 124
    check-cast v1, Ljavax/crypto/SecretKey;

    .line 125
    .local v1, "aesKey":Ljavax/crypto/SecretKey;
    new-instance v3, Lcom/nimbusds/jose/crypto/DirectDecrypter;

    invoke-direct {v3, v1}, Lcom/nimbusds/jose/crypto/DirectDecrypter;-><init>(Ljavax/crypto/SecretKey;)V

    .line 127
    .local v3, "directDecrypter":Lcom/nimbusds/jose/crypto/DirectDecrypter;
    invoke-virtual {v3}, Lcom/nimbusds/jose/crypto/DirectDecrypter;->supportedEncryptionMethods()Ljava/util/Set;

    move-result-object v7

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 128
    new-instance v7, Lcom/nimbusds/jose/KeyLengthException;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-virtual {v8}, Lcom/nimbusds/jose/EncryptionMethod;->cekBitLength()I

    move-result v8

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/nimbusds/jose/KeyLengthException;-><init>(ILcom/nimbusds/jose/Algorithm;)V

    throw v7

    .line 131
    :cond_5
    move-object v2, v3

    .line 133
    .restart local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    goto/16 :goto_0

    .end local v1    # "aesKey":Ljavax/crypto/SecretKey;
    .end local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    .end local v3    # "directDecrypter":Lcom/nimbusds/jose/crypto/DirectDecrypter;
    :cond_6
    sget-object v7, Lcom/nimbusds/jose/crypto/AESDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 134
    sget-object v7, Lcom/nimbusds/jose/crypto/AESDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 136
    instance-of v7, p2, Ljavax/crypto/SecretKey;

    if-nez v7, :cond_7

    .line 137
    new-instance v7, Lcom/nimbusds/jose/KeyTypeException;

    const-class v8, Ljavax/crypto/SecretKey;

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v7

    :cond_7
    move-object v1, p2

    .line 140
    check-cast v1, Ljavax/crypto/SecretKey;

    .line 141
    .restart local v1    # "aesKey":Ljavax/crypto/SecretKey;
    new-instance v0, Lcom/nimbusds/jose/crypto/AESDecrypter;

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/crypto/AESDecrypter;-><init>(Ljavax/crypto/SecretKey;)V

    .line 143
    .local v0, "aesDecrypter":Lcom/nimbusds/jose/crypto/AESDecrypter;
    invoke-virtual {v0}, Lcom/nimbusds/jose/crypto/AESDecrypter;->supportedJWEAlgorithms()Ljava/util/Set;

    move-result-object v7

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 144
    new-instance v7, Lcom/nimbusds/jose/KeyLengthException;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Lcom/nimbusds/jose/Algorithm;)V

    throw v7

    .line 147
    :cond_8
    move-object v2, v0

    .line 149
    .restart local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    goto/16 :goto_0

    .end local v0    # "aesDecrypter":Lcom/nimbusds/jose/crypto/AESDecrypter;
    .end local v1    # "aesKey":Ljavax/crypto/SecretKey;
    .end local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    :cond_9
    sget-object v7, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 150
    sget-object v7, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 152
    instance-of v7, p2, Ljavax/crypto/SecretKey;

    if-nez v7, :cond_a

    .line 153
    new-instance v7, Lcom/nimbusds/jose/KeyTypeException;

    const-class v8, Ljavax/crypto/SecretKey;

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v7

    .line 156
    :cond_a
    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v5

    .line 157
    .local v5, "password":[B
    new-instance v2, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;

    invoke-direct {v2, v5}, Lcom/nimbusds/jose/crypto/PasswordBasedDecrypter;-><init>([B)V

    .line 159
    .restart local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    goto/16 :goto_0

    .line 161
    .end local v2    # "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    .end local v5    # "password":[B
    :cond_b
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    const-string v8, "Unsupported JWE algorithm or encryption method"

    invoke-direct {v7, v8}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public bridge synthetic getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v0

    return-object v0
.end method

.method public getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->jcaContext:Lcom/nimbusds/jose/jca/JWEJCAContext;

    return-object v0
.end method

.method public supportedEncryptionMethods()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->SUPPORTED_ENCRYPTION_METHODS:Ljava/util/Set;

    return-object v0
.end method

.method public supportedJWEAlgorithms()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/JWEAlgorithm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    return-object v0
.end method
