.class public Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;
.super Ljava/lang/Object;
.source "DefaultJOSEProcessor.java"

# interfaces
.implements Lcom/nimbusds/jose/proc/ConfigurableJOSEProcessor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C::",
        "Lcom/nimbusds/jose/proc/SecurityContext;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/nimbusds/jose/proc/ConfigurableJOSEProcessor",
        "<TC;>;"
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field private static final INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

.field private static final NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

.field private static final NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

.field private static final PLAIN_JOSE_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;


# instance fields
.field private jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

.field private jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;"
        }
    .end annotation
.end field

.field private jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;"
        }
    .end annotation
.end field

.field private jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "Unsecured (plain) JOSE objects are rejected, extend class to handle"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 63
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->PLAIN_JOSE_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 66
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWS object rejected: No JWS key selector is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 65
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 68
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWE object rejected: No JWE key selector is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 67
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 70
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "No JWS verifier is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    .line 69
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    .line 72
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "No JWE decrypter is configured"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    .line 71
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    .line 74
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWS object rejected: No matching key(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 73
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 76
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWE object rejected: No matching key(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 75
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 78
    new-instance v0, Lcom/nimbusds/jose/proc/BadJWSException;

    const-string v1, "JWS object rejected: Invalid signature"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJWSException;-><init>(Ljava/lang/String;)V

    .line 77
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 80
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWS object rejected: No matching verifier(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 79
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 82
    new-instance v0, Lcom/nimbusds/jose/proc/BadJOSEException;

    const-string v1, "JWE object rejected: No matching decrypter(s) found"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/proc/BadJOSEException;-><init>(Ljava/lang/String;)V

    .line 81
    sput-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    .line 82
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;

    invoke-direct {v0}, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .line 106
    new-instance v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;

    invoke-direct {v0}, Lcom/nimbusds/jose/crypto/factories/DefaultJWEDecrypterFactory;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .line 60
    return-void
.end method


# virtual methods
.method public getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;
    .locals 1

    .prologue
    .line 154
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    return-object v0
.end method

.method public getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 126
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;

    return-object v0
.end method

.method public getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;

    return-object v0
.end method

.method public getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;
    .locals 1

    .prologue
    .line 140
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iget-object v0, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    return-object v0
.end method

.method public process(Lcom/nimbusds/jose/JOSEObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;
    .locals 3
    .param p1, "joseObject"    # Lcom/nimbusds/jose/JOSEObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/JOSEObject;",
            "TC;)",
            "Lcom/nimbusds/jose/Payload;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 177
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    instance-of v0, p1, Lcom/nimbusds/jose/JWSObject;

    if-eqz v0, :cond_0

    .line 178
    check-cast p1, Lcom/nimbusds/jose/JWSObject;

    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->process(Lcom/nimbusds/jose/JWSObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;

    move-result-object v0

    .line 186
    :goto_0
    return-object v0

    .line 181
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_0
    instance-of v0, p1, Lcom/nimbusds/jose/JWEObject;

    if-eqz v0, :cond_1

    .line 182
    check-cast p1, Lcom/nimbusds/jose/JWEObject;

    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->process(Lcom/nimbusds/jose/JWEObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;

    move-result-object v0

    goto :goto_0

    .line 185
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_1
    instance-of v0, p1, Lcom/nimbusds/jose/PlainObject;

    if-eqz v0, :cond_2

    .line 186
    check-cast p1, Lcom/nimbusds/jose/PlainObject;

    .end local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    invoke-virtual {p0, p1, p2}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->process(Lcom/nimbusds/jose/PlainObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;

    move-result-object v0

    goto :goto_0

    .line 190
    .restart local p1    # "joseObject":Lcom/nimbusds/jose/JOSEObject;
    :cond_2
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected JOSE object type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public process(Lcom/nimbusds/jose/JWEObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;
    .locals 8
    .param p1, "jweObject"    # Lcom/nimbusds/jose/JWEObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/JWEObject;",
            "TC;)",
            "Lcom/nimbusds/jose/Payload;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 251
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;

    move-result-object v5

    if-nez v5, :cond_0

    .line 253
    sget-object v5, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 256
    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    move-result-object v5

    if-nez v5, :cond_1

    .line 257
    sget-object v5, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_DECRYPTER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    throw v5

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWEKeySelector()Lcom/nimbusds/jose/proc/JWEKeySelector;

    move-result-object v5

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v6

    invoke-interface {v5, v6, p2}, Lcom/nimbusds/jose/proc/JWEKeySelector;->selectJWEKeys(Lcom/nimbusds/jose/JWEHeader;Lcom/nimbusds/jose/proc/SecurityContext;)Ljava/util/List;

    move-result-object v3

    .line 262
    .local v3, "keyCandidates":Ljava/util/List;, "Ljava/util/List<+Ljava/security/Key;>;"
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 263
    :cond_2
    sget-object v5, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWE_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 266
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 268
    .local v2, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<+Ljava/security/Key;>;"
    :cond_4
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 306
    sget-object v5, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_MATCHING_DECRYPTERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v5

    .line 270
    :cond_5
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWEDecrypterFactory()Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    move-result-object v6

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v7

    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/security/Key;

    invoke-interface {v6, v7, v5}, Lcom/nimbusds/jose/proc/JWEDecrypterFactory;->createJWEDecrypter(Lcom/nimbusds/jose/JWEHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWEDecrypter;

    move-result-object v0

    .line 272
    .local v0, "decrypter":Lcom/nimbusds/jose/JWEDecrypter;
    if-eqz v0, :cond_4

    .line 277
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/nimbusds/jose/JWEObject;->decrypt(Lcom/nimbusds/jose/JWEDecrypter;)V
    :try_end_0
    .catch Lcom/nimbusds/jose/JOSEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    const-string v5, "JWT"

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getHeader()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/JWEHeader;->getContentType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 293
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v5

    invoke-virtual {v5}, Lcom/nimbusds/jose/Payload;->toJWSObject()Lcom/nimbusds/jose/JWSObject;

    move-result-object v4

    .line 295
    .local v4, "nestedJWS":Lcom/nimbusds/jose/JWSObject;
    if-nez v4, :cond_6

    .line 297
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v5

    .line 303
    .end local v4    # "nestedJWS":Lcom/nimbusds/jose/JWSObject;
    :goto_0
    return-object v5

    .line 279
    :catch_0
    move-exception v1

    .line 281
    .local v1, "e":Lcom/nimbusds/jose/JOSEException;
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 287
    new-instance v5, Lcom/nimbusds/jose/proc/BadJWEException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "JWE object rejected: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/nimbusds/jose/JOSEException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lcom/nimbusds/jose/proc/BadJWEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 300
    .end local v1    # "e":Lcom/nimbusds/jose/JOSEException;
    .restart local v4    # "nestedJWS":Lcom/nimbusds/jose/JWSObject;
    :cond_6
    invoke-virtual {p0, v4, p2}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->process(Lcom/nimbusds/jose/JWSObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;

    move-result-object v5

    goto :goto_0

    .line 303
    .end local v4    # "nestedJWS":Lcom/nimbusds/jose/JWSObject;
    :cond_7
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEObject;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v5

    goto :goto_0
.end method

.method public process(Lcom/nimbusds/jose/JWSObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;
    .locals 7
    .param p1, "jwsObject"    # Lcom/nimbusds/jose/JWSObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/JWSObject;",
            "TC;)",
            "Lcom/nimbusds/jose/Payload;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;

    move-result-object v4

    if-nez v4, :cond_0

    .line 208
    sget-object v4, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_KEY_SELECTOR_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 211
    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    move-result-object v4

    if-nez v4, :cond_1

    .line 212
    sget-object v4, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_VERIFIER_FACTORY_EXCEPTION:Lcom/nimbusds/jose/JOSEException;

    throw v4

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWSKeySelector()Lcom/nimbusds/jose/proc/JWSKeySelector;

    move-result-object v4

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSObject;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v5

    invoke-interface {v4, v5, p2}, Lcom/nimbusds/jose/proc/JWSKeySelector;->selectJWSKeys(Lcom/nimbusds/jose/JWSHeader;Lcom/nimbusds/jose/proc/SecurityContext;)Ljava/util/List;

    move-result-object v1

    .line 217
    .local v1, "keyCandidates":Ljava/util/List;, "Ljava/util/List<+Ljava/security/Key;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 218
    :cond_2
    sget-object v4, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_JWS_KEY_CANDIDATES_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 221
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 223
    .local v0, "it":Ljava/util/ListIterator;, "Ljava/util/ListIterator<+Ljava/security/Key;>;"
    :cond_4
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 243
    sget-object v4, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->NO_MATCHING_VERIFIERS_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4

    .line 225
    :cond_5
    invoke-virtual {p0}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->getJWSVerifierFactory()Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    move-result-object v5

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSObject;->getHeader()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/security/Key;

    invoke-interface {v5, v6, v4}, Lcom/nimbusds/jose/proc/JWSVerifierFactory;->createJWSVerifier(Lcom/nimbusds/jose/JWSHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWSVerifier;

    move-result-object v3

    .line 227
    .local v3, "verifier":Lcom/nimbusds/jose/JWSVerifier;
    if-eqz v3, :cond_4

    .line 231
    invoke-virtual {p1, v3}, Lcom/nimbusds/jose/JWSObject;->verify(Lcom/nimbusds/jose/JWSVerifier;)Z

    move-result v2

    .line 233
    .local v2, "validSignature":Z
    if-eqz v2, :cond_6

    .line 234
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSObject;->getPayload()Lcom/nimbusds/jose/Payload;

    move-result-object v4

    return-object v4

    .line 237
    :cond_6
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 239
    sget-object v4, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->INVALID_SIGNATURE:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v4
.end method

.method public process(Lcom/nimbusds/jose/PlainObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;
    .locals 1
    .param p1, "plainObject"    # Lcom/nimbusds/jose/PlainObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/PlainObject;",
            "TC;)",
            "Lcom/nimbusds/jose/Payload;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/proc/BadJOSEException;
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    sget-object v0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->PLAIN_JOSE_REJECTED_EXCEPTION:Lcom/nimbusds/jose/proc/BadJOSEException;

    throw v0
.end method

.method public process(Ljava/lang/String;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;
    .locals 1
    .param p1, "compactJOSE"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TC;)",
            "Lcom/nimbusds/jose/Payload;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;,
            Lcom/nimbusds/jose/proc/BadJOSEException;,
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 169
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p2, "context":Lcom/nimbusds/jose/proc/SecurityContext;, "TC;"
    invoke-static {p1}, Lcom/nimbusds/jose/JOSEObject;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/JOSEObject;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->process(Lcom/nimbusds/jose/JOSEObject;Lcom/nimbusds/jose/proc/SecurityContext;)Lcom/nimbusds/jose/Payload;

    move-result-object v0

    return-object v0
.end method

.method public setJWEDecrypterFactory(Lcom/nimbusds/jose/proc/JWEDecrypterFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .prologue
    .line 161
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jweDecrypterFactory:Lcom/nimbusds/jose/proc/JWEDecrypterFactory;

    .line 162
    return-void
.end method

.method public setJWEKeySelector(Lcom/nimbusds/jose/proc/JWEKeySelector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/proc/JWEKeySelector",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p1, "jweKeySelector":Lcom/nimbusds/jose/proc/JWEKeySelector;, "Lcom/nimbusds/jose/proc/JWEKeySelector<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jweKeySelector:Lcom/nimbusds/jose/proc/JWEKeySelector;

    .line 134
    return-void
.end method

.method public setJWSKeySelector(Lcom/nimbusds/jose/proc/JWSKeySelector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/proc/JWSKeySelector",
            "<TC;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    .local p1, "jwsKeySelector":Lcom/nimbusds/jose/proc/JWSKeySelector;, "Lcom/nimbusds/jose/proc/JWSKeySelector<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jwsKeySelector:Lcom/nimbusds/jose/proc/JWSKeySelector;

    .line 120
    return-void
.end method

.method public setJWSVerifierFactory(Lcom/nimbusds/jose/proc/JWSVerifierFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .prologue
    .line 147
    .local p0, "this":Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;, "Lcom/nimbusds/jose/proc/DefaultJOSEProcessor<TC;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/DefaultJOSEProcessor;->jwsVerifierFactory:Lcom/nimbusds/jose/proc/JWSVerifierFactory;

    .line 148
    return-void
.end method
