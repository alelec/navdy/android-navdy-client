.class public Lcom/navdy/client/app/framework/ProdModule;
.super Ljava/lang/Object;
.source "ProdModule.java"


# annotations
.annotation runtime Ldagger/Module;
    injects = {
        Lcom/navdy/client/app/NavdyApplication;,
        Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;,
        Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;,
        Lcom/navdy/client/app/framework/util/TTSAudioRouter;,
        Lcom/navdy/client/app/framework/util/CarMdClient;,
        Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;,
        Lcom/navdy/client/app/framework/AppInstance;,
        Lcom/navdy/client/app/ui/search/SearchActivity;,
        Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;,
        Lcom/navdy/client/app/ui/routing/RoutingActivity;,
        Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;,
        Lcom/navdy/client/debug/SubmitTicketFragment;,
        Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;,
        Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;,
        Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;
    }
    library = true
.end annotation


# instance fields
.field private audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

.field private context:Landroid/content/Context;

.field private httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

.field private networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

.field private settingsSharedPreference:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/navdy/client/app/framework/ProdModule;->context:Landroid/content/Context;

    .line 58
    return-void
.end method


# virtual methods
.method public provideHttpManager()Lcom/navdy/service/library/network/http/IHttpManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/navdy/service/library/network/http/HttpManager;

    invoke-direct {v0}, Lcom/navdy/service/library/network/http/HttpManager;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->httpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    return-object v0
.end method

.method public provideNetworkStatusManager()Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .line 94
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->register()V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->networkStatusManager:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    return-object v0
.end method

.method public provideSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->settingsSharedPreference:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 83
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "settings"

    const/4 v2, 0x0

    .line 84
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->settingsSharedPreference:Landroid/content/SharedPreferences;

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->settingsSharedPreference:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public provideTTSAudioRouter()Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule;->audioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    return-object v0
.end method
