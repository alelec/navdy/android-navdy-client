.class public Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "EditCarInfoUsingWebApiActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final EXTRA_NEXT_STEP:Ljava/lang/String; = "extra_next_step"

.field static final INSTALL:Ljava/lang/String; = "installation_flow"

.field private static final OTHER_DIALOG_ID:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showDialogAboutOther()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private initCarSelectorScreen()V
    .locals 27

    .prologue
    .line 79
    new-instance v2, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v3, 0x7f0803fd

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 81
    const v2, 0x7f10020f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Spinner;

    .line 82
    .local v5, "make":Landroid/widget/Spinner;
    const v2, 0x7f100210

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Spinner;

    .line 83
    .local v6, "year":Landroid/widget/Spinner;
    const v2, 0x7f100211

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    .line 85
    .local v8, "model":Landroid/widget/Spinner;
    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    if-nez v8, :cond_1

    .line 88
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing layout element."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 343
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v20

    .line 94
    .local v20, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v21

    .line 95
    .local v21, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v2, "vehicle-make"

    const-string v3, ""

    move-object/from16 v0, v21

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 96
    .local v10, "makeString":Ljava/lang/String;
    const-string v2, "vehicle-year"

    const-string v3, ""

    move-object/from16 v0, v21

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 97
    .local v19, "yearString":Ljava/lang/String;
    const-string v2, "vehicle-model"

    const-string v3, ""

    move-object/from16 v0, v21

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 100
    .local v25, "modelString":Ljava/lang/String;
    new-instance v23, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 101
    .local v23, "makeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v2, 0x7f080467

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    new-instance v22, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v0, v5, v1}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 103
    .local v22, "makeAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 106
    new-instance v26, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 107
    .local v26, "yearList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v2, 0x7f08046c

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    new-instance v7, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, v26

    invoke-direct {v7, v6, v0}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 109
    .local v7, "yearAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    invoke-virtual {v6, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 112
    new-instance v24, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v24

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v24, "modelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v2, 0x7f080468

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v9, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    move-object/from16 v0, v24

    invoke-direct {v9, v8, v0}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 115
    .local v9, "modelAdapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    invoke-virtual {v8, v9}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 118
    invoke-static {}, Lcom/navdy/client/app/framework/util/CarMdClient;->getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;

    move-result-object v4

    .line 119
    .local v4, "carMdClient":Lcom/navdy/client/app/framework/util/CarMdClient;
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showProgressDialog()V

    .line 120
    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Lcom/navdy/client/app/framework/util/CarMdClient;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lcom/navdy/client/app/framework/util/CarMdClient;->getMakes(Lcom/navdy/client/app/framework/util/CarMdCallBack;)V

    .line 173
    new-instance v11, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    move-object/from16 v12, p0

    move-object v13, v5

    move-object v14, v6

    move-object v15, v7

    move-object/from16 v16, v8

    move-object/from16 v17, v9

    move-object/from16 v18, v4

    invoke-direct/range {v11 .. v19}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Lcom/navdy/client/app/framework/util/CarMdClient;Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 247
    new-instance v11, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;

    move-object/from16 v12, p0

    move-object v13, v5

    move-object v14, v6

    move-object v15, v8

    move-object/from16 v16, v9

    move-object/from16 v17, v4

    move-object/from16 v18, v25

    invoke-direct/range {v11 .. v18}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Lcom/navdy/client/app/framework/util/CarMdClient;Ljava/lang/String;)V

    invoke-virtual {v6, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 325
    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v8}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$4;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;)V

    invoke-virtual {v8, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto/16 :goto_0
.end method

.method private initManualEntryScreen()V
    .locals 9

    .prologue
    .line 536
    new-instance v7, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v7, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v8, 0x7f0803fd

    invoke-virtual {v7, v8}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 538
    const v7, 0x7f100209

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 539
    .local v1, "make":Landroid/widget/EditText;
    const v7, 0x7f100207

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 540
    .local v5, "year":Landroid/widget/EditText;
    const v7, 0x7f10020b

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 542
    .local v3, "model":Landroid/widget/EditText;
    if-eqz v1, :cond_0

    if-eqz v5, :cond_0

    if-nez v3, :cond_1

    .line 545
    :cond_0
    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Missing layout element."

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 557
    :goto_0
    return-void

    .line 549
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 550
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v7, "vehicle-make"

    const-string v8, ""

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 551
    .local v2, "makeString":Ljava/lang/String;
    const-string v7, "vehicle-year"

    const-string v8, ""

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 552
    .local v6, "yearString":Ljava/lang/String;
    const-string v7, "vehicle-model"

    const-string v8, ""

    invoke-interface {v0, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 554
    .local v4, "modelString":Ljava/lang/String;
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 555
    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 556
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static saveCarInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 10
    .param p0, "customerPrefs"    # Landroid/content/SharedPreferences;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "makeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "yearString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "modelString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "comesFromManualEntry"    # Z
    .param p5, "isInFle"    # Z

    .prologue
    .line 603
    const-string v6, "vehicle-make"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 604
    .local v1, "oldMakeString":Ljava/lang/String;
    const-string v6, "vehicle-year"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 605
    .local v3, "oldYearString":Ljava/lang/String;
    const-string v6, "vehicle-model"

    const-string v7, ""

    invoke-interface {p0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 607
    .local v2, "oldModelString":Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 608
    invoke-static {v3, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 609
    invoke-static {v2, p3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 611
    :cond_0
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Resetting car md data."

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 612
    invoke-static {p0}, Lcom/navdy/client/app/tracking/Tracker;->resetPhotoAndLocation(Landroid/content/SharedPreferences;)V

    .line 614
    new-instance v0, Ljava/util/HashMap;

    const/4 v6, 0x3

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(I)V

    .line 616
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "Car_Year"

    invoke-virtual {v0, v6, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    const-string v6, "Car_Make"

    invoke-virtual {v0, v6, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    const-string v6, "Car_Model"

    invoke-virtual {v0, v6, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    const-string v7, "During_Fle"

    if-eqz p5, :cond_2

    const-string v6, "True"

    :goto_0
    invoke-virtual {v0, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    const-string v6, "Car_Info_Changed"

    invoke-static {v6, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 624
    .end local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string v6, "profile_serial_number"

    sget-object v7, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-interface {p0, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 626
    .local v4, "serial":J
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 627
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 628
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    .line 631
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-make"

    .line 632
    invoke-interface {v6, v7, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-year"

    .line 633
    invoke-interface {v6, v7, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-model"

    .line 634
    invoke-interface {v6, v7, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "vehicle-data-entry-manual"

    .line 635
    invoke-interface {v6, v7, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "profile_serial_number"

    const-wide/16 v8, 0x1

    add-long/2addr v8, v4

    .line 636
    invoke-interface {v6, v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 637
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 639
    const-string v6, "Car_Make"

    invoke-static {v6, p1}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    const-string v6, "Car_Year"

    invoke-static {v6, p2}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    const-string v6, "Car_Model"

    invoke-static {v6, p3}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const-string v6, "Vehicle_Data_Entry_Manual"

    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/navdy/client/app/framework/LocalyticsManager;->setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    return-void

    .line 619
    .end local v4    # "serial":J
    .restart local v0    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const-string v6, "False"

    goto :goto_0
.end method

.method public static saveObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p0, "customerPrefs"    # Landroid/content/SharedPreferences;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "note"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "accessNote"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "locationNumber"    # I

    .prologue
    .line 649
    .line 650
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_obd_location_note"

    .line 651
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_obd_location_access_note"

    .line 652
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAR_OBD_LOCATION_NUM"

    .line 653
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 654
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 655
    return-void
.end method

.method private showDialogAboutOther()V
    .locals 3

    .prologue
    .line 526
    const/4 v0, 0x1

    const v1, 0x7f08029b

    .line 527
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08029c

    .line 528
    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 526
    invoke-virtual {p0, v0, v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 529
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showManualEntry(Landroid/view/View;)V

    .line 530
    return-void
.end method


# virtual methods
.method public fieldsAreValid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "makeString"    # Ljava/lang/String;
    .param p2, "yearString"    # Ljava/lang/String;
    .param p3, "modelString"    # Ljava/lang/String;

    .prologue
    .line 590
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[12][0-9]{3}"

    .line 591
    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 593
    invoke-static {p3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->finish()V

    .line 426
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f030081

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->setContentView(I)V

    .line 55
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V

    .line 56
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 62
    const-string v0, "First_Launch_Edit_Car_Info"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public onSetCarInfoClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 565
    const v0, 0x7f100209

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 566
    .local v6, "make":Landroid/widget/EditText;
    const v0, 0x7f100207

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 567
    .local v8, "year":Landroid/widget/EditText;
    const v0, 0x7f10020b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    .line 569
    .local v7, "model":Landroid/widget/EditText;
    if-eqz v6, :cond_0

    if-eqz v8, :cond_0

    if-nez v7, :cond_1

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Missing layout element."

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 587
    :goto_0
    return-void

    .line 576
    :cond_1
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 577
    .local v2, "makeString":Ljava/lang/String;
    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 578
    .local v3, "yearString":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 580
    .local v4, "modelString":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->fieldsAreValid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 581
    const v0, 0x7f0803a3

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 585
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 586
    .local v1, "customerPrefs":Landroid/content/SharedPreferences;
    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onSetCarMdInfoClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 351
    const v0, 0x7f10020f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Spinner;

    .line 352
    .local v6, "make":Landroid/widget/Spinner;
    const v0, 0x7f100210

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    .line 353
    .local v10, "year":Landroid/widget/Spinner;
    const v0, 0x7f100211

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Spinner;

    .line 355
    .local v8, "model":Landroid/widget/Spinner;
    if-eqz v6, :cond_0

    if-eqz v10, :cond_0

    if-nez v8, :cond_1

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Missing layout element."

    invoke-virtual {v0, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 362
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 363
    .local v1, "customerPrefs":Landroid/content/SharedPreferences;
    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v7

    .line 364
    .local v7, "makeInt":I
    invoke-virtual {v10}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v11

    .line 365
    .local v11, "yearInt":I
    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v9

    .line 366
    .local v9, "modelInt":I
    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 367
    .local v2, "makeString":Ljava/lang/String;
    invoke-virtual {v10}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 368
    .local v3, "yearString":Ljava/lang/String;
    invoke-virtual {v8}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 370
    .local v4, "modelString":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 371
    invoke-virtual {v6}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_2

    if-eqz v11, :cond_2

    .line 373
    invoke-virtual {v10}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v11, v0, :cond_2

    if-eqz v9, :cond_2

    .line 375
    invoke-virtual {v8}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v9, v0, :cond_2

    .line 376
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 377
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 378
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 379
    :cond_2
    const v0, 0x7f0803a3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move-object v0, p0

    .line 383
    invoke-virtual/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public saveCarInfoAndDownloadObdInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "customerPrefs"    # Landroid/content/SharedPreferences;
    .param p2, "makeString"    # Ljava/lang/String;
    .param p3, "yearString"    # Ljava/lang/String;
    .param p4, "modelString"    # Ljava/lang/String;
    .param p5, "comesFromManualEntry"    # Z

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showProgressDialog()V

    .line 396
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_next_step"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 397
    .local v6, "nextStep":Ljava/lang/String;
    const-string v0, "installation_flow"

    invoke-static {v6, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    .local v5, "isInFle":Z
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move v4, p5

    .line 400
    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->saveCarInfo(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 403
    invoke-static {p1}, Lcom/navdy/client/app/tracking/Tracker;->resetPhotoAndLocation(Landroid/content/SharedPreferences;)V

    .line 406
    invoke-static {}, Lcom/navdy/client/app/framework/util/CarMdClient;->getInstance()Lcom/navdy/client/app/framework/util/CarMdClient;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/content/SharedPreferences;)V

    .line 407
    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/navdy/client/app/framework/util/CarMdClient;->getObdLocation(Lcom/navdy/client/app/framework/util/CarMdCallBack;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$5;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$5;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 422
    return-void
.end method

.method public showCarSelector(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 71
    const v0, 0x7f030081

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->setContentView(I)V

    .line 72
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V

    .line 73
    return-void
.end method

.method public showManualEntry(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 66
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->setContentView(I)V

    .line 67
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initManualEntryScreen()V

    .line 68
    return-void
.end method
