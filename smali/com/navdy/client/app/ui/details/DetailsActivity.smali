.class public Lcom/navdy/client/app/ui/details/DetailsActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "DetailsActivity.java"

# interfaces
.implements Lcom/google/android/gms/location/LocationListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# static fields
.field private static final EXTRA_LOCATION:Ljava/lang/String; = "location"

.field public static final EXTRA_UPDATED_DESTINATION:Ljava/lang/String; = "updated_destination"

.field public static final FINISH_PARENT_CODE:I = 0x1


# instance fields
.field private addToFavoritesButton:Landroid/widget/ImageButton;

.field private address:Landroid/widget/TextView;

.field private badge:Landroid/widget/ImageView;

.field private destination:Lcom/navdy/client/app/framework/models/Destination;

.field private distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

.field private editFavoriteButton:Landroid/widget/ImageView;

.field private fab:Landroid/widget/ImageView;

.field private googleMap:Lcom/google/android/gms/maps/GoogleMap;

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private hasUpdatedDestination:Z

.field private isActivityForResult:Z

.field private lastLocation:Landroid/location/Location;

.field private mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

.field private myToolbar:Landroid/support/v7/widget/Toolbar;

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private openHoursFirstLine:Landroid/widget/TextView;

.field private openHoursSecondLine:Landroid/widget/TextView;

.field private phone:Landroid/widget/TextView;

.field private requestedLocationUpdates:Z

.field private title:Landroid/widget/TextView;

.field private website:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 87
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->hasUpdatedDestination:Z

    .line 88
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->isActivityForResult:Z

    .line 167
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-void
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/details/DetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->hasUpdatedDestination:Z

    return p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/details/DetailsActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->buildAndConnectGoogleApiClient()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->fillTextViews()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateDestinationType()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->finishThisAndParent()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/ui/details/DetailsActivity;Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;
    .param p1, "x1"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->addMarkerAndCenterMap()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/details/DetailsActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->badge:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->initFavoriteButton()V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    return-object v0
.end method

.method private addMarkerAndCenterMap()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 532
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v5, :cond_0

    .line 533
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v5}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 536
    :cond_0
    const/4 v0, 0x0

    .line 537
    .local v0, "destinationLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 538
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    .end local v0    # "destinationLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v5, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v8, v5, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 542
    .restart local v0    # "destinationLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v5, :cond_2

    .line 544
    new-instance v3, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 545
    .local v3, "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v5, v5, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 546
    invoke-virtual {v3, v10}, Lcom/google/android/gms/maps/model/MarkerOptions;->draggable(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 547
    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 548
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination;->getPinAsset()I

    move-result v4

    .line 549
    .local v4, "pinAsset":I
    invoke-static {v4}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v1

    .line 550
    .local v1, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 551
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 554
    new-instance v2, Landroid/location/Location;

    const-string v5, ""

    invoke-direct {v2, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 555
    .local v2, "location":Landroid/location/Location;
    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v2, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 556
    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-virtual {v2, v6, v7}, Landroid/location/Location;->setLongitude(D)V

    .line 558
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v5, :cond_2

    .line 559
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    const/high16 v6, 0x41600000    # 14.0f

    invoke-virtual {v5, v2, v6, v10}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Landroid/location/Location;FZ)V

    .line 562
    .end local v1    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .end local v2    # "location":Landroid/location/Location;
    .end local v3    # "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    .end local v4    # "pinAsset":I
    :cond_2
    return-void

    .line 539
    :cond_3
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 540
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    .end local v0    # "destinationLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v5, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v8, v5, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v0, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .restart local v0    # "destinationLatLng":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_0
.end method

.method private buildAndConnectGoogleApiClient()V
    .locals 2

    .prologue
    .line 610
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    .line 611
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 612
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/Places;->GEO_DATA_API:Lcom/google/android/gms/common/api/Api;

    .line 613
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/places/Places;->PLACE_DETECTION_API:Lcom/google/android/gms/common/api/Api;

    .line 614
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/LocationServices;->API:Lcom/google/android/gms/common/api/Api;

    .line 615
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 616
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 617
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    .line 618
    return-void
.end method

.method private createLocationRequest()V
    .locals 4

    .prologue
    .line 827
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "creating LocationRequest Object"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 828
    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->create()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const/16 v1, 0x64

    .line 829
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->setPriority(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 830
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    .line 831
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->setFastestInterval(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    .line 832
    return-void
.end method

.method private displayGooglePlaceDetailData(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;)V
    .locals 26
    .param p1, "destinationResult"    # Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .prologue
    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "destinationResult: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 666
    sget-object v22, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/navdy/client/app/framework/models/Destination;->persistPlaceDetailInfoAndUpdateListsAsync()V

    .line 671
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->fillTextViews()V

    .line 674
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->addMarkerAndCenterMap()V

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->badge:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 682
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    .line 683
    new-instance v12, Landroid/location/Location;

    const-string v22, ""

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 684
    .local v12, "destinationLocation":Landroid/location/Location;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lat:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 685
    .local v8, "destinationLat":D
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->lng:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 686
    .local v10, "destinationLng":D
    invoke-virtual {v12, v8, v9}, Landroid/location/Location;->setLatitude(D)V

    .line 687
    invoke-virtual {v12, v10, v11}, Landroid/location/Location;->setLongitude(D)V

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Destination Location lat long "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Landroid/location/Location;->getLatitude()D

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v12}, Landroid/location/Location;->getLongitude()D

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    .line 691
    .local v14, "floatDistanceMeters":Ljava/lang/Float;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    move-object/from16 v22, v0

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v23

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setVisibility(I)V

    .line 697
    .end local v8    # "destinationLat":D
    .end local v10    # "destinationLng":D
    .end local v12    # "destinationLocation":Landroid/location/Location;
    .end local v14    # "floatDistanceMeters":Ljava/lang/Float;
    :cond_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->phone:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->phone:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->phone:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 699
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->phone:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 702
    :cond_1
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->url:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_2

    .line 703
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "<a href=\""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->url:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "\">"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const v23, 0x7f0804fb

    .line 705
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "</a>"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 707
    .local v17, "string":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->website:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-static/range {v17 .. v17}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->website:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->website:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 712
    .end local v17    # "string":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    move-object/from16 v22, v0

    if-eqz v22, :cond_3

    .line 713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v22, v0

    const-string v23, "destinationResult is not null"

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 714
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_3

    .line 715
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 716
    .local v5, "calendar":Ljava/util/Calendar;
    const/16 v22, 0x7

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->get(I)I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getDayFromCalendarOrdinal(I)I

    move-result v7

    .line 718
    .local v7, "day":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 719
    .local v16, "openHours":Ljava/lang/String;
    const/16 v22, 0x3a

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v22

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 721
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_now:Z

    move/from16 v22, v0

    if-eqz v22, :cond_9

    .line 722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursFirstLine:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, 0x7f080325

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(I)V

    .line 726
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursFirstLine:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursSecondLine:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursSecondLine:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 733
    .end local v5    # "calendar":Ljava/util/Calendar;
    .end local v7    # "day":I
    .end local v16    # "openHours":Ljava/lang/String;
    :cond_3
    const/16 v20, 0x0

    .line 734
    .local v20, "typeString":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    move-object/from16 v22, v0

    if-eqz v22, :cond_5

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    if-lez v22, :cond_5

    .line 735
    const v22, 0x7f10012c

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 736
    .local v19, "type":Landroid/widget/TextView;
    if-eqz v19, :cond_5

    .line 737
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->types:[Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aget-object v21, v22, v23

    .line 739
    .local v21, "types":Ljava/lang/String;
    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 740
    .local v4, "array":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v22

    if-lez v22, :cond_4

    .line 741
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    check-cast v0, Ljava/lang/String;

    move-object/from16 v20, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    .end local v4    # "array":Lorg/json/JSONArray;
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->isValidPlaceType(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 748
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "place_type_"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    const-string v24, "string"

    const-string v25, "com.navdy.client"

    .line 749
    invoke-virtual/range {v22 .. v25}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 753
    .local v18, "stringId":I
    if-lez v18, :cond_a

    .line 754
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 755
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 764
    .end local v18    # "stringId":I
    .end local v19    # "type":Landroid/widget/TextView;
    .end local v21    # "types":Ljava/lang/String;
    :cond_5
    :goto_2
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_6

    .line 765
    const v22, 0x7f10012d

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 766
    .local v6, "cost":Landroid/widget/TextView;
    if-eqz v6, :cond_6

    .line 767
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getPriceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 768
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 772
    .end local v6    # "cost":Landroid/widget/TextView;
    :cond_6
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->open_hours:Ljava/util/List;

    move-object/from16 v22, v0

    if-nez v22, :cond_7

    .line 773
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->isValidPlaceType(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_7

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 774
    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_7

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->phone:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 775
    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_7

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->url:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 776
    invoke-static/range {v22 .. v22}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_8

    .line 777
    :cond_7
    const v22, 0x7f100127

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 778
    .local v15, "infoTitle":Landroid/widget/TextView;
    if-eqz v15, :cond_8

    .line 779
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 782
    .end local v15    # "infoTitle":Landroid/widget/TextView;
    :cond_8
    return-void

    .line 724
    .end local v20    # "typeString":Ljava/lang/String;
    .restart local v5    # "calendar":Ljava/util/Calendar;
    .restart local v7    # "day":I
    .restart local v16    # "openHours":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursFirstLine:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, 0x7f0804be

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 743
    .end local v5    # "calendar":Ljava/util/Calendar;
    .end local v7    # "day":I
    .end local v16    # "openHours":Ljava/lang/String;
    .restart local v19    # "type":Landroid/widget/TextView;
    .restart local v20    # "typeString":Ljava/lang/String;
    .restart local v21    # "types":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 744
    .local v13, "e":Lorg/json/JSONException;
    invoke-virtual {v13}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 758
    .end local v13    # "e":Lorg/json/JSONException;
    .restart local v18    # "stringId":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Unable to find a type string for type: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private fillTextViews()V
    .locals 3

    .prologue
    .line 582
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v0

    .line 584
    .local v0, "addressLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 585
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->title:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->title:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->address:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 589
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->address:Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    :cond_1
    :goto_0
    return-void

    .line 592
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->title:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 593
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->title:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    :cond_3
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->address:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 596
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->address:Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private finishThisAndParent()V
    .locals 1

    .prologue
    .line 843
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->setResult(I)V

    .line 845
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->finish()V

    .line 846
    return-void
.end method

.method private getDayFromCalendarOrdinal(I)I
    .locals 1
    .param p1, "day"    # I

    .prologue
    .line 793
    packed-switch p1, :pswitch_data_0

    .line 796
    :pswitch_0
    const/4 v0, 0x0

    .line 808
    :goto_0
    return v0

    .line 798
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 800
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 802
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 804
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 806
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 808
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 793
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getLocalizedNumberTypeString(Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "numberType"    # Lcom/navdy/service/library/events/contacts/PhoneNumberType;
    .param p2, "label"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 647
    sget-object v0, Lcom/navdy/client/app/ui/details/DetailsActivity$10;->$SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/contacts/PhoneNumberType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 656
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    const v0, 0x7f080326

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 659
    .end local p2    # "label":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p2

    .line 649
    .restart local p2    # "label":Ljava/lang/String;
    :pswitch_0
    const v0, 0x7f08023f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 651
    :pswitch_1
    const v0, 0x7f080503

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 653
    :pswitch_2
    const v0, 0x7f0802cb

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 647
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initFavoriteButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 112
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 113
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    :cond_1
    :goto_0
    new-instance v0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$1;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    .line 159
    .local v0, "favoriteClickListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 160
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    .line 163
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    :cond_3
    return-void

    .line 120
    .end local v0    # "favoriteClickListener":Landroid/view/View$OnClickListener;
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_5

    .line 121
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 123
    :cond_5
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private isValidPlaceType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "typeString"    # Ljava/lang/String;

    .prologue
    .line 785
    if-eqz p1, :cond_0

    const-string v0, "street_address"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestLocationUpdate()V
    .locals 4

    .prologue
    .line 814
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->createLocationRequest()V

    .line 815
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "calling requestLocationUpdates from FusedLocationApi"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 818
    :try_start_0
    sget-object v1, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iget-object v3, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mLocationRequest:Lcom/google/android/gms/location/LocationRequest;

    invoke-interface {v1, v2, v3, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->requestLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 824
    :goto_0
    return-void

    .line 821
    :catch_0
    move-exception v0

    .line 822
    .local v0, "se":Ljava/lang/SecurityException;
    sget-object v1, Lcom/navdy/client/app/ui/details/DetailsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private showPhoneNumbers()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 621
    iget-object v7, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/models/Destination;->getContacts()Ljava/util/ArrayList;

    move-result-object v1

    .line 623
    .local v1, "contacts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/contacts/Contact;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 625
    .local v4, "phoneNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/contacts/Contact;

    .line 626
    .local v0, "contact":Lcom/navdy/service/library/events/contacts/Contact;
    iget-object v8, v0, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 627
    iget-object v8, v0, Lcom/navdy/service/library/events/contacts/Contact;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    iget-object v9, v0, Lcom/navdy/service/library/events/contacts/Contact;->label:Ljava/lang/String;

    invoke-direct {p0, v8, v9}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getLocalizedNumberTypeString(Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 628
    .local v6, "type":Ljava/lang/String;
    const v8, 0x7f080563

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v0, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    aput-object v10, v9, v11

    const/4 v10, 0x1

    aput-object v6, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 629
    .local v3, "number":Ljava/lang/String;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 633
    .end local v0    # "contact":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v3    # "number":Ljava/lang/String;
    .end local v6    # "type":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 634
    const-string v7, "\n"

    invoke-static {v4, v7}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 635
    .local v5, "phoneNumbersString":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->phone:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget-object v7, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->phone:Landroid/widget/TextView;

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 638
    const v7, 0x7f100127

    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 639
    .local v2, "infoTitle":Landroid/widget/TextView;
    if-eqz v2, :cond_2

    .line 640
    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 643
    .end local v2    # "infoTitle":Landroid/widget/TextView;
    .end local v5    # "phoneNumbersString":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public static startDetailsActivity(Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V
    .locals 3
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    if-eqz p1, :cond_0

    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 92
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/details/DetailsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startDetailsActivity, invalid context"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "search_result"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 97
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static startDetailsActivityForResult(Lcom/navdy/client/app/framework/models/Destination;Landroid/content/Context;)V
    .locals 3
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    if-eqz p1, :cond_0

    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 102
    :cond_0
    sget-object v1, Lcom/navdy/client/app/ui/details/DetailsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startDetailsActivity, invalid context"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 108
    .end local p1    # "context":Landroid/content/Context;
    :goto_0
    return-void

    .line 105
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "search_result"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private stopLocationUpdates()V
    .locals 2

    .prologue
    .line 835
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->requestedLocationUpdates:Z

    if-eqz v0, :cond_0

    .line 836
    sget-object v0, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/location/FusedLocationProviderApi;->removeLocationUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/location/LocationListener;)Lcom/google/android/gms/common/api/PendingResult;

    .line 837
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->requestedLocationUpdates:Z

    .line 839
    :cond_0
    return-void
.end method

.method private updateConnectionIndicator()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 565
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 566
    .local v0, "applicationContext":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 567
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/Menu;->removeItem(I)V

    .line 568
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->fab:Landroid/widget/ImageView;

    const v3, 0x7f020082

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 579
    :goto_0
    return-void

    .line 570
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/client/app/ui/settings/BluetoothPairActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 571
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "1"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 572
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0802e3

    .line 573
    invoke-interface {v2, v4, v4, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f020151

    .line 574
    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 575
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v2

    .line 576
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 577
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->fab:Landroid/widget/ImageView;

    const v3, 0x7f020083

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateDestinationType()V
    .locals 2

    .prologue
    .line 520
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->addMarkerAndCenterMap()V

    .line 523
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->badge:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 526
    const v0, 0x7f100121

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    .line 527
    const v0, 0x7f100122

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    .line 528
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->initFavoriteButton()V

    .line 529
    return-void
.end method

.method private updateFavoriteButtonIfAlreadyFavorited(Ljava/lang/String;)V
    .locals 3
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 852
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/details/DetailsActivity$9;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity$9;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 869
    return-void
.end method

.method private updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 885
    const v2, 0x7f1000aa

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 886
    .local v1, "offlineBanner":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 887
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 888
    .local v0, "canReachInternet":Z
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 890
    .end local v0    # "canReachInternet":Z
    :cond_0
    return-void

    .line 888
    .restart local v0    # "canReachInternet":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public appInstanceDeviceConnectionEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 2
    .param p1, "deviceConnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 505
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "appInstanceDeviceConnectionEvent"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 506
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateConnectionIndicator()V

    .line 507
    return-void
.end method

.method public appInstanceDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 2
    .param p1, "deviceDisconnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 511
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "appInstanceDeviceDisconnectedEvent"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 512
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateConnectionIndicator()V

    .line 513
    return-void
.end method

.method public finish()V
    .locals 3

    .prologue
    .line 412
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->isActivityForResult:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->hasUpdatedDestination:Z

    if-eqz v1, :cond_0

    .line 413
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Destination was updated. Suggestions will be rebuilt"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 414
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 415
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "updated_destination"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 416
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->setResult(ILandroid/content/Intent;)V

    .line 418
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->finish()V

    .line 419
    return-void
.end method

.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 0
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 875
    if-eqz p1, :cond_0

    .line 880
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateOfflineBannerVisibility()V

    .line 882
    :cond_0
    return-void
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 446
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onConnected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    :try_start_0
    sget-object v1, Lcom/google/android/gms/location/LocationServices;->FusedLocationApi:Lcom/google/android/gms/location/FusedLocationProviderApi;

    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2}, Lcom/google/android/gms/location/FusedLocationProviderApi;->getLastLocation(Lcom/google/android/gms/common/api/GoogleApiClient;)Landroid/location/Location;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    if-nez v1, :cond_0

    .line 458
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "lastLocation was null. Now requesting location update."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 459
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->requestLocationUpdate()V

    .line 461
    :cond_0
    return-void

    .line 450
    :catch_0
    move-exception v0

    .line 451
    .local v0, "securityException":Ljava/lang/SecurityException;
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Security Exception: "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 477
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnectionFailed: ConnectionResult.getErrorCode() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 478
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 477
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 480
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 485
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient connection suspended."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 486
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x1

    .line 195
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "::onCreate"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 197
    const v4, 0x7f030045

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->setContentView(I)V

    .line 201
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 202
    .local v3, "i":Landroid/content/Intent;
    if-eqz v3, :cond_2

    .line 203
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 204
    iput-boolean v10, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->isActivityForResult:Z

    .line 207
    :cond_0
    const-string v4, "search_result"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/framework/models/Destination;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 208
    const-string v4, "location"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/location/Location;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    .line 209
    const-string v4, "Type"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 210
    const-string v4, "Type"

    .line 211
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 217
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "lastLocation coordinates: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    if-eqz v4, :cond_7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    .line 219
    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 217
    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 224
    :cond_2
    const v4, 0x7f1000a9

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/Toolbar;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    .line 225
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->myToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 226
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 227
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_3

    .line 228
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {v0, v10}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 231
    invoke-virtual {v0, v10}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 235
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const v5, 0x7f10011e

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 236
    const v4, 0x7f10011f

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->badge:Landroid/widget/ImageView;

    .line 237
    const v4, 0x7f100123

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->title:Landroid/widget/TextView;

    .line 238
    const v4, 0x7f100124

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->address:Landroid/widget/TextView;

    .line 239
    const v4, 0x7f100125

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->distance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    .line 241
    const v4, 0x7f100128

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->phone:Landroid/widget/TextView;

    .line 242
    const v4, 0x7f100129

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->website:Landroid/widget/TextView;

    .line 243
    const v4, 0x7f10012a

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursFirstLine:Landroid/widget/TextView;

    .line 244
    const v4, 0x7f10012b

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->openHoursSecondLine:Landroid/widget/TextView;

    .line 247
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->fillTextViews()V

    .line 250
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateDestinationType()V

    .line 253
    const v4, 0x7f1000e4

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->fab:Landroid/widget/ImageView;

    .line 254
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->fab:Landroid/widget/ImageView;

    new-instance v5, Lcom/navdy/client/app/ui/details/DetailsActivity$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$2;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v4, :cond_4

    .line 282
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v5, Lcom/navdy/client/app/ui/details/DetailsActivity$4;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$4;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 291
    :cond_4
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->isContact()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 292
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->showPhoneNumbers()V

    .line 296
    :cond_5
    new-instance v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->handler:Landroid/os/Handler;

    invoke-direct {v2, p0, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 297
    .local v2, "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 298
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDetailsSearchWebApi(Ljava/lang/String;)V

    .line 299
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateFavoriteButtonIfAlreadyFavorited(Ljava/lang/String;)V

    .line 358
    :goto_1
    new-instance v4, Lcom/navdy/client/app/ui/details/DetailsActivity$6;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$6;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    new-instance v5, Lcom/navdy/client/app/ui/details/DetailsActivity$7;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$7;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    invoke-virtual {p0, v4, v5}, Lcom/navdy/client/app/ui/details/DetailsActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 372
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v1

    .line 373
    .local v1, "currentLocation":Landroid/location/Location;
    if-eqz v1, :cond_6

    .line 374
    iput-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    .line 378
    :cond_6
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v4

    const-string v5, "Details"

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 379
    return-void

    .line 219
    .end local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    .end local v1    # "currentLocation":Landroid/location/Location;
    .end local v2    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    :cond_7
    const-string v4, "null"

    goto/16 :goto_0

    .line 301
    .restart local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    .restart local v2    # "googlePlacesSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    :cond_8
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v4

    if-nez v4, :cond_b

    .line 302
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_9

    .line 303
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->addToFavoritesButton:Landroid/widget/ImageButton;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 305
    :cond_9
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    if-eqz v4, :cond_a

    .line 306
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->editFavoriteButton:Landroid/widget/ImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 308
    :cond_a
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->showProgressDialog()V

    .line 311
    :cond_b
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Getting location coordinates for: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/Destination;->getAddressForDisplay()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 313
    iget-object v4, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v5, Lcom/navdy/client/app/ui/details/DetailsActivity$5;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$5;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 426
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateConnectionIndicator()V

    .line 427
    const/4 v0, 0x1

    return v0
.end method

.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 4
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 466
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "OnGoogleSearchResult"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 467
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 468
    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 470
    .local v1, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 471
    .local v0, "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->displayGooglePlaceDetailData(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;)V

    .line 473
    .end local v0    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v1    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :cond_0
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 493
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Location changed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 495
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity;->lastLocation:Landroid/location/Location;

    .line 496
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->stopLocationUpdates()V

    .line 498
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 432
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 437
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 434
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->onBackPressed()V

    .line 435
    const/4 v0, 0x1

    goto :goto_0

    .line 432
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 893
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 894
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 383
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 385
    invoke-direct {p0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->updateOfflineBannerVisibility()V

    .line 387
    new-instance v0, Lcom/navdy/client/app/ui/details/DetailsActivity$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$8;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 405
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity$8;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 407
    const-string v0, "Destination_Details"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 408
    return-void
.end method
