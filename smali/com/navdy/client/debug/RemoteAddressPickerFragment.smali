.class public Lcom/navdy/client/debug/RemoteAddressPickerFragment;
.super Landroid/app/ListFragment;
.source "RemoteAddressPickerFragment.java"


# static fields
.field private static final MI_TO_METERS:D = 1690.34

.field public static final PREFS_FILE_USER_HISTORY:Ljava/lang/String; = "UserHistory"

.field public static final PREFS_KEY_RECENT_SEARCHES:Ljava/lang/String; = "RecentSearches"

.field private static final SEARCH_RADIUS_METERS:I = 0x2944a

.field protected static fragmentTitle:I

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private activity:Landroid/app/Activity;

.field mButtonSearch:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100268
    .end annotation
.end field

.field protected mDestinationLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100267
    .end annotation
.end field

.field protected mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

.field protected mObjectMapper:Lcom/google/gson/Gson;

.field protected mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

.field protected mSearchResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field protected mSharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/RemoteAddressPickerFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 58
    const v0, 0x7f0804b4

    sput v0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->fragmentTitle:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 79
    return-void
.end method

.method private getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method private isAttached()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addDestinations(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "searchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 123
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 124
    .local v2, "result":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    iget-object v1, v2, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    .line 125
    .local v1, "label":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    .end local v1    # "label":Ljava/lang/String;
    .end local v2    # "result":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 129
    .local v0, "adapter":Landroid/widget/ArrayAdapter;
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 130
    return-void
.end method

.method protected hideKeyboard()V
    .locals 3

    .prologue
    .line 295
    .line 296
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    .line 297
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 298
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 299
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    invoke-virtual {v1}, Lorg/droidparts/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 301
    :cond_0
    return-void
.end method

.method protected loadRecentSearches()Lcom/navdy/client/debug/models/RecentSearches;
    .locals 5

    .prologue
    .line 134
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "RecentSearches"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "json":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mObjectMapper:Lcom/google/gson/Gson;

    const-class v3, Lcom/navdy/client/debug/models/RecentSearches;

    invoke-virtual {v2, v1, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/debug/models/RecentSearches;
    :try_end_0
    .catch Lcom/google/gson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1    # "json":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Lcom/google/gson/JsonParseException;
    sget-object v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to read recent searches"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    .end local v0    # "e":Lcom/google/gson/JsonParseException;
    :cond_0
    new-instance v2, Lcom/navdy/client/debug/models/RecentSearches;

    invoke-direct {v2}, Lcom/navdy/client/debug/models/RecentSearches;-><init>()V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 107
    iput-object p1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->activity:Landroid/app/Activity;

    .line 108
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 85
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mObjectMapper:Lcom/google/gson/Gson;

    .line 91
    const-string v1, "UserHistory"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSharedPrefs:Landroid/content/SharedPreferences;

    .line 92
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->loadRecentSearches()Lcom/navdy/client/debug/models/RecentSearches;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    .line 96
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090003

    const v4, 0x1020014

    iget-object v5, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

    invoke-virtual {v1}, Lcom/navdy/client/debug/models/RecentSearches;->getResults()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->addDestinations(Ljava/util/List;)V

    .line 101
    new-instance v1, Lcom/navdy/client/debug/navigation/HUDNavigationManager;

    invoke-direct {v1}, Lcom/navdy/client/debug/navigation/HUDNavigationManager;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    .line 102
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 156
    const v1, 0x7f030091

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 157
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 159
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    new-instance v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment$1;-><init>(Lcom/navdy/client/debug/RemoteAddressPickerFragment;)V

    invoke-virtual {v1, v2}, Lorg/droidparts/widget/ClearableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 169
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    new-instance v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment$2;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment$2;-><init>(Lcom/navdy/client/debug/RemoteAddressPickerFragment;)V

    invoke-virtual {v1, v2}, Lorg/droidparts/widget/ClearableEditText;->setListener(Lorg/droidparts/widget/ClearableEditText$Listener;)V

    .line 176
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->saveRecentSearches()V

    .line 195
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 196
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/ListFragment;->onDetach()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->activity:Landroid/app/Activity;

    .line 114
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 206
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 208
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 209
    .local v1, "locationLabel":Ljava/lang/String;
    sget-object v3, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "locationClick: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 212
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSearchResults:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 213
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSearchResults:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 217
    .local v0, "destination":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :goto_0
    iget-object v2, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    .line 220
    .local v2, "navigationPosition":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v2, :cond_1

    .line 221
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NavPosition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 230
    :goto_1
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

    invoke-virtual {v3, v0}, Lcom/navdy/client/debug/models/RecentSearches;->add(Lcom/navdy/service/library/events/places/PlacesSearchResult;)V

    .line 231
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->hideKeyboard()V

    .line 232
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->processSelectedLocation(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :goto_2
    return-void

    .line 215
    .end local v0    # "destination":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    .end local v2    # "navigationPosition":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

    invoke-virtual {v3}, Lcom/navdy/client/debug/models/RecentSearches;->getResults()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .restart local v0    # "destination":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    goto :goto_0

    .line 222
    .restart local v2    # "navigationPosition":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_1
    iget-object v3, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v3, :cond_2

    .line 223
    iget-object v2, v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    .line 224
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DestPosition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_1

    .line 226
    :cond_2
    const-string v3, "Unable to find coordinate."

    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onNavigationRouteResponse(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;)V
    .locals 7
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->isAttached()Z

    move-result v4

    if-nez v4, :cond_0

    .line 315
    sget-object v4, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "isAttached: false"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 341
    :goto_0
    return-void

    .line 318
    :cond_0
    sget-object v4, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 319
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-eq v4, v5, :cond_1

    .line 320
    sget-object v4, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Unable to get routes."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 324
    :cond_1
    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    .line 325
    .local v2, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 326
    :cond_2
    sget-object v4, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "No results."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :cond_3
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 331
    .local v3, "route":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/navdy/client/debug/RemoteNavControlFragment;->newInstance(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/debug/RemoteNavControlFragment;

    move-result-object v1

    .line 333
    .local v1, "navControlFragment":Lcom/navdy/client/debug/RemoteNavControlFragment;
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 334
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const v4, 0x7f1000c6

    invoke-virtual {v0, v4, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 335
    const/16 v4, 0x1003

    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 336
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 337
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 189
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 190
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 191
    return-void
.end method

.method public onPlacesSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 273
    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-eq v2, v3, :cond_0

    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "errorString":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 276
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 292
    .end local v0    # "errorString":Ljava/lang/String;
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    .line 282
    .local v1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 283
    :cond_1
    const-string v2, "No results"

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 284
    sget-object v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no results"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 285
    iget-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    invoke-virtual {v2}, Lorg/droidparts/widget/ClearableEditText;->requestFocus()Z

    goto :goto_0

    .line 287
    :cond_2
    sget-object v2, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "returned results: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 288
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->hideKeyboard()V

    .line 289
    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->addDestinations(Ljava/util/List;)V

    .line 290
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSearchResults:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 183
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->refreshUI()V

    .line 184
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method public onSearchClicked()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100268
        }
    .end annotation

    .prologue
    .line 244
    iget-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    invoke-virtual {v2}, Lorg/droidparts/widget/ClearableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 246
    .local v1, "queryText":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->addDestinations(Ljava/util/List;)V

    .line 253
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 254
    const-string v2, "Device not connected."

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x0

    .line 266
    .local v0, "postSuccess":Z
    :goto_1
    if-nez v0, :cond_0

    .line 267
    const-string v2, "Unable to post search"

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 258
    .end local v0    # "postSuccess":Z
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;-><init>()V

    .line 260
    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/search/NavdySearch;->SEARCH_AREA_VALUE:Ljava/lang/Integer;

    .line 261
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchArea(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    const/16 v3, 0x1e

    .line 262
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->maxResults(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    move-result-object v2

    .line 263
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    move-result-object v2

    .line 258
    invoke-static {v2}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v0

    .restart local v0    # "postSuccess":Z
    goto :goto_1
.end method

.method protected processSelectedLocation(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "location"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "locationLabel"    # Ljava/lang/String;
    .param p3, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mNavigationManager:Lcom/navdy/client/debug/navigation/NavigationManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/navdy/client/debug/navigation/NavigationManager;->startRouteRequest(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;)Z

    .line 239
    :cond_0
    return-void
.end method

.method public refreshUI()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->showKeyboard()V

    .line 202
    :cond_0
    return-void
.end method

.method protected saveRecentSearches()V
    .locals 3

    .prologue
    .line 147
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mObjectMapper:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mRecentSearches:Lcom/navdy/client/debug/models/RecentSearches;

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "json":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "RecentSearches"

    .line 149
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 150
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 151
    return-void
.end method

.method protected showError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 310
    return-void
.end method

.method protected showKeyboard()V
    .locals 3

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 305
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 306
    return-void
.end method
