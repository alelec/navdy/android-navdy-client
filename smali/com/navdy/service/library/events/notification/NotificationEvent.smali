.class public final Lcom/navdy/service/library/events/notification/NotificationEvent;
.super Lcom/squareup/wire/Message;
.source "NotificationEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPID:Ljava/lang/String; = ""

.field public static final DEFAULT_APPNAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CANNOTREPLYBACK:Ljava/lang/Boolean;

.field public static final DEFAULT_CATEGORY:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final DEFAULT_ICONRESOURCENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/Integer;

.field public static final DEFAULT_IMAGERESOURCENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTO:Lokio/ByteString;

.field public static final DEFAULT_SOURCEIDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_SUBTITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final actions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/notification/NotificationAction;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;"
        }
    .end annotation
.end field

.field public final appId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final appName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final cannotReplyBack:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final category:Lcom/navdy/service/library/events/notification/NotificationCategory;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final iconResourceName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final id:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final imageResourceName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photo:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->BYTES:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sourceIdentifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final subtitle:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->DEFAULT_ID:Ljava/lang/Integer;

    .line 31
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->DEFAULT_CATEGORY:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->DEFAULT_ACTIONS:Ljava/util/List;

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->DEFAULT_PHOTO:Lokio/ByteString;

    .line 41
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->DEFAULT_CANNOTREPLYBACK:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;)V
    .locals 14
    .param p1, "builder"    # Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;

    .prologue
    .line 123
    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->id:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    iget-object v3, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->title:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->subtitle:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->message:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appId:Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->actions:Ljava/util/List;

    iget-object v8, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->photo:Lokio/ByteString;

    iget-object v9, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->iconResourceName:Ljava/lang/String;

    iget-object v10, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->imageResourceName:Ljava/lang/String;

    iget-object v11, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->sourceIdentifier:Ljava/lang/String;

    iget-object v12, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->cannotReplyBack:Ljava/lang/Boolean;

    iget-object v13, p1, Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;->appName:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v13}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 125
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;Lcom/navdy/service/library/events/notification/NotificationEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/notification/NotificationEvent$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationEvent;-><init>(Lcom/navdy/service/library/events/notification/NotificationEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/navdy/service/library/events/notification/NotificationCategory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/Integer;
    .param p2, "category"    # Lcom/navdy/service/library/events/notification/NotificationCategory;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "message"    # Ljava/lang/String;
    .param p6, "appId"    # Ljava/lang/String;
    .param p8, "photo"    # Lokio/ByteString;
    .param p9, "iconResourceName"    # Ljava/lang/String;
    .param p10, "imageResourceName"    # Ljava/lang/String;
    .param p11, "sourceIdentifier"    # Ljava/lang/String;
    .param p12, "cannotReplyBack"    # Ljava/lang/Boolean;
    .param p13, "appName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Lcom/navdy/service/library/events/notification/NotificationCategory;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/notification/NotificationAction;",
            ">;",
            "Lokio/ByteString;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    .local p7, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/notification/NotificationAction;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    .line 108
    iput-object p2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 109
    iput-object p3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    .line 110
    iput-object p4, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    .line 111
    iput-object p5, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    .line 112
    iput-object p6, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    .line 113
    invoke-static {p7}, Lcom/navdy/service/library/events/notification/NotificationEvent;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    .line 114
    iput-object p8, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    .line 115
    iput-object p9, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    .line 116
    iput-object p10, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    .line 117
    iput-object p11, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    .line 118
    iput-object p12, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    .line 119
    iput-object p13, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    .line 120
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/navdy/service/library/events/notification/NotificationEvent;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    if-ne p1, p0, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/notification/NotificationEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 131
    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationEvent;

    .line 132
    .local v0, "o":Lcom/navdy/service/library/events/notification/NotificationEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 133
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    .line 134
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    .line 135
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    .line 136
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    .line 137
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    .line 138
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    .line 139
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    .line 140
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    .line 141
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    .line 142
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    .line 143
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    .line 144
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->hashCode:I

    .line 150
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 151
    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 152
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->category:Lcom/navdy/service/library/events/notification/NotificationCategory;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/NotificationCategory;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 153
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->title:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 154
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->subtitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 155
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->message:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 156
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 157
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->actions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 158
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->photo:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 159
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->iconResourceName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 160
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->imageResourceName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 161
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->sourceIdentifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v3, v2

    .line 162
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->cannotReplyBack:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v3, v2

    .line 163
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->appName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 164
    iput v0, p0, Lcom/navdy/service/library/events/notification/NotificationEvent;->hashCode:I

    .line 166
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 151
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 152
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 153
    goto/16 :goto_2

    :cond_5
    move v2, v1

    .line 154
    goto/16 :goto_3

    :cond_6
    move v2, v1

    .line 155
    goto/16 :goto_4

    :cond_7
    move v2, v1

    .line 156
    goto :goto_5

    .line 157
    :cond_8
    const/4 v2, 0x1

    goto :goto_6

    :cond_9
    move v2, v1

    .line 158
    goto :goto_7

    :cond_a
    move v2, v1

    .line 159
    goto :goto_8

    :cond_b
    move v2, v1

    .line 160
    goto :goto_9

    :cond_c
    move v2, v1

    .line 161
    goto :goto_a

    :cond_d
    move v2, v1

    .line 162
    goto :goto_b
.end method
