.class public final Lcom/navdy/service/library/events/places/PlacesSearchRequest;
.super Lcom/squareup/wire/Message;
.source "PlacesSearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAXRESULTS:Ljava/lang/Integer;

.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field public static final DEFAULT_SEARCHAREA:Ljava/lang/Integer;

.field public static final DEFAULT_SEARCHQUERY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final maxResults:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final searchArea:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final searchQuery:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->DEFAULT_SEARCHAREA:Ljava/lang/Integer;

    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->DEFAULT_MAXRESULTS:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;

    .prologue
    .line 54
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchQuery:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->searchArea:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->maxResults:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;->requestId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;Lcom/navdy/service/library/events/places/PlacesSearchRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/PlacesSearchRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "searchQuery"    # Ljava/lang/String;
    .param p2, "searchArea"    # Ljava/lang/Integer;
    .param p3, "maxResults"    # Ljava/lang/Integer;
    .param p4, "requestId"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    .line 49
    iput-object p3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    .line 50
    iput-object p4, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 62
    check-cast v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;

    .line 63
    .local v0, "o":Lcom/navdy/service/library/events/places/PlacesSearchRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    .line 65
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    .line 66
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 71
    iget v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->hashCode:I

    .line 72
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 73
    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchQuery:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 74
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->searchArea:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 75
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 76
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 77
    iput v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchRequest;->hashCode:I

    .line 79
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 73
    goto :goto_0

    :cond_3
    move v2, v1

    .line 74
    goto :goto_1

    :cond_4
    move v2, v1

    .line 75
    goto :goto_2
.end method
