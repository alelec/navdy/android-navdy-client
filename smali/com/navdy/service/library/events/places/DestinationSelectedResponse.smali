.class public final Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
.super Lcom/squareup/wire/Message;
.source "DestinationSelectedResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REQUEST_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final destination:Lcom/navdy/service/library/events/destination/Destination;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
    .end annotation
.end field

.field public final request_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final request_status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->DEFAULT_REQUEST_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;

    .prologue
    .line 48
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/destination/Destination;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;Lcom/navdy/service/library/events/places/DestinationSelectedResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;-><init>(Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;
    .param p2, "request_status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p3, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 44
    iput-object p3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 45
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 56
    check-cast v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    .line 57
    .local v0, "o":Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 58
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 59
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64
    iget v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->hashCode:I

    .line 65
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 66
    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 67
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 68
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 69
    iput v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->hashCode:I

    .line 71
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 66
    goto :goto_0

    :cond_3
    move v2, v1

    .line 67
    goto :goto_1
.end method
