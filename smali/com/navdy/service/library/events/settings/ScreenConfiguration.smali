.class public final Lcom/navdy/service/library/events/settings/ScreenConfiguration;
.super Lcom/squareup/wire/Message;
.source "ScreenConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MARGINBOTTOM:Ljava/lang/Integer;

.field public static final DEFAULT_MARGINLEFT:Ljava/lang/Integer;

.field public static final DEFAULT_MARGINRIGHT:Ljava/lang/Integer;

.field public static final DEFAULT_MARGINTOP:Ljava/lang/Integer;

.field public static final DEFAULT_SCALEX:Ljava/lang/Float;

.field public static final DEFAULT_SCALEY:Ljava/lang/Float;

.field private static final serialVersionUID:J


# instance fields
.field public final marginBottom:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final marginLeft:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final marginRight:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final marginTop:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final scaleX:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final scaleY:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_MARGINLEFT:Ljava/lang/Integer;

    .line 16
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_MARGINTOP:Ljava/lang/Integer;

    .line 17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_MARGINRIGHT:Ljava/lang/Integer;

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_MARGINBOTTOM:Ljava/lang/Integer;

    .line 19
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_SCALEX:Ljava/lang/Float;

    .line 20
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->DEFAULT_SCALEY:Ljava/lang/Float;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;

    .prologue
    .line 50
    iget-object v1, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginLeft:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginTop:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginRight:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->marginBottom:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleX:Ljava/lang/Float;

    iget-object v6, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;->scaleY:Ljava/lang/Float;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;Lcom/navdy/service/library/events/settings/ScreenConfiguration$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;)V
    .locals 0
    .param p1, "marginLeft"    # Ljava/lang/Integer;
    .param p2, "marginTop"    # Ljava/lang/Integer;
    .param p3, "marginRight"    # Ljava/lang/Integer;
    .param p4, "marginBottom"    # Ljava/lang/Integer;
    .param p5, "scaleX"    # Ljava/lang/Float;
    .param p6, "scaleY"    # Ljava/lang/Float;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    .line 42
    iput-object p2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    .line 43
    iput-object p3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    .line 44
    iput-object p4, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    .line 45
    iput-object p5, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    .line 46
    iput-object p6, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    .line 47
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 59
    .local v0, "o":Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    .line 60
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    .line 61
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    .line 62
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->hashCode:I

    .line 70
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 71
    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginLeft:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 72
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginTop:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 73
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginRight:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 74
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->marginBottom:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 75
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleX:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 76
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->scaleY:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 77
    iput v0, p0, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->hashCode:I

    .line 79
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 71
    goto :goto_0

    :cond_3
    move v2, v1

    .line 72
    goto :goto_1

    :cond_4
    move v2, v1

    .line 73
    goto :goto_2

    :cond_5
    move v2, v1

    .line 74
    goto :goto_3

    :cond_6
    move v2, v1

    .line 75
    goto :goto_4
.end method
