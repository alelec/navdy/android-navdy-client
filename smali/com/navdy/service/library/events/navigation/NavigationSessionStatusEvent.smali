.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
.super Lcom/squareup/wire/Message;
.source "NavigationSessionStatusEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DESTINATION_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_ROUTEID:Ljava/lang/String; = ""

.field public static final DEFAULT_SESSIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field private static final serialVersionUID:J


# instance fields
.field public final destination_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
    .end annotation
.end field

.field public final routeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->DEFAULT_SESSIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V
    .locals 0
    .param p1, "sessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "routeId"    # Ljava/lang/String;
    .param p4, "route"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p5, "destination_identifier"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 56
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 59
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    .line 60
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;

    .prologue
    .line 63
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->routeId:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->destination_identifier:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 70
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 71
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    .line 72
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->hashCode:I

    .line 82
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 83
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->hashCode()I

    move-result v0

    .line 84
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 85
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 86
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 87
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 88
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->hashCode:I

    .line 90
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 83
    goto :goto_0

    :cond_3
    move v2, v1

    .line 84
    goto :goto_1

    :cond_4
    move v2, v1

    .line 85
    goto :goto_2

    :cond_5
    move v2, v1

    .line 86
    goto :goto_3
.end method
