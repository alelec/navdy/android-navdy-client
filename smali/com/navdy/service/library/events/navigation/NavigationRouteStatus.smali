.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;
.super Lcom/squareup/wire/Message;
.source "NavigationRouteStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_HANDLE:Ljava/lang/String; = ""

.field public static final DEFAULT_PROGRESS:Ljava/lang/Integer;

.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final handle:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final progress:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->DEFAULT_PROGRESS:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;

    .prologue
    .line 43
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->handle:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->progress:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;->requestId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteStatus$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "handle"    # Ljava/lang/String;
    .param p2, "progress"    # Ljava/lang/Integer;
    .param p3, "requestId"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    .line 39
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 51
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;

    .line 52
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    .line 54
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->hashCode:I

    .line 60
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 61
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->handle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 62
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->progress:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 63
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->requestId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 64
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteStatus;->hashCode:I

    .line 66
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 61
    goto :goto_0

    :cond_3
    move v2, v1

    .line 62
    goto :goto_1
.end method
