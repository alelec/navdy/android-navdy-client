.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
.super Lcom/squareup/wire/Message;
.source "NavigationRouteRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;,
        Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_AUTONAVIGATE:Ljava/lang/Boolean;

.field public static final DEFAULT_CANCELCURRENT:Ljava/lang/Boolean;

.field public static final DEFAULT_DESTINATIONTYPE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final DEFAULT_DESTINATION_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_GEOCODESTREETADDRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field public static final DEFAULT_ROUTEATTRIBUTES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STREETADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_WAYPOINTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final autoNavigate:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final cancelCurrent:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
    .end annotation
.end field

.field public final destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
    .end annotation
.end field

.field public final destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final geoCodeStreetAddress:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final originDisplay:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final requestDestination:Lcom/navdy/service/library/events/destination/Destination;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
    .end annotation
.end field

.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeAttributes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        enumType = Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public final streetAddress:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final waypoints:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/location/Coordinate;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_WAYPOINTS:Ljava/util/List;

    .line 25
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_GEOCODESTREETADDRESS:Ljava/lang/Boolean;

    .line 26
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_AUTONAVIGATE:Ljava/lang/Boolean;

    .line 28
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_DESTINATIONTYPE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 29
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_ORIGINDISPLAY:Ljava/lang/Boolean;

    .line 30
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_CANCELCURRENT:Ljava/lang/Boolean;

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->DEFAULT_ROUTEATTRIBUTES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/util/List;Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "streetAddress"    # Ljava/lang/String;
    .param p5, "geoCodeStreetAddress"    # Ljava/lang/Boolean;
    .param p6, "autoNavigate"    # Ljava/lang/Boolean;
    .param p7, "destination_identifier"    # Ljava/lang/String;
    .param p8, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p9, "originDisplay"    # Ljava/lang/Boolean;
    .param p10, "cancelCurrent"    # Ljava/lang/Boolean;
    .param p11, "requestId"    # Ljava/lang/String;
    .param p12, "destinationDisplay"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p14, "requestDestination"    # Lcom/navdy/service/library/events/destination/Destination;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;",
            ">;",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ")V"
        }
    .end annotation

    .prologue
    .line 132
    .local p3, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    .local p13, "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 134
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 135
    invoke-static {p3}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    .line 136
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 137
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    .line 138
    iput-object p6, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    .line 139
    iput-object p7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    .line 140
    iput-object p8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 141
    iput-object p9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    .line 142
    iput-object p10, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    .line 143
    iput-object p11, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    .line 144
    iput-object p12, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    .line 145
    invoke-static {p13}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    .line 146
    iput-object p14, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 147
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;)V
    .locals 16
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;

    .prologue
    .line 150
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->waypoints:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->autoNavigate:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->cancelCurrent:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->routeAttributes:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v15}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;-><init>(Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/util/List;Lcom/navdy/service/library/events/destination/Destination;)V

    .line 151
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 152
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 19
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 156
    if-ne p1, p0, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v1

    .line 157
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 158
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .line 159
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    .line 160
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    .line 161
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    .line 162
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    .line 163
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    .line 164
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    .line 165
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 166
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    .line 167
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    .line 168
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    .line 169
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    .line 170
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    .line 171
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 172
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 177
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->hashCode:I

    .line 178
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 179
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v0

    .line 180
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 181
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 182
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 183
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 184
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 185
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v4, v2

    .line 186
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v4, v2

    .line 187
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v4, v2

    .line 188
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v4, v2

    .line 189
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v4, v2

    .line 190
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v4, v2

    .line 191
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v2, v3

    .line 192
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination;->hashCode()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 193
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->hashCode:I

    .line 195
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 179
    goto/16 :goto_0

    :cond_4
    move v2, v1

    .line 180
    goto/16 :goto_1

    :cond_5
    move v2, v3

    .line 181
    goto/16 :goto_2

    :cond_6
    move v2, v1

    .line 182
    goto/16 :goto_3

    :cond_7
    move v2, v1

    .line 183
    goto/16 :goto_4

    :cond_8
    move v2, v1

    .line 184
    goto/16 :goto_5

    :cond_9
    move v2, v1

    .line 185
    goto :goto_6

    :cond_a
    move v2, v1

    .line 186
    goto :goto_7

    :cond_b
    move v2, v1

    .line 187
    goto :goto_8

    :cond_c
    move v2, v1

    .line 188
    goto :goto_9

    :cond_d
    move v2, v1

    .line 189
    goto :goto_a

    :cond_e
    move v2, v1

    .line 190
    goto :goto_b
.end method
