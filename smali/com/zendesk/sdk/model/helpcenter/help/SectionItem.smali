.class public Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
.super Ljava/lang/Object;
.source "SectionItem.java"

# interfaces
.implements Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;


# instance fields
.field private categoryId:Ljava/lang/Long;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "category_id"
    .end annotation
.end field

.field private children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field private sectionId:Ljava/lang/Long;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private totalArticlesCount:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "article_count"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addChild(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    .locals 2
    .param p1, "child"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    .line 72
    :cond_0
    if-eqz p1, :cond_1

    .line 73
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_1
    return-void
.end method

.method public addChildren(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    if-nez v0, :cond_0

    .line 84
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ne p0, p1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 114
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 116
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .line 118
    .local v0, "that":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    iget-object v4, v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    .line 119
    goto :goto_0

    .line 118
    :cond_5
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    if-nez v3, :cond_4

    .line 120
    :cond_6
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    iget-object v2, v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_7
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParentId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    return-object v0
.end method

.method public getTotalArticlesCount()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->totalArticlesCount:I

    return v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x2

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v2, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->sectionId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 127
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->categoryId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 128
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 126
    goto :goto_0
.end method

.method public removeChild(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    .locals 1
    .param p1, "helpItem"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    return-void
.end method
