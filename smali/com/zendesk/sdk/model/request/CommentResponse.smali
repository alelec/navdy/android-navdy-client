.class public Lcom/zendesk/sdk/model/request/CommentResponse;
.super Ljava/lang/Object;
.source "CommentResponse.java"


# instance fields
.field private attachments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Attachment;",
            ">;"
        }
    .end annotation
.end field

.field private authorId:Ljava/lang/Long;

.field private body:Ljava/lang/String;

.field private createdAt:Ljava/util/Date;

.field private htmlBody:Ljava/lang/String;

.field private id:Ljava/lang/Long;

.field private isPublic:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "public"
    .end annotation
.end field

.field private requestId:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->isPublic:Z

    return-void
.end method


# virtual methods
.method public getAttachments()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->attachments:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->authorId:Ljava/lang/Long;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->createdAt:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->createdAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public getHtmlBody()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->htmlBody:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getRequestId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->requestId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->url:Ljava/lang/String;

    return-object v0
.end method

.method public isPublic()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->isPublic:Z

    return v0
.end method

.method public setAttachments(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Attachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "attachments":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Attachment;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->attachments:Ljava/util/List;

    .line 149
    return-void
.end method

.method public setAuthorId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "authorId"    # Ljava/lang/Long;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->authorId:Ljava/lang/Long;

    .line 130
    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->body:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setCreatedAt(Ljava/util/Date;)V
    .locals 0
    .param p1, "createdAt"    # Ljava/util/Date;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->createdAt:Ljava/util/Date;

    .line 168
    return-void
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CommentResponse;->id:Ljava/lang/Long;

    .line 63
    return-void
.end method
