.class public abstract Lorg/droidparts/adapter/cursor/CursorAdapter;
.super Landroid/widget/CursorAdapter;
.source "CursorAdapter.java"


# instance fields
.field private final ctx:Landroid/content/Context;

.field private layoutInflater:Landroid/view/LayoutInflater;
    .annotation runtime Lorg/droidparts/annotation/inject/InjectSystemService;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/adapter/cursor/CursorAdapter;->ctx:Landroid/content/Context;

    .line 35
    invoke-static {p1, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/droidparts/adapter/cursor/CursorAdapter;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method protected getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/droidparts/adapter/cursor/CursorAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public requeryData()V
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/droidparts/adapter/cursor/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 48
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 49
    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    .line 51
    :cond_0
    return-void
.end method
