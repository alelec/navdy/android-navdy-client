.class public Lorg/droidparts/Injector;
.super Ljava/lang/Object;
.source "Injector.java"


# static fields
.field private static volatile appCtx:Landroid/content/Context;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    return-void
.end method

.method public static getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lorg/droidparts/Injector;->appCtx:Landroid/content/Context;

    return-object v0
.end method

.method public static getDependency(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 69
    invoke-static {p0, p1}, Lorg/droidparts/inner/reader/DependencyReader;->readVal(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static inject(Landroid/app/Activity;)V
    .locals 2
    .param p0, "act"    # Landroid/app/Activity;

    .prologue
    .line 40
    invoke-static {p0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 41
    const v1, 0x1020002

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "root":Landroid/view/View;
    invoke-static {p0, v0, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public static inject(Landroid/app/Dialog;Ljava/lang/Object;)V
    .locals 2
    .param p0, "dialog"    # Landroid/app/Dialog;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 56
    const v1, 0x1020002

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "root":Landroid/view/View;
    invoke-static {v0, p1}, Lorg/droidparts/Injector;->inject(Landroid/view/View;Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public static inject(Landroid/app/Service;)V
    .locals 1
    .param p0, "serv"    # Landroid/app/Service;

    .prologue
    .line 46
    invoke-static {p0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    invoke-static {p0, v0, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method private static inject(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;)V
    .locals 20
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "root"    # Landroid/view/View;
    .param p2, "target"    # Ljava/lang/Object;

    .prologue
    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 96
    .local v12, "start":J
    invoke-virtual/range {p2 .. p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 97
    .local v5, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v5}, Lorg/droidparts/inner/ClassSpecRegistry;->getInjectSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v10

    .line 98
    .local v10, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    move-object v4, v10

    .local v4, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v9, v4, v7

    .line 100
    .local v9, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v9}, Lorg/droidparts/inner/reader/ValueReader;->getVal(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;Lorg/droidparts/inner/ann/FieldSpec;)Ljava/lang/Object;

    move-result-object v11

    .line 101
    .local v11, "val":Ljava/lang/Object;
    if-eqz v11, :cond_0

    .line 102
    iget-object v14, v9, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v0, p2

    invoke-static {v0, v14, v11}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v11    # "val":Ljava/lang/Object;
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v6

    .line 105
    .local v6, "e":Ljava/lang/Throwable;
    const-string v14, "Failed to inject %s#%s: %s."

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    iget-object v0, v9, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    invoke-static {v6}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_1

    .line 110
    .end local v6    # "e":Ljava/lang/Throwable;
    .end local v9    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    :cond_1
    const-string v14, "Injected into %s in %d ms."

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v12

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method public static inject(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 51
    invoke-static {p0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 52
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public static inject(Landroid/view/View;Ljava/lang/Object;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 62
    .local v0, "ctx":Landroid/content/Context;
    invoke-static {v0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 63
    invoke-static {v0, p0, p1}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Landroid/view/View;Ljava/lang/Object;)V

    .line 64
    return-void
.end method

.method private static setContext(Landroid/content/Context;)V
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 87
    sget-object v0, Lorg/droidparts/Injector;->appCtx:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lorg/droidparts/Injector;->appCtx:Landroid/content/Context;

    .line 90
    :cond_0
    return-void
.end method

.method public static setUp(Landroid/content/Context;)V
    .locals 0
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-static {p0}, Lorg/droidparts/Injector;->setContext(Landroid/content/Context;)V

    .line 78
    invoke-static {p0}, Lorg/droidparts/inner/reader/DependencyReader;->init(Landroid/content/Context;)V

    .line 79
    return-void
.end method

.method public static tearDown()V
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lorg/droidparts/inner/reader/DependencyReader;->tearDown()V

    .line 83
    const/4 v0, 0x0

    sput-object v0, Lorg/droidparts/Injector;->appCtx:Landroid/content/Context;

    .line 84
    return-void
.end method
