.class Lorg/droidparts/bus/EventBus$PostEventRunnable;
.super Ljava/lang/Object;
.source "EventBus.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/bus/EventBus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PostEventRunnable"
.end annotation


# instance fields
.field private final data:Ljava/lang/Object;

.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p1, p0, Lorg/droidparts/bus/EventBus$PostEventRunnable;->name:Ljava/lang/String;

    .line 176
    iput-object p2, p0, Lorg/droidparts/bus/EventBus$PostEventRunnable;->data:Ljava/lang/Object;

    .line 177
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 181
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 182
    .local v2, "receivers":Ljava/util/HashSet;, "Ljava/util/HashSet<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;>;"
    const-string v3, "__all__"

    invoke-static {v3}, Lorg/droidparts/bus/EventBus;->access$000(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 183
    iget-object v3, p0, Lorg/droidparts/bus/EventBus$PostEventRunnable;->name:Ljava/lang/String;

    invoke-static {v3}, Lorg/droidparts/bus/EventBus;->access$000(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 184
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/droidparts/bus/EventReceiver;

    .line 185
    .local v1, "rec":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    iget-object v3, p0, Lorg/droidparts/bus/EventBus$PostEventRunnable;->name:Ljava/lang/String;

    iget-object v4, p0, Lorg/droidparts/bus/EventBus$PostEventRunnable;->data:Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lorg/droidparts/bus/EventBus;->access$100(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 187
    .end local v1    # "rec":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    :cond_0
    return-void
.end method
