.class Lorg/droidparts/net/image/ImageFetcher$1;
.super Ljava/lang/Object;
.source "ImageFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/droidparts/net/image/ImageFetcher;->clearCacheOlderThan(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/droidparts/net/image/ImageFetcher;

.field final synthetic val$timestamp:J


# direct methods
.method constructor <init>(Lorg/droidparts/net/image/ImageFetcher;J)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lorg/droidparts/net/image/ImageFetcher$1;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iput-wide p2, p0, Lorg/droidparts/net/image/ImageFetcher$1;->val$timestamp:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher$1;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    invoke-static {v0}, Lorg/droidparts/net/image/ImageFetcher;->access$000(Lorg/droidparts/net/image/ImageFetcher;)Lorg/droidparts/net/image/cache/BitmapDiskCache;

    move-result-object v0

    iget-wide v2, p0, Lorg/droidparts/net/image/ImageFetcher$1;->val$timestamp:J

    invoke-virtual {v0, v2, v3}, Lorg/droidparts/net/image/cache/BitmapDiskCache;->purgeFilesAccessedBefore(J)V

    .line 192
    return-void
.end method
