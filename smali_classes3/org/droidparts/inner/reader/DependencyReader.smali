.class public Lorg/droidparts/inner/reader/DependencyReader;
.super Ljava/lang/Object;
.source "DependencyReader.java"


# static fields
.field private static dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

.field private static volatile inited:Z

.field private static methodRegistry:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/droidparts/inner/ann/MethodSpec",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lorg/droidparts/inner/reader/DependencyReader;->inited:Z

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/reader/DependencyReader;->methodRegistry:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDependencyProvider(Landroid/content/Context;)Lorg/droidparts/AbstractDependencyProvider;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 92
    :try_start_0
    const-string v6, "droidparts_dependency_provider"

    invoke-static {p0, v6}, Lorg/droidparts/inner/ManifestMetaData;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 99
    .local v1, "className":Ljava/lang/String;
    const-string v6, "."

    invoke-virtual {v1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 100
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    :cond_0
    :try_start_1
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 104
    .local v2, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    invoke-virtual {v2, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 105
    .local v3, "constr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/droidparts/AbstractDependencyProvider;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 111
    .end local v1    # "className":Ljava/lang/String;
    .end local v2    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "constr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-object v0

    .line 93
    :catch_0
    move-exception v4

    .line 94
    .local v4, "e":Ljava/lang/Exception;
    invoke-static {v4}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 95
    const-string v6, "No <meta-data android:name=\"%s\" android:value=\"...\"/> in AndroidManifest.xml."

    new-array v7, v10, [Ljava/lang/Object;

    const-string v8, "droidparts_dependency_provider"

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lorg/droidparts/util/L;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v5

    .line 97
    goto :goto_0

    .line 108
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "className":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 109
    .restart local v4    # "e":Ljava/lang/Exception;
    const-string v6, "Not a valid DroidParts dependency provider: %s."

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v1, v7, v9

    invoke-static {v6, v7}, Lorg/droidparts/util/L;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-static {v4}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    move-object v0, v5

    .line 111
    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 40
    sget-boolean v5, Lorg/droidparts/inner/reader/DependencyReader;->inited:Z

    if-nez v5, :cond_2

    .line 41
    const-class v6, Lorg/droidparts/inner/reader/DependencyReader;

    monitor-enter v6

    .line 42
    :try_start_0
    sget-boolean v5, Lorg/droidparts/inner/reader/DependencyReader;->inited:Z

    if-nez v5, :cond_1

    .line 43
    invoke-static {p0}, Lorg/droidparts/inner/reader/DependencyReader;->getDependencyProvider(Landroid/content/Context;)Lorg/droidparts/AbstractDependencyProvider;

    move-result-object v5

    sput-object v5, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    .line 44
    sget-object v5, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    if-eqz v5, :cond_0

    .line 45
    sget-object v5, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    .line 47
    .local v4, "methods":[Ljava/lang/reflect/Method;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 48
    .local v3, "method":Ljava/lang/reflect/Method;
    sget-object v5, Lorg/droidparts/inner/reader/DependencyReader;->methodRegistry:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v7

    new-instance v8, Lorg/droidparts/inner/ann/MethodSpec;

    const/4 v9, 0x0

    invoke-direct {v8, v3, v9}, Lorg/droidparts/inner/ann/MethodSpec;-><init>(Ljava/lang/reflect/Method;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "arr$":[Ljava/lang/reflect/Method;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "method":Ljava/lang/reflect/Method;
    .end local v4    # "methods":[Ljava/lang/reflect/Method;
    :cond_0
    const/4 v5, 0x1

    sput-boolean v5, Lorg/droidparts/inner/reader/DependencyReader;->inited:Z

    .line 55
    :cond_1
    monitor-exit v6

    .line 57
    :cond_2
    return-void

    .line 55
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public static readVal(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {p0}, Lorg/droidparts/inner/reader/DependencyReader;->init(Landroid/content/Context;)V

    .line 70
    const/4 v3, 0x0

    .line 71
    .local v3, "val":Ljava/lang/Object;, "TT;"
    sget-object v4, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    if-eqz v4, :cond_0

    .line 72
    sget-object v4, Lorg/droidparts/inner/reader/DependencyReader;->methodRegistry:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/droidparts/inner/ann/MethodSpec;

    .line 74
    .local v2, "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<*>;"
    :try_start_0
    iget-object v4, v2, Lorg/droidparts/inner/ann/MethodSpec;->paramTypes:[Ljava/lang/Class;

    array-length v1, v4

    .line 75
    .local v1, "paramCount":I
    if-nez v1, :cond_1

    .line 76
    iget-object v4, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    sget-object v5, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 86
    .end local v1    # "paramCount":I
    .end local v2    # "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<*>;"
    .end local v3    # "val":Ljava/lang/Object;, "TT;"
    :cond_0
    :goto_0
    return-object v3

    .line 78
    .restart local v1    # "paramCount":I
    .restart local v2    # "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<*>;"
    .restart local v3    # "val":Ljava/lang/Object;, "TT;"
    :cond_1
    iget-object v4, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    sget-object v5, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 80
    .end local v1    # "paramCount":I
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No valid DependencyProvider method for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public static tearDown()V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    invoke-virtual {v0}, Lorg/droidparts/AbstractDependencyProvider;->getDB()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 63
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lorg/droidparts/inner/reader/DependencyReader;->dependencyProvider:Lorg/droidparts/AbstractDependencyProvider;

    .line 64
    return-void
.end method
