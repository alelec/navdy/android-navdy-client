.class public Lorg/droidparts/inner/reader/ViewAndPreferenceReader;
.super Ljava/lang/Object;
.source "ViewAndPreferenceReader.java"


# static fields
.field private static findPreferenceMethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findPreferenceInFragment(Ljava/lang/Object;Ljava/lang/String;)Landroid/preference/Preference;
    .locals 6
    .param p0, "prefFragment"    # Ljava/lang/Object;
    .param p1, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 86
    :try_start_0
    sget-object v1, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->findPreferenceMethod:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 87
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "findPreference"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/CharSequence;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->findPreferenceMethod:Ljava/lang/reflect/Method;

    .line 90
    :cond_0
    sget-object v1, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->findPreferenceMethod:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/Preference;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-object v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 94
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static readVal(Landroid/content/Context;Landroid/view/View;IZLjava/lang/Object;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "rootView"    # Landroid/view/View;
    .param p2, "viewOrPrefId"    # I
    .param p3, "click"    # Z
    .param p4, "target"    # Ljava/lang/Object;
    .param p6, "valName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "IZ",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    .local p5, "valType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {p5}, Lorg/droidparts/inner/TypeHelper;->isView(Ljava/lang/Class;)Z

    move-result v1

    .line 35
    .local v1, "isView":Z
    invoke-static {p5}, Lorg/droidparts/inner/TypeHelper;->isPreference(Ljava/lang/Class;)Z

    move-result v0

    .line 36
    .local v0, "isPreference":Z
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 37
    new-instance v5, Ljava/lang/Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Not a View or Preference \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 40
    :cond_0
    if-nez p2, :cond_1

    .line 41
    if-eqz v1, :cond_2

    .line 42
    invoke-static {p0, p6}, Lorg/droidparts/util/ResourceUtils;->getResourceId(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    .line 47
    :cond_1
    :goto_0
    const/4 v4, 0x0

    .line 48
    .local v4, "viewOrPref":Ljava/lang/Object;
    if-eqz v1, :cond_5

    .line 49
    if-nez p1, :cond_3

    .line 50
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Null View."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 44
    .end local v4    # "viewOrPref":Ljava/lang/Object;
    :cond_2
    invoke-static {p0, p6}, Lorg/droidparts/util/ResourceUtils;->getStringId(Landroid/content/Context;Ljava/lang/String;)I

    move-result p2

    goto :goto_0

    .line 52
    .restart local v4    # "viewOrPref":Ljava/lang/Object;
    :cond_3
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 62
    .end local v4    # "viewOrPref":Ljava/lang/Object;
    :goto_1
    if-eqz v4, :cond_8

    .line 63
    if-eqz p3, :cond_4

    .line 64
    if-eqz v1, :cond_7

    move-object v5, v4

    .line 65
    check-cast v5, Landroid/view/View;

    invoke-static {v5, p4}, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->setListener(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v3

    .line 66
    .local v3, "success":Z
    if-nez v3, :cond_4

    .line 67
    const-string v5, "Failed to set OnClickListener"

    invoke-static {v5}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    .line 77
    .end local v3    # "success":Z
    :cond_4
    :goto_2
    return-object v4

    .line 54
    .restart local v4    # "viewOrPref":Ljava/lang/Object;
    :cond_5
    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "prefKey":Ljava/lang/String;
    instance-of v5, p4, Landroid/preference/PreferenceActivity;

    if-eqz v5, :cond_6

    move-object v5, p4

    .line 56
    check-cast v5, Landroid/preference/PreferenceActivity;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .local v4, "viewOrPref":Landroid/preference/Preference;
    goto :goto_1

    .line 59
    .local v4, "viewOrPref":Ljava/lang/Object;
    :cond_6
    invoke-static {p4, v2}, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->findPreferenceInFragment(Ljava/lang/Object;Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v4

    .local v4, "viewOrPref":Landroid/preference/Preference;
    goto :goto_1

    .end local v2    # "prefKey":Ljava/lang/String;
    .end local v4    # "viewOrPref":Landroid/preference/Preference;
    :cond_7
    move-object v5, v4

    .line 70
    check-cast v5, Landroid/preference/Preference;

    invoke-static {v5, p4}, Lorg/droidparts/inner/reader/ViewAndPreferenceReader;->setListener(Landroid/preference/Preference;Ljava/lang/Object;)Z

    move-result v3

    .line 72
    .restart local v3    # "success":Z
    if-nez v3, :cond_4

    .line 73
    const-string v5, "Failed to set OnPreferenceClickListener or OnPreferenceChangeListener."

    invoke-static {v5}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    goto :goto_2

    .line 79
    .end local v3    # "success":Z
    :cond_8
    new-instance v5, Ljava/lang/Exception;

    const-string v6, "View or Preference not found for id."

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private static setListener(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p0, "pref"    # Landroid/preference/Preference;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 109
    const/4 v0, 0x0

    .line 110
    .local v0, "success":Z
    instance-of v1, p1, Landroid/preference/Preference$OnPreferenceClickListener;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 111
    check-cast v1, Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {p0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 112
    const/4 v0, 0x1

    .line 114
    :cond_0
    instance-of v1, p1, Landroid/preference/Preference$OnPreferenceChangeListener;

    if-eqz v1, :cond_1

    .line 115
    check-cast p1, Landroid/preference/Preference$OnPreferenceChangeListener;

    .end local p1    # "target":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 116
    const/4 v0, 0x1

    .line 118
    :cond_1
    return v0
.end method

.method private static setListener(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 101
    instance-of v0, p1, Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 102
    check-cast p1, Landroid/view/View$OnClickListener;

    .end local p1    # "target":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const/4 v0, 0x1

    .line 105
    :goto_0
    return v0

    .restart local p1    # "target":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
